(function ($) {
    $(document).ready(function () {
        console.warn($('.col-left.sidebar > div'));
        $('.col-left.sidebar').pin({            
            minWidth: 1440,
            padding: {
                bottom: $('footer').parent().outerHeight() - 250
            }
        });

        $(document).on('click', '#read_more1, #read_more2', function () {
            $(this).parent().css("max-height","100%");
            $(this).hide();
        });

        function sticky_relocate() {
            var window_top = $(window).scrollTop();
            var div_top = $('#sticky-anchor').offset().top;
            if (window_top > div_top) {
                $('#sticky').addClass('stick');
                $('#sticky-anchor').height($('#sticky').outerHeight());
                $('.col-left.sidebar').addClass("mt120");
            } else {
                $('#sticky').removeClass('stick');
                $('#sticky-anchor').height(0);
                $('.col-left.sidebar').removeClass("mt120");
            }
        }

        $(function() {
            $(window).scroll(sticky_relocate);
            sticky_relocate();
        });
    });
})(jQuery);