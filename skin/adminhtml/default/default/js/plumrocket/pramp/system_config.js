/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_Amp
 * @copyright   Copyright (c) 2017 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */
 
 document.addEventListener("DOMContentLoaded", function(event) {
  function changeCheckoutPath()
  {
    var e = document.getElementById('referral-url');
    var input = document.getElementById('pramp_checkout_checkout_path');

    var v = getPath(input);

    var bUrl = BLANK_URL.substring(0, BLANK_URL.indexOf('/js/blank.html'));
    if (!v || v == '/') {
      v = '/checkout/onepage/';
    }

    if (v[v.length - 1] != '/') {
      v += '/';
    }

    v = v.toLowerCase();
    e.innerHTML = bUrl+v;
  };

  function getPath(e)
  {
    var p = e.value;
    var np = '';
    var success = true;
    for(i = 0; i < p.length; i++) {
      if (p[i].match(/^[a-zA-Z]/) || p[i].match(/^[0-9]/) || p[i] == '/' || p[i] == '-' || p[i] == '_') {
        np += p[i];
      } else {
        success = false;
      }
    }

    if (np.length && np[0] != '/') {
      np = '/' + p;
      success = false;
    }

    if (!success) {
      e.value = np;
    }

    return np;
  }

  changeCheckoutPath();
  document.getElementById('pramp_checkout_checkout_path').onkeyup = changeCheckoutPath;
});
