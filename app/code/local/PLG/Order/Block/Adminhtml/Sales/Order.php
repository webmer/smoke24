<?php
/**
 * Created by PhpStorm.
 * User: avolodin
 * Date: 28.09.16
 * Time: 11:37
 */

class PLG_Order_Block_Adminhtml_Sales_Order extends Mage_Adminhtml_Block_Sales_Order
{
    public function __construct()
    {
        parent::__construct();
        $this->_addButton('apply', array(
            'label' => Mage::helper('abo')->__('PLG Export'),
            'onclick'   => 'plgExportOrder(this,\'' . $this->getUrl('plgorder_admin/adminhtml_index',array('form_key' => Mage::getSingleton('core/session')->getFormKey())) .'\')',
            'class' => 'go'
        ), 0);
    }
}