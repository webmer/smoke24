<?php

class PLG_Order_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{



    public function indexAction()
    {
        $this->genFile( true );
    }

    protected function genFile($sku = false)
    {
        $ordersIds = Mage::app()->getRequest()->getParam('order_ids');
        $fileName = date('d-m-Y').'-133303-smoke24.ch.csv';
        $delimiter=",";
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$fileName.'";');

        $f = fopen('php://output', 'w');
        /**
         * @var $collection Mage_Sales_Model_Resource_Order_Collection
         * @var $order Customweb_Subscription_Model_Sales_Order
         */
        $collection = Mage::getModel('sales/order')
            ->getCollection()
            ->addAttributeToSelect('*') //Select everything from the table
//            ->addUrlRewrite();
        ;
        if($ordersIds) {
            $collection->addFieldToFilter('entity_id',array('in'=>$ordersIds));
        }
//        $collection->addFieldToFilter('state',array('neq'=>'complete'));
//        $collection->addFieldToFilter('state',array('neq'=>'canceled'));
//        $collection->addFieldToFilter('state',array('neq'=>'closed'));
//        $collection->addFieldToFilter('state',array('neq'=>'holded'));
//        $collection->addFieldToFilter('status',array(
//            array('eq'=>'pending'),
//            array('eq'=>'processing'),
//            array('eq'=>'pending_twintcw'),
//            array('eq'=>'pending_paypal'),
//            array('eq'=>'pending_paypalcw'),
//            array('eq'=>'pending_datatranscw'),
//            array('eq'=>'pending_payment'),
//            array('like'=>'pending_paypal%'),
//            array('like'=>'pending_twint%'),
//        ));
//        $collection->addFieldToFilter('status',array('eq'=>'pending_paypal'));
//        $collection->addFieldToFilter('status',array('eq'=>'pending_twint'));
//        $collection->addFieldToFilter('status',array('eq'=>'proceed'));
        $collection->addFieldToFilter('created_at',array('gt'=>date("Y-m-d H:i:s", strtotime('-10 day'))));
//        $collection->setPageSize(100);
        $collection->load();

        Mage::getSingleton('devel/devel');
        $count = 0;
        $max = 2000;
        fputcsv($f, array('code','sku','qty','product_name','status','order_number'), $delimiter);
        foreach ($collection->getItems() as $order){
            foreach ( $order->getAllVisibleItems() as $item){
                $line = [
                    137210,
                    '"'.$item->getSku().'"',
                    $item->getQtyOrdered(),
                    $item->getName(),
                    $order->getStatus(),
                    $order->getIncrementId(),
                ];
                fputcsv($f, $line, $delimiter);
                $count++;
                if($count >= $max) break 2;
            }
        }
    }

}