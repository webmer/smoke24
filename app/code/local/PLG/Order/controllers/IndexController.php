<?php

class PLG_Order_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $uName = $this->getRequest()->getParam('uname');
        $pwd = $this->getRequest()->getParam('pwd');

        $statuses = Mage::getModel('sales/order_status')->getCollection()
            ->toOptionArray();
    echo '<pre>';print_r($statuses);echo '</pre>';exit;
        if(!$this->canShow($uName, $pwd)){
            $this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
            $this->getResponse()->setHeader('Status','404 File not found');
            $this->_forward('defaultNoRoute');
        }

        $this->genFile();
    }



    protected function canShow($name, $password)
    {
        $allowNames = array(
            'partner'
        );
        $allowPwd = array(
          '2a2c4142b6a5c0857979abe3278f4dc1' //S3aA3x8LTfN9ed6v
        );
        if(!is_null($name) && !is_null($allowNames) && in_array($name, $allowNames) && in_array(md5($password), $allowPwd)){
            return true;
        }
        return false;
    }

    protected function genFile($sku = false)
    {
        $fileName = date('d-m-Y').'-133303-smoke24.ch.csv';
        $delimiter=",";
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$fileName.'";');

        $f = fopen('php://output', 'w');
        /**
         * @var $collection Mage_Sales_Model_Resource_Order_Collection
         * @var $order Customweb_Subscription_Model_Sales_Order
         */
        $collection = Mage::getModel('sales/order')
            ->getCollection()
            ->addAttributeToSelect('*') //Select everything from the table
//            ->addUrlRewrite();
        ;
//        $collection->addFieldToFilter('state',array('neq'=>'complete'));
//        $collection->addFieldToFilter('state',array('neq'=>'canceled'));
//        $collection->addFieldToFilter('state',array('neq'=>'closed'));
//        $collection->addFieldToFilter('state',array('neq'=>'holded'));
        $collection->addFieldToFilter('status',array(
            array('eq'=>'pending'),
            array('eq'=>'processing'),
            array('eq'=>'pending_twintcw'),
            array('eq'=>'pending_paypal'),
            array('eq'=>'pending_paypalcw'),
            array('eq'=>'pending_datatranscw'),
            array('eq'=>'pending_payment'),
            array('like'=>'pending_paypal%'),
            array('like'=>'pending_twint%'),
        ));
//        $collection->addFieldToFilter('status',array('eq'=>'pending_paypal'));
//        $collection->addFieldToFilter('status',array('eq'=>'pending_twint'));
//        $collection->addFieldToFilter('status',array('eq'=>'proceed'));
        $collection->addFieldToFilter('created_at',array('gt'=>date("Y-m-d H:i:s", strtotime('-10 day'))));
//        $collection->setPageSize(100);
        $collection->load();

        Mage::getSingleton('devel/devel');
        $count = 0;
        $max = 2000;
        fputcsv($f, array('code','sku','qty','product_name','status','order_number'), $delimiter);
        foreach ($collection->getItems() as $order){
            foreach ( $order->getAllVisibleItems() as $item){
                $line = [
                    137210,
                    '"'.$item->getSku().'"',
                    $item->getQtyOrdered(),
                    $item->getName(),
                    $order->getStatus(),
                    $order->getIncrementId(),
                ];
                fputcsv($f, $line, $delimiter);
                $count++;
                if($count >= $max) break 2;
            }
        }
    }

}