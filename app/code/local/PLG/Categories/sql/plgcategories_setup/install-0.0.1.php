<?php
/**
 * @var Mage_Core_Model_Resource_Setup $this
 * @var Magento_Db_Adapter_Pdo_Mysql $connection
 */
$tableNews = $this->getTable('plgcategories/table_name_link');
//die($tableNews);
$this->startSetup();

$connection = $this->getConnection();
$connection->dropTable($tableNews);
$table = $connection
    ->newTable($tableNews)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
    ))
    ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
    ))
    ->addColumn('content', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ))
    ->addColumn('created', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable'  => false,
        'default' => 'CURRENT_TIMESTAMP'
    ));
$this->getConnection()->createTable($table);
// delete register module from core
//$installer->run("DELETE FROM `core_resource` WHERE `code` = 'plgbrandfooter_setup';");
$this->endSetup();