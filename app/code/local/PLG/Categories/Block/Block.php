<?php

class PLG_Categories_Block_Block extends Mage_Core_Block_Template
{

    public function getLinkFooterCollection()
    {
        $linkCollection = Mage::getModel('plgcategories/link')->getCollection();
        $linkCollection->setOrder('created', 'DESC');
        return $linkCollection;
    }

    public function getCurrentLink()
    {
        $currentCategory = Mage::registry('current_category');
        $currentLink = Mage::getModel('plgcategories/link')->load($currentCategory->getId(), 'category_id');
        return $currentLink;
    }

}