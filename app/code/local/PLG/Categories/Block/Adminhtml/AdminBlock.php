<?php

class PLG_Categories_Block_Adminhtml_AdminBlock extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    protected function _construct()
    {
        parent::_construct();

        $helper = Mage::helper('plgcategories');
        $this->_blockGroup = 'plgcategories';
        $this->_controller = 'adminhtml_link';

        $this->_headerText = $helper->__('Link Management');
        $this->_addButtonLabel = $helper->__('Add Link');
    }

//    public function _toHtml()
//    {
//        return '<h1>News Module: Admin section</h1>';
//    }

}