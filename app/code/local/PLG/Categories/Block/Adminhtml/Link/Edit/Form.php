<?php

class PLG_Categories_Block_Adminhtml_Link_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        Mage::getSingleton('devel/devel');
        $helper = Mage::helper('plgcategories');
        $model = Mage::registry('current_link');

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array(
                'id' => $this->getRequest()->getParam('id')
            )),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $this->setForm($form);

        $fieldset = $form->addFieldset('link_form', array('legend' => $helper->__('Link Information')));

        $fieldset->addField('category_id', 'select', array(
            'label' => $helper->__('Category'),
            'required' => true,
            'values' => Mage::helper('plgcategories')->getCategoriesSelectValue(),
            'name' => 'category_id',
        ));

        $inputType = 'textarea';
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $inputType = 'editor';
        }

        $wysiwygConfig = Mage::getSingleton('awshopbybrand/source_wysiwyg')->getConfig(
            array('add_variables' => false)
        );
        $fieldset->addField(
            'content',
            $inputType,
            array(
                'required' => true,
                'name'    => 'content',
                'label'   => $this->__('Link content'),
                'title'   => $this->__('Link content'),
                'config'  => $wysiwygConfig,
                'wysiwyg' => true,
            )
        );



        $form->setUseContainer(true);

        if($data = Mage::getSingleton('adminhtml/session')->getFormData()){
            $form->setValues($data);
        } else {
            $form->setValues($model->getData());
        }

        return parent::_prepareForm();
    }

}