<?php
class PLG_Categories_Block_Adminhtml_Link_Grid_WidgetRender extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $value =  $row->getData($this->getColumn()->getIndex());
        return '<span style="color:red;">'.$value->getName().'</span>';
    }

}