<?php

class PLG_Categories_Block_Adminhtml_Link_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    protected $_links;

    protected function _prepareCollection()
    {
        Mage::getSingleton('devel/devel');
        $collection = Mage::getModel('plgcategories/link')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        Mage::getSingleton('devel/devel');
        $helper = Mage::helper('plgcategories');
        $this->addColumn('id', array(
            'header' => $helper->__('ID'),
            'index' => 'id',
            'width' => '50px',
        ));

        $this->addColumn('category_id', array(
            'header' => $helper->__('Category ID'),
            'index' => 'category',
            'renderer' => 'plgcategories/adminhtml_link_grid_widgetRender',
            'type' => 'text',
        ));

        $this->addColumn('created', array(
            'header' => $helper->__('Created'),
            'index' => 'created',
            'type' => 'date',
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }

    public function getRowUrl($model)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $model->getId(),
        ));
    }

}