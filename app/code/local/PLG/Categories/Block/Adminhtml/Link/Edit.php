<?php

class PLG_Categories_Block_Adminhtml_Link_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        $this->_blockGroup = 'plgcategories';
        $this->_controller = 'adminhtml_link';
    }

    public function getHeaderText()
    {
        $helper = Mage::helper('plgcategories');
        $model = Mage::registry('current_link');

        if ($model->getId()) {
            return $helper->__("Edit Link item '%s'", $this->escapeHtml($model->getTitle()));
        } else {
            return $helper->__("Add Link item");
        }
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        }
    }

}