<?php

class PLG_Categories_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getCategoriesSelectValue()
    {
        $categoryCollection = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('*');;
        $categoryArray = [
            ['value'=>-1, 'label'=>'Please select']
        ];
        foreach($categoryCollection as $item){
//            var_dump($item->getData(),'<hr>');
            if($item->getLevel() > 1){
                $categoryArray[] = [
                    'value' => $item->getId(),
                    'label' => $item->getName()
                ];
            }
        }
        return $categoryArray;
    }
}