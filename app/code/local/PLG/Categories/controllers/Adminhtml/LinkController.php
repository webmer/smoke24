<?php

class PLG_Categories_Adminhtml_LinkController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->loadLayout()->_setActiveMenu('plgcategories');
        $this->_addContent($this->getLayout()->createBlock('plgcategories/adminhtml_adminBlock'));
        $this->renderLayout();
    }

    public function massDeleteAction()
    {
        $news = $this->getRequest()->getParam('id', null);

        if (is_array($news) && sizeof($news) > 0) {
            try {
                foreach ($news as $id) {
                    Mage::getModel('plgcategories/link')->setId($id)->delete();
                }
                $this->_getSession()->addSuccess($this->__('Total of %d link have been deleted', sizeof($news)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('Please select link'));
        }
        $this->_redirect('*/*');
    }

    /**
     * @see PLG_Brandfooter_Adminhtml_BrandfooterController::editAction
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        Mage::register('current_link', Mage::getModel('plgcategories/link')->load($id));

        $this->loadLayout()->_setActiveMenu('plgcategories');
        $this->_addContent($this->getLayout()->createBlock('plgcategories/adminhtml_link_edit'));
        $this->renderLayout();
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                $model = Mage::getModel('plgcategories/link');
                $model->setData($data)->setId($this->getRequest()->getParam('id'));
                if(!$model->getCreated()){
                    $model->setCreated(now());
                }
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Link was saved successfully'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array(
                    'id' => $this->getRequest()->getParam('id')
                ));
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                Mage::getModel('plgcategories/link')->setId($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Link was deleted successfully'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }

}