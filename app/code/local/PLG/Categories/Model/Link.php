<?php

class PLG_Categories_Model_Link extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('plgcategories/link');
    }

}