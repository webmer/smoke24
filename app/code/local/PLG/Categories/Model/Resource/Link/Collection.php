<?php

class PLG_Categories_Model_Resource_Link_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('plgcategories/link');
    }

    protected function _afterLoad()
    {
        foreach($this->getItems() as $item ){
            $category_id = $item->getCategoryId();
            if($category_id){
                $category = Mage::getModel('catalog/category')->load($category_id);
                $item->setData('category', $category);
            }
        }

        return parent::_afterLoad();
    }

}