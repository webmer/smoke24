<?php
/**
 * @var Mage_Core_Model_Resource_Setup $this
 * @var Magento_Db_Adapter_Pdo_Mysql $connection
 */
$tableName = $this->getTable('plgbanners/table_name');
//die($tableName);
$this->startSetup();

$connection = $this->getConnection();
$connection->dropTable($tableName);
$table = $connection
    ->newTable($tableName)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
    ))
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ))
    ->addColumn('url', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ))
    ->addColumn('content', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ))
    ->addColumn('begin', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable'  => false,
        'default' => 'CURRENT_TIMESTAMP'
    ))
    ->addColumn('end', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
    ))
    ->addColumn('created', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable'  => false,
        'default' => 'CURRENT_TIMESTAMP'
    ));
$this->getConnection()->createTable($table);
// delete register module from core
//$installer->run("DELETE FROM `core_resource` WHERE `code` = 'plgbanners_setup';");
$this->endSetup();