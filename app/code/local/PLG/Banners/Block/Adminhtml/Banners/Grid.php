<?php

class PLG_Banners_Block_Adminhtml_Banners_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    protected $_brands;

    protected function _prepareCollection()
    {
        Mage::getSingleton('devel/devel');
        $collection = Mage::getModel('plgbanners/item')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        Mage::getSingleton('devel/devel');
        $helper = Mage::helper('plgbanners');
        $this->addColumn('id', array(
            'header' => $helper->__('ID'),
            'index' => 'id',
            'width' => '50px',
        ));

        $this->addColumn('title', array(
            'header' => $helper->__('Title'),
            'index' => 'title',
            'type' => 'text',
        ));

        $this->addColumn('begin', array(
            'header' => $helper->__('Begin'),
            'index' => 'begin',
            'type' => 'date',
        ));

        $this->addColumn('end', array(
            'header' => $helper->__('End'),
            'index' => 'end',
            'type' => 'date',
        ));

        $this->addColumn('created', array(
            'header' => $helper->__('Created'),
            'index' => 'created',
            'type' => 'date',
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }

    public function getRowUrl($model)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $model->getId(),
        ));
    }

}