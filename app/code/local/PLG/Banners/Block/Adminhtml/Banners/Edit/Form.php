<?php

class PLG_Banners_Block_Adminhtml_Banners_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        Mage::getSingleton('devel/devel');
        $helper = Mage::helper('plgbanners');
        $model = Mage::registry('current_banner');

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array(
                'id' => $this->getRequest()->getParam('id')
            )),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $this->setForm($form);

        $fieldset = $form->addFieldset('banners_form', array('legend' => $helper->__('Banner Information')));

        $fieldset->addField('title', 'text', array(
            'label' => $helper->__('title'),
            'required' => true,
            'name' => 'title',
        ));

        $fieldset->addField('url', 'text', array(
            'label' => $helper->__('url'),
            'required' => true,
            'name' => 'url',
        ));

        $inputType = 'textarea';
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $inputType = 'editor';
        }

        $wysiwygConfig = Mage::getSingleton('awshopbybrand/source_wysiwyg')->getConfig(
            array('add_variables' => false)
        );
        $fieldset->addField(
            'content',
            $inputType,
            array(
                'required' => true,
                'name'    => 'content',
                'label'   => $this->__('Banner content'),
                'title'   => $this->__('Banner content'),
                'config'  => $wysiwygConfig,
                'wysiwyg' => true,
            )
        );

        $fieldset->addField('begin', 'date', array(
            'name'               => 'begin',
            'label'              => $helper->__('Begin'),
            'tabindex'           => 1,
            'image'              => $this->getSkinUrl('images/grid-cal.gif'),
            'format'             => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT) ,
        ));

        $fieldset->addField('end', 'date', array(
            'name'               => 'end',
            'label'              => $helper->__('End'),
            'tabindex'           => 1,
            'image'              => $this->getSkinUrl('images/grid-cal.gif'),
            'format'             => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT) ,
        ));

        $form->setUseContainer(true);

        if($data = Mage::getSingleton('adminhtml/session')->getFormData()){
            $form->setValues($data);
        } else {
            $form->setValues($model->getData());
        }

        return parent::_prepareForm();
    }

}