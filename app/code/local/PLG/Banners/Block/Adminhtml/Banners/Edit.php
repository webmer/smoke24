<?php

class PLG_Banners_Block_Adminhtml_Banners_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        $this->_blockGroup = 'plgbanners';
        $this->_controller = 'adminhtml_banners';
    }

    public function getHeaderText()
    {
        $helper = Mage::helper('plgbanners');
        $model = Mage::registry('current_banner');

        if ($model->getId()) {
            return $helper->__("Edit Banner item '%s'", $this->escapeHtml($model->getTitle()));
        } else {
            return $helper->__("Add Banner item");
        }
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

}