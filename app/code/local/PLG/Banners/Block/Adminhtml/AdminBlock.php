<?php

class PLG_Banners_Block_Adminhtml_AdminBlock extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    protected function _construct()
    {
        parent::_construct();

        $helper = Mage::helper('plgbanners');
        $this->_blockGroup = 'plgbanners';
        $this->_controller = 'adminhtml_banners';

        $this->_headerText = $helper->__('Banners Management');
        $this->_addButtonLabel = $helper->__('Add banner');
    }

//    public function _toHtml()
//    {
//        return '<h1>News Module: Admin section</h1>';
//    }

}