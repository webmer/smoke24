<?php

class PLG_Banners_Block_Block extends Mage_Core_Block_Template
{


    public function getCurrentBanner()
    {
        $item = null;
        $currentUrl = Mage::helper('core/url')->getCurrentUrl();
        $url = Mage::getSingleton('core/url')->parseUrl($currentUrl);
        $path = $url->getPath();
        /**
         * @var PLG_Banners_Model_Resource_Item_Collection $collection
         */
        $collection = Mage::getModel('plgbanners/item')->getCollection();
        $collection->addFieldToFilter('begin',array(
            array('to' => date("Y-m-d")),
            array('begin', 'null' => '')
        ));

        $collection->addFieldToFilter('end',array(
            array('gteq' => date("Y-m-d")),
            array('end', 'null' => '')
        ));

        $explode = explode('/', str_replace('.html', '', $path));
        unset($explode[0]);
        $explode = array_values($explode); //
        $urls = [];
        for($i = count($explode); $i > 0; $i--){
            $urls[] = '/'.join('/',$explode);
            unset($explode[$i-1]);
        }
        $collection->addFieldToFilter('url',$urls);
        $itemsGroup =[];
        foreach($collection->getItems() as $item){
            $itemsGroup[$item->getUrl()] = $item;
        }
        foreach($urls as $url){
            if(isset($itemsGroup[$url])){
                $item = $itemsGroup[$url];
                break;
            }
        }

        return $item;
    }
    public function getDefaultBanner(){
        return $this->getLayout()->createBlock('cms/block')->setBlockId('advert_footer')->toHtml();
    }

}