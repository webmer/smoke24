<?php
/**
 * Created by PhpStorm.
 * User: avolodin
 * Date: 08.08.16
 * Time: 18:09
 */

class PLG_Product_Block_Adminhtml_Catalog_Product extends Mage_Adminhtml_Block_Catalog_Product
{
    public function __construct()
    {
        parent::__construct();
        $this->_addButton('export', array(
            'label'     => 'PLG Export',
            'onclick'   => 'setLocation(\'' . $this->getUrl('plgexport') .'\')',
            'class'     => 'export',
        ));
    }
}