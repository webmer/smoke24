<?php

class PLG_Product_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{

    const COUNT_TIERS = 100;
    const CATEGORY_ID = 50;


    public function indexAction()
    {
        set_time_limit(-1);
        /**
         * @var $category Mage_Catalog_Model_Category
         * @var $productCollection Mage_Catalog_Model_Resource_Product_Collection
         * @var $product Customweb_Subscription_Model_Catalog_Product
         */
        Mage::getSingleton('devel/devel');
        $store = Mage::app()->getStore();
        $storeId = $store->getData('store_id');
        $category = Mage::getModel('catalog/category')->setStoreId($storeId)->load(self::CATEGORY_ID);
        $productCollection = $category->getProductCollection();
        $productCollection->clear()->addAttributeToSelect('*')->setCurPage(1)->setPageSize(false);

        $tableName = Mage::getSingleton('core/resource')->getTableName('catalog/product_attribute_tier_price');
        /**
         * @var $connection Magento_Db_Adapter_Pdo_Mysql
         */
        $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
// insert
        $sql = "TRUNCATE TABLE `{$tableName}`";
        $result = $connection->query($sql)->execute();
        $result = [];
        foreach($productCollection->getItems() as $product){
            if( array_search(14,$product->getCategoryIds()) ) {
                continue;
            }
            $tierPrices = [];
            for($i=0,$count=self::COUNT_TIERS;$i<$count;$i++){
                $qty = $i+1;
                $discount = Mage::getSingleton('abo/calculator')->getQtyDiscountAmount($qty);
                $tierPrices[] = [
                    'cust_group' => '32000',
                    'price_qty' => $qty,
                    'price' => ($product->getFinalPrice()*$qty-$discount)/$qty,
                    'website_id' => '0'
                ];
            }
            $result[] = [
                'id' => $product->getId(),
                'name' => $product->getName(),
                'tier' => $tierPrices
            ];
            $product->setTierPrice($tierPrices);
            $product->save();
        }

        Mage::getSingleton('core/session')->addSuccess('Tier price applied');
//        echo '<pre>';
//        print_r($productCollection->getAllIds());
//        echo '</pre>';
        $this->_redirect('aboadmin/adminhtml_abo');
    }

    /**
     * @param $collection Mage_Catalog_Model_Resource_Product_Collection
     * @throws Exception
     */
    protected function clearTiersPriceProducts($collection)
    {
        /**
         * @var $product Customweb_Subscription_Model_Catalog_Product
         */
        foreach($collection as $product){
            $product->setTierPrice([]);
            $product->save();
            break;
        }
    }

}