<?php

class PLG_Product_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $fileName = 'product.csv';
        $delimiter=",";
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$fileName.'";');

        $f = fopen('php://output', 'w');

        $collection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*') //Select everything from the table
            ->addUrlRewrite();
        $collection->load();

        fputcsv($f, array('sku','name','meta_title','meta_description','url_path','content','delivery_deadline','description','meta_keywords','product_name','nvp','mixture'), $delimiter);
        foreach ($collection as $product) {

            $model = Mage::getModel('bxp_product/discount')->discount($product->getId(), 1);
            $v = $model->getDiscountData();
            $nvp = $v['price'];
            $line = [
                $product->getSku(),
                $product->getName(),
                $product->getMetaTitle(),
                $product->getMetaDescription(),
                Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).$product->getUrlPath(),
                $product->getContent(),
                $product->getDeliveryDeadline(),
                $product->getDescription(),
                $product->getMetaKeyword(),
                $product->getProductName(),
                $nvp,
                $product->getMixture(),
            ];
            fputcsv($f, $line, $delimiter);
        }
//        $content = $this->getLayout()
//            ->createBlock('plgproduct/adminhtml_catalog_product_csv')
//            ->getCsvFile()
//        ;
//        $this->_prepareDownloadResponse($fileName, $content);
    }

}