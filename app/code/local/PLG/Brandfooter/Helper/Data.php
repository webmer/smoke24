<?php

class PLG_Brandfooter_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getBrandSelectValue()
    {
        $brandCollection = Mage::getModel('awshopbybrand/brand')->getCollection();
        $brandArray = [
            ['value'=>-1, 'label'=>'Please select']
        ];
        foreach($brandCollection as $brand){
            $brandArray[] = [
                'value' => $brand->getId(),
                'label' => $brand->getTitle()
            ];
        }
        return $brandArray;
    }
}