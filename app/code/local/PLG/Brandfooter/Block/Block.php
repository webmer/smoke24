<?php

class PLG_Brandfooter_Block_Block extends Mage_Core_Block_Template
{

    public function getBrandFooterCollection()
    {
        $brandfooterCollection = Mage::getModel('plgbrandfooter/brandfooter')->getCollection();
        $brandfooterCollection->setOrder('created', 'DESC');
        return $brandfooterCollection;
    }

    public function getCurrentBrandFooter()
    {
        $currentBrand = Mage::registry('current_brand');
        $currentBrandFooter = Mage::getModel('plgbrandfooter/brandfooter')->load($currentBrand->getId(), 'brand_id');
        return $currentBrandFooter;
    }

}