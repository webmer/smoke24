<?php

class PLG_Brandfooter_Block_Adminhtml_AdminBlock extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    protected function _construct()
    {
        parent::_construct();

        $helper = Mage::helper('plgbrandfooter');
        $this->_blockGroup = 'plgbrandfooter';
        $this->_controller = 'adminhtml_brandfooter';

        $this->_headerText = $helper->__('Brand Footer Management');
        $this->_addButtonLabel = $helper->__('Add Brand Footer');
    }

//    public function _toHtml()
//    {
//        return '<h1>News Module: Admin section</h1>';
//    }

}