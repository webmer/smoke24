<?php

class PLG_Brandfooter_Block_Adminhtml_Brandfooter_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        $this->_blockGroup = 'plgbrandfooter';
        $this->_controller = 'adminhtml_brandfooter';
    }

    public function getHeaderText()
    {
        $helper = Mage::helper('plgbrandfooter');
        $model = Mage::registry('current_brand_footer');

        if ($model->getId()) {
            return $helper->__("Edit Brand Footer item '%s'", $this->escapeHtml($model->getTitle()));
        } else {
            return $helper->__("Add Brand Footer item");
        }
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

}