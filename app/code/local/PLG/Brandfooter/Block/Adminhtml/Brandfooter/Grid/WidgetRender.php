<?php
class PLG_Brandfooter_Block_Adminhtml_Brandfooter_Grid_WidgetRender extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $value =  $row->getData($this->getColumn()->getIndex());
        return '<span style="color:red;">'.$value->getTitle().'</span>';
    }

}