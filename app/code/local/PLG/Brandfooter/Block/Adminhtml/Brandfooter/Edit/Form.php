<?php

class PLG_Brandfooter_Block_Adminhtml_Brandfooter_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        Mage::getSingleton('devel/devel');
        $helper = Mage::helper('plgbrandfooter');
        $model = Mage::registry('current_brand_footer');

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array(
                'id' => $this->getRequest()->getParam('id')
            )),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $this->setForm($form);

        $fieldset = $form->addFieldset('brand_footer_form', array('legend' => $helper->__('Brand footer Information')));

        $fieldset->addField('brand_id', 'select', array(
            'label' => $helper->__('Brand'),
            'required' => true,
            'values' => Mage::helper('plgbrandfooter')->getBrandSelectValue(),
            'name' => 'brand_id',
        ));

        $inputType = 'textarea';
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $inputType = 'editor';
        }

        $wysiwygConfig = Mage::getSingleton('awshopbybrand/source_wysiwyg')->getConfig(
            array('add_variables' => false)
        );
        $fieldset->addField(
            'content',
            $inputType,
            array(
                'required' => true,
                'name'    => 'content',
                'label'   => $this->__('Brand Footer content'),
                'title'   => $this->__('Brand Footer content'),
                'config'  => $wysiwygConfig,
                'wysiwyg' => true,
            )
        );



        $form->setUseContainer(true);

        if($data = Mage::getSingleton('adminhtml/session')->getFormData()){
            $form->setValues($data);
        } else {
            $form->setValues($model->getData());
        }

        return parent::_prepareForm();
    }

}