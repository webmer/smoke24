<?php

class PLG_Brandfooter_Block_Adminhtml_Brandfooter_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    protected $_brands;

    protected function _prepareCollection()
    {
        Mage::getSingleton('devel/devel');
        $collection = Mage::getModel('plgbrandfooter/brandfooter')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        Mage::getSingleton('devel/devel');
        $helper = Mage::helper('plgbrandfooter');
        $this->addColumn('id', array(
            'header' => $helper->__('ID'),
            'index' => 'id',
            'width' => '50px',
        ));

        $this->addColumn('brand_id', array(
            'header' => $helper->__('Brand ID'),
            'index' => 'brand',
            'renderer' => 'plgbrandfooter/adminhtml_brandfooter_grid_widgetRender',
            'type' => 'text',
        ));

        $this->addColumn('created', array(
            'header' => $helper->__('Created'),
            'index' => 'created',
            'type' => 'date',
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }

    public function getRowUrl($model)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $model->getId(),
        ));
    }

}