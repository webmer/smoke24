<?php

class PLG_Brandfooter_Model_Resource_Brandfooter_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('plgbrandfooter/brandfooter');
    }

    protected function _afterLoad()
    {
        foreach($this->getItems() as $item ){
            $brand_id = $item->getBrandId();
            if($brand_id){
                $brand = Mage::getModel('awshopbybrand/brand')->load($brand_id);
                $item->setData('brand', $brand);
            }
        }

        return parent::_afterLoad();
    }

}