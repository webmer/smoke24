<?php

class PLG_Brandfooter_Adminhtml_BrandfooterController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->loadLayout()->_setActiveMenu('plgbrandfooter');
        $this->_addContent($this->getLayout()->createBlock('plgbrandfooter/adminhtml_adminBlock'));
        $this->renderLayout();
    }

    public function massDeleteAction()
    {
        $news = $this->getRequest()->getParam('id', null);

        if (is_array($news) && sizeof($news) > 0) {
            try {
                foreach ($news as $id) {
                    Mage::getModel('plgbrandfooter/brandfooter')->setId($id)->delete();
                }
                $this->_getSession()->addSuccess($this->__('Total of %d brand footer have been deleted', sizeof($news)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('Please select brand footer'));
        }
        $this->_redirect('*/*');
    }

    /**
     * @see PLG_Brandfooter_Adminhtml_BrandfooterController::editAction
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        Mage::register('current_brand_footer', Mage::getModel('plgbrandfooter/brandfooter')->load($id));

        $this->loadLayout()->_setActiveMenu('plgbrandfooter');
        $this->_addContent($this->getLayout()->createBlock('plgbrandfooter/adminhtml_brandfooter_edit'));
        $this->renderLayout();
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                $model = Mage::getModel('plgbrandfooter/brandfooter');
                $model->setData($data)->setId($this->getRequest()->getParam('id'));
                if(!$model->getCreated()){
                    /**
                     * @TODO
                     */
                    $model->setCreated(now());
                }
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Brand footer was saved successfully'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array(
                    'id' => $this->getRequest()->getParam('id')
                ));
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                Mage::getModel('plgbrandfooter/brandfooter')->setId($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Brand footer was deleted successfully'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }

}