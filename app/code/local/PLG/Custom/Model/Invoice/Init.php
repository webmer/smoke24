<?php

/**
 *
 */
class PLG_Custom_Model_Invoice_Init extends Mage_Sales_Model_Order_Invoice_Total_Abstract {

    public function collect(Mage_Sales_Model_Order_Invoice $invoice){
        $invoice->setDiscountAmount(-($invoice->getOrder()->getDiscountAmount()) );
        $invoice->setBaseDiscountAmount(-($invoice->getOrder()->getBaseDiscountAmount()) );
        /*new changes*/
        $initAmount = $invoice->getOrder()->getSubscriptionInitAmount();
        $baseInitAmount = $invoice->getOrder()->getBaseSubscriptionInitAmount();
        $invoice->setGrandTotal($invoice->getGrandTotal() + $initAmount);
        $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $baseInitAmount);
        
        /*old changes*/
        //$invoice->setGrandTotal($invoice->getOrder()->getGrandTotal());
        //$invoice->setBaseGrandTotal($invoice->getOrder()->getBaseGrandTotal());

        return $this;
    }
}