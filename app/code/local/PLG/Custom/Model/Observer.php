<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2017 by PLG
 *

 */
class PLG_Custom_Model_Observer {

	/**
	 * Disable payment method that cannot be used with recurring products.
	 *
	 * @param Varien_Event_Observer $observer
	 */
	public function paymentMethodIsActive(Varien_Event_Observer $observer){
        /**
         * @var $method Mage_Payment_Model_Method_Abstract
         */
	    /* call get payment method */
        $method = $observer->getEvent()->getMethodInstance();

        /**
         * @var $quote Customweb_Subscription_Model_Sales_Quote
         */
        /*   get  Quote  */
        $quote = $observer->getEvent()->getQuote();

        $result = $observer->getEvent()->getResult();

        /* Disable Your payment method for   adminStore */
        if(strpos($method->getCode(),'visa') ||  strpos($method->getCode(),'mastercard') || strpos($method->getCode(),'masterpass')) {
            /**
             * @var $item Customweb_Subscription_Model_Sales_Quote_Item
             */
            foreach ($quote->getAllItems() as $item) {
                // get Cart item product Type //
                if( in_array(113, $item->getProduct()->getCategoryIds()) ){
                    $result->isAvailable = false;
                }
            }
        }
    }
}