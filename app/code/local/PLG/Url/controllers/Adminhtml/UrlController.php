<?php

class PLG_Url_Adminhtml_UrlController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {

        $brandsCollection = Mage::getModel('awshopbybrand/brand')->getCollection();
        $brandsCollection->load();
        foreach ($brandsCollection as $brand){
            echo '<pre>';
            var_dump($brand->getUrlKey());
            echo '</pre>';
            echo PHP_EOL;

            $products = $brand->getProducts();

            /** @var $productCollection Mage_Catalog_Model_Resource_Product_Collection */
            $productCollection = Mage::getModel('catalog/product')->getCollection();
            $productCollection->resetData();
            $productCollection->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToFilter('entity_id', array('in' => $products));

            /**
             * @var $product Customweb_Subscription_Model_Catalog_Product
             */
            $categories = [];
            foreach ($productCollection->getItems() as $product) {
                $categories = array_unique (array_merge($categories,$product->getCategoryIds()));
            }
            if($brand->getUrlKey() == 'al_capone'){
                echo '<pre>';
                print_r(['test',$categories]);
                echo '</pre>';
                echo PHP_EOL;
            }

            foreach ($categories as $catId)
            {
                /**
                 * @var $category Mage_Catalog_Model_Category
                 */
                $category = Mage::getModel('catalog/category')->load($catId);
                if($brand->getUrlKey() == 'al_capone') {
                    echo '<pre>';
                    var_dump([
                        'category_' . $category->getUrlKey() . '_' . Mage::helper('awshopbybrand/config')->getAllBrandsUrlKey() . '_' . $category->getUrlKey(),
                        str_replace('_', '-', $brand->getUrlKey()) . '-' . $category->getUrlKey(),
                        DS . Mage::helper('awshopbybrand/config')->getAllBrandsUrlKey() . DS . $brand->getUrlKey() . DS . $category->getId()
                    ]);
                    echo '</pre>';
                }
                $this->createRule(
                    'category_'.$category->getUrlKey().'_'.Mage::helper('awshopbybrand/config')->getAllBrandsUrlKey().'_'.$brand->getUrlKey(),
                    str_replace('_','-',$brand->getUrlKey()).'-'.$category->getUrlKey(),
                    DS.Mage::helper('awshopbybrand/config')->getAllBrandsUrlKey().DS.$brand->getUrlKey().DS.$category->getId()
                );
            }

            /**
             * @var AW_Shopbybrand_Model_Brand $brand
             * @var Mage_Catalog_Model_Resource_Product_Collection $products
             */
            $this->createRule(
                Mage::helper('awshopbybrand/config')->getAllBrandsUrlKey().'_'.$brand->getUrlKey(),
                str_replace('_','-',$brand->getUrlKey()),
                DS.Mage::helper('awshopbybrand/config')->getAllBrandsUrlKey().DS.$brand->getUrlKey()
            );

            if(Mage::helper('awshopbybrand/config')->getAllBrandsUrlKey() != 'brands'){
                $this->createRule(
                    'brands_'.$brand->getUrlKey(),
                    'brands/'.str_replace('_','-',$brand->getUrlKey()),
                    str_replace('_','-',$brand->getUrlKey())
                );
            }
        }
//        return $this->_redirect('admin_new/dashboard');
    }

    protected function createRule($id, $fromUrl, $toUrl)
    {
        try{
            // Create rewrite:
            /** @var Mage_Core_Model_Url_Rewrite $rewrite */
            $rewrite = Mage::getSingleton('core/factory')->getUrlRewriteInstance();
            $store = Mage::app()->getStore();
            // Check for existing rewrites:
            // Attempt loading it first, to prevent duplicates:
            $rewrite = $rewrite->loadByIdPath($id);
            $rewrite->setIsSystem(0); //Custom
            $rewrite->setRequestPath($fromUrl);
            $rewrite->setStoreId(2);
            //            $rewrite->setOptions('RP');
            $rewrite->setIdentifier($fromUrl);
            $rewrite->setTargetPath($toUrl);
            $rewrite->setEntityType(Mage_Core_Model_Url_Rewrite::TYPE_CUSTOM);
            $rewrite->setIdPath($id);

            $rewrite->save();
        }catch (Exception $e) {
            var_dump($e->getMessage(),$e->getTraceAsString());
            Mage::getSingleton('core/session')->addError($e->getMessage());
        }
    }

}