<?php

class PLG_Sitemap_Adminhtml_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $errors = array();

        // check if scheduled generation enabled
        if (!Mage::getStoreConfigFlag(Mage_Sitemap_Model_Observer::XML_PATH_GENERATION_ENABLED)) {
            return;
        }

        /**
         * @var $collection Mage_Sitemap_Model_Resource_Sitemap_Collection
         */
        $collection = Mage::getModel('sitemap/sitemap')->getCollection();
        /* @var $collection Mage_Sitemap_Model_Mysql4_Sitemap_Collection */
        foreach ($collection as $sitemap) {
            /* @var $sitemap Mage_Sitemap_Model_Sitemap */

            try {
                $sitemap->generateXml();
            }
            catch (Exception $e) {
                $errors[] = $e->getMessage();
            }
        }

        if ($errors && Mage::getStoreConfig(Mage_Sitemap_Model_Observer::XML_PATH_ERROR_RECIPIENT)) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);

            $emailTemplate = Mage::getModel('core/email_template');
            /* @var $emailTemplate Mage_Core_Model_Email_Template */
            $emailTemplate->setDesignConfig(array('area' => 'backend'))
                ->sendTransactional(
                    Mage::getStoreConfig(Mage_Sitemap_Model_Observer::XML_PATH_ERROR_TEMPLATE),
                    Mage::getStoreConfig(Mage_Sitemap_Model_Observer::XML_PATH_ERROR_IDENTITY),
                    Mage::getStoreConfig(Mage_Sitemap_Model_Observer::XML_PATH_ERROR_RECIPIENT),
                    null,
                    array('warnings' => join("\n", $errors))
                );

            $translate->setTranslateInline(true);
        }

    }

}