<?php
/**
 * @var Mage_Core_Model_Resource_Setup $this
 * @var Magento_Db_Adapter_Pdo_Mysql $connection
 */
$tableName = $this->getTable('plgwettbewerb/table_name');
//die($tableNews);
$this->startSetup();

$connection = $this->getConnection();
$connection->addColumn($tableName,'gender',array(
    'type'=>Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable' => true,
    'default' => null,
    'comment' => 'Gender'
));

/*$connection->dropTable($tableName);
$table = $connection
    ->newTable($tableName)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
    ))
    ->addColumn('created', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable'  => false,
        'default' => 'CURRENT_TIMESTAMP'
    ))
    ->addColumn('vorname', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ))
    ->addColumn('strasse', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ))
    ->addColumn('plz', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
    ))
    ->addColumn('ort', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ))
    ->addColumn('email', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ))
    ->addColumn('phone', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ))
    ->addColumn('conditions', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, array(
        'nullable'  => false,
    ))
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ));
$this->getConnection()->createTable($table);*/
// delete register module from core
//$installer->run("DELETE FROM `core_resource` WHERE `code` = 'plgwettbewerb_setup';");
$this->endSetup();