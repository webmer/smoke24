<?php

class PLG_Wettbewerb_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        Mage::register('current_wettbewerb_user', Mage::getModel('plgwettbewerb/users')->load($id));

        $this->loadLayout();
        /**
         * generate captcha block for use in template
         */
        $this->getLayout()->createBlock('studioforty9_recaptcha/explicit','recaptcha');
//        $this->_addContent($this->getLayout()->createBlock('plgwettbewerb/adminhtml_users_edit'));
        $this->renderLayout();
    }

    public function tyAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function saveAction()
    {
        Mage::getSingleton('devel/devel');
        $flag = $this->getFlag('',Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH);
        if($flag){
            Mage::getSingleton('core/session')->addError(
                'There was an error with the recaptcha code, please try again.'
            );
            $this->_redirect('*/*/');
            return;
        }
        if ($data = $this->getRequest()->getPost()) {
            try {
                $model = Mage::getModel('plgwettbewerb/users');
                $model->setData($data)->setId($this->getRequest()->getParam('id'));
                if(!$model->getCreated()){
                    /**
                     * @TODO
                     */
                    $model->setCreated(now());
                }
                $model->save();

//                Mage::getSingleton('core/session')->addSuccess($this->__('User was saved successfully'));
                Mage::getSingleton('core/session')->setFormData(false);
                $this->_redirect('*/*/ty');
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError($e->getMessage());
                Mage::getSingleton('core/session')->setFormData($data);
            }
            return;
        }
        $this->_redirect('*/*/');
    }

}