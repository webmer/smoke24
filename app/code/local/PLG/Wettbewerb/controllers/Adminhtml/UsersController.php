<?php

class PLG_Wettbewerb_Adminhtml_UsersController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->loadLayout()->_setActiveMenu('plgwettbewerb');
        $this->_addContent($this->getLayout()->createBlock('plgwettbewerb/adminhtml_users'));
        $this->renderLayout();
    }

    public function massDeleteAction()
    {
        $news = $this->getRequest()->getParam('id', null);

        if (is_array($news) && sizeof($news) > 0) {
            try {
                foreach ($news as $id) {
                    Mage::getModel('plgwettbewerb/users')->setId($id)->delete();
                }
                $this->_getSession()->addSuccess($this->__('Total of %d users have been deleted', sizeof($news)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('Please select users'));
        }
        $this->_redirect('*/*');
    }

    /**
     * @see PLG_Brandfooter_Adminhtml_BrandfooterController::editAction
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        Mage::register('current_wettbewerb_user', Mage::getModel('plgwettbewerb/users')->load($id));

        $this->loadLayout()->_setActiveMenu('plgwettbewerb');
        $this->_addContent($this->getLayout()->createBlock('plgwettbewerb/adminhtml_users_edit'));
        $this->renderLayout();
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                $model = Mage::getModel('plgwettbewerb/users');
                $model->setData($data)->setId($this->getRequest()->getParam('id'));
                if(!$model->getCreated()){
                    /**
                     * @TODO
                     */
                    $model->setCreated(now());
                }
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('User was saved successfully'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array(
                    'id' => $this->getRequest()->getParam('id')
                ));
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                Mage::getModel('plgwettbewerb/users')->setId($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('User was deleted successfully'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }

}