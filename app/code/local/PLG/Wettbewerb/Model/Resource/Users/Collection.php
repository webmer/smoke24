<?php

class PLG_Wettbewerb_Model_Resource_Users_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('plgwettbewerb/users');
    }

}