<?php

class PLG_Wettbewerb_Block_Adminhtml_Users_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('plgwettbewerb/users')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $helper = Mage::helper('plgwettbewerb');
        $this->addColumn('id', array(
            'header' => $helper->__('ID'),
            'index' => 'id',
            'width' => '50px',
        ));

        $this->addColumn('vorname', array(
            'header' => $helper->__('Vorname'),
            'index' => 'vorname',
            'type' => 'text',
        ));

        $this->addColumn('name', array(
            'header' => $helper->__('Name'),
            'index' => 'name',
            'type' => 'text',
        ));

        $this->addColumn('gender', array(
            'header' => $helper->__('Gender'),
            'index' => 'gender',
            'type' => 'text',
        ));

        $this->addColumn('strasse', array(
            'header' => $helper->__('Strasse'),
            'index' => 'strasse',
            'type' => 'text',
        ));

        $this->addColumn('plz', array(
            'header' => $helper->__('plz'),
            'index' => 'plz',
            'type' => 'text',
            'width' => '50px'
        ));

        $this->addColumn('ort', array(
            'header' => $helper->__('ort'),
            'index' => 'ort',
            'type' => 'text',
        ));

        $this->addColumn('email', array(
            'header' => $helper->__('email'),
            'index' => 'email',
            'type' => 'text',
        ));

        $this->addColumn('date_birth', array(
            'header' => $helper->__('Date Birth Old'),
            'index' => 'date_birth',
            'type' => 'date',
        ));

        $this->addColumn('birth', array(
            'header' => $helper->__('Date Birth'),
            'index' => 'birth',
            'type' => 'date',
        ));

        $this->addColumn('created', array(
            'header' => $helper->__('Created'),
            'index' => 'created',
            'type' => 'date',
        ));


        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }

    public function getRowUrl($model)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $model->getId(),
        ));
    }

}