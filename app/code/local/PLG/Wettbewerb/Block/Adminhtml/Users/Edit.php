<?php

class PLG_Wettbewerb_Block_Adminhtml_Users_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        $this->_blockGroup = 'plgwettbewerb';
        $this->_controller = 'adminhtml_users';
    }

    public function getHeaderText()
    {
        $helper = Mage::helper('plgwettbewerb');
        $model = Mage::registry('current_wettbewerb_user');

        if ($model->getId()) {
            return $helper->__("Edit Wettbewerb item '%s'", $this->escapeHtml($model->getTitle()));
        } else {
            return $helper->__("Add Wettbewerb item");
        }
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

}