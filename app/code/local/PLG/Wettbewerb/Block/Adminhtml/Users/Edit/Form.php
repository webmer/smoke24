<?php

class PLG_Wettbewerb_Block_Adminhtml_Users_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $helper = Mage::helper('plgwettbewerb');
        $model = Mage::registry('current_wettbewerb_user');

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('plgwettbewerb_admin/*/save', array(
                'id' => $this->getRequest()->getParam('id')
            )),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $this->setForm($form);

        $fieldset = $form->addFieldset('wettbewerb_user_form', array('legend' => $helper->__('User Information')));

        $fieldset->addField('vorname', 'text', array(
            'label' => $helper->__('vorname'),
            'required' => true,
            'name' => 'vorname',
        ));

        $fieldset->addField('name', 'text', array(
            'label' => $helper->__('name'),
            'required' => true,
            'name' => 'name',
        ));

        $fieldset->addField('gender', 'text', array(
            'label' => $helper->__('Gender'),
            'required' => true,
            'name' => 'gender',
        ));

        $fieldset->addField('strasse', 'text', array(
            'label' => $helper->__('strasse'),
            'required' => true,
            'name' => 'strasse',
        ));

        $fieldset->addField('plz', 'text', array(
            'label' => $helper->__('plz'),
            'required' => true,
            'name' => 'plz',
        ));

        $fieldset->addField('ort', 'text', array(
            'label' => $helper->__('ort'),
            'required' => true,
            'name' => 'ort',
        ));

        $fieldset->addField('email', 'text', array(
            'label' => $helper->__('email'),
            'required' => true,
            'name' => 'email',
        ));

        $fieldset->addField('phone', 'text', array(
            'label' => $helper->__('phone'),
            'required' => true,
            'name' => 'phone',
        ));

        $fieldset->addField('date_birth', 'date', array(
            'name'               => 'date_birth',
            'label'              => $helper->__('Date Old'),
            'tabindex'           => 1,
            'image'              => $this->getSkinUrl('images/grid-cal.gif'),
            'format'             => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT) ,
            'value'              => date( Mage::app()->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
                strtotime('next weekday') )
        ));

        $fieldset->addField('birth', 'date', array(
            'name'               => 'birth',
            'label'              => $helper->__('Date'),
            'tabindex'           => 1,
            'image'              => $this->getSkinUrl('images/grid-cal.gif'),
            'format'             => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT) ,
            'value'              => date( Mage::app()->getLocale()->getDateStrFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
                strtotime('next weekday') )
        ));

        $form->setUseContainer(true);

        if($data = Mage::getSingleton('adminhtml/session')->getFormData()){
            $form->setValues($data);
        } else {
            $form->setValues($model->getData());
        }

        return parent::_prepareForm();
    }

}