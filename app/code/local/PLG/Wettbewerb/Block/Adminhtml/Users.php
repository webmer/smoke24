<?php

class PLG_Wettbewerb_Block_Adminhtml_Users extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    protected function _construct()
    {
        parent::_construct();

        $helper = Mage::helper('plgwettbewerb');
        $this->_blockGroup = 'plgwettbewerb';
        $this->_controller = 'adminhtml_users';

        $this->_headerText = $helper->__('Wettbewerb Management');
        $this->_addButtonLabel = $helper->__('Add user');
    }

}