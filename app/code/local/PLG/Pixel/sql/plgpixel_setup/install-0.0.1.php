<?php
/**
 * @var Mage_Core_Model_Resource_Setup $this
 * @var Magento_Db_Adapter_Pdo_Mysql $connection
 */
$tableNews = $this->getTable('plgpixel/table_name');
//die($tableNews);
$this->startSetup();

$connection = $this->getConnection();
$connection->dropTable($tableNews);
$table = $connection
    ->newTable($tableNews)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
    ))
    ->addColumn('created', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable'  => false,
        'default' => 'CURRENT_TIMESTAMP'
    ))
    ->addColumn('url', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ))
    ->addColumn('url_typ', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => true,
    ))
    ->addColumn('eid', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => true,
    ))
    ->addColumn('content', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => true,
    ));
$this->getConnection()->createTable($table);
// delete register module from core
//$installer->run("DELETE FROM `core_resource` WHERE `code` = 'plgpixel_setup';");
$this->endSetup();