<?php

class PLG_Pixel_Block_Adminhtml_Item extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    protected function _construct()
    {
        parent::_construct();

        $helper = Mage::helper('plgpixel');
        $this->_blockGroup = 'plgpixel';
        $this->_controller = 'adminhtml_item';

        $this->_headerText = $helper->__('Pixel Management');
        $this->_addButtonLabel = $helper->__('Add pixel');
    }

}