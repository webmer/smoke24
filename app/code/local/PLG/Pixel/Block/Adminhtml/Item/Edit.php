<?php

class PLG_Pixel_Block_Adminhtml_Item_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        $this->_blockGroup = 'plgpixel';
        $this->_controller = 'adminhtml_item';
    }

    public function getHeaderText()
    {
        $helper = Mage::helper('plgpixel');
        $model = Mage::registry('current_pixel_item');

        if ($model->getId()) {
            return $helper->__("Edit Pixel '%s'", $this->escapeHtml($model->getTitle()));
        } else {
            return $helper->__("Add Pixel");
        }
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

}