<?php

class PLG_Pixel_Block_Adminhtml_Item_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('plgpixel/item')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $helper = Mage::helper('plgpixel');
        $this->addColumn('id', array(
            'header' => $helper->__('ID'),
            'index' => 'id',
            'width' => '50px',
        ));

        $this->addColumn('eid', array(
            'header' => $helper->__('EId'),
            'index' => 'eid',
            'type' => 'text',
            'width' => '50px',
        ));

        $this->addColumn('vorname', array(
            'header' => $helper->__('Url'),
            'index' => 'url',
            'type' => 'text',
        ));

        $this->addColumn('created', array(
            'header' => $helper->__('Created'),
            'index' => 'created',
            'type' => 'date',
        ));


        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }

    public function getRowUrl($model)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $model->getId(),
        ));
    }

}