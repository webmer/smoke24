<?php

class PLG_Pixel_Block_Adminhtml_Item_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        Mage::getSingleton('devel/devel');
        $helper = Mage::helper('plgpixel');
        $model = Mage::registry('current_pixel_item');

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array(
                'id' => $this->getRequest()->getParam('id')
            )),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $this->setForm($form);

        $fieldset = $form->addFieldset('pixel_item_form', array('legend' => $helper->__('Pixel Information')));

        $fieldset->addField('eid', 'text', array(
            'label' => $helper->__('eid'),
            'required' => true,
            'name' => 'eid',
        ));

        $fieldset->addField('url', 'text', array(
            'label' => $helper->__('url'),
            'required' => true,
            'name' => 'url',
        ));

        $fieldset->addField('content', 'text', array(
            'label' => $helper->__('eid typ'),
            'required' => false,
            'name' => 'content',
        ));

        $fieldset->addField('url_typ', 'text', array(
            'label' => $helper->__('url typ'),
            'required' => false,
            'name' => 'url_typ',
        ));


        $form->setUseContainer(true);

        if($data = Mage::getSingleton('adminhtml/session')->getFormData()){
            $form->setValues($data);
        } else {
            $form->setValues($model->getData());
        }

        return parent::_prepareForm();
    }

}