<?php

class PLG_Pixel_Adminhtml_ItemController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->loadLayout()->_setActiveMenu('plgpixel');
        $this->_addContent($this->getLayout()->createBlock('plgpixel/adminhtml_item'));
        $this->renderLayout();
    }

    public function massDeleteAction()
    {
        $item = $this->getRequest()->getParam('id', null);

        if (is_array($item) && sizeof($item) > 0) {
            try {
                foreach ($item as $id) {
                    Mage::getModel('plgpixel/item')->setId($id)->delete();
                }
                $this->_getSession()->addSuccess($this->__('Total of %d pixel have been deleted', sizeof($item)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('Please select pixel'));
        }
        $this->_redirect('*/*');
    }

    /**
     * @see PLG_Pixel_Adminhtml_ItemController::editAction
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        Mage::register('current_pixel_item', Mage::getModel('plgpixel/item')->load($id));

        $this->loadLayout()->_setActiveMenu('plgpixel');
        $this->_addContent($this->getLayout()->createBlock('plgpixel/adminhtml_item_edit'));
        $this->renderLayout();
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                $model = Mage::getModel('plgpixel/item');
                $model->setData($data)->setId($this->getRequest()->getParam('id'));
                if(!$model->getCreated()){
                    $model->setCreated(now());
                }
                $model->save();
//                var_dump($data,$model);die;

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('pixel was saved successfully'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array(
                    'id' => $this->getRequest()->getParam('id')
                ));
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                Mage::getModel('plgpixel/item')->setId($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('pixel was deleted successfully'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }

}