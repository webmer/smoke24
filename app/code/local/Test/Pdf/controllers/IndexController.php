<?php

class Test_Pdf_IndexController extends Mage_Core_Controller_Front_Action {

    public function invoicesAction() {
        $orderId = (int) $this->getRequest()->getParam('order_id');
        /**
         * @var $order Customweb_Subscription_Model_Sales_Order
         */
        $order = Mage::getModel('sales/order')->load($orderId);


        if ($this->_canViewOrder($order)) {
            /**
             * @var $invoices Mage_Sales_Model_Resource_Order_Invoice_Collection
             */
            $invoices = $order->getInvoiceCollection();
            if(count($invoices->getAllIds()) == 0){
                /**
                 * @var $model Mage_Sales_Model_Service_Order
                 */
                $model = Mage::getModel('sales/service_order', $order);
                $invoice = $model->prepareInvoice();
                if (!$invoice->getTotalQty()) {
                    Mage::throwException(Mage::helper('core')->__('Cannot create an invoice without products.'));
                }
                $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
                $invoice->register();
                $transactionSave = Mage::getModel('core/resource_transaction')
                    ->addObject($invoice)
                    ->addObject($invoice->getOrder());
                $transactionSave->save();
                $invoices = [$invoice];
            }
            $pdf = Mage::getModel('sales/order_pdf_invoice')->getPdf($invoices);

            return $this->_prepareDownloadResponse(
                'invoice'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(),
                'application/pdf'
            );

        }else{
            $this->_redirect('/sales/order/history/');
        }
    }

    /**
     * @param Customweb_Subscription_Model_Sales_Order $order
     * @return bool
     */
    protected function _canViewOrder($order)
    {
        $customerId = Mage::getSingleton('customer/session')->getCustomerId();
        $availableStates = Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates();
        if ($order->getId() && $order->getCustomerId() && ($order->getCustomerId() == $customerId)
            && in_array($order->getState(), $availableStates, $strict = true)
        ) {
            return true;
        }
        return false;
    }
}