<?php

class Queldorei_Priceslider_SliderController extends Mage_Core_Controller_Front_Action
{
    public function viewAction()
    {
        $categoryId = (int)$this->getRequest()->getParam('id', false);
        $category = Mage::getModel('catalog/category')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($categoryId);
        Mage::register('current_category', $category);



        $this->loadLayout();

        $filter = Mage::app()->getRequest()->getParam('category');
        if( $filter ) {
            $filterCategory = Mage::getModel('catalog/category')
                ->load($filter);

            /**
             * @var $layout Mage_Core_Model_Layout
             */
            $layout = $this->getLayout();
            $pCollection = $layout->getBlockSingleton('catalog/product_list')->getLoadedProductCollection();
//            $select = $pCollection->getSelect();
//            $select->where('category_id in ('.$filter.')');
            $pCollection->joinField('category_id', 'catalog/category_product', 'category_id',
                'product_id = entity_id', null, 'left')
                ->addAttributeToFilter('category_id', array('in' => array($category->getId(), $filterCategory->getId())));
            $pIds = [];
            $productCollection = Mage::getModel('catalog/product')->getCollection()->addFieldToFilter('entity_id', array('in' => $pCollection->getAllIds()));

            foreach ($productCollection as $item) {
                if (in_array($category->getId(), $item->getCategoryIds()) && in_array($filterCategory->getId(), $item->getCategoryIds())) {
                    $pIds[] = $item->getId();
                }
            }

            $pCollection->addFieldToFilter('entity_id', array('in' => $pIds));
            $pCollection->getSelect()
                ->group('e.entity_id');

        }

        $this->renderLayout();
    }
}