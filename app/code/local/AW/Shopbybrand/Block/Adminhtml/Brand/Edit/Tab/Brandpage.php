<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Shopbybrand
 * @version    1.3.2
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Shopbybrand_Block_Adminhtml_Brand_Edit_Tab_Brandpage
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    public function getTabLabel()
    {
        return $this->__('Brand Page');
    }

    public function getTabTitle()
    {
        return $this->__('Brand Page');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();

        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->setForm($form);

        $_fieldset = $form->addFieldset(
            'brandpage_form',
            array(
                'legend' => $this->__('Brand Page')
            )
        );

        $_data = Mage::registry('current_brand');

        $_fieldset->addField(
            'page_title',
            'text',
            array(
                'name'     => 'page_title',
                'label'    => $this->__('Page Title'),
                'title'    => $this->__('Page Title'),
            )
        );

        $_fieldset->addField(
            'url_key',
            'text',
            array(
                'name'     => 'url_key',
                'label'    => $this->__('URL key'),
                'title'    => $this->__('URL key'),
                'required' => true,
                'class'    => 'validate-identifier',
            )
        );
        $inputType = 'textarea';
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $inputType = 'editor';
        }

        $wysiwygConfig = Mage::getSingleton('awshopbybrand/source_wysiwyg')->getConfig(
            array('add_variables' => false)
        );
        $_fieldset->addField(
            'description',
            $inputType,
            array(
                'name'    => 'description',
                'label'   => $this->__('Brand description'),
                'title'   => $this->__('Brand description'),
                'config'  => $wysiwygConfig,
                'wysiwyg' => true,
            )
        );

        $_fieldset->addField(
            'meta_keywords',
            'textarea',
            array(
                'name'  => 'meta_keywords',
                'label' => $this->__('Meta keywords'),
                'title' => $this->__('Meta keywords'),
            )
        );

        $_fieldset->addField(
            'meta_description',
            'textarea',
            array(
                'name'  => 'meta_description',
                'label' => $this->__('Meta description'),
                'title' => $this->__('Meta description'),
            )
        );

        $form->setValues($_data->getData());
        return parent::_prepareForm();
    }
}