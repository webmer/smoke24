<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Shopbybrand
 * @version    1.3.2
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Shopbybrand_Model_Observer
{
    protected function forwardToAllBrands($request)
    {
        $request->initForward()
            ->setControllerName('index')
            ->setModuleName('shopbybrands')
            ->setActionName('allBrandsView')
            ->setDispatched(false)
        ;
        return $this;
    }

    public function afterProductSave(Varien_Event_Observer $observer)
    {
        $product = $observer->getEvent()->getDataObject();

        $newBrandId = $product->getData('aw_shopbybrand_brand');
        $oldBrandId = $product->getOrigData('aw_shopbybrand_brand');
        $productId = $product->getId();

        if (!$productId || $newBrandId == $oldBrandId) {
            return $this;
        }

        if ($oldBrandId) {
            $oldBrand = Mage::getModel('awshopbybrand/brand')->load($oldBrandId);
            $oldBrand->removeProduct($productId);
            $oldBrand->save();
        }

        if ($newBrandId) {
            $newBrand = Mage::getModel('awshopbybrand/brand')->load($newBrandId);
            $newBrand->addProduct($productId);
            $newBrand->save();
        }

        return $this;
    }


    public function preDispatch($event)
    {
        if (!Mage::helper('awshopbybrand')->isModuleEnabled()) {
            return $this;
        }

        $pathRoutes = explode('/', Mage::app()->getFrontController()->getRequest()->getPathInfo());
        $controllerAction = $event->getControllerAction();
        /**
         * @var $request Mage_Core_Controller_Request_Http
         */
        $request = $controllerAction->getRequest();
        $key = array_search(Mage::helper('awshopbybrand/config')->getAllBrandsUrlKey(), $pathRoutes);

        if ($key == 1) {
            if (!isset($pathRoutes[++$key])) {
                $this->forwardToAllBrands($request);
            } else {

                //Load brand and add it to registry
                $brandUrlKey = $pathRoutes[$key];
                if (!$brandUrlKey) {
                    return $this->forwardToAllBrands($request);
                }

                /** @var $brandsCollection AW_Shopbybrand_Model_Resource_Brand_Collection */
                $brandsCollection = Mage::getModel('awshopbybrand/brand')->getCollection();
                $brandsCollection
                    ->addFieldToFilter('url_key', array('eq' => $brandUrlKey))
                    ->addStatusFilter()
                ;

                $brand = $brandsCollection->getFirstItem();

                if (null === $brand->getId()) {
                    return $this;
                }

                $query = $request->getQuery();
                if(isset($pathRoutes[++$key]) && !empty($pathRoutes[$key])) {
                    $query['cat'] = isset($query['cat']) && !empty($query['cat']) ?$query['cat']:$pathRoutes[$key];
                }

                if (isset($query['cat']) && is_numeric($query['cat'])) {
                    $categoryModel = Mage::getModel('catalog/category')->load($query['cat']);
                    $currentUrl = Mage::helper('core/url')->getCurrentUrl();
                    if (!strpos($currentUrl, $categoryModel->getUrlKey())) {
                        $url = Mage::getSingleton('core/url')->parseUrl($currentUrl);
                        $path = trim(str_replace('-' . $categoryModel->getUrlKey(), '', $url->getPath()), '/').'-'.$categoryModel->getUrlKey();

                        Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl($path));
                        Mage::app()->getResponse()->sendResponse();
                        exit;
                    }
                }
//                if(isset($pathRoutes[++$key]) && !empty($pathRoutes[$key])){
//
//                    $productUrl = $pathRoutes[$key];
//                    $oRewrite = Mage::getModel('core/url_rewrite')
//                        ->setStoreId(Mage::app()->getStore()->getId())
//                        ->loadByRequestPath($productUrl);
//                    $productId = $oRewrite->getProductId();
//                    $request->initForward()
//                        ->setControllerName('index')
//                        ->setModuleName('shopbybrands')
//                        ->setActionName('brandProductPageView')
//                        ->setPost('brand_id', $brand->getId())
//                        ->setPost('product_id', $productId)
//                        ->setDispatched(false)
//                    ;
//                }else{
                $request->initForward()
                    ->setQuery($query)
                    ->setControllerName('index')
                    ->setModuleName('shopbybrands')
                    ->setActionName('brandPageView')
                    ->setPost('brand_id', $brand->getId())
                    ->setDispatched(false)
                ;
//                }

            }
        }
        return $this;
    }

    /**
     * @param $observer Varien_Event_Observer
     */
    public function prepareProductUrl($observer)
    {
        Mage::getSingleton('devel/devel');
        $event = $observer->getEvent();
        $collection = $event->getCollection();
        $currentUrl = Mage::helper('core/url')->getCurrentUrl();
        $url = Mage::getSingleton('core/url')->parseUrl($currentUrl);
        $path = $url->getPath();
        if(strpos($path,'/brands') === false){
            return;
        }
        $path = $this->str_replace_once('/','',$path);
        foreach($collection->getItems() as $item){
            /**
             * @var $item Customweb_Subscription_Model_Catalog_Product
             */
//            $product = $item->getProduct();
            $url = $item->getProductUrl();
            $url = str_replace(Mage::getBaseUrl(), Mage::getBaseUrl().$path.'/', $url);
            $item->setUrl($url);
        }
    }

    protected function str_replace_once($search, $replace, $text)
    {
        $pos = strpos($text, $search);
        return $pos!==false ? substr_replace($text, $replace, $pos, strlen($search)) : $text;
    }
}