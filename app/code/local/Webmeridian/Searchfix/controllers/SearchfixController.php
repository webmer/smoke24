<?php

class Webmeridian_Searchfix_SearchfixController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $posts = $this->getRequest()->getPost();
        $arr=[];

        foreach ($posts as $post){
            $base_url = (basename($post));
            $rewrite = Mage::getModel('core/url_rewrite')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->loadByRequestPath($base_url);

            $productId = $rewrite->getProductId();
            $product = Mage::getModel('catalog/product')->load($productId);
            $promo_price = Mage::getModel('catalogrule/rule')->calcProductPriceRule($product,$product->getPrice());
            $productFinalPrice = Mage::getModel('abo/calculator')->round($product->getFinalPrice());
            if($product->getSpecialPrice() == null && $promo_price > 0){
                if($product->getData('tier_price')){
                    $productFinalPrice = $product->getTierPrice()[0]['price'];;
                }
                $arr[] = array('html' => $post, 'price' => number_format($productFinalPrice, 2). " CHF");
            }else{
                if($product->getData('tier_price')){
                    $productFinalPrice = $product->getTierPrice()[0]['price'];
                    $arr[] = array('html' => $post, 'price' => number_format($productFinalPrice, 2). " CHF");
                }
            }
        }
        echo json_encode($arr);
        return;

    }

    public function scrollAction()
    {
        $posts = $this->getRequest()->getPost();
        $arr = [];
        if (isset($posts) && !empty($posts)) {
            foreach ($posts as $post){
                $base_url = (basename($post));
                $rewrite = Mage::getModel('core/url_rewrite')
                    ->setStoreId(Mage::app()->getStore()->getId())
                    ->loadByRequestPath($base_url);

                $productId = $rewrite->getProductId();
                $product = Mage::getModel('catalog/product')->load($productId);
                $promo_price = Mage::getModel('catalogrule/rule')->calcProductPriceRule($product,$product->getPrice());
                $productFinalPrice = Mage::getModel('abo/calculator')->round($product->getFinalPrice());
                if($product->getSpecialPrice() == null && $promo_price > 0){
                    if($product->getData('tier_price')){
                        $productFinalPrice = $product->getTierPrice()[0]['price'];;
                    }
                    $arr[] = array('html' => $post, 'price' => number_format($productFinalPrice, 2). " CHF");
                }else{
                    if($product->getData('tier_price')){
                        $productFinalPrice = $product->getTierPrice()[0]['price'];
                        $arr[] = array('html' => $post, 'price' => number_format($productFinalPrice, 2). " CHF");
                    }
                }
            }
        }

        echo json_encode($arr);
        return;

    }
}