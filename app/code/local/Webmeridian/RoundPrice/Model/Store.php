<?php

class Webmeridian_RoundPrice_Model_Store extends Mage_Core_Model_Store{

    /**
     * Round price
     *
     * @param mixed $price
     * @return double
     */
    public function roundPrice($price)
    {
        return Mage::getModel('abo/calculator')->round($price);
    }
}