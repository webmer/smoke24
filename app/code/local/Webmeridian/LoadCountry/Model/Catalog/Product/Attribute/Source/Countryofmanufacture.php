<?php

class Webmeridian_LoadCountry_Model_Catalog_Product_Attribute_Source_Countryofmanufacture
    extends Mage_Catalog_Model_Product_Attribute_Source_Countryofmanufacture
{
    /**
     * Get list of all available countries
     *
     * @return mixed
     */
    public function getAllOptions()
    {
        $cacheKey = 'DIRECTORY_COUNTRY_SELECT_STORE_' . Mage::app()->getStore()->getCode();
        if (Mage::app()->useCache('config') && $cache = Mage::app()->loadCache($cacheKey)) {
            $options = unserialize($cache);
        } else {
            if (Mage::app()->getStore()->isAdmin()) {
                $collection = Mage::getModel('directory/country')->getResourceCollection();
            } else {
                $collection = Mage::getModel('directory/country')->getResourceCollection()->loadByStore();
            }
            $options = $collection->toOptionArray();

            if (Mage::app()->useCache('config')) {
                Mage::app()->saveCache(serialize($options), $cacheKey, ['config']);
            }
        }
        return $options;
    }
}