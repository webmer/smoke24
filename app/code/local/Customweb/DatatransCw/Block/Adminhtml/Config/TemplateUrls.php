<?php 

class Customweb_DatatransCw_Block_Adminhtml_Config_TemplateUrls extends Mage_Adminhtml_Block_System_Config_Form_Field {

	public function render(Varien_Data_Form_Element_Abstract $element)
	{
		$content = $this->getContainerContent();
		return sprintf('<tr class="system-fieldset-sub-head" id="row_%s"><td colspan="5">%s</td></tr>',
			$element->getHtmlId(), $content
		);
	}
	
	protected function getContainerContent() {
		$storeViews = Mage::app()->getStores();
		
		$result = '';
		foreach ($storeViews as $view) {
			$url = Mage::getUrl('DatatransCw/checkout/template', array(
				'_secure' => true,
				'_store' => $view->getId(),
				'_store_to_url' => true
			));
			$result .= '<a href="' . $url . '">' . $url . '</a><br />';
		}
		
		return $result;
	}
	
}