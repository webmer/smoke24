<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2013 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.customweb.ch/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.customweb.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 *
 * @category	Customweb
 * @package		Customweb_DatatransCw
 * @version		1.2.96
 *
 * @Bean
 */
class Customweb_DatatransCw_Model_BackendOperation_DeferredAuthorizationAdapter implements Customweb_Payment_BackendOperation_Adapter_Shop_IDeferredAuthorization
{

	public function authorize(Customweb_Payment_Authorization_ITransaction $transaction)
	{
		
		$transactionModel = $transaction->getTransactionContext()->getTransactionModel();
		$order = Mage::getModel('sales/order')->load($transactionModel->getOrderId());
		$transactionModel->setTransactionObject($transaction);
		$transactionModel->save();

		$order->getPayment()->getMethodInstance()->processPayment($order, $transactionModel);

		if ($transaction->isAuthorized()) {
			$order = Mage::getModel('sales/order')->load($transactionModel->getOrderId());
			$order->addStatusToHistory($transaction->getOrderStatus(), Mage::helper('DatatransCw')->__('Transaction was authorized over the deferred authorization mechanism.'));
			$order->save();
		}
		
	}
}
