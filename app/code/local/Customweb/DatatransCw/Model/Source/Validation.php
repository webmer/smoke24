<?php
class Customweb_DatatransCw_Model_Source_Validation{
	public function toOptionArray(){
		$options = array(
			array('value'=>'validation_before', 'label'=>Mage::helper('adminhtml')->__("Before payment selection")),
			array('value'=>'validation_after', 'label'=>Mage::helper('adminhtml')->__("After payment selection")),
			array('value'=>'validation_authorization', 'label'=>Mage::helper('adminhtml')->__("During authorization"))
		);
		return $options;
	}
}
