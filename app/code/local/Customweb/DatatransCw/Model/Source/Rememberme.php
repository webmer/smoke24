<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2018 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 *
 * @category	Customweb
 * @package		Customweb_DatatransCw
 */

class Customweb_DatatransCw_Model_Source_Rememberme
{
	public function toOptionArray()
	{
		$options = array(
			array('value' => 'inactive', 'label' => Mage::helper('adminhtml')->__("Checkbox is not shown")),
			array('value' => 'yes', 'label' => Mage::helper('adminhtml')->__("Remember me checkbox appears")),
			array('value' => 'checked', 'label' => Mage::helper('adminhtml')->__("Remember me checkbox appears and is checked
						"))
		);
		return $options;
	}
}
