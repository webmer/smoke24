<?php
class Customweb_DatatransCw_Model_Source_PowerpayOpenInvoiceAuthorizationMethod{
	public function toOptionArray(){
		$options = array(
			array('value'=>'PaymentPage', 'label'=>Mage::helper('adminhtml')->__("Payment Page (Payment Page)")),
			array('value'=>'ServerAuthorization', 'label'=>Mage::helper('adminhtml')->__("Server Authorization (XML Authorisation)"))
		);
		return $options;
	}
}
