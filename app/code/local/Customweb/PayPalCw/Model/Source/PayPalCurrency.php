<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 *
 * @category	Customweb
 * @package		Customweb_PayPalCw
 */

class Customweb_PayPalCw_Model_Source_PayPalCurrency
{
	public function toOptionArray()
	{
		$options = array(
			array('value' => 'USD', 'label' => Mage::helper('adminhtml')->__("United States dollar (USD)")),
			array('value' => 'CHF', 'label' => Mage::helper('adminhtml')->__("Swiss franc (CHF)")),
			array('value' => 'EUR', 'label' => Mage::helper('adminhtml')->__("Euro (EUR)")),
			array('value' => 'AUD', 'label' => Mage::helper('adminhtml')->__("Australian dollar (AUD)")),
			array('value' => 'BRL', 'label' => Mage::helper('adminhtml')->__("Brazilian real (BRL)")),
			array('value' => 'CAD', 'label' => Mage::helper('adminhtml')->__("Canadian dollar (CAD)")),
			array('value' => 'CZK', 'label' => Mage::helper('adminhtml')->__("Czech koruna (CZK)")),
			array('value' => 'DKK', 'label' => Mage::helper('adminhtml')->__("Danish krone (DKK)")),
			array('value' => 'HKD', 'label' => Mage::helper('adminhtml')->__("Hong Kong dollar (HKD)")),
			array('value' => 'HUF', 'label' => Mage::helper('adminhtml')->__("Hungarian forint (HUF)")),
			array('value' => 'ILS', 'label' => Mage::helper('adminhtml')->__("Israeli new shekel (ILS)")),
			array('value' => 'JPY', 'label' => Mage::helper('adminhtml')->__("Japanese yen (JPY)")),
			array('value' => 'MYR', 'label' => Mage::helper('adminhtml')->__("Malaysian ringgit (MYR)")),
			array('value' => 'MXN', 'label' => Mage::helper('adminhtml')->__("Mexican peso (MXN)")),
			array('value' => 'NOK', 'label' => Mage::helper('adminhtml')->__("Norwegian krone (NOK)")),
			array('value' => 'NZD', 'label' => Mage::helper('adminhtml')->__("New Zealand dollar (NZD)")),
			array('value' => 'PHP', 'label' => Mage::helper('adminhtml')->__("Philippine peso (PHP)")),
			array('value' => 'PLN', 'label' => Mage::helper('adminhtml')->__("Polish złoty (PLN)")),
			array('value' => 'GBP', 'label' => Mage::helper('adminhtml')->__("Pound sterling (GBP)")),
			array('value' => 'SGD', 'label' => Mage::helper('adminhtml')->__("Singapore dollar (SGD)")),
			array('value' => 'SEK', 'label' => Mage::helper('adminhtml')->__("Swedish krona (SEK)")),
			array('value' => 'TWD', 'label' => Mage::helper('adminhtml')->__("New Taiwan dollar (TWD)")),
			array('value' => 'THB', 'label' => Mage::helper('adminhtml')->__("Thai baht (THB)")),
			array('value' => 'TRY', 'label' => Mage::helper('adminhtml')->__("Turkish lira (TRY)"))
		);
		return $options;
	}
}
