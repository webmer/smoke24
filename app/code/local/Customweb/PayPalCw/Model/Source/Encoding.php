<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 *
 * @category	Customweb
 * @package		Customweb_PayPalCw
 */

class Customweb_PayPalCw_Model_Source_Encoding
{
	public function toOptionArray()
	{
		$options = array(
			array('value' => 'ISO-8859-1', 'label' => Mage::helper('adminhtml')->__("ISO-8859-1")),
			array('value' => 'ISO-8859-2', 'label' => Mage::helper('adminhtml')->__("ISO-8859-2")),
			array('value' => 'ISO-8859-3', 'label' => Mage::helper('adminhtml')->__("ISO-8859-3")),
			array('value' => 'ISO-8859-4', 'label' => Mage::helper('adminhtml')->__("ISO-8859-4")),
			array('value' => 'ISO-8859-5', 'label' => Mage::helper('adminhtml')->__("ISO-8859-5")),
			array('value' => 'ISO-8859-6', 'label' => Mage::helper('adminhtml')->__("ISO-8859-6")),
			array('value' => 'ISO-8859-7', 'label' => Mage::helper('adminhtml')->__("ISO-8859-7")),
			array('value' => 'ISO-8859-8', 'label' => Mage::helper('adminhtml')->__("ISO-8859-8")),
			array('value' => 'ISO-8859-9', 'label' => Mage::helper('adminhtml')->__("ISO-8859-9")),
			array('value' => 'ISO-8859-13', 'label' => Mage::helper('adminhtml')->__("ISO-8859-13")),
			array('value' => 'ISO-8859-15', 'label' => Mage::helper('adminhtml')->__("ISO-8859-15")),
			array('value' => 'UTF-8', 'label' => Mage::helper('adminhtml')->__("UTF-8")),
			array('value' => 'ASCII', 'label' => Mage::helper('adminhtml')->__("US-ASCII")),
			array('value' => 'windows-1250', 'label' => Mage::helper('adminhtml')->__("Windows-1250")),
			array('value' => 'windows-1251', 'label' => Mage::helper('adminhtml')->__("Windows-1251")),
			array('value' => 'windows-1252', 'label' => Mage::helper('adminhtml')->__("Windows-1252")),
			array('value' => 'windows-1253', 'label' => Mage::helper('adminhtml')->__("Windows-1253")),
			array('value' => 'windows-1254', 'label' => Mage::helper('adminhtml')->__("Windows-1254")),
			array('value' => 'windows-1255', 'label' => Mage::helper('adminhtml')->__("Windows-1255")),
			array('value' => 'windows-1256', 'label' => Mage::helper('adminhtml')->__("Windows-1256")),
			array('value' => 'windows-1257', 'label' => Mage::helper('adminhtml')->__("Windows-1257")),
			array('value' => 'windows-1258', 'label' => Mage::helper('adminhtml')->__("Windows-1258")),
			array('value' => 'windows-874', 'label' => Mage::helper('adminhtml')->__("Windows-874")),
			array('value' => 'MAC-GREEK', 'label' => Mage::helper('adminhtml')->__("x-mac-greek")),
			array('value' => 'MAC-TURKISH', 'label' => Mage::helper('adminhtml')->__("x-mac-turkish")),
			array('value' => 'MAC-ROMAN', 'label' => Mage::helper('adminhtml')->__("x-mac-centraleurroman")),
			array('value' => 'MAC-CYRILLIC', 'label' => Mage::helper('adminhtml')->__("x-mac-cyrillic"))
		);
		return $options;
	}
}
