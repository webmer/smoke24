<?php
class Customweb_Subscription_Block_Adminhtml_System_Config_PaymentAttempts extends Customweb_Subscription_Block_Adminhtml_System_Config_TableForm
{
	protected function _getPrefix()
	{
		return 'attempts-';
	}

	protected function _getTableTemplate()
	{
		return 'customweb/subscription/system/config/attempts.phtml';
	}

	protected function _getRowTemplate()
	{
		return 'customweb/subscription/system/config/attempts/row.phtml';
	}

	protected function _getTemplateRowData()
	{
		$template = new stdClass();
		$template->days = '#{days}';
		$template->email_template_pending = '#{email_template_pending}';
		$template->email_template_pending_label = '#{email_template_pending_label}';
		$template->email_template_update = '#{email_template_update}';
		$template->email_template_update_label = '#{email_template_update_label}';

		return $template;
	}

	protected function _getRow($data, $index)
	{
		if (!isset($data->email_template_pending_label)) {
			$data->email_template_pending_label = $this->_getEmailTemplateLabel('customweb_subscription_email_template_pending', $data->email_template_pending);
		}

		if (!isset($data->email_template_update_label)) {
			$data->email_template_update_label = $this->_getEmailTemplateLabel('customweb_subscription_email_template_update', $data->email_template_update);
		}

		return parent::_getRow($data, $index);
	}

	protected function _getEmailTemplateLabel($path, $value)
	{
		$options = Mage::getModel('adminhtml/system_config_source_email_template')->setPath($path)->toOptionArray();
		foreach ($options as $option) {
			if ($option['value'] == $value) {
				return $option['label'];
			}
		}
	}
}