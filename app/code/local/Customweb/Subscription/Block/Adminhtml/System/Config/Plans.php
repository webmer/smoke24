<?php
class Customweb_Subscription_Block_Adminhtml_System_Config_Plans extends Customweb_Subscription_Block_Adminhtml_System_Config_TableForm
{
	protected function _getPrefix()
	{
		return 'plans-';
	}

	protected function _getTableTemplate()
	{
		return 'customweb/subscription/system/config/plans.phtml';
	}

	protected function _getRowTemplate()
	{
		return 'customweb/subscription/system/config/plans/row.phtml';
	}

	protected function _getTableBlock()
	{
		$block = parent::_getTableBlock();
		$block->setPeriodUnitOptions($this->_getPeriodUnitOptions(Mage::helper('adminhtml')->__('-- Please Select --')));
		return $block;
	}

	protected function _getTemplateRowData()
	{
		$template = Mage::getModel('customweb_subscription/plan');
		$template->setDescription('#{description}');
		$template->setPeriodUnitLabel('#{period_unit_label}');
		$template->setPeriodUnit('#{period_unit}');
		$template->setPeriodFrequency('#{period_frequency}');
		$template->setPeriodMaxCycles('#{period_max_cycles}');
		$template->setCancelPeriod('#{cancel_period}');
		$template->setOrder('#{order}');

		return $template;
	}

	/**
	 * Getter for period unit options with "Please Select" label
	 *
	 * @return array
	 */
	protected function _getPeriodUnitOptions($emptyLabel)
	{
		return array_merge(array(
			'' => $emptyLabel
		), Mage::getModel('customweb_subscription/schedule')->getAllPeriodUnits());
	}
}