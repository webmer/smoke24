<?php
/**
 * Backend configuration model that persists values serialized.
 *
 * @author Simon Schurter
 */
class Customweb_Subscription_Model_System_Config_Backend_Plans extends Mage_Core_Model_Config_Data
{
    protected function _afterLoad()
    {
    	if (!($this->getValue() instanceof Customweb_Subscription_Model_Plan)) {
    		$value = $this->getValue();
    		$this->setValue(empty($value) ? false : unserialize(base64_decode($value)));
    	}
    }

    protected function _beforeSave()
    {
        $value = $this->getValue();
        if (is_array($value)) {
        	unset($value['__empty']);
            foreach ($value as $code => $field) {
            	unset($value[$code]['#{index}']);
            }

            $plans = array();
            $index = 0;
            foreach ($value['order'] as $key => $data) {
            	$plan = Mage::getModel('customweb_subscription/plan');
            	$plan->setIndex($index);
            	$plan->setDescription($value['description'][$key]);
            	$plan->setPeriodUnit($value['period_unit'][$key]);
            	$plan->setPeriodFrequency($value['period_frequency'][$key]);
            	$plan->setPeriodMaxCycles($value['period_max_cycles'][$key]);
            	$plan->setCancelPeriod($value['cancel_period'][$key]);
            	$plan->setOrder($value['order'][$key]);
            	$plans[$index] = $plan;
            	$index++;
            }
        }
        $this->setValue($plans);

   		if (is_array($this->getValue())) {
    		$this->setValue(base64_encode(serialize($this->getValue())));
    	}
    }
}
