<?php
/**
 * Backend configuration model that persists values serialized.
 *
 * @author Simon Schurter
 */
class Customweb_Subscription_Model_System_Config_Backend_PaymentAttempts extends Mage_Core_Model_Config_Data
{
    protected function _afterLoad()
    {
    	if (!is_array($this->getValue())) {
    		$value = $this->getValue();
    		$this->setValue(empty($value) ? false : unserialize(base64_decode($value)));
    	}
    }

    protected function _beforeSave()
    {
        $value = $this->getValue();
        if (is_array($value)) {
        	unset($value['__empty']);
            foreach ($value as $code => $field) {
            	unset($value[$code]['#{index}']);
            }

            natsort($value['days']);

            $attempts = array();
            $index = 1;
            foreach ($value['days'] as $key => $data) {
            	$attempt = new stdClass();
            	$attempt->level = $index;
            	$attempt->days = $value['days'][$key];
            	$attempt->email_template_pending = $value['email_template_pending'][$key];
            	$attempt->email_template_update = $value['email_template_update'][$key];
            	$attempts[$index] = $attempt;
            	$index++;
            }
        }
        $this->setValue($attempts);

   		if (is_array($this->getValue())) {
    		$this->setValue(base64_encode(serialize($this->getValue())));
    	}
    }
}
