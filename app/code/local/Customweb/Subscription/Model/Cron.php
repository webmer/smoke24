<?php
class Customweb_Subscription_Model_Cron
{
	/**
	 * This method tests helps to test whether the cron is running correctly.
	 */
	public function test($force = false)
	{
		if (Mage::getStoreConfig('customweb_subscription/test/enabled') == 1
			&& (Mage::getStoreConfig('customweb_subscription/test/enable_cron') == 1 || $force === true)) {
			Mage::helper('customweb_subscription')->incrementTestDate();
			$this->update();
		}
	}

	/**
	 * This method is run by the cron tab. It checks cancelation requests,
	 * due authorizations and captures and updates the subscription's status.
	 *
	 * @return Customweb_Subscription_Model_Observer
	 */
	public function update()
	{
		$collection = Mage::getModel('customweb_subscription/subscription')->getCollection()
			->addFieldToFilter('status', array('in' => array(Customweb_Subscription_Model_Subscription::STATUS_ACTIVE,
			Customweb_Subscription_Model_Subscription::STATUS_PENDING,
			Customweb_Subscription_Model_Subscription::STATUS_AUTHORIZED,
			Customweb_Subscription_Model_Subscription::STATUS_CAPTURED)));
		foreach ($collection->getItems() as $subscription) {
			try {
				$subscription = Mage::getModel('customweb_subscription/subscription')->load($subscription->getId());
				if ($subscription->isCancelRequested()) {
					$subscription->processCancelRequest();
				}
				if ($subscription->canAuthorize() && $subscription->isDueToAuthorize()) {
					try {
						$subscription->requestPayment();
					} catch(Exception $e) {
						Mage::logException($e);
					}
				}
				if ($subscription->canCapture() && $subscription->isDueToCapture()) {
					try {
						$subscription->capture();
					} catch(Exception $e) {
						Mage::logException($e);
					}
				}
				try {
					$subscription->updateStatus();
				} catch(Exception $e) {
					Mage::logException($e);
				}
			} catch(Exception $e) {
				Mage::logException($e);
			}
		}

		return $this;
	}
}