<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Grid
 */
class Bxp_Report_Block_Adminhtml_Product_Sold_Grid extends Mage_Adminhtml_Block_Report_Product_Sold_Grid {

    protected function _construct() {
        parent::_construct();
        Mage::getSingleton('devel/devel');
    }

    protected function _prepareColumns() {
        $this->addColumn('sku', array(
            'header' => Mage::helper('reports')->__('SKU'),
            'width' => '120px',
            'align' => 'left',
            'index' => 'sku'
        ));
        return parent::_prepareColumns();
    }

}
