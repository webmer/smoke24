<?php

/**
 * Description of Onepage
 */
class Bxp_AgeVerification_Model_Checkout_Onepage extends Mage_Checkout_Model_Type_Onepage {

    public function saveBilling($data, $customerAddressId) {
        if (empty($data)) {
            return array('error' => -1, 'message' => Mage::helper('checkout')->__('Invalid data.'));
        }
        if (isset($data['day'], $data['month'], $data['year'])) {
            $dob = $data['year'] . '-' . $data['month'] . '-' . $data['day'];
            if (!Mage::helper('ageverification')->verifyAge($dob)) {
                return array('error' => -1, 'message' => Mage::helper('ageverification')->__('You must be at least 18 years old to use this website.'));
            }
        }
        return parent::saveBilling($data, $customerAddressId);
    }

}
