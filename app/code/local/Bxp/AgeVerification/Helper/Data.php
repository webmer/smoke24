<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Data
 */
class Bxp_AgeVerification_Helper_Data extends Mage_Core_Helper_Abstract {

    public function verifyAge($dob, $format = 'Y-m-d') {
        $today = new DateTime(date($format));
        $date = DateTime::createFromFormat($format, $dob);
        $today->modify('-18 years');
        Mage::log($today->format('Y'));
        Mage::log($date->format('Y'));
        Mage::log('dob:' . $dob);
        if ($today < $date) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
