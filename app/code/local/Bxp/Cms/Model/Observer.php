<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Observer
 */
class Bxp_Cms_Model_Observer extends Varien_Event_Observer {

    public function extendCms($observer = NULL) {
        $model = Mage::registry('cms_page');
        
        /* @var $form Varien_Data_Form */
        $form = $observer->getForm();
        
        $fieldset = $form->addFieldset('bxp_settings', array(
            'legend' => Mage::helper('bxp_cms')->__('BXP'),
            'class' => 'fieldset-wide'
        ));

        $fieldset->addField('show_top', 'select', array(
            'name' => 'show_top',
            'label' => Mage::helper('bxp_cms')->__('Show in top links'),
            'title' => Mage::helper('bxp_cms')->__('Show in top links'),
            'disabled' => FALSE,
            'value' => $model->getShowTop(),
            'options' => array(
                Mage::helper('bxp_cms')->__('No'),
                Mage::helper('bxp_cms')->__('Yes'))
        ));
    }

}
