<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Advert
 */
class Bxp_Cms_Block_Advert extends Mage_Core_Block_Template {

    protected $_template = 'bxp/advert_block.phtml';
    protected $_dimensions = array();

    protected function _construct() {
        parent::_construct();
        Mage::getSingleton('devel/devel');
    }

    /**
     * 
     */
    protected function _beforeToHtml() {
        $this->setContent($this->getLayout()->createBlock('cms/block')->setBlockId($this->getBlockId())->toHtml());
    }

    public function setDimensions($width, $height) {
        $this->_dimensions['width'] = $width;
        $this->_dimensions['height'] = $height;
    }

    public function getDimensions($type = NULL) {
        if ($type && isset($this->_dimensions[$type])) {
            return $this->_dimensions[$type];
        }
        return $this->_dimensions;
    }

    public function getDimensionsHtml() {
        if ($this->getPlane()) {
            return $this->getPlane() . ':' . $this->getDimensions($this->getPlane()) . 'px;';
        } else {
            return 'width:' . $this->getDimensions('width') . 'px;' . 'height:' .
                    $this->getDimensions('height') . 'px;';
        }
    }

}
