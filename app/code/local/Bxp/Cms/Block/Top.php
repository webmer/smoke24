<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Top
 */
class Bxp_Cms_Block_Top extends Mage_Core_Block_Template {


    /**
     * 
     */
    protected function _beforeToHtml() {
        $this->_prepareCollection();
        return parent::_beforeToHtml();
    }

    /**
     * 
     * @return Mage_Cms_Model_Page_Collection
     */
    protected function _prepareCollection(){
       
        $collection = Mage::getModel('cms/page')->getCollection()
                ->addStoreFilter(Mage::app()->getStore())
                ->addFieldToFilter('show_top', 1);
        $this->setCollection($collection);
    }

}
