<?php

$installer = $this;

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
$installer->run(' ALTER TABLE `cms_page` ADD `show_top` INT(1) NULL; ');

$installer->endSetup();
