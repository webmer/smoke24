<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Observer
 * @method public getInit() Is the class inited
 */
class Bxp_AdvancedRules_Model_Observer extends Varien_Event_Observer {

    protected $_rulesItemTotals;
    protected $_validator;
    protected $_rules;

    protected function _construct() {
        $this->setInit(FALSE);
        parent::_construct();
    }

    /**
     * Init validator
     * Init process load collection of rules for specific website,
     * customer group and coupon code
     *
     * @param   int $websiteId
     * @param   int $customerGroupId
     * @param   string $couponCode
     * @return  Mage_SalesRule_Model_Validator
     */
    public function init() {
        if (!$this->getInit()) {
            $this->_validator = Mage::getSingleton('salesrule/validator');
            $key = $this->_validator->getWebsiteId() . '_' . $this->_validator->getCustomerGroupId() . '_' . $this->_validator->getCouponCode();
            if (!isset($this->_rules[$key])) {
                $this->_rules[$key] = Mage::getResourceModel('salesrule/rule_collection')
                        ->setValidationFilter($this->_validator->getWebsiteId(), $this->_validator->getCustomerGroupId(), $this->_validator->getCouponCode())
                        ->load();
            }
            $this->setInit(TRUE);
        }
        return $this;
    }

    /**
     * Get rules collection for current object state
     *
     * @return Mage_SalesRule_Model_Mysql4_Rule_Collection
     */
    protected function _getRules() {
        $key = $this->_validator->getWebsiteId() . '_' . $this->_validator->getCustomerGroupId() . '_' . $this->_validator->getCouponCode();
        return $this->_rules[$key];
    }

    /**
     * Check if rule can be applied for specific address/quote/customer
     *
     * @param   Mage_SalesRule_Model_Rule $rule
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  bool
     */
    protected function _canProcessRule($rule, $address) {

        return TRUE;
    }

    public function promoQuoteActionsForm($observer = NULL) {
        /** @param Varien_Data_Form $form */
        $form = $observer->getForm();
        if ($form) {
            Mage::getSingleton('devel/devel');
            $actions = $form->getElement('simple_action');
            $fieldset = $actions->getContainer();

            $options = $actions->getOptions();
            $options[Bxp_AdvancedRules_Helper_Data::BXP_QTY_ACTION] = Mage::helper('advancedrules')->__('Quantity based discount formula');
            $fieldset->removeField('simple_action');
            $fieldset->addField('simple_action', 'select', array(
                'label' => $actions->getLabel(),
                'name' => $actions->getName(),
                'options' => $options,
                    ), '^');
            $form->removeField('simple_action');
        }
    }

    /**
     * Process discounts for BXP actions
     * @param type $observer
     * @return type
     */
    public function actionValidationProcess($observer = NULL) {
        Mage::getSingleton('devel/devel');
        $rule = $observer->getRule();
        //krumo($rule->getSimpleAction());
        if ($rule->getSimpleAction() === Bxp_AdvancedRules_Helper_Data::BXP_QTY_ACTION) {
            //Mage::log('Rule Validation Inited:' . Bxp_AdvancedRules_Helper_Data::BXP_QTY_ACTION, Zend_Log::DEBUG, 'bxp_logging.log');
            $this->init();
            $address = $observer->getAddress();
            $quote = $observer->getQuote();
            $result = $observer->getResult();
            $this->initTotals($address->getAllNonNominalItems(), $address);
            /**
             * prevent applying whole cart discount for every shipping order, but only for first order
             */
            if ($quote->getIsMultiShipping()) {
                $usedForAddressId = $this->_validator->getCartFixedRuleUsedForAddress($rule->getId());
                if ($usedForAddressId && $usedForAddressId != $address->getId()) {
                    return $observer;
                } else {
                    $this->_validator->setCartFixedRuleUsedForAddress($rule->getId(), $address->getId());
                }
            }
            $cartRules = $address->getCartFixedRules();
            if (!isset($cartRules[$rule->getId()])) {
                $cartRules[$rule->getId()] = $rule->getDiscountAmount();
            }
            // if ($cartRules[$rule->getId()] > 0) {
            if (!$this->_rulesItemTotals[$rule->getId()]['processed']) {
                $abo = Mage::getModel('abo/calculator');
                $discountAmount = $abo->getQtyDiscountAmount($this->_rulesItemTotals[$rule->getId()]['total_qty']);
                $discountAmount = $quote->getStore()->convertPrice($discountAmount);

                $this->_rulesItemTotals[$rule->getId()]['items_count'] --;
                $cartRules[$rule->getId()] = $discountAmount;
                $address->setCartFixedRules($cartRules);
                $baseDiscountAmount = $result->getBaseDiscountAmount();
                $discountAmount += $result->getDiscountAmount();
                $baseDiscountAmount += $discountAmount;
                $result->setDiscountAmount($discountAmount);
                $result->setBaseDiscountAmount($baseDiscountAmount);
                $this->_rulesItemTotals[$rule->getId()]['processed'] = TRUE;
            }

            //}
//            $result = $observer->getResult();
//            $discountAmount = (float) round(($rule->getItemsQty() - 2 ) * 3);
//            $discountAmount += $result->getDiscountAmount();
//            $result->setDiscountAmount($discountAmount);
        }
        return $observer;
    }

    /**
     * Calculate quote totals for each rule and save results
     *
     * @param mixed $items
     * @param Mage_Sales_Model_Quote_Address $address
     * @return Mage_SalesRule_Model_Validator
     */
    public function initTotals($items, Mage_Sales_Model_Quote_Address $address) {
        if (!$items || $this->getTotalsInited() === TRUE) {
            return $this;
        }
        foreach ($this->_getRules() as $rule) {

            if (Bxp_AdvancedRules_Helper_Data::BXP_QTY_ACTION == $rule->getSimpleAction()) {
                $ruleTotalItemsPrice = 0;
                $ruleTotalBaseItemsPrice = 0;
                $validItemsCount = 0;
                $total_qty = 0;
                foreach ($items as $item) {
                    //Skipping child items to avoid double calculations
                    if ($item->getParentItemId()) {
                        continue;
                    }
                    if (!$rule->getActions()->validate($item)) {
                        continue;
                    }
                    $qty = $this->_getItemQty($item, $rule);

                    $ruleTotalItemsPrice += $this->_getItemPrice($item) * $qty;
                    $ruleTotalBaseItemsPrice += $this->_getItemBasePrice($item) * $qty;
                    $validItemsCount++;
                    $total_qty += $qty;
                }
                $this->_rulesItemTotals[$rule->getId()] = array(
                    'items_price' => $ruleTotalItemsPrice,
                    'base_items_price' => $ruleTotalBaseItemsPrice,
                    'items_count' => $validItemsCount,
                    'total_qty' => $total_qty,
                    'processed' => FALSE
                );
            }
        }
        $this->setTotalsInited(TRUE);
        return $this;
    }

    public function salesRuleSaveAfter($observer = NULL) {
        Mage::getSingleton('devel/devel');
    }

    /**
     * Return item price
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return float
     */
    protected function _getItemPrice($item) {
        $price = $item->getDiscountCalculationPrice();
        $calcPrice = $item->getCalculationPrice();
        return ($price !== null) ? $price : $calcPrice;
    }

    /**
     * Return item original price
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return float
     */
    protected function _getItemOriginalPrice($item) {
        return Mage::helper('tax')->getPrice($item, $item->getOriginalPrice(), true);
    }

    /**
     * Return item base price
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return float
     */
    protected function _getItemBasePrice($item) {
        $price = $item->getDiscountCalculationPrice();
        return ($price !== null) ? $item->getBaseDiscountCalculationPrice() : $item->getBaseCalculationPrice();
    }

    /**
     * Return item base original price
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @return float
     */
    protected function _getItemBaseOriginalPrice($item) {
        return Mage::helper('tax')->getPrice($item, $item->getBaseOriginalPrice(), true);
    }

    /**
     * Return discount item qty
     *
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     * @param Mage_SalesRule_Model_Rule $rule
     * @return int
     */
    protected function _getItemQty($item, $rule) {
        $qty = $item->getTotalQty();
        return $rule->getDiscountQty() ? min($qty, $rule->getDiscountQty()) : $qty;
    }

}
