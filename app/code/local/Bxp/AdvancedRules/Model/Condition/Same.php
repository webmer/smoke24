<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Same
 */
class Bxp_AdvancedRules_Model_Condition_Same extends Mage_Rule_Model_Condition_Abstract {

    public function loadAttributeOptions() {
        $attributes = array(
            'category_ids' => Mage::helper('advancedrules')->__('Category'),
            'amount' => Mage::helper('advancedrules')->__('Amount'),
        );

        $this->setAttributeOption($attributes);

        return $this;
    }

    public function getAttributeElement() {
        $element = parent::getAttributeElement();
        $element->setShowAsText(true);
        return $element;
    }

    public function getInputType() {

        switch ($this->getAttribute()) {
            case 'category_ids':
                return 'category';
        }
        return 'string';
    }

    public function getValueElementType() {
        return 'text';
    }

    public function validate(Varien_Object $object) {
        $test = $object;
        return TRUE;
    }

}
