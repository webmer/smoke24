<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Top
 */
class Bxp_Navigation_Block_Top extends Mage_Catalog_Block_Navigation {

    protected $_template = "bxp/navigation/top.phtml";
    protected $_pages = array();
    protected $_currentPage;
    protected $_columnHtml;

    /**
     * 
     */
    protected function _construct() {
        parent::_construct();
        $this->_currentPage = Mage::getSingleton('cms/page')->getIdentifier();
        $this->addData(array(
            'cache_lifetime' => 7200,
            'cache_tags' => array(Mage_Cms_Model_Page::CACHE_TAG),
        ));
    }

    /**
     *
     * @return type
     */
    public function getLinks() {
        $links = array();
        foreach ($this->getPages() as $id => $sub) {
            $sub_menu = is_array($sub) ? $sub : NULL;
            $link = $this->_buildLink($id, $sub_menu);
            if ($link) {
                $links[] = $link;
            }
        }

        return $links;
    }

    protected function _buildLink($id, $sub_menu = NULL, $title = NULL) {
        /** @var Mage_Cms_Model_Page $cms */
        $cms = Mage::getModel('cms/page')->setStoreId(Mage::app()->getStore()->getId())->load($id);
        if ($cms->getId()) {
            return array(
                'url' => Mage::getUrl(null, array('_direct' => $cms->getIdentifier())),
                'title' => $title ? $title : $cms->getTitle(),
                'id' => $cms->getId(),
                'classes' => $this->_isActive($cms->getIdentifier()) ? 'active' : '',
                'sub_menu' => is_null($sub_menu) ? FALSE : array($this->_buildLink($sub_menu['id'], NULL, $sub_menu['title']))
            );
        }
    }

    /**
     *
     * @param type $id
     * @return type
     */
    protected function _isActive($id) {
        return ($id === $this->_currentPage);
    }

    /**
     *
     * @param type $id
     */
    public function addPage($id, $sub = NULL, $sub_title = NULL) {
        if (is_null($sub)) {
            $this->_pages[$id] = $id;
        } else {
            $this->_pages[$id] = array('id' => $sub, 'title' => $sub_title);
        }
    }

    /**
     *
     * @param type $blockId
     */
    public function addBlock($blockId) {
        
    }

    /**
     *
     * @return type
     */
    public function getPages() {
        return $this->_pages;
    }

    public function renderSpecialCategory($id, $level = 0, $isLast = false, $isFirst = false, $isOutermost = false, $outermostItemClass = '', $childrenWrapClass = 'sub-wrapper', $noEventAttributes = false) {
        $category = Mage::getModel('catalog/category')->load($id);
        if (!$category->getIsActive()) {
            return '';
        }
        $html = array();

        // get all children
        $children = $category->getProductCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('status', 1)
                ->addAttributeToFilter('visibility', 4)
                ->setOrder('price', 'ASC');

        $childrenCount = $children->count();
        $hasChildren = ($children && $childrenCount);

        // select active children
        $activeChildren = array();
        foreach ($children as $child) {
            $activeChildren[] = $child;
        }
        $activeChildrenCount = count($activeChildren);
        $hasActiveChildren = ($activeChildrenCount > 0);

        // prepare list item html classes
        $classes = array();
        $classes[] = 'level' . $level;
        $classes[] = 'nav-' . $level;
//        if ($this->isCategoryActive($category)) {
//            $classes[] = 'active';
//        }
        $linkClass = '';
        if ($isOutermost && $outermostItemClass) {
            $classes[] = $outermostItemClass;
            $linkClass = ' class="' . $outermostItemClass . '"';
        }
        if ($isFirst) {
            $classes[] = 'first';
        }
        if ($isLast) {
            $classes[] = 'last';
        }
        if ($hasActiveChildren) {
            $classes[] = 'parent';
        }

        // prepare list item attributes
        $attributes = array();
        if (count($classes) > 0) {
            $attributes['class'] = implode(' ', $classes);
        }
//        if ($hasActiveChildren && !$noEventAttributes) {
//            $attributes['onmouseover'] = 'toggleMenu(this,1)';
//            $attributes['onmouseout'] = 'toggleMenu(this,0)';
//        }
        // assemble list item with attributes
        $htmlLi = '<li';
        foreach ($attributes as $attrName => $attrValue) {
            $htmlLi .= ' ' . $attrName . '="' . str_replace('"', '\"', $attrValue) . '"';
        }
        $htmlLi .= '>';
        $html[] = $htmlLi;

        $html[] = '<a href="' . $this->getCategoryUrl($category) . '"' . $linkClass . '>';
        $html[] = '<span>' . $this->escapeHtml($category->getName()) . '</span>';
        $html[] = '</a>';

        $columnItemsNum = array();
        if ($level == 0 && $activeChildrenCount) {
            $items_per_column = Mage::getStoreConfig('shoppersettings/navigation/column_items');
            $columns = ceil($activeChildrenCount / $items_per_column);
            $columnItemsNum = array_fill(0, $columns, $items_per_column);
            $this->_columnHtml = array();
        }

        // render children
        $htmlChildren = '';
        $j = 0; //child index
        $i = 0; //column index
        $itemsCount = $activeChildrenCount;
        if (isset($columnItemsNum[$i])) {
            $itemsCount = $columnItemsNum[$i];
        }
        foreach ($activeChildren as $child) {

            if ($level == 0) {
                $isLast = (($j + 1) == $itemsCount || $j == $activeChildrenCount - 1);
                if ($isLast) {
                    $i++;
                    if (isset($columnItemsNum[$i])) {
                        $itemsCount += $columnItemsNum[$i];
                    }
                }
            } else {
                $isLast = ($j == $activeChildrenCount - 1);
            }

            $childHtml = $this->_renderSpecialChildren(
                    $child, ($level + 1), $isLast, ($j == 0), false, $outermostItemClass, 'sub-wrapper', $noEventAttributes
            );
            if ($level == 0) {
                $this->_columnHtml[] = $childHtml;
            } else {
                $htmlChildren .= $childHtml;
            }
            $j++;
        }

        if ($level == 0 && $this->_columnHtml) {
            $i = 0;
            foreach ($columnItemsNum as $columnNum) {
                $chunk = array_slice($this->_columnHtml, $i, $columnNum);
                $i += $columnNum;
                $htmlChildren .= '<li ' . (count($this->_columnHtml) == $i ? 'class="last"' : '') . '><ol>';
                foreach ($chunk as $item) {
                    $htmlChildren .= $item;
                }
                $htmlChildren .= '</ol></li>';
            }
        }

        if (!empty($htmlChildren)) {
            if ($childrenWrapClass) {
                $html[] = '<div class="' . $childrenWrapClass . '">';
            }
            $html[] = '<ul class="level' . $level . '">';
            $html[] = $htmlChildren;
            $html[] = '</ul>';
            if ($childrenWrapClass) {
                $html[] = '</div>';
            }
        }

        $html[] = '</li>';

        $html = implode("\n", $html);
        return $html;
    }

    protected function _renderSpecialChildren($product, $level = 0, $isLast = false, $isFirst = false, $isOutermost = false, $outermostItemClass = '', $childrenWrapClass = '', $noEventAttributes = false) {
        $html = array();
        $html[] = '<li class="product-id-' . $product->getId() . '">';
        $html[] = '<a href="' . $this->getUrl(NULL, array('_direct' => $product->getUrlPath())) . '">' . $product->getName() . '</a>';
        $html[] = '</li>';
        return implode("\n", $html);
    }

}
