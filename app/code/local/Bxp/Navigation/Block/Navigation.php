<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Navigation
 */
class Bxp_Navigation_Block_Navigation extends Queldorei_ShopperSettings_Block_Navigation {

    /**
     * Override to deny category 16;
     * @param type $category
     * @param type $level
     * @param type $isLast
     * @param type $isFirst
     * @param type $isOutermost
     * @param type $outermostItemClass
     * @param type $childrenWrapClass
     * @param type $noEventAttributes
     * @return type
     */
    protected function _renderCategoryMenuItemHtml($category, $level = 0, $isLast = false, $isFirst = false, $isOutermost = false, $outermostItemClass = '', $childrenWrapClass = '', $noEventAttributes = false) {
        if ($category->getId() == 16) {
            return;
        }
        return parent::_renderCategoryMenuItemHtml($category, $level, $isLast, $isFirst, $isOutermost, $outermostItemClass, $childrenWrapClass, $noEventAttributes);
    }

}
