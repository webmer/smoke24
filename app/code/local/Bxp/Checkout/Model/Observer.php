<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Observer
 */
class Bxp_Checkout_Model_Observer extends Varien_Event_Observer {

    public function checkPayment($observer = NULL) {
        $e = $observer->getEvent();
        $method = $e->getMethodInstance();
        $result = $e->getResult();
        $quote = $e->getQuote();
        if ($quote) {
            $items = $quote->getAllVisibleItems();
            $denied = FALSE;
            foreach ($items as $item) {
                if ($item->getProductType() == 'aw_giftcard') {
                    $denied = TRUE;
                }
            }
            if ($method->getCode() == 'datatranscw_powerpayopeninvoice' && $denied === TRUE) {
                $result->isAvailable = FALSE;
            }
        }
    }

    /**
     *
     * @param type $observer
     * @return type
     */
    public function checkGiftcard($observer = NULL) {
        $result = $observer->getEvent()->getResult();
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        $giftCard = $observer->getEvent()->getGiftcard();
        $items = $quote->getAllVisibleItems();
        foreach ($items as $item) {
            if ($item->getProductType() == 'aw_giftcard') {
                $result->setMessage(Mage::helper('bxp_checkout')->__('Giftcards can\'t be used to pay for another giftcard.'));
                // We need to remove the giftcard from quote
                try {
                    Mage::helper('aw_giftcard/totals')->removeCardFromQuote($giftCard->getCode(), $quote);
                } catch (Exception $exc) {
                    
                }
                return;
            }
        }
    }

    /**
     *
     * @param type $observer
     */
    public function addToCart($observer = NULL) {
        // We need to check if the product added is giftcard and remove any
        // giftcards from the quote
        $product = $observer->getEvent()->getProduct();
        if ($product->getTypeId() == 'aw_giftcard') {
            $giftHelper = Mage::helper('aw_giftcard/totals');
            $gifts = $giftHelper->getQuoteGiftCards();
            foreach ($gifts as $gift) {
                $gift->delete();
            }
        }
    }

}
