<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Form
 */
class Bxp_Checkout_Block_Onepage_Payment_Form extends Customweb_DatatransCw_Block_Form {

    /**
     * 
     * @return string
     */
    public function getMethodLabelAfterHtml() {

        if (in_array($this->getMethodCode(), $this->_getMoreInfoPayments())) {
            return Mage::helper('bxp_checkout')->__('Verschiedene Banken verlangen eine zusätzliche Authentifizierung Ihrer Kreditkarte. Es kann sein, dass nach Abschicken Ihrer Daten sich ein SecureCode Eingabfeld von der Bank öffnet. Dort geben Sie Ihren persönlichen SecureCode ein und identifizieren sich dadurch als rechtmässiger Karteninhaber. Der SecureCode ist ein Sicherheitswert, den nur Sie und Ihre Bank kennen.');
        }
        return '';
    }

    /**
     *
     * @return type
     */
    protected function _getMoreInfoPayments() {
        return array('datatranscw_mastercard', 'datatranscw_visa');
    }

}
