<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Onepage
 */
class Bxp_Checkout_Block_Onepage extends Mage_Checkout_Block_Onepage {

    /**
     * Get 'one step checkout' step data
     *
     * @return array
     */
    public function getSteps() {
        Mage::getSingleton('devel/devel');
        $steps = array();
        $stepCodes = $this->_getStepCodes();

        if ($this->isCustomerLoggedIn()) {
            $stepCodes = array_diff($stepCodes, array('login'));
        }

        foreach ($stepCodes as $step) {
            $steps[$step] = $this->getCheckout()->getStepData($step);
        }

        return $steps;
    }

    protected function _getStepCodes() {
        return array('login', 'billing', 'shipping', 'payment', 'review');
    }

}
