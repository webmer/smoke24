<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Calculator
 */
class Bxp_Abo_Model_Calculator extends Mage_Core_Model_Abstract {

    protected $_shipping_rules = [];

    public function calculateDiscount($values, $product, $validate = FALSE) {
        Mage::getSingleton('devel/devel');
        if (!isset($values['quantities']) && empty($values['quantities'])) {
            return 100;
        }
        $helper = Mage::helper('core');
        $result = array();
        foreach ($values['quantities'] as $step => $qty) {
            // Check to see if product is allowed for discount.
            if ($product->getNoQtyDiscount() == 0) {
                $discount = $this->getQtyDiscountAmount($qty);
            } else {
                $discount = 0;
            }
            $carton_cost = $product->getPrice();
            $cost_absolute = $qty * $carton_cost - $discount;
            $carton_discount = $cost_absolute / $qty;
			
            //$pack_price = round(($cost - $discount) / ($qty * 10), 2);
            $total = $cost_absolute;
            $result[$step] = array(
                // 'cost_pack' => $helper->formatPrice($pack_price, TRUE),
                'cost_carton' => $helper->formatPrice($carton_discount, TRUE),
                'cost_total' => $helper->formatPrice($total, TRUE),
                'total_discount' => $helper->formatPrice($discount, TRUE),
                'qty' => $qty,
                'free_shipping' => $this->_isFreeShipping($total),
                'cart_url' => $this->_getAddToCartUrl($product, $qty, $step),
            );
        }

        return $result;
    }

    /**
     * Subscription getter wrapper
     * @return type
     */
    public function getSubscriptions() {
        return Mage::helper('customweb_subscription/cart')->getCartSubscriptionPlans();
    }

    /**
     * Get discount based on QTY
     * @return array Discounts based on qty. $qty => $discount
     * @todo Cache!!
     */
    public function getQtyDiscount() {
        return Mage::helper('abo')->getQtyDiscounts();
    }

    /**
     * Get discount based on quantity.
     * @param int $qty
     * @return float Discount amount
     */
    public function getQtyDiscountAmount($qty) {
        Mage::getSingleton('devel/devel');
        if (is_numeric($qty)) {
            if ($qty > 0) {
                $discounts = $this->getQtyDiscount();
                if ($qty > 0 && $qty <= 20) {
                    if (isset($discounts[$qty])) {
                        //krumo($discounts[$qty]);
                        return (float) $discounts[$qty];
                    } else {
                        Mage::throwException('Not a valid qty for this discount');
                    }
                } else {
                    //We have qty bigger than 10 we need to apply the largest discount
                    asort($discounts, SORT_ASC);
                    $clone = $discounts;
                    //krumo(array_pop($clone));
                    $largestDiscount = array_pop($discounts);
                    $variable = Mage::helper('abo')->getDiscountVariable();
                    $step = $qty - 20;
                    return (float) $largestDiscount + $step * $variable;
                }
            }
        } else {
            Mage::throwException('Requested discount have no qty. Qty expected');
        }
    }
    protected function _getShippingRules()
    {
        $rulesCollection = Mage::getModel('salesrule/rule')->getCollection();
        if(empty($this->_shipping_rules)){
            foreach ($rulesCollection as $rule) {
                if (($rule->getSimpleFreeShipping() == 1 || $rule->getSimpleFreeShipping() == 2) && $rule->getIsActive() && $rule->getApplyToShipping() ) {
                    // $rules[$rule->getId()]['rule'] = $rule;
                    $cond = unserialize($rule->getConditionsSerialized());
                    if (isset($cond['conditions'][0]['value'])) {
                        $this->_shipping_rules[$rule->getId()] = array(
                            'value' => $cond['conditions'][0]['value'],
                            //'categories' => $cond['conditions'][0]['conditions'][0]['value'],
                            'valid' => TRUE
                        );

                    }
                }
            }
        }
        return $this->_shipping_rules;
    }

    /**
     * Is free shipping allowed for this plan.
     * @param type $total
     * @return type
     */
    protected function _isFreeShipping($total) {

        foreach ($this->_getShippingRules() as $key => $_rule) {
            if ($_rule['valid'] === TRUE && $total <= $_rule['value']) {
                return Mage::helper('core')->formatPrice(Mage::getStoreConfig('carriers/flatrate/price'));
            }
        }

        return Mage::helper('core')->formatPrice(0);
    }

    /**
     * Get add to cart url for the calculator
     * @param type $product
     * @param type $qty
     * @param type $subscription
     * @return type
     */
    protected function _getAddToCartUrl($product, $qty, $subscription) {
        $plans = $this->getSubscriptions();
        $sub = 'single';
        foreach ($plans as $plan) {

            if ($plan->getPeriodFrequency() == $subscription) {
                // +1 is nexessary because on the ajax controller we substract
                // one so that the dropdowns could work.
                $sub = $plan->getIndex() + 1;
            }
        }
        //Clear the requies_uri so that we fit in string.
        $_SERVER['REQUEST_URI'] = Mage::getBaseUrl();

        return Mage::helper('checkout/cart')->getAddUrl($product, array('qty' => $qty, 'sub' => $sub));
    }

    /**
     *
     * @return type
     */
    protected function _getRequiredValues() {
        return array('postalCharges', 'logistic', 'psp', 'packaging', 'marketing', 'ccFee', 'postalExtra', 'size', 'granularity', 'totalFix');
    }

    public function round($price){
        return round($price *2,1)/2;
    }

}
