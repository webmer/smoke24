<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Table
 * @method Bxp_Abo_Block_Calculator_Table setSize(int $value) Sets the size of the grid
 * @method int getSize() Size getter.
 * @method Bxp_Abo_Block_Calculator_Table setStep(int $step) Sets values step
 * @method int getStep() Sets values step
 */
class Bxp_Abo_Block_Calculator_Table extends Mage_Core_Block_Template {

    protected $_template = 'bxp/abo/calculator/table.phtml';
    protected $_months;
    protected $_granularity = 'w';

    const GRANULARITY_MONTH = 'm';
    const GRANULARITY_WEEK = 'w';

    public function getMonths() {
        if ($this->_months) {
            return $this->_months;
        } else {

        }
    }

    public function setGranularity($granularity) {
        if ($granularity == self::GRANULARITY_MONTH) {
            $this->_granularity = self::GRANULARITY_MONTH;
        } elseif ($granularity == self::GRANULARITY_WEEK) {
            $this->_granularity = self::GRANULARITY_WEEK;
        } else {
            Mage::throwException('Not a valid granularity');
        }
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getGranularity() {
        return $this->_granularity;
    }

    /**
     *
     * @return array
     */
    public function getTimeArray() {
        $range = range($this->getStep(), $this->getSize(), $this->getStep());
        foreach ($range as $item) {
            $time[$item] = $this->getTimeName($item);
        }
        return $time;
    }

    /**
     *
     * @param int $int
     * @return string
     */
    public function getTimeName($int) {
        if (!is_int($int)) {
            Mage::throwException('Time variable not int');
        }
        switch ($this->getGranularity()) {
            case self::GRANULARITY_MONTH:
                if ($int == 1) {
                    return $this->helper('abo')->__('%s Month', $int);
                } else {
                    return $this->helper('abo')->__('%s Months', $int);
                }
                break;
            case self::GRANULARITY_WEEK:
                if ($int == 1) {
                    return $this->helper('abo')->__('%s Week',$int);
                } else {
                    return $this->helper('abo')->__('%s Weeks', $int);
                }
                break;
            default:
                break;
        }
    }

}
