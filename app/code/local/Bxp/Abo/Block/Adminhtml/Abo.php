<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Abo
 */
class Bxp_Abo_Block_Adminhtml_Abo extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        parent::__construct();
        $this->_blockGroup = 'abo';
        $this->_controller = 'adminhtml_abo';
        $this->_headerText = Mage::helper('abo')->__('Discount configuration (ONLY FOR CIGARETTES)');
        $this->_addButton('save_and_continue', array(
            'label' => Mage::helper('abo')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save'
                ), 100);
        $this->_addButton('apply', array(
            'label' => Mage::helper('abo')->__('Apply'),
            'onclick'   => 'setLocation(\'' . $this->getUrl('plgproduct_admin/adminhtml_index') .'\')',
            'class' => 'save'
        ), 101);
        $this->_formScripts[] = " function saveAndContinueEdit(){
            editForm.submit($('edit_form').action+'back/edit/');
        }
    ";
    }

}
