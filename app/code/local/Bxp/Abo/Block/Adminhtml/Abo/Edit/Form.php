<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Form
 */
class Bxp_Abo_Block_Adminhtml_Abo_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

    /**
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm() {
        $form = new Varien_Data_Form(
                array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save'),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));
        $form->setUseContainer(TRUE);
        $this->setForm($form);
        $data = Mage::getSingleton('adminhtml/session')->getFormData();

        $fieldset = $form->addFieldset('discounts', array(
            'label' => Mage::helper('abo')->__('Qty Discounts'),
        ));
        $range = range(1, 20);

        foreach ($range as $qty) {
            $fieldset->addField('discount_' . $qty, 'text', array(
                'name' => "discount[$qty]",
                'class' => 'validate-number',
                'label' => Mage::helper('abo')->__('%s Qty', $qty),
                'note' => Mage::helper('abo')->__('Discount amount'),
                'after_element_html' => $qty == 20 ? '<strong>' .
                        Mage::helper('abo')->__('For quantities larger than 20 the discount variable formula will be used')
                        . '</strong>' : ''
            ));
        }
        $fieldset->addField('discount_variable', 'text', array(
            'name' => "discount_variable",
            'class' => 'validate-number',
            'label' => Mage::helper('abo')->__('Discount Variable'),
            'note' => Mage::helper('abo')->__('This Variable will be used for discount arithmetic progression'),
        ));
        $form->setValues($data);
        return parent::_prepareForm();
    }

}
