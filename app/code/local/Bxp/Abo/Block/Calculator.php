<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Calculator
 */
class Bxp_Abo_Block_Calculator extends Mage_Core_Block_Template {

    /**
     *
     * @var Varien_Data_Form 
     */
    protected $_consumptionForm;

    /**
     *
     * @var string Html
     */
    protected $_results;

    protected $_granularity = 'w';

    protected $_step = 2;

    protected $_size = 26;

    /**
     *
     * @var string Block Tempalte
     */
    protected $_template = 'bxp/abo/calculator.phtml';

    protected function _construct() {
        
        //$type, $name, $params=null, $if=null, $cond=null
        parent::_construct();
    }

    protected function _prepareLayout() {
        $this->getLayout()->getBlock('head')->addItem('skin_js', 'js/select2/select2.min.js');
        parent::_prepareLayout();
    }

    protected function _beforeToHtml() {
        
        $this->_prepareConsumptionForm();
        $this->_prepareResultTable();
        parent::_beforeToHtml();
    }

    /**
     *
     * @return string
     */
    public function renderConsumptionForm() {
        return $this->getConsumptionFormObject()->toHtml();
    }

    /**
     * Prepare form
     */
    protected function _prepareConsumptionForm() {
        $form = new Varien_Data_Form(array(
            'id' => 'abo-calculator-form',
        ));
        $form->setUseContainer(TRUE);
        $fieldset = $form->addFieldset('calculator', array('class' => 'calculator-fieldset'));
        $fieldset->addField('cd', 'text', array(
            'name' => 'cd',
            'id' => 'field-cd',
            'label' => $this->helper('abo')->__('Consumption / day'),
            'class' => 'required-entry validate-greater-than-zero',
            'after_element_html' => '<p class="note"><small>' . $this->helper('abo')->__('Number of cigarettes') . '</small></p>'
        ));
        $fieldset->addField('calc', 'submit', array(
            'name' => 'calc',
            'value' => $this->helper('abo')->__('Calcuate')
        ));
        //$test = new Mage_Adminhtml_Block_Widget_Form;

        $this->_consumptionForm = $form;
        return $this;
    }

    /**
     *
     * @return type
     */
    public function getConsumptionFormObject() {
        return $this->_consumptionForm;
    }

    protected function getProductForm(){
        $form = new Varien_Data_Form(array(
            'id' => 'product-select-form',
        ));
        $form->setUseContainer(TRUE);
        $fieldset = $form->addFieldset('product-select-fieldset', array('class' => 'product-select-fieldset'));
        $fieldset->addField('product', 'text', array(
            'name' => 'product',
            'id' => 'product-select'
        ));
    }

    public function getResultTable() {
        return $this->_results->toHtml();
    }

    /**
     * 
     */
    protected function _prepareResultTable() {
        $this->_results = $this->getLayout()
                ->createBlock('abo/calculator_table')
                ->setName('abo.table')
                ->setSize($this->_size)
                ->setStep($this->_step)
                ->setGranularity($this->_granularity);
        return $this;
    }

    public function getCalculatorJson() {
        $data = array(
//            'postalCharges' => 7,
//            'logistic' => 4,
//            'psp' => 0.38,
//            'packaging' => 1,
//            'marketing' => 1,
//            'ccFee' => 1.72,
//            'postalExtra' => 12,
            'size' => $this->_size,
            'granularity' => $this->_granularity,
            'step' => $this->_step
        );
        //$data['priceNoVat'] = round($data['price'] / 108 * 100, 2);
        //$data['margin_1'] = $data['priceNoVat'] - $data['net'];
//        $data['totalFix'] = array_sum(array(
//            $data['postalCharges'],
//            $data['logistic'],
//            $data['psp'],
//            $data['packaging'],
//            $data['marketing'],
//            $data['ccFee'],
//        ));
        return Mage::helper('core')->jsonEncode($data);
    }

}
