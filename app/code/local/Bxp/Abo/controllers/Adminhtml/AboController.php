<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IndexController
 */
class Bxp_Abo_Adminhtml_AboController extends Mage_Adminhtml_Controller_Action {

    /**
     *
     */
    protected function _construct() {
        parent::_construct();
        Mage::getSingleton('devel/devel');
    }

    /**
     *
     * @return boolean
     */
    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('promo/abo');
    }

    /**
     * Prepare configuration of the discounts.
     */
    protected function _prepareData() {
        $discounts = Mage::helper('abo')->getQtyDiscounts();
        $form_data = array();
        if (is_array($discounts)) {
            foreach ($discounts as $qty => $discount) {
                $form_data['discount_' . $qty] = $discount;
            }
        }
        $form_data['discount_variable'] = Mage::helper('abo')->getDiscountVariable();
        Mage::getSingleton('adminhtml/session')->setFormData($form_data);
    }

    /**
     *
     */
    public function indexAction() {
        $this->loadLayout();
        $this->_setActiveMenu('promo/abo');
        $this->_addBreadcrumb(Mage::helper('abo')->__('BXP Qty Discounts'), Mage::helper('abo')->__('BXP Qty Discounts'));
        $this->_prepareData();
        $this->_addContent($this->getLayout()->createBlock('abo/adminhtml_abo'));
        $this->renderLayout();
    }

    /**
     * 
     */
    public function saveAction() {
        $post = $this->getRequest()->getPost();
        if (isset($post['discount'])) {
            $discounts = array();
            foreach ($post['discount'] as $qty => $discount) {
                if ($discount !== '' && !is_numeric($discount)) {
                    $discounts[$qty] = 0.00;
                } else {
                    $discounts[$qty] = (float) $discount;
                }
            }
            try {
                Mage::getModel('core/config')->saveConfig('bxp/abo/discounts', serialize($discounts));
                Mage::getModel('core/config')->saveConfig('bxp/abo/discount_variable', $post['discount_variable']);
            } catch (Exception $exc) {
                Mage::getSingleton('adminhtml/session')->addError($exc->getMessage());
                Mage::logException($exc);
            }
        }
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('abo')->__('Discounts successfuly saved'));
        if ($this->getRequest()->getParam('back')) {
            $this->_redirect('*/*/index');
        } else {
            $this->_redirectReferer();
        }
    }

}
