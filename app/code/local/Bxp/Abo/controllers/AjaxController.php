<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AjaxController
 */
class Bxp_Abo_AjaxController extends Mage_Core_Controller_Front_Action {

    public function findAction($args = NULL) {
        $this->loadLayout();
        /* @var  $calc Bxp_Abo_Model_Calculator */
        $calc = Mage::getSingleton('abo/calculator');
        $result = array('products' => array());
        $params = $this->getRequest()->getParams();
        $limit = isset($params['limit']) ? $params['limit'] : NULL;
        if (!is_numeric($limit)) {
            Mage::throwException('Wrong limit provided');
        }
        if (isset($params['term'])) {
            $term = htmlspecialchars_decode(Mage::helper('core')->escapeHtml($params['term']));
        }
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
        $category = Mage::getModel('catalog/category')->load(50);
        $collection = $collection->addStoreFilter()
                ->addCategoryFilter($category)
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                ->addAttributeToSelect(array('net', 'engros', 'no_qty_discount'))
                ->addUrlRewrite();

        if (isset($term)) {
            $collection->addFieldToFilter('name', array('like' => '%' . $term . '%'));
        }
        if (isset($params['id']) && is_numeric($params['id'])) {
            $collection->addFieldToFilter('entity_id', $params['id']);
        }
        $collection->setPageSize($limit)
                ->setCurPage(1);
        if ($collection->getSize()) {
            foreach ($collection as $item) {
                $result['products'][] = array(
                    'html' => $this->getLayout()->createBlock('abo/product')->setProduct($item)->toHtml(),
                    'id' => $item->getId(),
                    'name' => $item->getName(),
                    'net' => $item->getNet(),
                    'engros' => $item->getEngros(),
                    'price' => $item->getPrice(),
                    'data' => $calc->calculateDiscount($params['config'], $item, FALSE)
                );
            }
        }
        $this->_sendJson($result);
    }

    private function _sendJson($body) {
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($body));
    }

}
