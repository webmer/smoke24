<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Data
 */
class Bxp_Abo_Helper_Data extends Mage_Core_Helper_Abstract {


    public function getQtyDiscounts(){
        return unserialize(Mage::getStoreConfig('bxp/abo/discounts'));
    }
    public function getDiscountVariable(){
        return Mage::getStoreConfig('bxp/abo/discount_variable');
    }

}
