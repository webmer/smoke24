<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Discount
 */
class Bxp_Product_Model_Discount extends Mage_Catalog_Model_Product {

    protected $_discountData;
    protected $_product;

    /**
     *
     * @param type $id
     * @param type $qty
     * @return type
     */
    public function discount($id, $qty) {
        $this->_product = $this->load($id);
        /**
         * For quantities of 1 we use a trick to make the product look discounted
         * on the fronend so we do not need to show this discount here.
         */
        if ($qty == 1) {
            $this->_discountData = array(
                'price' => $this->_product->getFinalPrice(),
                'regular_price' => $this->_product->getPrice(),
                'discount' => 0,
                'discount_price' => 0
            );
        } else {
            $regularPrice = $this->_product->getPrice() * $qty;
            $finalPrice = $this->_product->getFinalPrice();
            $price = $finalPrice * $qty;
            $this->_discountData = array(
                'price' => $price,
                'regular_price' => $regularPrice,
                'discount' => 0,
                'discount_price' => $price,
            );
        
			//if (Mage::helper('customer')->isLoggedIn()) {
			$this->_applyQtyDiscounts($qty);
			$this->_applyPriceDiscounts();
			//}
			
			/**
			 * 06.09.2015
			 * 
			 * Get real price within discount.
			 * JS file gets the whole html element, so we've to calculate the price here.
			 * !This is not a good solution, just quick and dirty... sorry
			 */
			 //$this->_discountData['price'] = $this->_discountData['regular_price'] - $this->_discountData['discount'];

			 //$this->_discountData['special_price'] = $this->_discountData['regular_price'] - $this->_discountData['discount'];
        }
			
        return $this;
		
    }

    /**
     * Get discount data
     *
     * For correct old-price format
     *
     * @param string $attribute
     * @param bool $formated
     * @return array|float
     */
    public function getDiscountData($attribute = NULL, $formated = FALSE) {
        if (!is_null($attribute)) {
            if (array_key_exists($attribute, $this->_discountData)) {
                return $formated ? $this->_formatPrice($this->_discountData[$attribute]) : $this->_discountData[$attribute];
            }
        }
        if ($formated) {
            foreach ($this->_discountData as $key => &$data) {
                //Check to see if discount is 0 and return not a formated price
                if ($key == 'discount' && $data === 0) {
                    continue;
                }
                $data = $this->_formatPrice($data);
            }
        }
        return $this->_discountData;
    }

    /**
     *
     * @return \Bxp_Product_Model_Discount
     */
    public function addDiscount($amount) {
        if ($amount > 0) {
            $this->_discountData['discount'] += $amount;
            $this->_discountData['discount_price'] = $this->_discountData['price'] - $this->_discountData['discount'];
        }
        return $this;
    }

    /**
     * 
     * @return \Bxp_Product_Model_Discount
     */
    protected function _applyPriceDiscounts() {
        $categoriesDiscount = $this->_getCategoryPriceDiscounts();
        $product = $this->getProduct();
        $categories = $product->getCategoryIds();
        foreach ($categories as $category) {
            foreach ($categoriesDiscount as $condition => $rule) {
                if (in_array($category, $rule)) {
                    if ($this->getDiscountData('regular_price') > $condition) {
                        $discount = (5 / 100) * $this->getDiscountData('regular_price');
                        $this->addDiscount(Mage::app()->getStore()->roundPrice($discount));
                    }
                }
            }
        }
        return $this;
    }

    /**
     *
     * @return \Bxp_Product_Model_Discount
     */
    protected function _applyQtyDiscounts($qty) {

        $product = $this->getProduct();
        if ($product instanceof Mage_Catalog_Model_Product) {
            $categories = $product->getCategoryIds();
            if (in_array(60, $categories)) {
                $discount = Mage::getModel('abo/calculator')->getQtyDiscountAmount($qty);
                $this->addDiscount($discount);
            }
        } else {
            Mage::throwException('Product is missing');
        }

        return $this;
    }

    /**
     * Get Price => Category discounts. There are also rules for this.
     * @return array
     */
    protected function _getCategoryPriceDiscounts() {
        return array(
            50 => array(9, 13),
            80 => array(5, 6),
            150 => array(10),
            300 => array(12)
        );
    }

    /**
     * Format Price Wrapper
     * @param float $price
     * @return string
     */
    protected function _formatPrice($price) {
        return Mage::helper('core')->formatPrice($price);
    }

    /**
     *
     */
    public function getProduct() {
        return $this->_product;
    }

}
