<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Discount
 */
class Bxp_Product_Model_Discount extends Mage_Catalog_Model_Product {

    protected  $_cat = 50;
    protected $_discountData;
    /**
     * @var Mage_Catalog_Model_Product
     */
    protected $_product;

    /**
     * test
     * @param mixed $product
     * @param int $qty
     * @return type
     */ 
    public function discount($product, $qty) {
        if($product instanceof Mage_Catalog_Model_Product){
            $this->_product = $product;

        }else{
            $this->_product = $this->load($product);
        }
        /**
         * For quantities of 1 we use a trick to make the product look discounted
         * on the fronend so we do not need to show this discount here.
         */
//        if ($qty == 1 && false) {
//            var_dump(['<pre>',$this->_product->getData(),'</pre>']);
            $this->_discountData = array(
//                'price' => $price,
                'price' => Mage::getModel('abo/calculator')->round($this->_product->getPriceModel()->getFinalPrice($qty, $this->_product))* $qty,
                'regular_price' => $this->_product->getPrice() * $qty,
                'discount' => 0,
                'discount_price' => 0,
            );
//        } else {
//            $regularPrice = Mage::getModel('abo/calculator')->round($this->_product->getPrice());
//            $dis = Mage::getModel('catalogrule/rule')->calcProductPriceRule($this->_product,$this->_product->getPrice());
//            $finalPrice = Mage::getModel('abo/calculator')->round(($dis ?: ($this->_product->getOriginalSpecialPrice() ?:  ( $this->_product->getSpecialPriceCustom() == 'true' ? $this->_product->getSpecialPrice() : $this->_product->getPrice()))));//ceil($this->_product->getFinalPrice() * 20)/20;
//            $finalPrice = Mage::getModel('abo/calculator')->round(
//                ($this->_product->getSpecialPriceCustom() && $this->_product->getOriginalSpecialPrice() ? $this->_product->getOriginalSpecialPrice() : ($dis ?: $this->_product->getFinalPrice()))
//            );
//            $this->_discountData = array(
//                'p' => $this->_product->getPrice(),
//                'fp' => $this->_product->getFinalPrice(),
//                'osp' => $this->_product->getOriginalSpecialPrice(),
//                'spc' => $this->_product->getSpecialPriceCustom(),
//                'dd' => ($this->_product->getSpecialPriceCustom() ? $this->_product->getOriginalSpecialPrice() : ($dis ?: $this->_product->getFinalPrice())),
//                'tt' => ($this->_product->getSpecialPriceCustom() && $this->_product->getOriginalSpecialPrice() ? $this->_product->getOriginalSpecialPrice() : ($dis ?: $this->_product->getFinalPrice())),
//                'discount_price' =>  $finalPrice * $qty,
//                'regular_price' => $regularPrice * $qty,
//                'discount' => 0,
//                'price' => 0,
//            );
//
//			//if (Mage::helper('customer')->isLoggedIn()) {
//			$this->_applyQtyDiscounts($qty);
////            var_dump($this->_discountData);
////			$this->_applyPriceDiscounts();
//			//}
//
//			/**
//			 * 06.09.2015
//			 *
//             *
//			 * Get real price within discount.
//			 * JS file gets the whole html element, so we've to calculate the price here.
//			 * !This is not a good solution, just quick and dirty... sorry :(
//             *
//             * We set a custom attribute to check for the custom special price.
//             * The magento price attributes contain wrong values, don't know the reason.
//             * Maybe some methods from this module override the core methods for calculating.
//			 */
//
//			 $product = $this->getProduct();
//			 $categories = $product->getCategoryIds();
//			 if (in_array($this->_cat, $categories)) {
//                if($this->_product->getSpecialPriceCustom() != 'true') {
//                    $this->_discountData['discount_price'] = $this->_discountData['regular_price'] - $this->_discountData['discount'];
//	            }
//			 }
//        }
        return $this;
    }

//    public function discountBeforeLoad($product, $qty) {
//        if($product instanceof Mage_Catalog_Model_Product){
//            $this->_product = $product;
//
//        }else{
//            $this->_product = $this->load($product);
//        }
//        $product = $this->getProduct();
//        $categories = $product->getCategoryIds();
//
//        $regularPrice = Mage::getModel('abo/calculator')->round($this->_product->getPrice());
//        $dis = Mage::getModel('catalogrule/rule')->calcProductPriceRule($this->_product,$this->_product->getPrice());
////        $finalPrice = Mage::getModel('abo/calculator')->round(($this->_product->getOriginalSpecialPrice() ?: ($dis ?: $this->_product->getPrice())));//ceil($this->_product->getFinalPrice() * 20)/20;
//            $finalPrice = $this->_product->getFinalPrice();
//            if(in_array($this->_cat, $categories)){
//                $finalPrice = $this->_product->getOriginalSpecialPrice() ?: ($dis ?: $this->_product->getPrice());
//            }
//            $finalPrice = Mage::getModel('abo/calculator')->round($finalPrice);
//            $this->_discountData = array(
//                'p' => $this->_product->getPrice(),
//                'fp' => $this->_product->getFinalPrice(),
//                'osp' => $this->_product->getOriginalSpecialPrice(),
//                'spc' => $this->_product->getSpecialPriceCustom() == 'true' ? 1 : 0,
//                'dd' => $finalPrice,
//                'current' => ($dis ?: ($this->_product->getOriginalSpecialPrice() ?: ( $this->_product->getSpecialPrice() ?: $this->_product->getPrice()))),
//                'dis' => $dis,
//                'discount_price' =>  $finalPrice * $qty,
//                'regular_price' => $regularPrice * $qty,
//                'discount' => 0,
//                'price' => 0,
//            );
//
//        $this->_applyQtyDiscounts($qty);
//
//
//        if (in_array($this->_cat, $categories)) {
//            if($this->_product->getSpecialPriceCustom() != 'true') {
//                $this->_discountData['discount_price'] = $this->_discountData['regular_price'] - $this->_discountData['discount'];
//            }
//        }
//        return $this;
//    }

    /**
     * Get discount data
     *
     * For correct old-price format
     *
     * @param string $attribute
     * @param bool $formated
     * @return array|float
     */
    public function getDiscountData($attribute = NULL, $formated = FALSE) {
        if (!is_null($attribute)) {
            if (array_key_exists($attribute, $this->_discountData)) {
                return $formated ? $this->_formatPrice($this->_discountData[$attribute]) : $this->_discountData[$attribute];
            }
        }
        if ($formated) {
            foreach ($this->_discountData as $key => &$data) {
                //Check to see if discount is 0 and return not a formated price
                if ($key == 'discount' && $data === 0) {
                    continue;
                }
                $data = $this->_formatPrice($data);
            }
        }
        return $this->_discountData;
    }

    /**
     *
     * @return \Bxp_Product_Model_Discount
     */
//    public function addDiscount($amount) {
//            $this->_discountData['discount'] += $amount;
//            $this->_discountData['price'] = $this->_discountData['discount_price'] - $this->_discountData['discount'];
////            $this->_discountData['price'] = $this->_discountData['regular_price'] - $this->_discountData['discount'];
//        return $this;
//    }

    /**
     *
     * @return \Bxp_Product_Model_Discount
     */
//    protected function _applyPriceDiscounts() {
//        $categoriesDiscount = $this->_getCategoryPriceDiscounts();
//        $product = $this->getProduct();
//        $categories = $product->getCategoryIds();
//        foreach ($categories as $category) {
//            foreach ($categoriesDiscount as $condition => $rule) {
//                if (in_array($category, $rule)) {
//                    if ($this->getDiscountData('regular_price') > $condition) {
//                        $discount = (5 / 100) * $this->getDiscountData('regular_price');
//                        $this->addDiscount(Mage::app()->getStore()->roundPrice($discount));
//                    }
//                }
//            }
//        }
//        return $this;
//    }

    /**
     *
     * @return \Bxp_Product_Model_Discount
     */
//    protected function _applyQtyDiscounts($qty) {
//
//        $product = $this->getProduct();
//        $discount = 0;
//        if ($product instanceof Mage_Catalog_Model_Product) {
//            $categories = $product->getCategoryIds();
//            if (in_array($this->_cat, $categories)) {
//                $discount = Mage::getModel('abo/calculator')->getQtyDiscountAmount($qty,$product);
//            }
//            $this->addDiscount($discount);
//        } else {
//            Mage::throwException('Product is missing');
//        }
//
//        return $this;
//    }

    /**
     * Get Price => Category discounts. There are also rules for this.
     * @return array
     */
//    protected function _getCategoryPriceDiscounts() {
//        return array(
//            50 => array(9, 13),
//            80 => array(5, 6),
//            150 => array(10),
//            300 => array(12)
//        );
//    }

    /**
     * Format Price Wrapper
     * @param float $price
     * @return string
     */
    protected function _formatPrice($price) {
        return Mage::helper('core')->formatPrice($price);
    }

    /**
     * @return Mage_Catalog_Model_Product
     */
//    public function getProduct() {
//        return $this->_product;
//    }

}
