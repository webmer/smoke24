<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Observer
 */
class Bxp_Product_Model_Observer extends Varien_Event_Observer {

    protected $_run = FALSE;

    protected function _construct() {
        Mage::getModel('devel/devel');
        return parent::_construct();
    }

    /**
     *
     * @param type $observer
     */
//    public function roundCatalogRulePrices($observer = NULL, $recurse = TRUE) {
//        $collection = Mage::getModel('catalogrule/rule_product_price')->getCollection();
//        $collection->getSelect()
//                //->columns(array('digit' => new Zend_Db_Expr('(CAST(MOD(main_table.rule_price * 100,10) AS UNSIGNED))')))
//                ->where(new Zend_Db_Expr('(CAST(MOD(main_table.rule_price * 100,10) AS UNSIGNED)) NOT IN (5,0)'));
//
//        foreach ($collection as $rule) {
//            $processedPrice = $this->_processPrice($rule->getRulePrice(), $rule->getId());
//            $rule->setData('rule_price', $processedPrice);
//        }
//        try {
//            $collection->save();
//        } catch (Exception $exc) {
//            Mage::logException($exc);
//        }
//        // We need to recurse once again after save ;(
//        if ($recurse) {
//            $this->roundCatalogRulePrices(NULL, FALSE);
//        }
//    }

    public function roundCollectionPrices($observer = NULL) {
//        if ($this->_run === FALSE) {
//            $collection = Mage::getModel('catalogrule/rule_product_price')->getCollection();
//            $collection->getSelect()
//                    ->columns(array('digit' => new Zend_Db_Expr('(CAST(MOD(main_table.rule_price * 100,10) AS UNSIGNED))')))
//                    ->where(new Zend_Db_Expr('(CAST(MOD(main_table.rule_price * 100,10) AS UNSIGNED)) NOT IN (5,0)'));
//            Mage::log((string) $collection->getSelect());
//            Mage::log($collection->getSize());
//            foreach ($collection as $rule) {
//                $processedPrice = $this->_processPrice($rule->getRulePrice(), $rule->getId());
//                Mage::log($rule->getId() . ':' . $processedPrice);
//                $rule->setData('rule_price', $processedPrice);
//            }
//            try {
//                $collection->save();
//            } catch (Exception $exc) {
//                Mage::logException($exc);
//            }
//            $this->_run = TRUE;
//        }
    }

    /**
     * Process the price and return appropratly rounded one. If no return original
     * @param float $price
     */
    protected function _processPrice($price, $debug = '') {
        $dec2 = round($price * 100) % 10;
        $whole = round($price * 100);
        if ($dec2 > 0 && $dec2 != 5) {
            $whole = $price * 100;
            if ($dec2 < 5) {
                $whole += 5 - $dec2;
            } elseif ($dec2 > 5) {
                $whole += 10 - $dec2;
            }
            return number_format($whole / 100, 4);
        }
        return number_format($whole / 100, 4);
    }

//    public function checkDiscount($observer){
//        /**
//         * @var $event Varien_Event
//         * @var $observer Varien_Event_Observer
//         * @var $collection Mage_Catalog_Model_Resource_Product_Collection
//         */
//        $event = $observer->getEvent();
//        $collection = $event->getCollection();
//        $items = $collection->getItems();
//        foreach($items as $key => &$item){
//            $model = Mage::getModel('bxp_product/discount')->discount($item, 1);
//            $discountData = $model->getDiscountData();
//            $item->setFinalPrice($discountData['price'] - $item->getOriginalDiscountAmount());
////        krumo($item->getFinalPrice());
//        }
//        $collection->setDataToAll($items);
//    }

//    public function checkDiscountProduct($observer)
//    {
//        /**
//         * @var $event Varien_Event
//         * @var $observer Varien_Event_Observer
//         */
//        $event = $observer->getEvent();
//        $product = $event->getProduct();
//        $processProductPrices = $this->getData('process_product_prices');
//        if(!isset($processProductPrices[$product->getId()])){
//            $model = Mage::getModel('bxp_product/discount')->discount($product, 1);
//            $discountData = $model->getDiscountData();
//            $product->setFinalPrice($discountData['price']);
//            $processProductPrices[$product->getId()] = true;
//            $this->setData('process_product_prices', $processProductPrices);
//        }
//
//    }

}
