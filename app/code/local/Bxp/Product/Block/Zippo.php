<?php

class Bxp_Product_Block_Zippo extends Bxp_Product_Block_Abstract {

    private $categoryId=74;

    protected function _getProductCollection() {

        $products = Mage::getSingleton('catalog/category')->load($this->categoryId)
                ->getProductCollection()
                ->addAttributeToSelect('*')
                ->setPageSize(9);
        return $products;
    }

    public function setCategory($id) {
        $this->categoryId = $id;
    }

}
