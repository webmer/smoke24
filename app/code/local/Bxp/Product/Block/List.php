<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of List
 */
class Bxp_Product_Block_List extends Mage_Catalog_Block_Product_List {

    /**
     * Render product config form.
     * @return type
     */
    protected function getProductQuickForm($id) {
        $block = $this->getLayout()->createBlock('bxp_product/quickform', 'quick_form_list');
        $block->setProduct($id);
        return $block->toHtml();
    }

}
