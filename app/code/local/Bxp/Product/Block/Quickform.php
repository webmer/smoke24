<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Quick
 */
class Bxp_Product_Block_Quickform extends Mage_Core_Block_Abstract {

    protected $_productId;
    protected $_js = TRUE;
    protected $_cacheTag = 'bxp_product_quickform';

    /**
     * 
     */
    protected function _beforeToHtml() {
        if (!$this->_productId) {
            $current_product = Mage::registry('current_product');
            if ($current_product) {
                $this->setProduct($current_product->getId());
            } else {
                Mage::throwException('You need to specify product for block Bxp_Product_Block_Quickform');
            }
        }
    }

    /**
     *
     * @return type
     */
    protected function _toHtml() {
        return $this->getProductQuickForm();
    }

    /**
     *
     * @param type $id
     */
    public function setProduct($id) {
        $this->_productId = $id;
    }

    /**
     *
     * @return type
     */
    public function getProduct() {
        if ($this->_productId) {
            return $this->_productId;
        } else {
            Mage::throwException('You need to specify product for block Bxp_Product_Block_Quickform');
        }
    }

    public function useAjax($flag = NULL) {
        if (!is_bool($flag)) {
            return $this->_js;
        }
        $this->_js = $flag;
    }



    /**
     * Render product config form.
     * @return type
     */
    protected function getProductQuickForm() {
        $form = new Varien_Data_Form();
        $range = range(1, 20);
        $options = array_combine($range, $range);
        $id = $this->getProduct();
        $qty_classes = array(
            'qty-select', 'pid-' . $id
        );
        if ($this->useAjax()) {
            $qty_classes[] = 'price-update';
        }

        $form->addField('qty-' . $id, 'select', array(
//            'label' => $this->helper('bxp_product')->getProductUnit($id),
            'values' => $options,
            'id' => 'qty-' . $id,
            'name' => 'qty',
            'class' => implode(' ', $qty_classes)
        ));
//        $subs_options = array(
//            '0' => $this->helper('bxp_product')->__('Liefertakt'),
//            'single' => $this->helper('bxp_product')->__('Single purchase')
//        );
//
//        $plans = $this->getSubscriptionPlans();
//
//        foreach ($plans as $plan) {
//            /**
//             * The +1 is required in order 0 to be the default single purcahse.
//             * We need to always decrement by 1 to get the index afterwards!!!
//             */
//            $subs_options[$plan->getIndex() + 1] = $this->helper('bxp_product')->__('every %s weeks', $plan->getPeriodFrequency());
//        }
//        $sub_classes = array(
//            'sub-select', 'pid-' . $id,
//        );
//        $form->addField('sub-' . $id, 'select', array(
//            'values' => $subs_options,
//            'id' => 'sub-' . $id,
//            'name' => 'sub',
//            'class' => implode(' ', $sub_classes),
//        ));
        return $form->getHtml();
    }

    /**
     * Get cart subscription plans and cache;
     * @return type
     */
    public function getSubscriptionPlans() {
        if (false !== ($plans = Mage::app()->getCache()->load('bxp_quickform_plans'))) {
            $plans = unserialize($plans);
        } else {
            $plans = Mage::helper('customweb_subscription')->getCartSubscriptionPlans();
            Mage::app()->getCache()->save(serialize($plans), 'bxp_quickform_plans', array($this->_cacheTag), '60*60*24');
        }
        return $plans;
    }

}
