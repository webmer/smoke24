<?php

/**
 * Description of Elab_Product_Block_Tabs
 */
class Bxp_Product_Block_Tabs extends Mage_Catalog_Block_Product_View_Tabs {

    public function addTab($alias, $title, $block, $template, $position = NULL) {
        if (!$title || !$block || !$template) {
            return false;
        }

        $tab = array(
            'alias' => $alias,
            'title' => $title
        );

        $this->_insertTab($tab, $position);
        $this->setChild($alias, $this->getLayout()->createBlock($block, $alias)
                        ->setTemplate($template)
        );
    }

    public function getTabName($idx) {
        return $this->_tabs[$idx]['title'];
    }

    protected function _insertTab($tab, $position) {
        if (!is_null($position) && is_string($position)) {
            if ($position === 'first')
                array_unshift($this->_tabs, $tab);
            return;
        }
        $this->_tabs[] = $tab;
    }

}
