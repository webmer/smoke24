<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Bxp_Product_Block_Bestseller
 */
class Bxp_Product_Block_Bestseller extends Bxp_Product_Block_Abstract {

    /**
     * Get Key pieces for caching block content
     *
     * @return array
     */
    public function getCacheKeyInfo() {
        return array(
            'CATALOG_PRODUCT_BESTSELLER',
            Mage::app()->getStore()->getId(),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template'),
            Mage::getSingleton('customer/session')->getCustomerGroupId(),
            'template' => $this->getTemplate(),
            $this->getProductsCount()
        );
    }

    /**
     * 
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    protected function _getProductCollection() {
        /** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());

        $collection = $this->_addProductAttributesAndPrices($collection)
                ->addStoreFilter()
                ->addAttributeToFilter('bestseller', 1)
                ->setPageSize($this->getProductsCount())
                ->setCurPage(1);
        
        return $collection;
    }

    /**
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getBestsellers(){
        return $this->_getProductCollection();
    }
}
