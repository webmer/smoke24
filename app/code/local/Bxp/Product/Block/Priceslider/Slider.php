<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Slider
 */
class Bxp_Product_Block_Priceslider_Slider extends Queldorei_Priceslider_Block_Slider {

    /**
     * Render product config form.
     * @return strig Mulstiple Select Options HTML
     */
    protected function getProductQuickForm($id) {
        $block = $this->getLayout()->createBlock('bxp_product/quickform', 'quick_form_list');
        $block->setProduct($id);
        return $block->toHtml();
    }


    protected function getProductQuickFormLabel($id) {
        return $this->helper('bxp_product')->getProductUnit($id);
    }
}
