<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Abstract
 */
class Bxp_Product_Block_Abstract extends Mage_Catalog_Block_Product_New {

    /**
     * Render product config form.
     * @return type
     */
    protected function getProductQuickForm($id) {
        $block = $this->getLayout()->createBlock('bxp_product/quickform', 'quick_form_list');
        $block->setProduct($id);
        return $block->toHtml();
    }

    protected function getProductQuickFormLabel($id) {
        return $this->helper('bxp_product')->getProductUnit($id);
    }

}