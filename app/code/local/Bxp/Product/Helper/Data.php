<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Data
 */
class Bxp_Product_Helper_Data extends Mage_Core_Helper_Abstract {

    /**
     *
     * @param type $product
     * @return boolean
     */
    public function getBrandUrl($product) {
        $manufacturer = $product->getData('manufacturer');
        if ($manufacturer) {
            $url = Mage::getModel('catalog/category')->load(3)->getUrlPath() . '?manufacturer=' . $manufacturer;
            return substr(Mage::getUrl($url), 0, -1);
        } else {
            return FALSE;
        }
    }

    /**
     * Get translated product unit.
     * @param type $productId
     * @return string Translated unit
     */
    public function getProductUnit($productId) {
        $product = Mage::getModel('catalog/product')->load($productId);
        if ($product->getUnit()) {
            return $product->getResource()->getAttribute('unit')->getFrontend()->getValue($product);
        } else {
            // If no unit get default unit from categories.
            $catIds = $product->getCategoryIds();

            $units = $this->_getCategoryUnits();
            foreach ($catIds as $catId) {
                if (array_key_exists($catId, $units)) {
                    return $units[$catId];
                }
            }
        }
    }

    /**
     * Define categories units
     * @return array available categories and their units
     */
    protected function _getCategoryUnits() {
        $units = array(
            4 => $this->__('Carton of cigarettes'),
            5 => $this->__('Box'),
            6 => $this->__('Box'),
            7 => $this->__('Piece'),
            8 => $this->__('Piece'),
            9 => $this->__('Piece'),
            10 => $this->__('Pack'),
            11 => $this->__('Cans'),
            12 => $this->__('Pack'),
            13 => $this->__('Cans'),
        );
        return $units;
    }

}
