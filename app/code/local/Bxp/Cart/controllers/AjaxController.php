<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AjaxController
 */
require_once (Mage::getModuleDir('controllers', 'Excellence_Ajax') . DS . 'IndexController.php');

class Bxp_Cart_AjaxController extends Excellence_Ajax_IndexController {

    public function addAction() {
        $cart = $this->_getCart();
        $params = $this->getRequest()->getParams();
        if ($params['isAjax'] == 1) {
            $response = array();
            try {
                if (isset($params['qty'])) {
                    $filter = new Zend_Filter_LocalizedToNormalized(
                            array('locale' => Mage::app()->getLocale()->getLocaleCode())
                    );
                    $params['qty'] = $filter->filter($params['qty']);
                }

                $product = $this->_initProduct();
                $related = $this->getRequest()->getParam('related_product');

                /**
                 * Check product availability
                 */
                if (!$product) {
                    $response['status'] = 'ERROR';
                    $response['message'] = $this->__('Unable to find Product ID');
                }

                $cart->addProduct($product, $params);
                if (!empty($related)) {
                    $cart->addProductsByIds(explode(',', $related));
                }

                $cart->save();
                //Apply Plan action 
                if (isset($params['sub']) && $params['sub'] != 'single') {
                    $subIndex = $filter->filter($params['sub']);
                    if (is_numeric($subIndex)) {
                        $this->_applyPlanAction($subIndex - 1);
                    }
                }
                $this->_getSession()->setCartWasUpdated(true);

                /**
                 * @todo remove wishlist observer processAddToCart
                 */
                Mage::dispatchEvent('checkout_cart_add_product_complete', array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
                );

                if (!$cart->getQuote()->getHasError()) {
                    $message = $this->__($this->__('%s was added to your shopping cart.') . '<br/> <a href="' . Mage::getUrl('checkout/cart') . '">' . $this->__('View Cart') . '</a> | <a href="' . Mage::helper('checkout/url')->getCheckoutUrl() . '">' . $this->__('Checkout') . '</a>', Mage::helper('core')->htmlEscape($product->getName()));
                    $response['status'] = 'SUCCESS';
                    $response['message'] = $message;

                    $this->loadLayout();
                    Mage::register('referrer_url', $this->_getRefererUrl());
                    $sidebar_header = $this->getLayout()->getBlock('cart_top')->toHtml();
                    $response['cart_top'] = $sidebar_header;
                }
            } catch (Mage_Core_Exception $e) {
                $msg = "";
                if ($this->_getSession()->getUseNotice(true)) {
                    $msg = $e->getMessage();
                } else {
                    $messages = array_unique(explode("\n", $e->getMessage()));
                    foreach ($messages as $message) {
                        $msg .= $message . '<br/>';
                    }
                }

                $response['status'] = 'ERROR';
                $response['message'] = $msg;
            } catch (Exception $e) {
                $response['status'] = 'ERROR';
                $response['message'] = $this->__('Cannot add the item to shopping cart.');
                Mage::logException($e);
            }
            $this->_sendJson($response);
            return;
        } else {
            return parent::addAction();
        }
    }

    /**
     *
     * @see Customweb_Subscription_IndexController
     * @return type
     */
    protected function _applyPlanAction($index) {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();

        /**
         * No reason continue with empty shopping cart
         */
        if (!$quote->getItemsCount()) {
            return;
        }

        if ($index !== '' && $index >= 0) {
            $subscriptionPlan = Mage::getModel('customweb_subscription/cartPlan')->loadByIndex($index);
        } else {
            $subscriptionPlan = null;
        }

        $oldSubscriptionPlan = $quote->getSubscriptionPlan();

        if ($subscriptionPlan == null && $oldSubscriptionPlan == null) {
            return;
        }

        try {
            $quote->setSubscriptionPlan($subscriptionPlan)
                    ->collectTotals()
                    ->save();

            if ($subscriptionPlan != null) {
                return TRUE;
//                Mage::getSingleton('checkout/session')->addSuccess(
//                        $this->__('You subscribed to the plan <em>%s</em>.', Mage::helper('core')->htmlEscape($subscriptionPlan->getDescription()))
//                );
            } else {
                // Mage::getSingleton('checkout/session')->addSuccess($this->__('The subscription plan was removed.'));
            }
        } catch (Mage_Core_Exception $e) {
            // Mage::getSingleton('checkout/session')->addError($e->getMessage());
        } catch (Exception $e) {
            // Mage::getSingleton('checkout/session')->addError($this->__('Cannot apply the subscription plan.'));
            Mage::logException($e);
        }
    }

    /**
     * send json respond
     *
     * @param array $response - response data
     */
    protected function _sendJson($response) {
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody((string) $this->getRequest()->getParam('callback') . '(' . Mage::helper('core')->jsonEncode($response) . ')');
    }

}
