<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Observer
 */
class Bxp_Cart_Model_Observer extends Varien_Event_Observer {

    public function checkShipping($observer = NULL) {
        Mage::getSingleton('devel/devel');
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        if ($quote->getItemsCount()) {
            $freeShipping = FALSE;
            $quoteRules = $quote->getData('applied_rule_ids');
            $rulesArray = explode(',', $quoteRules);
            //krumo($rules);
            //Lets loop rules and check if it is a discount one
            $rules = array();
            $rulesCollection = Mage::getModel('salesrule/rule')->getCollection();
            // We collect all roules that are with fre shipping and get their
            // minimym values. Also we add the categories.
            foreach ($rulesCollection as $rule) {

                $data = $rule->getData();
                if (($rule->getSimpleFreeShipping() == 1 || $rule->getSimpleFreeShipping() == 2) && $rule->getIsActive() && $rule->getApplyToShipping() ) {
                   // $rules[$rule->getId()]['rule'] = $rule;
                    $cond = unserialize($rule->getConditionsSerialized());
                    if (isset($cond['conditions'][0]['value'])) {
                        $rules[$rule->getId()] = array(
                            'value' => $cond['conditions'][0]['value'],
                            //'categories' => $cond['conditions'][0]['conditions'][0]['value'],
                            'valid' => TRUE
                        );

                    }
                }
            }

            foreach ($rules as $id => $rule) {
                if (in_array($id, $rulesArray)) {
                    $freeShipping = TRUE;
                    break;
                }
            }
           // var_dump($rules);
            if (!$freeShipping && !empty($rules)) {
                // We sort by min value
                uasort($rules, function($a, $b) {
                    return $a['value'] - $b['value'];
                });
                $items = $quote->getAllVisibleItems();
                // We get all the products and coollect all the categories
                foreach ($items as $item) {
                    $product = Mage::getModel('catalog/product')->load($item->getProduct()->getId());
                    $cats = $product->getCategoryIds($product);
                    if($product->getFreeShippingDiscount())
                    {
                        $rules = false;
                    }
//                    foreach ($rules as &$rule) {
//                        if (!$rule['valid']) {
//                            $productCategories = explode(',', $rule['categories']);
//                            //Yay we hitted it
//                            $hits = array_intersect($productCategories, $cats);
//                            if (!empty($hits)) {
//                                $rule['valid'] = TRUE;
//                            }
//                        }
//                    }
                }
                if($rules) {
                    foreach ($rules as $key => $_rule) {
                        if ($_rule['valid'] === TRUE && $quote->getSubtotalWithDiscount() <= $_rule['value']) {
                            Mage::getSingleton('core/session')->addNotice(Mage::helper('core')->__('Bestelle noch für %s mehr und wir schenken Dir die Versandgebühren! ', Mage::helper('core')->formatPrice($_rule['value'] - $quote->getSubtotalWithDiscount())));
                            break;
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $observer Varien_Event_Observer
     */
//    public function checkDiscount($observer)
//    {
//        /**
//         * @var $quote Customweb_Subscription_Model_Sales_Quote
//         */
//        $quote=$observer->getEvent()->getQuote();
//        $quoteid=$quote->getId();
//        $items = $quote->getAllItems();
//        if(count($items) == 0){
//            return;
//        }
//
//        $discountAmount = 0;
//        $subTotal = 0;
//        $grandTotal = 0;
//        /**
//         * for local test
//         */
//        $itemDiscounts = [];
//        foreach($items as $item){
//            /**
//             * @var $item Customweb_Subscription_Model_Sales_Quote_Item
//             * @var $product Customweb_Subscription_Model_Catalog_Product
//             */
//            $product = $item->getProduct();
//            /**
//             * @var $rule Mage_CatalogRule_Model_Rule
//             */
//            $model = Mage::getModel('bxp_product/discount')->discount($product->getId(), $item->getQty());
//            $modelOne = Mage::getModel('bxp_product/discount')->discount($product->getId(), 1);
//            $v = $model->getDiscountData();
//            $discountOne = $modelOne->getDiscountData();
//            $grandTotal += $v['regular_price'];
//            $subTotal += $v['price'];
//            $v['one'] = $discountOne;
////            $discount = $v['regular_price'] - $v['price'];
//            $itemDiscounts[$item->getId()] = $v;
//        }
//        $discountAmount = $grandTotal - $subTotal;
//
//        if($quoteid) {
//
//            if($discountAmount>0) {
//                $total=$quote->getBaseSubtotal();
//                $quote->setSubtotal(0);
//                $quote->setBaseSubtotal(0);
//
//                $quote->setSubtotalWithDiscount(0);
//                $quote->setBaseSubtotalWithDiscount(0);
//
//                $quote->setGrandTotal(0);
//                $quote->setBaseGrandTotal(0);
//
//
//                $canAddItems = $quote->isVirtual()? ('billing') : ('shipping');
//                /**
//                 * @var $address Mage_Sales_Model_Quote_Address
//                 */
//                foreach ($quote->getAllAddresses() as $address) {
//
//                    $buff = [
//                        'Subtotal' => $address->getSubtotal(),
//                        'BaseSubtotal' => $address->getBaseSubtotal(),
//                        'GrandTotal' => $address->getGrandTotal(),
//                        'BaseGrandTotal' => $address->getBaseGrandTotal(),
//                    ];
//
//                    $address->setGrandTotal(0);
//                    $address->setBaseGrandTotal(0);
//
//                    $address->collectTotals();
//                    $address->setBaseSubtotal($buff['BaseSubtotal']);
//                    $address->setSubtotal($buff['Subtotal']);
//
//                    $quote->setSubtotal((float) $quote->getSubtotal() + $address->getSubtotal());
//                    $quote->setBaseSubtotal((float) $quote->getBaseSubtotal() + $address->getBaseSubtotal());
//
//                    $quote->setSubtotalWithDiscount(
//                        (float) $quote->getSubtotalWithDiscount() + $address->getSubtotalWithDiscount()
//                    );
//                    $quote->setBaseSubtotalWithDiscount(
//                        (float) $quote->getBaseSubtotalWithDiscount() + $address->getBaseSubtotalWithDiscount()
//                    );
//
//                    $quote->setGrandTotal((float) $quote->getGrandTotal() + $address->getGrandTotal());
//                    $quote->setBaseGrandTotal((float) $quote->getBaseGrandTotal() + $address->getBaseGrandTotal());
//
//                    $quote ->save();
//
//                    $quote
//                        ->setGrandTotal($grandTotal)
//                        ->setBaseGrandTotal($quote->getBaseSubtotal()-$discountAmount)
//                        ->setSubtotalWithDiscount($quote->getBaseSubtotal()-$discountAmount)
//                        ->setBaseSubtotalWithDiscount($quote->getBaseSubtotal()-$discountAmount)
//                        ->save();
//
//
//                    if($address->getAddressType()==$canAddItems) {
//                        $address->setSubtotalInclTax(0);
//                        $address->setSubtotal($grandTotal);
//                        $address->setSubtotalWithDiscount((float) $address->getSubtotalWithDiscount()-$discountAmount);
//                        $address->setBaseSubtotalWithDiscount((float) $address->getBaseSubtotalWithDiscount()-$discountAmount);
//                        $address->setBaseGrandTotal((float) $address->getBaseGrandTotal()-$discountAmount);
//                        if($address->getDiscountDescription()){
//                            $address->setGrandTotal((float) $grandTotal-(-($address->getDiscountAmount()-$discountAmount)));
//                            $address->setDiscountAmount(-($address->getDiscountAmount()-$discountAmount));
//                            $address->setBaseDiscountAmount($address->getBaseDiscountAmount()-$discountAmount);
//                        }else {
//                            $address->setGrandTotal((float) $grandTotal-$discountAmount);
//                            $address->setDiscountAmount($discountAmount);
//                            $address->setBaseDiscountAmount($discountAmount);
//                        }
//                        $address->save();
//                    }//end: if
//                } //end: foreach
//                //echo $quote->getGrandTotal();
//
//                foreach($items as $item){
//                    //We apply discount amount based on the ratio between the GrandTotal and the RowTotal
//                    if(isset($itemDiscounts[$item->getId()])){
//                        $discount = $itemDiscounts[$item->getId()];
//    //                    var_dump([$ratdisc, $item->getDiscountAmount(), $item->getQty()]);
//                        $item->setPriceInclTax($discount['one']['price']);
//                        $item->setBasePriceInclTax($discount['one']['price']);
//                        $item->setPrice($discount['one']['price']);
//                        $item->setOriginalPrice($discount['one']['regular_price']);
//                        $item->setBaseOriginalPrice($discount['one']['regular_price']);
//                        $item->setRowTotal($discount['regular_price']);
//                        $item->setBaseRowTotal($discount['regular_price']);
//                        $item->setRowTotalInclTax($discount['regular_price']);
//                        $item->setBaseRowTotalInclTax($discount['regular_price']);
//                        $item->setDiscountAmount($discount['regular_price'] - $discount['price']);//($item->getDiscountAmount()+$ratdisc) * $item->getQty());
//                        $item->setBaseDiscountAmount($discount['regular_price'] - $discount['price'])->save();//($item->getBaseDiscountAmount()+$ratdisc) * $item->getQty())->save();
//                    }
//                }
//
//
//            }
//
//        }
//    }

}
