<?php

/**
 * Product:       Xtento_TrackingImport (2.2.3)
 * ID:            8MiiAC0m15NfhCVUICfoCx333zQ2K3rMDsvq5AADgzo=
 * Packaged:      2016-12-08T12:51:32+00:00
 * Last Modified: 2013-11-03T16:33:42+01:00
 * File:          app/code/local/Xtento/TrackingImport/Block/Adminhtml/Source/Grid/Renderer/Status.php
 * Copyright:     Copyright (c) 2016 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_TrackingImport_Block_Adminhtml_Source_Grid_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        return Mage::helper('xtento_trackingimport')->__('Used in <strong>%d</strong> profile(s)', count($row->getProfileUsage()));
    }
}