<?php

/**
 * Product:       Xtento_OrderExport (1.9.7)
 * ID:            8MiiAC0m15NfhCVUICfoCx333zQ2K3rMDsvq5AADgzo=
 * Packaged:      2016-12-08T12:51:32+00:00
 * Last Modified: 2012-11-18T21:15:23+01:00
 * File:          app/code/local/Xtento/OrderExport/Model/Destination/Abstract.php
 * Copyright:     Copyright (c) 2016 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

abstract class Xtento_OrderExport_Model_Destination_Abstract extends Mage_Core_Model_Abstract implements Xtento_OrderExport_Model_Destination_Interface
{
    protected $_connection;
}