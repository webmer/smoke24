<?php

/**
 * Product:       Xtento_OrderExport (1.9.7)
 * ID:            8MiiAC0m15NfhCVUICfoCx333zQ2K3rMDsvq5AADgzo=
 * Packaged:      2016-12-08T12:51:32+00:00
 * Last Modified: 2012-12-08T12:40:25+01:00
 * File:          app/code/local/Xtento/OrderExport/Block/Adminhtml/Log.php
 * Copyright:     Copyright (c) 2016 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_OrderExport_Block_Adminhtml_Log extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'xtento_orderexport';
        $this->_controller = 'adminhtml_log';
        $this->_headerText = Mage::helper('xtento_orderexport')->__('Sales Export - Execution Log');
        parent::__construct();
        $this->_removeButton('add');
    }

    protected function _toHtml()
    {
        return $this->getLayout()->createBlock('xtento_orderexport/adminhtml_widget_menu')->toHtml() . parent::_toHtml();
    }
}