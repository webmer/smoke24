<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Devel
 */
class Elab_Devel_Model_Devel {

    public function __construct() {
        include_once(Mage::getBaseDir('lib') . '/Elab/Krumo/class.krumo.php');
    }

    function getCaller($what = NULL) {
        $trace = debug_backtrace();
        $previousCall = $trace[1]; // 0 is this call, 1 is call in previous function, 2 is caller of that function

        if (isset($what)) {
            return $previousCall[$what];
        } else {
            return $previousCall;
        }
    }

}
