<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IndexController
 */
class Elab_Reports_Adminhtml_ReportsController extends Elab_Reports_Controller_AdminhtmlController {

    public function ordersAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function exportExcelAction() {
        $date = new DateTime();
        $fileName = 'Advanced_Order_Report_' . $date->format('Ymd') . '.xls';
        $content = $this->getLayout()->createBlock('elab_reports/adminhtml_orders_grid')->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }
    public function exportCsvAction() {
        $date = new DateTime();
        $fileName = 'Advanced_Order_Report_' . $date->format('Ymd') . '.csv';
        $content = $this->getLayout()->createBlock('elab_reports/adminhtml_orders_grid')->getCsvFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

}
