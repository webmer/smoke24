<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Orders
 */
class Elab_Reports_Block_Adminhtml_Orders extends Mage_Adminhtml_Block_Widget_Grid_Container {

    protected function _construct() {
        parent::_construct();
        $this->_blockGroup = 'elab_reports';
        $this->_controller = 'adminhtml_orders';
        $this->_headerText = $this->__('Advanced Order Report');
    }

}
