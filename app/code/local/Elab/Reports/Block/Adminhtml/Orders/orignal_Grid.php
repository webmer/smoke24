<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Grid
 */
class Elab_Reports_Block_Adminhtml_Orders_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setDefaultSort('order.order_id');
        $this->setDefaultDir('DESC');
        $this->addExportType('*/*/exportExcel', Mage::helper('core')->__('Excel'));
        $this->addExportType('*/*/exportCsv', Mage::helper('core')->__('CSV'));
    }

    /**
     *
     * @return Mage_Sales_Model_Resource_Order_Collection
     */
    protected function _prepareCollection() {
        /* @var $collection Mage_Sales_Model_Resource_Order_Collection */
        $collection = Mage::getModel('sales/order_item')->getCollection();
  
  
                /*
SELECT c3.sku, c2.value
FROM catalog_category_product c1
INNER JOIN catalog_category_entity_varchar c2 ON ( c1.category_id = c2.entity_id )
INNER JOIN catalog_product_entity c3 ON ( c1.product_id = c3.entity_id )
WHERE c2.attribute_id = (
SELECT attribute_id
FROM eav_attribute
WHERE attribute_code = 'name'
AND entity_type_id =3 )
AND c3.entity_id =1757
*/
        $collection->getSelect()->join(array(
                    'order' => 'sales_flat_order'
                        ), 'main_table.order_id = order.entity_id', array('date_ordered' => 'created_at', 'status', 'coupon_code', 'increment_id', 'customer_gender', 'customer_dob'))
                ->join(array(
                    'sh_addr' => 'sales_flat_order_address'
                        ), 'order.entity_id = sh_addr.parent_id AND sh_addr.address_type = "shipping"', array('city' => 'city', 'postcode' => 'postcode', 'firstname' => 'firstname', 'lastname' => 'lastname','email'=> 'email'))
                ->joinLeft(array(
                    'pe' => 'catalog_product_entity'
                        ), 'main_table.product_id = pe.entity_id', array()
                )
  
                ->joinLeft(array(
                    'cpei_unit' => 'catalog_product_entity_int'
                        ), 'pe.entity_id = cpei_unit.entity_id AND cpei_unit.attribute_id = 158', array()
                )
//                        ->join(array(
//                            'eao' => 'eav_attribute_option'
//                                ), 'eao.option_id = cpei_unit.value AND eao.attribute_id = 158', array()
//                        )
                ->joinLeft(array(
                    'eaov_unit' => 'eav_attribute_option_value'
                        ), 'eaov_unit.option_id = cpei_unit.value AND eaov_unit.store_id = 0', array('unit' => 'value')
                )
                ->joinLeft(array(
                    'cpei_brand' => 'catalog_product_entity_int'
                        ), 'pe.entity_id = cpei_brand.entity_id AND cpei_brand.attribute_id = 81', array()
                )
//                        ->join(array(
//                            'eao' => 'eav_attribute_option'
//                                ), 'eao.option_id = cpei_unit.value AND eao.attribute_id = 158', array()
//                        )
                ->joinLeft(array(
                    'eaov_brand' => 'eav_attribute_option_value'
                        ), 'eaov_brand.option_id = cpei_brand.value AND eaov_brand.store_id = 0', array('brand' => 'value')
        );
        //158 eav_attribute_option
        // 81 manufacturer

        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns() {
        $this->addColumn('increment_id', array(
            'header' => $this->__('Order ID'),
            'index' => 'increment_id',
            'filter_index' => 'order.increment_id',
            'width' => '90px'
                )
        );
        $this->addColumn('fname', array(
            'header' => $this->__('First Name'),
            'index' => 'firstname',
            'filter_index' => 'sh_addr.firstname',
            'width' => '90px'
                )
        );
        $this->addColumn('lname', array(
            'header' => $this->__('Last Name'),
            'index' => 'lastname',
            'filter_index' => 'sh_addr.lastname',
            'width' => '90px'
                )
        );
        $this->addColumn('mail_address', array(
            'header' => $this->__('Mail Address'),
            'index' => 'email',
            'filter_index' => 'sh_addr.email',
            'width' => '90px'
                )
        );
        $this->addColumn('customer_dob', array(
            'header' => $this->__('Date of birth'),
            'index' => 'customer_dob',
            'type' => 'date',
            'filter_index' => 'order.customer_dob',
            'width' => '80px'
                )
        );
        $this->addColumn('customer_gender', array(
            'header' => $this->__('Gender'),
            'index' => 'customer_gender',
            'filter_index' => 'order.customer_gender',
            'renderer' => 'Elab_Reports_Block_Adminhtml_Orders_Renderer_Gender',
            'type' => 'options',
            'options' => array(
                1 => Mage::helper('customer')->__('Male'),
                2 => Mage::helper('customer')->__('Female')
            ),
            'width' => '80px'
                )
        );

        $this->addColumn('place', array(
            'header' => $this->__('Place'),
            'index' => 'city',
            'filter_index' => 'sh_addr.city',
            'width' => '90px'
                )
        );
        $this->addColumn('postcode', array(
            'header' => $this->__('Postcode'),
            'index' => 'postcode',
            'filter_index' => 'sh_addr.postcode',
            'width' => '40px'
                )
        );
        $this->addColumn('brand', array(
            'header' => $this->__('Brand'),
            'index' => 'brand',
            'filter_index' => 'eaov_brand.value',
            'width' => '70px'
                )
        );
        $this->addColumn('product_name', array(
            'header' => $this->__('Product'),
            'index' => 'name',
            'filter_index' => 'main_table.name'
                )
        );
        $this->addColumn('sku', array(
            'header' => $this->__('SKU'),
            'index' => 'sku',
            'filter_index' => 'main_table.sku',
            'width' => '90px'
                )
        );
        $this->addColumn('status', array(
            'header' => $this->__('Status'),
            'index' => 'status',
            'filter_index' => 'order.status',
            'type' => 'options',
            'options' => Mage::getModel('sales/order_status')->getCollection()->toOptionHash(),
            'width' => '100px'
                )
        );
        $this->addColumn('qty', array(
            'header' => $this->__('Qty'),
            'index' => 'qty_ordered',
            'filter_index' => 'main_table.qty_ordered',
            'width' => '50px'
                )
        );
        $this->addColumn('unit', array(
            'header' => $this->__('Unit'),
            'index' => 'unit',
            'filter_index' => 'eaov_unit.value',
            'width' => '50px'
                )
        );

        $this->addColumn('date_ordered', array(
            'header' => $this->__('Date of purchase'),
            'index' => 'date_ordered',
            'type' => 'date',
            'filter_index' => 'order.created_at',
            'width' => '90px',
            'align' => 'right'
                )
        );

        $websites = Mage::app()->getWebsites();
        $store = $websites[1]->getDefaultStore();
        $this->addColumn('price', array(
            'header' => $this->__('Price'),
            'index' => 'base_row_total_incl_tax',
            'type' => 'price',
            'currency_code' => $store->getBaseCurrency()->getCode(),
            'filter_index' => 'main_table.base_row_total_incl_tax',
            'width' => '50px'
                )
        );
        $this->addColumn('coupon_code', array(
            'header' => $this->__('Coupon Code'),
            'index' => 'coupon_code',
            'filter_index' => 'order.coupon_code',
            'width' => '50px'
                )
        );
    }

}
