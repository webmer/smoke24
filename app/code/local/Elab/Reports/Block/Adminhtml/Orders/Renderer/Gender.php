<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Gender
 */
class Elab_Reports_Block_Adminhtml_Orders_Renderer_Gender extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $value = $row->getData($this->getColumn()->getIndex());
        if ($value == 1) {
            return Mage::helper('customer')->__('Male');
        } elseif ($value == 2) {
            return Mage::helper('customer')->__('Female');
        }else{
            return Mage::helper('customer')->__('Unknown');
        }
    }

}
