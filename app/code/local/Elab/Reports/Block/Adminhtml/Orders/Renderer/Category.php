<?php

/**
 * Description of Category Product
 */
class Elab_Reports_Block_Adminhtml_Orders_Renderer_Category extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {

        $value = $row->getData($this->getColumn()->getIndex());
        $order = Mage::getModel('sales/order')->loadByIncrementId($value);


        $items = $order->getAllVisibleItems();
        foreach($items as $i) {
            $product = Mage::getModel('catalog/product')->load($i->getProductId());
            $cats = $product->getCategoryIds();
            foreach ($cats as $category_id) {
                $_cat = Mage::getModel('catalog/category')->setStoreId(Mage::app()->getStore()->getId())->load($category_id);
                $productCategory[] =  $_cat->getName();
            }
        }
        $result = array_unique($productCategory);
        $results = implode(", ", $result);
        return $results;
    }

}
