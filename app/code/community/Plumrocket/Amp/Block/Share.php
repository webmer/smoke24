<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_Amp
 * @copyright   Copyright (c) 2016 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

class Plumrocket_Amp_Block_Share extends Mage_Core_Block_Template
{
    protected $_allowedShareButtons;

    /**
     * Retrieve allowed social buttons
     * @param  int $store
     * @return array
     */
    public function getActiveShareButtons()
    {
        if ($this->_allowedShareButtons === null) {
            $this->_allowedShareButtons = array();

            $helper = $this->helper('pramp');

            if (!$helper->getSocialSharingEnabled()) {
                return $this->_allowedShareButtons;
            }

            $shareButtons = explode(',', $helper->getActiveShareButtons());

            if (is_array($shareButtons) && count($shareButtons)) {
                foreach ($shareButtons as $type) {
                    $params = array();

                    $methodName = 'get' . ucfirst($type) . 'ButtonParams';
                    if (method_exists($this, $methodName)) {
                        $params = array_merge($params, $this->{$methodName}());
                    }

                    $isValidParams = true;
                    foreach ($params as $param => $paramData) {
                        if (isset($paramData['require']) && $paramData['require'] && !$paramData['value']) {
                            $isValidParams = false;
                            break;
                        }
                    }

                    if ($isValidParams) {
                        $this->_allowedShareButtons[$type] = $params;
                    }
                }
            }
        }

        return $this->_allowedShareButtons;
    }

    public function getEmailButtonParams()
    {
        return array(
            'data-param-body' => array(
                'value' => $this->helper('pramp')->getAmpUrl(),
                'require' => false,
            ),
        );
    }

    public function getFacebookButtonParams()
    {
        return array(
            'data-param-app_id' => array(
                'value' => $this->helper('pramp')->getShareButtonFacebookAppID(),
                'require' => true,
            ),
        );
    }

    public function getModuleName()
    {
        return 'Mage_Catalog';
    }
}
