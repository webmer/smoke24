<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_Amp
 * @copyright   Copyright (c) 2017 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

class Plumrocket_Amp_Model_AW_Ajaxcartpro_Observer extends AW_Ajaxcartpro_Model_Observer
{

    /**
     * Rewrite parent method
     *
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function beforeRenderLayout($observer)
    {
        if (Mage::helper('pramp')->isOnlyOptionsRequest()) {
            return;
        }

        parent::beforeRenderLayout($observer);
    }

    /**
     * Rewrite parent method
     *
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function sendResponseBefore($observer)
    {
        if (Mage::helper('pramp')->isOnlyOptionsRequest()) {
            return;
        }

        parent::sendResponseBefore($observer);
    }

    /**
     * Rewrite parent method
     *
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function frontendLoadLayoutBefore($observer)
    {
        if (Mage::helper('pramp')->isOnlyOptionsRequest()) {
            return;
        }

        parent::frontendLoadLayoutBefore($observer);
    }

    /**
     * Rewrite parent method
     *
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function checkoutCartProductAddAfter($observer)
    {
        if (Mage::helper('pramp')->isOnlyOptionsRequest()) {
            return;
        }

        parent::checkoutCartProductAddAfter($observer);
    }

    /**
     * Rewrite parent method
     *
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function checkoutCartProductAddBefore($observer)
    {
        if (Mage::helper('pramp')->isOnlyOptionsRequest()) {
            return;
        }

        parent::checkoutCartProductAddBefore($observer);
    }
}
