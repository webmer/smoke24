<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_Amp
 * @copyright   Copyright (c) 2017 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */


class Plumrocket_Amp_Model_System_Config_Source_Share_Button
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = array();
        foreach ($this->toArray() as $value => $label) {
            $result[] = array(
                'value' => $value,
                'label' => $label,
            );
        }

        return $result;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'email'     => Mage::helper('pramp')->__('Email'),
            'facebook'  => Mage::helper('pramp')->__('Facebook'),
            'gplus'     => Mage::helper('pramp')->__('Google Plus'),
            'linkedin'  => Mage::helper('pramp')->__('LinkedIn'),
            'pinterest' => Mage::helper('pramp')->__('Pinterest'),
            'tumblr'    => Mage::helper('pramp')->__('Tumblr'),
            'twitter'   => Mage::helper('pramp')->__('Twitter'),
            'whatsapp'  => Mage::helper('pramp')->__('WhatsApp'),
        );
    }

}
