<?php
/**
 * Plumrocket Inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement
 * that is available through the world-wide-web at this URL:
 * http://wiki.plumrocket.net/wiki/EULA
 * If you are unable to obtain it through the world-wide-web, please
 * send an email to support@plumrocket.com so we can send you a copy immediately.
 *
 * @package     Plumrocket_Amp
 * @copyright   Copyright (c) 2017 Plumrocket Inc. (http://www.plumrocket.com)
 * @license     http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 */

try {
    $enterpriseCackeExist = false;
    if (file_exists(MAGENTO_ROOT . '/app/code/core/Enterprise/PageCache/Model/Processor.php')) {
        $enterpriseCackeExist = true;
        class Plumrocket_Amp_Model_Enterprise_PageCache_Processor_Proxy extends Enterprise_PageCache_Model_Processor
        {
        }
    }
} catch (Exception $e) {
}

if (!$enterpriseCackeExist) {
    class Plumrocket_Amp_Model_Enterprise_PageCache_Processor_Proxy
    {
        public function extractContent($content)
        {
            return $content;
        }
    }
}

class Plumrocket_Amp_Model_Enterprise_PageCache_Processor extends Plumrocket_Amp_Model_Enterprise_PageCache_Processor_Proxy
{
    public function prepareCacheId($id)
    {
        $cacheId = parent::prepareCacheId($id);

        $mobileDetect = new Mobile_Detect();
        if ($mobileDetect->isTablet()) {
            $cacheId .= '|tablet';
        } elseif ($mobileDetect->isMobile()) {
            $cacheId .= '|mobile';
        }

        return $cacheId;
    }

}
