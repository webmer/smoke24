<?php

class Ext4mage_Html2pdf_Model_Gridobserver {

	const XPATH_CONFIG_SETTINGS_IS_ACTIVE		= 'html2pdf/settings/is_active';
	
	protected function _isActive() {
		return Mage::getStoreConfig(self::XPATH_CONFIG_SETTINGS_IS_ACTIVE);
	}
	
    public function massaction($observer) {

        if($this->_isActive() && ($observer->getEvent()->getBlock() instanceof Mage_Adminhtml_Block_Widget_Grid_Massaction ||
        	$observer->getEvent()->getBlock() instanceof Enterprise_SalesArchive_Block_Adminhtml_Sales_Order_Grid_Massaction)) {
            
        	if($observer->getEvent()->getBlock()->getRequest()->getControllerName() =='sales_order') {
                $observer->getEvent()->getBlock()->addItem('pdforders_order', array(
                    'label'=> Mage::helper('html2pdf')->__('Print Orders'),
                    'url'  => Mage::helper('adminhtml')->getUrl('html2pdf/admin_order/pdforders'),
                ));
            }
        }
    }
}