<?php

class Ext4mage_Html2pdf_Model_Column extends Mage_Core_Model_Abstract
{
	public function _construct()
	{
		parent::_construct();
		$this->_init('html2pdf/column');
	}

	/**
	 * get Table Column Collection
	 *
	 * @param Ext4mage_Html2pdf_Model_Table $table
	 * @return Ext4mage_Html2pdf_Model_Mysql4_Column_Collection
	 */
	public function getColumnByTable(Ext4mage_Html2pdf_Model_Table $table)
	{
		$collection = $this->getCollection()
		->addFieldToFilter('table_id', $table->getId())
		->setOrder('sort_order', 'asc')
		->setOrder('title', 'asc');
		return $collection;
	}

}