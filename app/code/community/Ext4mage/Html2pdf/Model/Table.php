<?php

class Ext4mage_Html2pdf_Model_Table extends Mage_Core_Model_Abstract
{

	protected $_column = array();
	protected $_columnInstance;

	public function _construct()
	{
		parent::_construct();
		$this->_init('html2pdf/table');
	}

	/**
	 * Retrieve active table for form
	 *
	 * @return array
	 */
	public function getTablesForForm()
	{
		$options = array();
		$allElements = Mage::getResourceModel('html2pdf/table')->getTableByActive();

		$options[] = array(
            'label' => Mage::helper('html2pdf')->__('Select element'),
            'value' => 0
		);

		foreach ($allElements as $element) {
			$options[] = array(
                'label' => $element['title'],
                'value' => $element['table_id']
			);
		}
		return $options;
	}

	/**
	 * Retrieve column instance
	 *
	 * @return Ext4mage_Html2pdf_Model_Column
	 */
	public function getColumnInstance()
	{
		if (!$this->_columnInstance) {
			$this->_columnInstance = Mage::getSingleton('html2pdf/column');
		}
		return $this->_columnInstance;
	}

	/**
	 * Retrieve column collection of table
	 *
	 * @return Ext4mage_Html2pdf_Model_Mysql4_Column_Collection
	 */
	public function getTableColumnCollection()
	{
		$collection = $this->getColumnInstance()
		->getColumnByTable($this);

		return $collection;
	}

	/**
	 * Add column to array of table columns
	 *
	 * @param Ext4mage_Html2pdf_Model_Column $column
	 * @return Ext4mage_Html2pdf_Model_Table
	 */
	public function addColumn(Ext4mage_Html2pdf_Model_Column $column)
	{
		$this->_column[$column->getId()] = $column;
		return $this;
	}

	/**
	 * Get column from columns array of table by given id
	 *
	 * @param int $columnId
	 * @return Ext4mage_Html2pdf_Model_Column | null
	 */
	public function getColumnById($columnId)
	{
		if (isset($this->_column[$columnId])) {
			return $this->_column[$columnId];
		}

		return null;
	}

	/**
	 * Get all columns of table
	 *
	 * @return array
	 */
	public function getColumns()
	{
		return $this->_column;
	}

	/**
	 * Load table columns
	 *
	 * @return Ext4mage_Html2pdf_Model_Table
	 */
	protected function _afterLoad()
	{
		parent::_afterLoad();

		/**
		 * Load columns
		 */
		foreach ($this->getTableColumnCollection() as $column) {
			$this->addColumn($column);
		}
		return $this;
	}

}