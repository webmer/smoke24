<?php
/**
* Ext4mage Html2pdf Module
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to Henrik Kier <info@ext4mage.com> so we can send you a copy immediately.
*
* @category   Ext4mage
* @package    Ext4mage_Html2pdf
* @copyright  Copyright (c) 2012 Ext4mage (http://ext4mage.com)
* @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
* @author     Henrik Kier <info@ext4mage.com>
* */
class Ext4mage_Html2pdf_Model_File extends Mage_Core_Model_Abstract
{

	protected $_column = array();
	protected $_columnInstance;
	const XPATH_CONFIG_SAVE_ORDER           = 'html2pdf/settings/order_save';
	const XPATH_CONFIG_SAVE_INVOICE         = 'html2pdf/settings/invoice_save';
	const XPATH_CONFIG_SAVE_SHIPPING        = 'html2pdf/settings/shipping_save';
	const XPATH_CONFIG_SAVE_CREDITMEMO      = 'html2pdf/settings/creditmemo_save';
	
	public function _construct()
	{
		parent::_construct();
		$this->_init('html2pdf/file');
		
	}
	
	public function getConfigSaveOrder(){
		return Mage::getStoreConfig(self::XPATH_CONFIG_SAVE_ORDER);
	}
	
	public function getConfigSaveInvoice(){
		return Mage::getStoreConfig(self::XPATH_CONFIG_SAVE_INVOICE);
	}
	
	public function getConfigSaveShipping(){
		return Mage::getStoreConfig(self::XPATH_CONFIG_SAVE_SHIPPING);
	}
	
	public function getConfigSaveCreditmemo(){
		return Mage::getStoreConfig(self::XPATH_CONFIG_SAVE_CREDITMEMO);
	}
	
	// Check if order PDF shall be generated
	public function generatOrderPdf($orderId, $template=null){
		$regeneratPdf = null;
		if($this->getConfigSaveOrder() == 1){
			$regeneratPdf = $this->getOrderPdfGenerated($orderId,$template->getId());
		}elseif($this->getConfigSaveOrder() == 2){
			$mostRecentDate = Mage::helper('html2pdf')->getNewestTemplateUpdate($template);
			$regeneratPdf = $this->getOrderPdfGenerated($orderId, $template->getId(), $mostRecentDate);
		}elseif($this->getConfigSaveOrder() == 3){
			$regeneratPdf = $this->getOrderPdfGenerated($orderId);
		}
		if($regeneratPdf == null){
			$generatedPdf = $this->getOrderPdfGenerated($orderId);
			return array(true,$generatedPdf);
		}else{
			return array(false,$regeneratPdf);
		}
	}
	
	// Check if order PDF has be generated
	public function getOrderPdfGenerated($orderId, $templateId=null, $date=null){
		return Mage::getResourceModel('html2pdf/file')->getOrderPdfGenerated($templateId, $orderId, $date);		
	}

	// Check if invoice PDF shall be generated
	public function generatInvoicePdf($invoiceId, $template=null){
 		$regeneratPdf = null;
		if($this->getConfigSaveInvoice() == 1){
			$regeneratPdf = $this->getInvoicePdfGenerated($invoiceId,$template->getId());
		}elseif($this->getConfigSaveInvoice() == 2){
			$mostRecentDate = Mage::helper('html2pdf')->getNewestTemplateUpdate($template);
			$regeneratPdf = $this->getInvoicePdfGenerated($invoiceId, $template->getId(), $mostRecentDate);
		}elseif($this->getConfigSaveInvoice() == 3){
			$regeneratPdf = $this->getInvoicePdfGenerated($invoiceId);
		}
		if($regeneratPdf == null){
			$generatedPdf = $this->getInvoicePdfGenerated($invoiceId);
			return array(true,$generatedPdf);
		}else{
			return array(false,$regeneratPdf);
		}
	}
	
	// Check if invoice PDF has be generated
	public function getInvoicePdfGenerated($invoiceId, $templateId=null, $date=null){
		return Mage::getResourceModel('html2pdf/file')->getInvoicePdfGenerated($templateId, $invoiceId, $date);		
	}
	
	// Check if shipment PDF shall be generated
	public function generatShipmentPdf($shipmentId, $template=null){
 		$regeneratPdf = null;
		if($this->getConfigSaveShipping() == 1){
			$regeneratPdf = $this->getShipmentPdfGenerated($shipmentId,$template->getId());
		}elseif($this->getConfigSaveShipping() == 2){
			$mostRecentDate = Mage::helper('html2pdf')->getNewestTemplateUpdate($template);
			$regeneratPdf = $this->getShipmentPdfGenerated($shipmentId, $template->getId(), $mostRecentDate);
		}elseif($this->getConfigSaveShipping() == 3){
			$regeneratPdf = $this->getShipmentPdfGenerated($shipmentId);
		}
		if($regeneratPdf == null){
			$generatedPdf = $this->getShipmentPdfGenerated($shipmentId);
			return array(true,$generatedPdf);
		}else{
			return array(false,$regeneratPdf);
		}
	}
	
	// Check if shipment PDF has be generated
	public function getShipmentPdfGenerated($shipmentId, $templateId=null, $date=null){
		return Mage::getResourceModel('html2pdf/file')->getShipmentPdfGenerated($templateId, $shipmentId, $date);		
	}
	
	// Check if credit PDF shall be generated
	public function generatCreditPdf($creditId, $template=null){
 		$regeneratPdf = null;
		if($this->getConfigSaveCreditmemo() == 1){
			$regeneratPdf = $this->getCreditPdfGenerated($creditId,$template->getId());
		}elseif($this->getConfigSaveCreditmemo() == 2){
			$mostRecentDate = Mage::helper('html2pdf')->getNewestTemplateUpdate($template);
			$regeneratPdf = $this->getCreditPdfGenerated($creditId, $template->getId(), $mostRecentDate);
		}elseif($this->getConfigSaveCreditmemo() == 3){
			$regeneratPdf = $this->getCreditPdfGenerated($creditId);
		}
		if($regeneratPdf == null){
			$generatedPdf = $this->getCreditPdfGenerated($creditId);
			return array(true,$generatedPdf);
		}else{
			return array(false,$regeneratPdf);
		}
	}
	
	// Check if credit PDF has be generated
	public function getCreditPdfGenerated($creditId, $templateId=null, $date=null){
		return Mage::getResourceModel('html2pdf/file')->getCreditPdfGenerated($templateId, $creditId, $date);		
	}
	
}