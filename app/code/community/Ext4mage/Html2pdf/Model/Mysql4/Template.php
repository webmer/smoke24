<?php

class Ext4mage_Html2pdf_Model_Mysql4_Template extends Mage_Core_Model_Mysql4_Abstract
{
	public function _construct()
	{
		// Note that the html2pdf_id refers to the key field in your database table.
		$this->_init('html2pdf/template', 'template_id');
	}

	protected function _beforeSave(Mage_Core_Model_Abstract $object)
	{
		/*
		 * The two attributes that is datetime data in DB
		* we make converting such as:
		* If they are empty we need to convert them into DB
		* type NULL so in DB they will be empty and not some default value.
		*/
		foreach (array('active_from', 'active_to', 'cross_sell_element_id') as $dataKey) {
			if (!$object->getData($dataKey)) {
				$object->setData($dataKey, new Zend_Db_Expr('NULL'));
			}
		}
		
		if (! $object->getId()) {
			$object->setCreationTime(Mage::getSingleton('core/date')->gmtDate());
		}

		$object->setUpdateTime(Mage::getSingleton('core/date')->gmtDate());
		return $this;
	}

	/**
	 * Assign template to store views
	 *
	 * @param Mage_Core_Model_Abstract $object
	 */
	protected function _afterSave(Mage_Core_Model_Abstract $object)
	{
		if(!$object->getIsMassupdate()){
			$condition = $this->_getWriteAdapter()->quoteInto('template_id = ?', $object->getId());
			$this->_getWriteAdapter()->delete($this->getTable('html2pdf/template_store'), $condition);
		}

		foreach ((array)$object->getData('stores') as $store) {
			$storeArray = array();
			$storeArray['template_id'] = $object->getId();
			$storeArray['store_id'] = $store;
			$this->_getWriteAdapter()->insert($this->getTable('html2pdf/template_store'), $storeArray);
		}

		return parent::_afterSave($object);
	}

	/**
	 * Load template store view association
	 *
	 * @param Mage_Core_Model_Abstract $object
	 */
	protected function _afterLoad(Mage_Core_Model_Abstract $object)
	{
		$select = $this->_getReadAdapter()->select()
		->from($this->getTable('html2pdf/template_store'))
		->where('template_id = ?', $object->getId());

		if ($data = $this->_getReadAdapter()->fetchAll($select)) {
			$storesArray = array();
			foreach ($data as $row) {
				$storesArray[] = $row['store_id'];
			}
			$object->setData('store_id', $storesArray);
		}

		return parent::_afterLoad($object);
	}

	/**
	 * Select a single template, return all texts and tables as well
	 *
	 * @param int $type
	 * @param int $storeId
	 */
	public function getTemplateByType ($type, $storeId){
		 
		$currentTemplate = null;
		 
		// Fetch the template within the storeId
		$select = $this->_getReadAdapter()->select();
		$select->from($this->getMainTable());
		$select->join(array('tempstore' => $this->getTable('html2pdf/template_store')),
		$this->getMainTable().'.template_id = `tempstore`.template_id');
		$select->where("is_active = 1 AND `tempstore`.store_id = ?", $storeId);
		$select->where("type = ?",$type);
		$select->order('rand()');
		$select->limit(1);
		 
		$currentTemplate = $this->_getReadAdapter()->fetchOne($select);
		 
		if($currentTemplate == null){
			// Fetch the template within the storeId
			$select = $this->_getReadAdapter()->select();
			$select->from($this->getMainTable());
			$select->join(array('tempstore' => $this->getTable('html2pdf/template_store')),
			$this->getMainTable().'.template_id = `tempstore`.template_id');
			$select->where("is_active = 1 AND `tempstore`.store_id = 0");
			$select->where("type = ?",$type);
			$select->order('rand()');
			$select->limit(1);

			$currentTemplate = $this->_getReadAdapter()->fetchOne($select);
		}

		return $currentTemplate;
	}
}