<?php

class Ext4mage_Html2pdf_Model_Mysql4_Text extends Mage_Core_Model_Mysql4_Abstract
{
	public function _construct()
	{
		// Note that the html2pdf_id refers to the key field in your database table.
		$this->_init('html2pdf/text', 'text_id');
	}

	protected function _beforeSave(Mage_Core_Model_Abstract $object)
	{
		if (! $object->getId()) {
			$object->setCreationTime(Mage::getSingleton('core/date')->gmtDate());
		}

		$object->setUpdateTime(Mage::getSingleton('core/date')->gmtDate());
		return $this;
	}

	public function getTextByType ($type){
		$objects = array();
		 
		$select = $this->_getReadAdapter()->select();
		 
		$select->from($this->getMainTable());
		$select->where("is_active = 1");
		if($type!=null){
			$select->where("type = ?", $type);
		}
		$select->order('title DESC');

		return $this->_getReadAdapter()->fetchAll($select);
	}

}