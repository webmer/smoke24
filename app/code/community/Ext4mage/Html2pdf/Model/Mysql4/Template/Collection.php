<?php

class Ext4mage_Html2pdf_Model_Mysql4_Template_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	public function _construct()
	{
		parent::_construct();
		$this->_init('html2pdf/template');
		$this->_map['fields']['store']   = 'store_table.store_id';

	}

	/**
	 * Add filter by store
	 *
	 * @param int|Mage_Core_Model_Store $store
	 * @return Mage_Cms_Model_Mysql4_Block_Collection
	 */
	public function addStoreFilter($store, $withAdmin = true)
	{
		if ($store instanceof Mage_Core_Model_Store) {
			$store = array($store->getId());
		}
		$this->addFilter('store', array('in' => ($withAdmin ? array(0, $store) : $store)), 'public');
		return $this;
	}

	/**
	 * Join store relation table if there is store filter
	 */
	protected function _renderFiltersBefore()
	{
		if ($this->getFilter('store')) {
			$this->getSelect()->join(
			array('store_table' => $this->getTable('html2pdf/template_store')),
                'main_table.template_id = store_table.template_id',
			array()
			)->group('main_table.template_id');
		}
		return parent::_renderFiltersBefore();
	}

}