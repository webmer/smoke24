<?php
/**
* Ext4mage Html2pdf Module
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to Henrik Kier <info@ext4mage.com> so we can send you a copy immediately.
*
* @category   Ext4mage
* @package    Ext4mage_Html2pdf
* @copyright  Copyright (c) 2012 Ext4mage (http://ext4mage.com)
* @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
* @author     Henrik Kier <info@ext4mage.com>
* */
class Ext4mage_Html2pdf_Model_Mysql4_File extends Mage_Core_Model_Mysql4_Abstract
{
	public function _construct()
	{
		// Note that the html2pdf_id refers to the key field in your database table.
		$this->_init('html2pdf/file', 'file_id');
		
	}

	protected function _beforeSave(Mage_Core_Model_Abstract $object)
	{
		$object->setCreationTime(Mage::getSingleton('core/date')->gmtDate());
		return $this;
	}
	
	// Check if order PDF has be generated
	public function getOrderPdfGenerated($templateId, $orderId, $date){
		
		$select = $this->_getReadAdapter()->select()
			->from($this->getMainTable());
			if($templateId != null)
				$select->where("template_id = ?", $templateId);
			if($date != null)
				$select->where("creation_time > ?", $date);
			$select->where("order_id = ?", $orderId);
		
		return $this->_getReadAdapter()->fetchRow($select);
	}
	
	// Check if invoice PDF has be generated
	public function getInvoicePdfGenerated($templateId, $invoiceId, $date){
		
		$select = $this->_getReadAdapter()->select()
			->from($this->getMainTable());
			if($templateId != null)
				$select->where("template_id = ?", $templateId);
			if($date != null)
				$select->where("creation_time > ?", $date);
			$select->where("invoice_id = ?", $invoiceId);
		
		return $this->_getReadAdapter()->fetchRow($select);
	}
	
	// Check if shipment PDF has be generated
	public function getShipmentPdfGenerated($templateId, $shipmentId, $date){
		
		$select = $this->_getReadAdapter()->select()
			->from($this->getMainTable());
			if($templateId != null)
				$select->where("template_id = ?", $templateId);
			if($date != null)
				$select->where("creation_time > ?", $date);
			$select->where("shipment_id = ?", $shipmentId);
		
		return $this->_getReadAdapter()->fetchRow($select);
	}
	
	// Check if credit PDF has be generated
	public function getCreditPdfGenerated($templateId, $creditId, $date){
		
		$select = $this->_getReadAdapter()->select()
			->from($this->getMainTable());
			if($templateId != null)
				$select->where("template_id = ?", $templateId);
			if($date != null)
				$select->where("creation_time > ?", $date);
			$select->where("creditmemo_id = ?", $creditId);
		
		return $this->_getReadAdapter()->fetchRow($select);
	}
	
}