<?php

class Ext4mage_Html2pdf_Model_Mysql4_Column_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	public function _construct()
	{
		parent::_construct();
		$this->_init('html2pdf/column');
	}
}