<?php

class Ext4mage_Html2pdf_Model_Mysql4_Column extends Mage_Core_Model_Mysql4_Abstract
{
	public function _construct()
	{
		// Note that the html2pdf_id refers to the key field in your database table.
		$this->_init('html2pdf/column', 'column_id');
	}

	protected function _beforeSave(Mage_Core_Model_Abstract $object)
	{
		/*
		 * The attributes that are empty and not required we need to convert them into DB
		* type NULL so in DB they will be empty and not some default value.
		*/
		foreach (array('option_value', 'bundle_value', 'bundle_option_value', 'download_value', 'download_option_value', 'virtual_value', 'virtual_option_value') as $dataKey) {
			if (!$object->getData($dataKey)) {
				$object->setData($dataKey, new Zend_Db_Expr('NULL'));
			}
		}


		if (! $object->getId()) {
			$object->setCreationTime(Mage::getSingleton('core/date')->gmtDate());
		}

		$object->setUpdateTime(Mage::getSingleton('core/date')->gmtDate());
		return $this;
	}

	public function getColumnByTable ($tableId){
		$select = $this->_getReadAdapter()->select()
		->from($this->getMainTable())
		->where("table_id = ?", $tableId)
		->order('sort_order DESC');
		return $this->_getReadAdapter()->fetchAll($select);
	}

}