<?php

class Ext4mage_Html2pdf_Model_Text extends Mage_Core_Model_Abstract
{
	public function _construct()
	{
		parent::_construct();
		$this->_init('html2pdf/text');
	}

	/**
	 * Retrieve text elements
	 *
	 * @param int $type
	 * @return array
	 */
	public function getTextElementsForForm($type = null)
	{
		$options = array();
		$allElements = Mage::getResourceModel('html2pdf/text')->getTextByType($type);

		$options[] = array(
            'label' => Mage::helper('html2pdf')->__('Select element'),
            'value' => 0
		);

		foreach ($allElements as $element) {
			$options[] = array(
                'label' => $element['title'],
                'value' => $element['text_id']
			);
		}
		return $options;
	}

}