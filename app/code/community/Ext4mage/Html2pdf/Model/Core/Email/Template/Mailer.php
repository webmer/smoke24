<?php
/**
* Sales Order PDF abstract model
*
* Generel class used by all types of pdf print - so used by both order, invoice, shipping and credit memo
* The class if abstract and is extended by the different pdf types
*
* @category   Ext4Mage
* @package    Ext4mage_Html2pdf
* @author     ITCura <info@ITCura.com>
*/
class Ext4mage_Html2pdf_Model_Core_Email_Template_Mailer extends Mage_Core_Model_Email_Template_Mailer
{
	const XPATH_CONFIG_SETTINGS_IS_ACTIVE		= 'html2pdf/settings/is_active';
	
	protected function _isActive() {
		return Mage::getStoreConfig(self::XPATH_CONFIG_SETTINGS_IS_ACTIVE);
	}
	
	public function send(){
    	
		if ($this->_isActive()){
	    	$emailTemplate = Mage::getModel('core/email_template');
	        // Send all emails from corresponding list
	        while (!empty($this->_emailInfos)) {
	            $emailInfo = array_pop($this->_emailInfos);
	            $this->attachHtml2PdfDocument($emailTemplate);
	            // Handle "Bcc" recepients of the current email
	            $emailTemplate->addBcc($emailInfo->getBccEmails());
	            // Set required design parameters and delegate email sending to Mage_Core_Model_Email_Template
	            $emailTemplate->setDesignConfig(array('area' => 'frontend', 'store' => $this->getStoreId()))
	                ->sendTransactional(
	                $this->getTemplateId(),
	                $this->getSender(),
	                $emailInfo->getToEmails(),
	                $emailInfo->getToNames(),
	                $this->getTemplateParams(),
	                $this->getStoreId()
	            );
	        }
	        return $this;
		}else{
			return parent::send();
		}
    }

    public function attachHtml2PdfDocument($emailTemplate)
    {
        $storeId = $this->getStoreId();
        
        switch ( $this->getTemplateId()) {
            //Order template
            case Mage::getStoreConfig(Mage_Sales_Model_Order::XML_PATH_EMAIL_TEMPLATE, $storeId):
            case Mage::getStoreConfig(Mage_Sales_Model_Order::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId):
            	$this->attachOnOrderSend($emailTemplate);
                break;
            //Order comment template
            case Mage::getStoreConfig(Mage_Sales_Model_Order::XML_PATH_UPDATE_EMAIL_TEMPLATE, $storeId):
            case Mage::getStoreConfig(Mage_Sales_Model_Order::XML_PATH_UPDATE_EMAIL_GUEST_TEMPLATE, $storeId):
				$this->attachOnOrderSend($emailTemplate, true);
            	break;
            //Invoice template
            case Mage::getStoreConfig(Mage_Sales_Model_Order_Invoice::XML_PATH_EMAIL_TEMPLATE, $storeId):
            case Mage::getStoreConfig(Mage_Sales_Model_Order_Invoice::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId):
                $this->attachOnInvoiceSend($emailTemplate);
            	break;
            //Invoice comment template
            case Mage::getStoreConfig(Mage_Sales_Model_Order_Invoice::XML_PATH_UPDATE_EMAIL_TEMPLATE, $storeId):
            case Mage::getStoreConfig(Mage_Sales_Model_Order_Invoice::XML_PATH_UPDATE_EMAIL_GUEST_TEMPLATE, $storeId):
                $this->attachOnInvoiceSend($emailTemplate, true);
            	break;
            //Shipment template
            case Mage::getStoreConfig(Mage_Sales_Model_Order_Shipment::XML_PATH_EMAIL_TEMPLATE, $storeId):
            case Mage::getStoreConfig(Mage_Sales_Model_Order_Shipment::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId):
                $this->attachOnShipmentSend($emailTemplate);
            	break;
            //Shipment comment template
            case Mage::getStoreConfig(Mage_Sales_Model_Order_Shipment::XML_PATH_UPDATE_EMAIL_TEMPLATE, $storeId):
            case Mage::getStoreConfig(Mage_Sales_Model_Order_Shipment::XML_PATH_UPDATE_EMAIL_GUEST_TEMPLATE, $storeId):
                $this->attachOnShipmentSend($emailTemplate, true);
            	break;
            //Creditmemo template
            case Mage::getStoreConfig(Mage_Sales_Model_Order_Creditmemo::XML_PATH_EMAIL_TEMPLATE, $storeId):
            case Mage::getStoreConfig(Mage_Sales_Model_Order_Creditmemo::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId):
                $this->attachOnCreditmemoSend($emailTemplate);
            	break;
            //Creditmemo comment template
            case Mage::getStoreConfig(Mage_Sales_Model_Order_Creditmemo::XML_PATH_UPDATE_EMAIL_TEMPLATE, $storeId):
            case Mage::getStoreConfig(Mage_Sales_Model_Order_Creditmemo::XML_PATH_UPDATE_EMAIL_GUEST_TEMPLATE, $storeId):
            	$this->attachOnCreditmemoSend($emailTemplate, true);
        }
    }
    
    public function attachOnOrderSend($emailTemplate, $isComment = false){
    	$tempParms = $this->getTemplateParams();
    	$order = $tempParms['order'];
    	$storeId = $order->getStoreId();
    	$configPath = $isComment ? 'order_comment' : 'order';
    	
        if (Mage::getStoreConfig('sales_email/' . $configPath . '/attach_pdf', $storeId)) {
            $pdf = Mage::getModel('html2pdf/order_pdf_order')->getPdf(array($order));
            $this->addAttachment($pdf, $emailTemplate, Mage::helper('sales')->__('Order') . "-" . $order->getIncrementId());
        }

        if (Mage::getStoreConfig('sales_email/' . $configPath . '/attach_agreement', $storeId)) {
            $this->addAgreements($storeId, $emailTemplate);
        }
    }
    
    public function attachOnInvoiceSend($emailTemplate, $isComment = false){
    	$tempParms = $this->getTemplateParams();
    	$invoice = $tempParms['invoice'];
    	$storeId = $invoice->getStoreId();
    	$configPath = $isComment ? 'invoice_comment' : 'invoice';
    	
        if (Mage::getStoreConfig('sales_email/' . $configPath . '/attach_pdf', $storeId)) {
            $pdf = Mage::getModel('sales/order_pdf_invoice')->getPdf(array($invoice));
            $this->addAttachment($pdf, $emailTemplate, Mage::helper('sales')->__('Invoice') . "-" . $invoice->getIncrementId());
        }

            if (Mage::getStoreConfig('sales_email/' . $configPath . '/attach_agreement', $storeId)) {
            $this->addAgreements($storeId, $emailTemplate);
        }
            }
    
    public function attachOnShipmentSend($emailTemplate, $isComment = false){
        $tempParms = $this->getTemplateParams();
    	$shipment = $tempParms['shipment'];
    	$storeId = $shipment->getStoreId();
    	$configPath = $isComment ? 'shipment_comment' : 'shipment';
    	
        if (Mage::getStoreConfig('sales_email/' . $configPath . '/attach_pdf', $storeId)) {
            $pdf = Mage::getModel('sales/order_pdf_shipment')->getPdf(array($shipment));
            $this->addAttachment($pdf, $emailTemplate, Mage::helper('sales')->__('Shipment') . "-" . $shipment->getIncrementId());
        }

        if (Mage::getStoreConfig('sales_email/' . $configPath . '/attach_agreement', $storeId)) {
            $this->addAgreements($storeId, $emailTemplate);
        }
    }
    
    public function attachOnCreditmemoSend($emailTemplate, $isComment = false){
      	$tempParms = $this->getTemplateParams();
    	$creditmemo = $tempParms['creditmemo'];
    	$storeId = $creditmemo->getStoreId();
    	$configPath = $isComment ? 'creditmemo_comment' : 'creditmemo';
    	
        if (Mage::getStoreConfig('sales_email/' . $configPath . '/attach_pdf', $storeId)) {
            $pdf = Mage::getModel('sales/order_pdf_creditmemo')->getPdf(array($creditmemo));
            $this->addAttachment($pdf, $emailTemplate, Mage::helper('sales')->__('Credit Memo') . "-" . $creditmemo->getIncrementId());
        }

        if (Mage::getStoreConfig('sales_email/' . $configPath . '/attach_agreement', $storeId)) {
            $this->addAgreements($storeId, $emailTemplate);
        }
    }
    
    public function addAttachment($pdf, $emailTemplate, $name = "order.pdf") {
    	try{
    		$file = $pdf->render();
    		$emailTemplate->getMail()->createAttachment($file,'application/pdf',Zend_Mime::DISPOSITION_ATTACHMENT,Zend_Mime::ENCODING_BASE64,$name.'.pdf');
    	} catch (Exception $e){
    		Mage::log(Mage::helper('html2pdf')->__('HTML2PDF - Attaching pdf to mail error: %s'),$e->getMessage());
    	}
    }
    
    public function addAgreements($storeId,$emailTemplate) {
    	$agreements = Mage::getModel('checkout/agreement')->getCollection()
    		->addStoreFilter($storeId)
    		->addFieldToFilter('is_active', 1);
    	if ($agreements) {
    		foreach ($agreements as $agreement) {
    			$agreement->load($agreement->getId());
    			if($agreement->getIsHtml()) {
    				$html='<html><head><title>'.$agreement->getName().'</title></head><body>'.$agreement->getContent().'</body></html>';
    				$emailTemplate->getMail()->createAttachment($html,'text/html',Zend_Mime::DISPOSITION_ATTACHMENT,Zend_Mime::ENCODING_BASE64,urlencode($agreement->getName()).'.html');
    			} else {
    				$emailTemplate->getMail()->createAttachment(Mage::helper('core')->htmlEscape($agreement->getContent()),'text/plain',Zend_Mime::DISPOSITION_ATTACHMENT,Zend_Mime::ENCODING_BASE64,urlencode($agreement->getName()).'.txt');
    			}
    		}
    	}
    }
}