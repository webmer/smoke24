<?php

class Ext4mage_Html2pdf_Model_Template extends Mage_Core_Model_Abstract
{

	protected $_headerText;
	protected $_mainText;
	protected $_footerText;
	protected $_table;
	protected $_crossSell;
	const XPATH_CONFIG_SETTINGS_LICENSE           = 'html2pdf/settings/license_code';

	public $pageFormat = array(
	array('label'=>'A4 (210x297 mm)','value'=>'A4'),
	array('label'=>'A0 (841x1189 mm)','value'=>'A0'),
	array('label'=>'A1 (594x841 mm)','value'=>'A1'),
	array('label'=>'A2 (420x594 mm)','value'=>'A2'),
	array('label'=>'A3 (297x420 mm)','value'=>'A3'),
	array('label'=>'A5 (148x210 mm)','value'=>'A5'),
	array('label'=>'A6 (105x148 mm)','value'=>'A6'),
	array('label'=>'A7 (74x105 mm)','value'=>'A7'),
	array('label'=>'A8 (52x74 mm)','value'=>'A8'),
	array('label'=>'US LETTER (216x279 mm ; 8.50x11.00 in)','value'=>'LETTER'),
	array('label'=>'US LEGAL (216x356 mm ; 8.50x14.00 in)','value'=>'LEGAL'),
	array('label'=>'US GLETTER (203x267 mm ; 8.00x10.50 in)','value'=>'GLETTER'),
	array('label'=>'US JLEGAL (203x127 mm ; 8.00x5.00 in)','value'=>'JLEGAL'),
	array('label'=>'B0 (1000x1414 mm)','value'=>'B0'),
	array('label'=>'B1 (707x1000 mm)','value'=>'B1'),
	array('label'=>'B2 (500x707 mm)','value'=>'B2'),
	array('label'=>'B3 (353x500 mm)','value'=>'B3'),
	array('label'=>'B4 (250x353 mm)','value'=>'B4'),
	array('label'=>'B5 (176x250 mm)','value'=>'B5'),
	array('label'=>'B6 (125x176 mm)','value'=>'B6'),
	array('label'=>'B7 (88x125 mm)','value'=>'B7'),
	array('label'=>'B8 (62x88 mm)','value'=>'B8'),
	array('label'=>'C0 (917x1297 mm)','value'=>'C0'),
	array('label'=>'C1 (648x917 mm)','value'=>'C1'),
	array('label'=>'C2 (458x648 mm)','value'=>'C2'),
	array('label'=>'C3 (324x458 mm)','value'=>'C3'),
	array('label'=>'C4 (229x324 mm)','value'=>'C4'),
	array('label'=>'C5 (162x229 mm)','value'=>'C5'),
	array('label'=>'C6 (114x162 mm)','value'=>'C6'),
	array('label'=>'C7 (81x114 mm)','value'=>'C7'),
	array('label'=>'C8 (57x81 mm)','value'=>'C8'),
	array('label'=>'DL (110x220 mm)','value'=>'DL')
	);

	public $pageFont = array(
	array('label'=>'Times New Roman','value'=>'times'),
	array('label'=>'Courier','value'=>'courier'),
	array('label'=>'Helvetica','value'=>'helvetica'),
	array('label'=>'MyungJo Medium (Korean)','value'=>'hysmyeongjostdmedium'),
	array('label'=>'Kozuka Gothic Pro (Japanese Sans-Serif)','value'=>'kozgopromedium'),
	array('label'=>'Kozuka Mincho Pro (Japanese Serif)','value'=>'kozminproregular'),
	array('label'=>'MSung Light (Trad. Chinese)','value'=>'msungstdlight'),
	array('label'=>'STSong Light (Simp. Chinese)','value'=>'stsongstdlight'),
	array('label'=>'Zapf Dingbats','value'=>'zapfdingbats'),
	array('label'=>'Symbol','value'=>'symbol'),
	array('label'=>'Dejavusans','value'=>'dejavusans'),
	);
	 
	public function _construct()
	{
		parent::_construct();
		$this->_init('html2pdf/template');
	}

	/**
	 * Load all text and table elements
	 *
	 * @return Ext4mage_Html2pdf_Model_Template
	 */
	protected function _afterLoad()
	{
		parent::_afterLoad();

		if ($this->getHeaderElementId() > 0) {
			$this->_headerText = Mage::getModel('html2pdf/text')->load($this->getHeaderElementId());
		}
		if ($this->getTextElementId() > 0) {
			$this->_mainText = Mage::getModel('html2pdf/text')->load($this->getTextElementId());
		}
		if ($this->getFooterElementId() > 0) {
			$this->_footerText = Mage::getModel('html2pdf/text')->load($this->getFooterElementId());
		}
		if ($this->getTableElementId() > 0) {
			$this->_table = Mage::getModel('html2pdf/table')->load($this->getTableElementId());
		}
		if ($this->getCrossSellElementId() > 0) {
			$this->_crossSell = Mage::getModel('html2pdf/text')->load($this->getCrossSellElementId());
		}
		 
		return $this;
	}

	/**
	 * Select a single template, return all texts and tables as well
	 *
	 * @param int $type
	 * @param int $storeId
	 * @return Ext4mage_Html2pdf_Model_Template
	 */
	public function getTemplateByType ($type, $storeId){
		 
		$this->load(Mage::getResourceModel('html2pdf/template')->getTemplateByType($type, $storeId));
		 
		return $this;
	}

	/**
	 * Get header text of template
	 *
	 * @return Ext4mage_Html2pdf_Model_Text
	 */
	public function getHeaderText()
	{
		return $this->_headerText;
	}

	/**
	 * Get main text of template
	 *
	 * @return Ext4mage_Html2pdf_Model_Text
	 */
	public function getMainText()
	{
		return $this->_mainText;
	}

	/**
	 * Get footer text of template
	 *
	 * @return Ext4mage_Html2pdf_Model_Text
	 */
	public function getFooterText()
	{
		return $this->_footerText;
	}

	/**
	 * Get table of template
	 *
	 * @return Ext4mage_Html2pdf_Model_Table
	 */
	public function getTable()
	{
		return $this->_table;
	}

	/**
	 * Get Cross Sell of template
	 *
	 * @return Ext4mage_Html2pdf_Model_Text
	 */
	public function getCrossSellText()
	{
		return $this->_crossSell;
	}
}