<?php
/**
 * Magento
 *
 * NOTICE
 *
 * This source file a part of Ext4mage_Html2pdf extension
 * all rights to this modul belongs to ext4mage.com
 *
 * @category    Ext4mage
 * @package     Ext4mage_Html2pdf
 * @copyright   Copyright (c) 2011 ext4mage (http://www.ext4mage.com)
 */
set_include_path( BP.DS.'lib'.DS.'ext4magehtml2pdf'. PS . get_include_path());
require_once 'ext4magehtml2pdf'.DS.'ext4magehtml2pdf.php';
//require_once 'ext4magehtml2pdf'.DS.'config'.DS.'lang'.DS.'eng.php'; // Change to config setting

class Ext4mage_Html2pdf_Model_Order_Pdf_Invoice extends Ext4mage_Html2pdf_Model_Order_Pdf_Abstract
{
	public $current_invoice;

	public function getPdf($invoices = array(), $templateId = null){
		if($this->_isActive() == 0 && $templateId == null){
			return Mage::getModel('sales/order_pdf_invoice_old')->getPdf($invoices);
		}
		
		$this->_beforeGetPdf();
		$this->_initRenderer('invoice');
		
		if(Zend_Version::compareVersion('1.11.0')>0){
			$this->pdf = new Ext4mageHtml2PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		}else{
			$pdfTotal = new Zend_Pdf();
			$extractor = new Zend_Pdf_Resource_Extractor();
		}
				
		foreach ($invoices as $invoice) {
			$this->current_invoice = $invoice;
			$template = null;
			
			if($templateId == null){
				$template = Mage::getSingleton('html2pdf/template')->getTemplateByType(2, $invoice->getStore()->getStoreId());
			}else{
				$template = Mage::getModel('html2pdf/template')->load($templateId);
			}
			
			if(Zend_Version::compareVersion('1.11.0')>0){
				$this->doInvoicePdf($template);
			}else{					
				if(!$template->getId()){
					return Mage::getModel('sales/order_pdf_invoice_old')->getPdf($invoices);
				}else{
					if($this->_isDevMode()){
						$this->doInvoicePdf($template);
					}else{
					$pdfFile = Mage::getModel('html2pdf/file')->generatInvoicePdf($invoice->getId(), $template);
					if($pdfFile[0]){
						//Generat PDF
						$this->doInvoicePdf($template);
						//Clone pages of generated pdf
						$pdfGenerated = $this->pdf->Output("./pdfs/example.pdf", "S");
						$pdfToClone = Zend_Pdf::parse($pdfGenerated);
						//Save PDF if set to save
						if(Mage::getModel('html2pdf/file')->getConfigSaveInvoice() > 0){
							//get filename to be used
							if($pdfFile[1] == null){
								$filepath = $this->_getFileName('invoice_'.$invoice->getIncrementId());
								$pdfGeneratedFile = Mage::getModel('html2pdf/file');
								$pdfGeneratedFile->setFilepath($filepath);
								$pdfGeneratedFile->setTemplateId($template->getId());
								$pdfGeneratedFile->setInvoiceId($invoice->getId());
							}else{
								$pdfGeneratedFile = Mage::getModel('html2pdf/file')->load($pdfFile[1]['file_id']);
								$pdfGeneratedFile->setTemplateId($template->getId());
								$filepath = $pdfGeneratedFile->getFilepath();
								if (file_exists($filepath)) unlink($filepath);
								if(empty($filepath) || strlen($filepath)<5){
									$filepath = $this->_getFileName('invoice_'.$invoice->getIncrementId());
									$pdfGeneratedFile->setFilepath($filepath);
								}	
							}
							//Save file on disk
							$pdfGeneratedFile->save();
							$this->pdf->Output($filepath, 'F');
						}
					}else{
						if(Mage::getModel('html2pdf/file')->getConfigSaveInvoice() == 0){
							$this->doInvoicePdf($template);
							$pdfGenerated = $this->pdf->Output("./pdfs/example.pdf", "S");
							$pdfToClone = Zend_Pdf::parse($pdfGenerated);
						}else{
							//get file from disk
							if (!file_exists($pdfFile[1]['filepath'])){
								//Generat PDF
								$this->doInvoicePdf($template);
								$pdfGeneratedFile = Mage::getModel('html2pdf/file')->load($pdfFile[1]['file_id']);
								$filepath = $pdfGeneratedFile->getFilepath();
								if(empty($filepath) || strlen($filepath)<5){
									$filepath = $this->_getFileName('invoice_'.$invoice->getIncrementId());
									$pdfGeneratedFile->setFilepath($filepath);
									$pdfGeneratedFile->save();
								}
								//Save file on disk
								$this->pdf->Output($filepath, 'F');
								$pdfFile[1]['filepath'] = $filepath;
							}
							$pdfToClone = Zend_Pdf::load($pdfFile[1]['filepath']);
						}
					}
					}
				}
				foreach ($pdfToClone->pages as $p) $pdfTotal->pages[] = $extractor->clonePage($p);
			}
		}
		if(Zend_Version::compareVersion('1.11.0')>0){
			$pdf2 = $this->pdf->Output("./pdfs/example.pdf", "S");
			$pdfTotal = Zend_Pdf::parse($pdf2);
		}
		$this->_afterGetPdf();
		return $pdfTotal;
	}
	
	private function doInvoicePdf($template){
		if($this->_isDevMode()){
			echo "<html><head><meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\"></head>";
		}
		
		if(Zend_Version::compareVersion('1.11.0')!=1){
			$this->pdf = new Ext4mageHtml2PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);
		}
		$this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$this->pdf->setFontSubsetting(false);
		$this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		
		$invoice = $this->current_invoice;
		$order = $invoice->getOrder();
		
		$textColors = Mage::helper('html2pdf')->html2rgb($template->getDefaultFontColor());
		
		$this->pdf->startPageGroup();
		
		//set pdf options 
		$this->pdf->SetMargins($template->getPageMarginLeft(), $template->getPageMarginBottom(), $template->getPageMarginRight());
		$this->pdf->SetHeaderMargin($template->getPageMarginTop());
		$this->pdf->SetFooterMargin($template->getPageMarginBottom());
		$this->pdf->SetFont($template->getDefaultFont(), '', $template->getDefaultFontSize());
		$this->pdf->SetTextColor($textColors[0],$textColors[1],$textColors[2]);
		$this->pdf->setOptimizeImageSize($template->getImageOptimize());
			
		// ---------------------------------------------------------
		// Set header and footer from Template
		if(!$this->_isDevMode()){
			$this->pdf->setHeaderHtml($this->convertText($order, $order, $template->getHeaderText()->getTextcontent(), 'replaceText'), $template->getDefaultFont(), $template->getDefaultFontSize(), $textColors);
			$this->pdf->setFooterHtml($this->convertText($order, $order, $template->getFooterText()->getTextcontent(), 'replaceText'), $template->getDefaultFont(), $template->getDefaultFontSize(), $textColors);
		}
		
		$this->pdf->AddPage($template->getPageOrientation(), $template->getPageSize());
		// Write main content of invoice pdf
		
		$html = '<table border="0" cellspacing="0" cellpadding="0" width="100%">';
		$html .= '<tr><td>';
		
		$html .= $this->convertText($order, $order, $template->getMainText()->getTextcontent(), 'replaceText');
		
		// Draw all items html - using abstract by default
		$itemshtml = $this->drawItems($invoice->getAllItems(), $template, $order);
		$html = preg_replace('#{{product_table}}#', $itemshtml, $html);
		
		// Draw all cross sell items html - using abstract by default
		if(stristr($html,'{{cross_sell}}')){
			$crosshtml = $this->drawCross($invoice->getAllItems(), $template, $order);
			$crosshtml = $this->convertText($order, $order, $crosshtml, 'replaceText');
			$html = preg_replace('#{{cross_sell}}#', $crosshtml, $html);
		}
		
		$html .= '</td></tr>
			        </table>';
		
		if(!$this->_isDevMode()){
			$this->pdf->writeHTML($html, true, 0, true, 0);
		}else{
			echo "<br /><br /><b>Header text</b><br /><br />";
			echo $this->convertText($order, $order, $template->getHeaderText()->getTextcontent(), 'replaceText');
			echo "<br /><br /><b>Main text</b><br /><br />";
			echo $html;
			echo "<br /><br /><b>Footer text</b><br /><br />";
			echo $this->convertText($order, $order, $template->getFooterText()->getTextcontent(), 'replaceText');
			exit;
		}
	}

	public function replaceText($order, $text, $obj){
		$replacements = array (
    		'#{{invoice_data_(.*?)}}#s' => '$item->getData($matches[1])',
    		'#{{invoice_shipping_data_(.*?)}}#s' => 'is_object($item->getShippingAddress())?$item->getShippingAddress()->getData($matches[1]):""',
    		'#{{invoice_shipping_country_name}}#s' => 'is_object($item->getShippingAddress())?$item->getShippingAddress()->getCountryModel()->getName():""', 
			'#{{invoice_billing_data_(.*?)}}#s' => 'is_object($item->getBillingAddress())?$item->getBillingAddress()->getData($matches[1]):""',
    		'#{{invoice_billing_country_name}}#s' => 'is_object($item->getBillingAddress())?$item->getBillingAddress()->getCountryModel()->getName():""', 
    		'#{{invoice_comments_last_data_(.*?)}}#s' => 'is_object(end($item->getCommentsCollection()->getItems()))?end($item->getCommentsCollection()->getItems())->getData($matches[1]):""',
    		'#{{invoice_order_increment_id}}#s' => '$item->getOrderIncrementId()',
    		'#{{invoice_state_name}}#s' => '$item->getStateName()',
    		'#{{invoice_was_pay_called}}#s' => '$item->wasPayCalled()'
		);
		$currentInvoice = $obj->current_invoice;
		 
		foreach ($replacements as $key => $value) {
			$obj->callbackParms = array($value, $currentInvoice);
			$regCount = 0;
			$text = preg_replace_callback($key, array(&$obj, 'parent::checkReplaceResult'), $text, -1, $regCount);
			if($regCount>0){
				return $text;
			}
		}

		$text = parent::replaceText($order, $text, $obj);

		if(preg_match ('#{{invoice_totals_(.*?)}}#s',$text, $match) == 1){
			list($source_field, $totalElement) = explode('_', $match[1], 2);
			$total = Mage::helper('html2pdf')->getTotalForDisplaying($currentInvoice, $order, $source_field);
			$value = 'is_array($item)?$item[$matches[1]]:""';
			if(isset($total)){
				$obj->callbackParms = array($value, $total);
				$regCount = 0;
				$text = preg_replace_callback('#{{invoice_totals_'.$source_field.'_(.*?)}}#s', array(&$obj, 'checkReplaceResult'), $text, -1, $regCount);
				if($regCount>0){
					return $text;
				}
			}
		}

		return $text;
	}


	public function replaceItemText($item, $text, $obj){
		$replacements = array ('#{{invoice_item_data_(.*?)}}#s' => '$item->getData($matches[1])',
				'#{{parent_invoice_item_data_(.*?)}}#s' => '$item->getData($matches[1])'
				);
		 
		foreach ($replacements as $key => $value) {
			$obj->callbackParms = array($value, $item);
			$regCount = 0;
			$text = preg_replace_callback($key, array(&$obj, 'parent::checkReplaceResult'), $text, -1, $regCount);
			if($regCount>0){
				return $text;
			}
		}
		$text = parent::replaceItemText($item, $text, $obj);
		return $text;
	}

	public function replaceBundleItemText($item, $text, $obj){
		$replacements = array ('#{{invoice_item_data_(.*?)}}#s' => '$item->getData($matches[1])');
		 
		$itemInvoice = null;
		foreach ($obj->current_invoice->getItemsCollection() as $iitem) {
			if($iitem->getData('order_item_id') == $item->getId()){
				$itemInvoice = $iitem;
				continue;
			}
		}

		foreach ($replacements as $key => $value) {
			$obj->callbackParms = array($value, $itemInvoice);
			$regCount = 0;
			$text = preg_replace_callback($key, array(&$obj, 'parent::checkReplaceResult'), $text, -1, $regCount);
			if($regCount>0){
				return $text;
			}
		}
		$text = parent::replaceBundleItemText($item, $text, $obj);
		return $text;
	}

	public function isBundleItemIn($item){
		foreach ($this->current_invoice->getItemsCollection() as $iitem) {
			if($iitem->getData('order_item_id') == $item->getId()){
				return true;
			}
		}
		return false;
	}
}