<?php
/**
 * Sales Order PDF abstract model
 *
 * Generel class used by all types of pdf print - so used by both order, invoice, shipping and credit memo
 * The class if abstract and is extended by the different pdf types
 *
 * @category   ITCura
 * @package    Ext4mage_Html2pdf
 * @author     ITCura <info@ITCura.com>
 */
abstract class Ext4mage_Html2pdf_Model_Order_Pdf_Abstract extends Varien_Object
{
	const XPATH_CONFIG_SETTINGS_IS_ACTIVE		= 'html2pdf/settings/is_active';
	const XPATH_CONFIG_SETTINGS_IS_DEV         	= 'html2pdf/settings/is_dev';
	const XPATH_CONFIG_SETTINGS_LICENSE 		= 'html2pdf/settings/license_code';

	abstract public function getPdf();

	abstract public function isBundleItemIn($item);

	public $callbackParms;
	protected $pdf;

	protected function _isActive() {
		return Mage::getStoreConfig(self::XPATH_CONFIG_SETTINGS_IS_ACTIVE);
	}

	protected function _isDevMode() {
		return Mage::getStoreConfig(self::XPATH_CONFIG_SETTINGS_IS_DEV);
	}

	protected function _beforeGetPdf() {
		$translate = Mage::getSingleton('core/translate');
		/* @var $translate Mage_Core_Model_Translate */
		$translate->setTranslateInline(false);
	}

	protected function _afterGetPdf() {
		$translate = Mage::getSingleton('core/translate');
		/* @var $translate Mage_Core_Model_Translate */
		$translate->setTranslateInline(true);
	}

	protected function _initRenderer($type)
	{
// 		$node = Mage::getConfig()->getNode('global/pdf/'.$type);
// 		$nodeChild = $node->children();
// 		foreach ($nodeChild as $renderer) {
// 			$renderName = $renderer->getName();
// 			$this->_renderers[$renderName] = array(
//                 'model'     => (string)$renderer,
//                 'renderer'  => null
// 			);
// 		}
	}
	
	/**
	 * Function for saving file
	 * @param name string - the start of the name to save as
	 * @return filename string - the full name as the file has been saved under
	 */
	protected function _getFileName($name){
		$pdfFolder = 'html2pdf'.DS.'pdfs'.DS;
		$base_path = Mage::getBaseDir('media');
		$folder_path = $base_path.DS.$pdfFolder;
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < 15; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		$name = preg_replace("/[^A-Za-z0-9]/", "_", $name);
		return $folder_path.$name.'_'.$randomString.'.pdf';
	}

	/**
	 * Get all option values from a general item
	 *
	 * @param invoice_item $item
	 * @return array of options
	 */
	public function getItemOptions($item) {
		$result = array();
		if(method_exists($item, 'getOrderItem')){
			$orderItem = $item->getOrderItem();
		}else{
			$orderItem = $item;
		}
		if ($options = $orderItem->getProductOptions()) {
			if (isset($options['options'])) {
				$result = array_merge($result, $options['options']);
			}
			if (isset($options['additional_options'])) {
				$result = array_merge($result, $options['additional_options']);
			}
			if (isset($options['attributes_info'])) {
				$result = array_merge($result, $options['attributes_info']);
			}
			if (isset($options['bundle_options'])) {
				$result = array_merge($result, $options['bundle_options']);
			}
			if (isset($options['bundle_selection_attributes'])) {
				$result = array_merge($result, unserialize($options['bundle_selection_attributes']));
			}
		}
		return $result;
	}

	/**
	 * Draw the items table used in the pdf - so header and all items will be initiated from this function
	 *
	 * @param array<invoice_item> $items
	 * @param html2pdf_template $template
	 * @param mag_order $order
	 * @return html $itemsHtml
	 */
	public function drawItems($items, $template, $order){
		//    	exit;
		$columns = $template->getTable()->getColumns();
		 
		$itemsHtml = "";
		$itemsHtml = '<table border="0" cellspacing="'.$template->getTable()->getTableCellspacing().'px" cellpadding="'.$template->getTable()->getTableCellpadding().'px" width="'.$template->getTable()->getTableWidth().'%" style="table-layout:fixed;'.$template->getTable()->getTableStyle().'"><thead><tr style="'.$template->getTable()->getHeaderStyle().'">';
		foreach ($columns as $column){
			$itemsHtml .= '<th width="'.$column->getWidth().'%" style="'.$column->getCustomStyle().'">'.$column->getTitle().'</th>';
		}
		$itemsHtml .= '</tr></thead>';

		$rowOdd = true;
		foreach ($items as $item){
			if(method_exists($item, 'getOrderItem')){
				$orderItem = $item->getOrderItem();
			}else{
				$orderItem = $item;
			}
			if ($orderItem->getParentItem()) {
				continue;
			}
			$rowStyle = (($rowOdd = !$rowOdd)?$template->getTable()->getOddRowStyle():$template->getTable()->getEvenRowStyle());
			$itemsHtml .= '<tr style="'.$rowStyle.'" border="0"><td border="0" colspan="'.count($columns).'" width="100%" style="'.$rowStyle.'">';
			switch ($orderItem->getProductType()){
				case "bundle":
					$itemsHtml .= $this->drawBundleItem($item, $columns, $order);
					break;
				case "downloadable":
					$itemsHtml .= $this->drawDownloadableItem($item, $columns, $order);
					break;
				case "virtual":
					$itemsHtml .= $this->drawVirtualItem($item, $columns, $order);
					break;
				default:
					$itemsHtml .= $this->drawDefaultItem($item, $columns, $order);
			}
			$itemsHtml .= '</td></tr>';
		}
		$itemsHtml .= '</table>';
		return $itemsHtml;
	}

	/**
	 * Draws a single default item - this is the default method. It will generat the html for a row in the tabel
	 */
	public function drawDefaultItem($item, $columns, $order){
		$rowHtml = '<table border="0" cellspacing="0" cellpadding="0" width="100%" style="table-layout:fixed;"><tr>';
		foreach ($columns as $column){
			$itemHtml = "&nbsp;";
			if($column->getValue() != null){
				$itemHtml = $this->convertText($item, $order, $column->getValue(), 'replaceItemText');
				$itemHtml = $this->convertText($order, $order, $itemHtml, 'replaceText');
			}
			$rowspan = $column->getRowSpan() == 1?' rowspan="100"':'';
			$rowHtml .= '<td width="'.$column->getWidth().'%" style="'.$column->getCustomStyle().'"'.$rowspan.'>'.
			$itemHtml.'</td>';
		}
		$rowHtml .= "</tr>".$this->drawItemOptions($item, $columns, $order)."</table>";
		return $rowHtml;
	}

	/**
	 * Draws a single bundle item. It will generat the html for a row in the tabel
	 */
	public function drawBundleItem($item, $columns, $order){
		$rowHtml = '<table border="0" cellspacing="0" cellpadding="0" width="100%" style="table-layout:fixed;"><tr>';
		$showAnyColumns = false;
		$rowItemHtml = "";
		foreach ($columns as $column){
			$itemHtml = "&nbsp;";
			if($column->getBundleShow() == 2){
				$showAnyColumns = true;
				if($column->getBundleProduct() != null){
					$itemHtml = $this->convertText($item, $order, $column->getBundleProduct(), 'replaceItemText');
					$itemHtml = $this->convertText($order, $order, $itemHtml, 'replaceText');
				}
			}
			$rowspan = $column->getRowSpan() == 1?' rowspan="100"':'';
			$rowItemHtml .= '<td width="'.$column->getWidth().'%" style="'.$column->getCustomStyle().'"'.$rowspan.'>'.$itemHtml.'</td>';
				
		}
		$rowHtml .= $showAnyColumns ? $rowItemHtml: "";
		$rowHtml .= "</tr>";
		$rowHtml .= $showAnyColumns ? $this->drawBundelOptions($item, $columns, $order): "";
		$rowHtml .= "</table>";
		return $rowHtml;
	}

	/**
	 * Draws a single downloadable item. It will generat the html for a row in the tabel
	 */
	public function drawDownloadableItem($item, $columns, $order){
		$rowHtml = '<table border="0" cellspacing="0" cellpadding="0" width="100%" style="table-layout:fixed;"><tr>';
		$showAnyColumns = false;
		$rowItemHtml = "";
		foreach ($columns as $column){
			$itemHtml = "&nbsp;";
			if($column->getDownloadShow() == 2){
				$showAnyColumns = true;
				if($column->getDownloadValue() != null){
					$itemHtml = $this->convertText($item, $order, $column->getDownloadValue(), 'replaceItemText');
					$itemHtml = $this->convertText($order, $order, $itemHtml, 'replaceText');
				}
			}else if($column->getDownloadShow() == 1){
				$showAnyColumns = true;
				if($column->getValue() != null){
					$itemHtml = $this->convertText($item, $order, $column->getValue(), 'replaceItemText');
					$itemHtml = $this->convertText($order, $order, $itemHtml, 'replaceText');
				}
			}
			$rowspan = $column->getRowSpan() == 1?' rowspan="100"':'';
			$rowItemHtml .= '<td width="'.$column->getWidth().'%" style="'.$column->getCustomStyle().'"'.$rowspan.'>'.$itemHtml.'</td>';
		}
		$rowHtml .= $showAnyColumns ? $rowItemHtml : "";
		$rowHtml .= "</tr>";
		$rowHtml .= $showAnyColumns ? $this->drawDownloadableOptions($item, $columns, $order) : "";
		$rowHtml .= "</table>";
		return $rowHtml;
	}

	/**
	 * Draws a single virtual item. It will generat the html for a row in the tabel
	 */
	public function drawVirtualItem($item, $columns, $order){
		$rowHtml = '<table border="0" cellspacing="0" cellpadding="0" width="100%" style="table-layout:fixed;"><tr>';
		$showAnyColumns = false;
		foreach ($columns as $column){
			$itemHtml = "&nbsp;";
			if($column->getVirtualShow() == 2){
				$showAnyColumns = true;
				if($column->getValue() != null){
					$itemHtml = $this->convertText($item, $order, $column->getVirtualValue(), 'replaceItemText');
					$itemHtml = $this->convertText($order, $order, $itemHtml, 'replaceText');
				}
			}else if($column->getVirtualShow() == 1){
				$showAnyColumns = true;
				if($column->getValue() != null){
					$itemHtml = $this->convertText($item, $order, $column->getValue(), 'replaceItemText');
					//$itemHtml = $this->convertText($order, $order, $itemHtml, 'replaceText');
				}
			}
			$rowspan = $column->getRowSpan() == 1?' rowspan="100"':'';
			$rowItemHtml .= '<td width="'.$column->getWidth().'%" style="'.$column->getCustomStyle().'"'.$rowspan.'>'.$itemHtml.'</td>';
		}
		$rowHtml .= $showAnyColumns ? $rowItemHtml : "";
		$rowHtml .= "</tr>";
		$rowHtml .= $showAnyColumns? $this->drawVirtualOptions($item, $columns, $order): "";
		$rowHtml .= "</table>";
		return $rowHtml;
	}

	/**
	 * Draws options of an default item - makes a new row under the relevent item
	 */
	public function drawItemOptions($item, $columns, $order){
		// if item has options print
		$itemHtml = "";
		$options = $this->getItemOptions($item);
		if ($options) {
			foreach ($options as $option) {
				$itemHtmlToAdd = '<tr>';
				$totalOptionHtmlSize = 0; 
				foreach ($columns as $column){
					if($column->getRowSpan() == 0){
						$optionHtml = '&nbsp;';
						if($column->getOptionValue() != null){
							$optionHtml = $this->convertText($option, $order, $column->getOptionValue(), 'replaceItemOptionText');
							$optionHtml = $this->convertText($item, $order, $optionHtml, 'replaceItemText');
							$optionHtml = $this->convertText($order, $order, $optionHtml, 'replaceText');
						}
	
						$itemHtmlToAdd .= '<td width="'.$column->getWidth().'%" style="'.$column->getCustomStyle().'" border="0">'.$optionHtml.'</td>';
						$totalOptionHtmlSize += strlen($optionHtml);
					}
				}
				$itemHtmlToAdd .= '</tr>';
				if($totalOptionHtmlSize>0)
					$itemHtml .= $itemHtmlToAdd;
			}
		}
		return $itemHtml;
	}

	/**
	 * Draws options of an bundle item - makes a new row under the relevent item with all the product in the bundle
	 */
	public function drawBundelOptions($item, $columns, $order){
		// if item has options print
		$itemHtml = "";
		$options = array();
		 
		if(method_exists($item, 'getOrderItem')){
			$orderItem = $item->getOrderItem();
		}else{
			$orderItem = $item;
		}
		$children = $orderItem->getChildrenItems();
		foreach ($children as $child){
			$bundleAttr = $this->getItemOptions($child);
			$optionId = $bundleAttr['option_id'];
			if(!array_key_exists($optionId, $options)){
				$options[$optionId] = $bundleAttr;
			}else{
				$options[$optionId]['price'] = $options[$optionId]['price'] + $bundleAttr['price'];
			}
			$options[$optionId]['value'][] = $child;
		}

		foreach ($options as $key => $optionGroup) {
			$hasNoActiveItems = true;
			foreach ($optionGroup['value'] as $optionItem){
				if($this->isBundleItemIn($optionItem)){
					$hasNoActiveItems = false;
					continue;
				}
			}
			if($hasNoActiveItems) unset($options[$key]);
		}

		if ($options) {
			foreach ($options as $optionGroup) {
				$itemHtmlToAdd = '<tr>';
				$totalOptionHtmlSize = 0;
				foreach ($columns as $column){
					if($column->getRowSpan() == 0){
						if($column->getBundleShow() == 2){
							$optionHtml = '&nbsp;';
							if($column->getBundleItemGroup() != null){
								$items = array(1=>$optionGroup, 2=>$item, 3=>$order);
								$functions = array(1=>'replaceBundleItemGroupText', 2=>'replaceItemText', 3=>'replaceText');
								$optionHtml = $column->getBundleItemGroup();
	// 							$optionHtml = $this->convertText($optionGroup, $order, $column->getBundleItemGroup(), 'replaceBundleItemGroupText');
	// 							$optionHtml = $this->convertText($item, $order, $optionHtml, 'replaceItemText');
	// 							$optionHtml = $this->convertText($order, $order, $optionHtml, 'replaceText');
								$optionHtml = $this->convertText($items, $order, $optionHtml, $functions);
							}
							$itemHtmlToAdd .= '<td width="'.$column->getWidth().'%" style="'.$column->getCustomStyle().'" border="0">'.$optionHtml.'</td>';
							$totalOptionHtmlSize += strlen($optionHtml);
							
						}
					}
				}
				$itemHtmlToAdd .= '</tr>';
				if($totalOptionHtmlSize>0)
					$itemHtml .= $itemHtmlToAdd;
				
				foreach ($optionGroup['value'] as $optionItem){
					if($this->isBundleItemIn($optionItem)){
						$itemHtmlToAdd = '<tr>';
						$totalOptionHtmlSize = 0;
						foreach ($columns as $column){
							if($column->getRowSpan() == 0){
								if($column->getBundleShow() == 2){
									$optionHtml = '&nbsp;';
									if($column->getBundleItem() != null){
										$items = array(1=>$optionItem, 2=>$optionGroup, 3=>$item, 4=>$order);
										$functions = array(1=>'replaceBundleItemText', 2=>'replaceBundleItemGroupText', 3=>'replaceItemText', 4=>'replaceText');
										$optionHtml = $column->getBundleItem();
	// 									$optionHtml = $this->convertText($optionItem, $order, $column->getBundleItem(), 'replaceBundleItemText');
	// 									$optionHtml = $this->convertText($optionGroup, $order, $optionHtml, 'replaceBundleItemGroupText');
	// 									$optionHtml = $this->convertText($item, $order, $optionHtml, 'replaceItemText');
	// 									$optionHtml = $this->convertText($order, $order, $optionHtml, 'replaceText');
										$optionHtml = $this->convertText($items, $order, $optionHtml, $functions);
									}
									$itemHtmlToAdd .= '<td width="'.$column->getWidth().'%" style="'.$column->getCustomStyle().'" border="0">'.$optionHtml.'</td>';
									$totalOptionHtmlSize += strlen($optionHtml);
								}
							}
						}
						$itemHtmlToAdd .= '</tr>';
						if($totalOptionHtmlSize>0)
							$itemHtml .= $itemHtmlToAdd;
					}
				}
			}
		}
		return $itemHtml;
	}

	/**
	 * Draws options of an download item - makes a new row under the relevent item
	 */
	public function drawDownloadableOptions($item, $columns, $order){
		// if item has options print
		$itemHtml = "";
		$options = $this->getItemOptions($item);
		if ($options) {
			foreach ($options as $option) {
				$itemHtmlToAdd = '<tr>';
				$totalOptionHtmlSize = 0; 
				foreach ($columns as $column){
					if($column->getRowSpan() == 0){
						if($column->getDownloadShow() == 2){
							$optionHtml = '&nbsp;';
							if($column->getDownloadOptionValue() != null){
								$optionHtml = $this->convertText($option, $order, $column->getDownloadOptionValue(), 'replaceItemOptionText');
								$optionHtml = $this->convertText($item, $order, $optionHtml, 'replaceItemText');
								$optionHtml = $this->convertText($order, $order, $optionHtml, 'replaceText');
							}
							$itemHtmlToAdd .= '<td width="'.$column->getWidth().'%" style="'.$column->getCustomStyle().'" border="0">'.$optionHtml.'</td>';
							$totalOptionHtmlSize += strlen($optionHtml);
						}else if($column->getDownloadShow() == 1){
							$optionHtml = '&nbsp;';
							if($column->getOptionValue() != null){
								$optionHtml = $this->convertText($option, $order, $column->getOptionValue(), 'replaceItemOptionText');
								$optionHtml = $this->convertText($item, $order, $optionHtml, 'replaceItemText');
								$optionHtml = $this->convertText($order, $order, $optionHtml, 'replaceText');
							}
							$itemHtmlToAdd .= '<td width="'.$column->getWidth().'%" style="'.$column->getCustomStyle().'" border="0">'.$optionHtml.'</td>';
							$totalOptionHtmlSize += strlen($optionHtml);
						}
					}
				}
				$itemHtmlToAdd .= '</tr>';
				if($totalOptionHtmlSize>0)
					$itemHtml .= $itemHtmlToAdd;
			}
		}
		return $itemHtml;
	}

	/**
	 * Draws options of an virtual item - makes a new row under the relevent item
	 */
	public function drawVirtualOptions($item, $columns, $order){
		// if item has options print
		$itemHtml = "";
		$options = $this->getItemOptions($item);
		if ($options) {
			foreach ($options as $option) {
				$itemHtmlToAdd = '<tr>';
				$totalOptionHtmlSize = 0; 
				foreach ($columns as $column){
					if($column->getRowSpan() == 0){
						if($column->getVirtualShow() == 2){
							$optionHtml = '&nbsp;';
							if($column->getVirtualOptionValue() != null){
								$optionHtml = $this->convertText($option, $order, $column->getVirtualOptionValue(), 'replaceItemOptionText');
								$optionHtml = $this->convertText($item, $order, $optionHtml, 'replaceItemText');
								$optionHtml = $this->convertText($order, $order, $optionHtml, 'replaceText');
							}
							$itemHtmlToAdd .= '<td width="'.$column->getWidth().'%" style="'.$column->getCustomStyle().'" border="0">'.$optionHtml.'</td>';
							$totalOptionHtmlSize += strlen($optionHtml);
						}else if($column->getVirtualShow() == 1){
							$optionHtml = '&nbsp;';
							if($column->getOptionValue() != null){
								$optionHtml = $this->convertText($option, $order, $column->getOptionValue(), 'replaceItemOptionText');
								$optionHtml = $this->convertText($item, $order, $optionHtml, 'replaceItemText');
								$optionHtml = $this->convertText($order, $order, $optionHtml, 'replaceText');
							}
							$itemHtmlToAdd .= '<td width="'.$column->getWidth().'%" style="'.$column->getCustomStyle().'" border="0">'.$optionHtml.'</td>';
							$totalOptionHtmlSize += strlen($optionHtml);
						}
					}
				}
				$itemHtmlToAdd .= '</tr>';
				if($totalOptionHtmlSize>0)
					$itemHtml .= $itemHtmlToAdd;
			}
		}
		return $itemHtml;
	}

	/**
	* Draw cross sell items in the pdf
	*
	* @param array<item> $items
	* @param html2pdf_template $template
	* @param mag_order $order
	* @return html $crossHtml
	*/
	public function drawCross($items, $template, $order){
		if($template->getCrossSellText() == null){
			return "";
		}
		
		$text = $template->getCrossSellText()->getTextcontent();
		
		if($text == null || $text == ""){
			return "";
		}
		
		$numCross = $template->getCrossSellNum();
		if($numCross != null && $numCross > 0)
			$crossItems = Mage::helper('html2pdf/crosssell')->getItems($items, $numCross);
		else
			$crossItems = Mage::helper('html2pdf/crosssell')->getItems($items);
		
		if(!isset($crossItems) || count($crossItems)==0){
			return "";
		}
		
		$ptn =	"#{{cross_(\d?)_start}}(.*?){{cross_(\d?)_end}}#s";
		if(preg_match_all($ptn, $text, $matches)>0){
			$matchNum = 0;
			foreach($matches[0] as $match){
				$itemNum = $matches[1][$matchNum];
				if(isset($crossItems[$itemNum])){
					$crossItemHtml = html_entity_decode($matches[2][$matchNum]);
					$crossItemHtml = $this->convertText($crossItems[$itemNum], $order, $crossItemHtml, 'replaceCrossText');
					$crossItemHtml = $this->convertText($order, $order, $crossItemHtml, 'replaceText');
					
				}else{
					$crossItemHtml = "";
				}
				$text = str_replace($match, $crossItemHtml, $text);
				$matchNum++;
			}
		}
		return $text;		
	}

	/**
	* Function for replacing with values that is defined in the cross sell section 
	*/
	public function replaceCrossText($item, $text, $obj){
		$replacements = array (
	    		'#{{cross_data_(.*?)}}#s' => '$item->getData($matches[1])' 
		);
			
		$newText = Mage::helper('cms')->getBlockTemplateProcessor()->filter($text);
		if (!strcasecmp($text, $newText) == 0) {
			return $newText;
		}
			
		foreach ($replacements as $key => $value) {
			$obj->callbackParms = array($value, $item);
			$regCount = 0;
			$text = preg_replace_callback($key, array(&$obj, 'checkReplaceResult'), $text, -1, $regCount);
			if($regCount>0){
				return $text;
			}
		}
			
		return $text;
	}
	
	
	/**
	 * generel function for replacing input text related to order with the correct value, could also be implemented in sub class
	 */
	public function replaceText($order, $text, $obj){
		$replacements = array (
    		'#{{order_data_(.*?)}}#s' => '$item->getData($matches[1])', 
			'#{{order_shipping_data_(.*?)}}#s' => 'is_object($item->getShippingAddress())?$item->getShippingAddress()->getData($matches[1]):""', 
			'#{{order_shipping_street_data_(.*?)}}#s' => 'is_object($item->getShippingAddress())?${1|${1}=$item->getShippingAddress()->getStreet()}[$matches[1]]:""', 
			'#{{order_shipping_country_name}}#s' => 'is_object($item->getShippingAddress())?$item->getShippingAddress()->getCountryModel()->getName():""', 
			'#{{order_billing_data_(.*?)}}#s' => 'is_object($item->getBillingAddress())?$item->getBillingAddress()->getData($matches[1]):""', 
			'#{{order_billing_street_data_(.*?)}}#s' => 'is_object($item->getBillingAddress())?${1|${1}=$item->getBillingAddress()->getStreet()}[$matches[1]]:""', 
			'#{{order_billing_country_name}}#s' => 'is_object($item->getBillingAddress())?$item->getBillingAddress()->getCountryModel()->getName():""', 
    		'#{{order_customer_group}}#'=>'is_object(Mage::getModel("customer/group")->load((int)$item->getCustomerGroupId()))?Mage::getModel("customer/group")->load((int)$item->getCustomerGroupId())->getCode():""',
			'#{{order_shipping_description}}#'=>'$item->getShippingDescription()',
			'#{{order_payment_block}}#'=>'is_object($item->getPayment())?$item->getPayment()->getMethodInstance()->getTitle():""',
			'#{{order_base_currency_data_(.*?)}}#s'=>'is_object($item->getBaseCurrency())?$item->getBaseCurrency()->getData($matches[1]):""',
			'#{{order_base_total_due}}#'=>'$item->getBaseTotalDue()',
			'#{{order_created_full}}#'=>'$item->getCreatedAtFormated("full")',
			'#{{order_created_long}}#'=>'$item->getCreatedAtFormated("long")',
			'#{{order_created_medium}}#'=>'$item->getCreatedAtFormated("medium")',
			'#{{order_created_short}}#'=>'$item->getCreatedAtFormated("short")',
			'#{{order_email_customer_note}}#'=>'$item->getEmailCustomerNote()',
			'#{{order_is_not_virtual}}#'=>'$item->getIsNotVirtual()',
			'#{{order_currency_data_(.*?)}}#s'=>'is_object($item->getOrderCurrency())?$item->getOrderCurrency()->getData($matches[1]):""',
			'#{{order_payment_data_(.*?)}}#s'=>'is_object($item->getPayment())?$item->getPayment()->getData($matches[1]):""',
			'#{{order_payment_auth_trans_data_(.*?)}}#s'=>'is_object($item->getPayment()->getAuthorizationTransaction())?$item->getPayment()->getAuthorizationTransaction()->getData($matches[1]):""',
			'#{{order_real_id}}#'=>'$item->getRealOrderId()',
			'#{{order_shipping_carrier_code}}#'=>'is_object($item->getShippingCarrier())?$item->getShippingCarrier()->getCarrierCode():""',
			'#{{order_shipping_carrier_data_(.*?)}}#s'=>'is_object($item->getShippingCarrier())?$item->getShippingCarrier()->getData($matches[1]):""',
			'#{{order_status_label}}#'=>'$item->getStatusLabel()',
			'#{{order_store_data_(.*?)}}#s'=>'is_object($item->getStore())?$item->getStore()->getData($matches[1]):""',
			'#{{order_store_url}}#'=>'$item->getStore()->getUrl()',
			'#{{order_store_group_data_(.*?)}}#s'=>'is_object($item->getStore()->getGroup())?$item->getStore()->getGroup()->getData($matches[1]):""',
			'#{{order_store_base_url}}#'=>'$item->getStore()->getBaseUrl()',
    		'#{{order_num_invoices}}#' => '$item->hasInvoices()',
    		'#{{order_num_shipments}}#' => '$item->hasShipments()',
    		'#{{order_num_creditmemos}}#' => '$item->hasCreditmemos()',
    		'#{{customer_data_(.*?)}}#s'=>"is_object(Mage::getModel('customer/customer')->load(\$item->getCustomerId()))?Mage::getModel('customer/customer')->load(\$item->getCustomerId())->getData(\$matches[1]):\"\"",
    		'#{{customer_billing_data_(.*?)}}#s'=>"is_object(Mage::getModel('customer/customer')->load(\$item->getCustomerId())->getDefaultBillingAddress())?Mage::getModel('customer/customer')->load(\$item->getCustomerId())->getDefaultBillingAddress()->getData(\$matches[1]):\"\"",
    		'#{{customer_shipping_data_(.*?)}}#s'=>"is_object(Mage::getModel('customer/customer')->load(\$item->getCustomerId())->getDefaultShippingAddress())?Mage::getModel('customer/customer')->load(\$item->getCustomerId())->getDefaultShippingAddress()->getData(\$matches[1]):\"\"",
			'#{{order_gift_data_(.*?)}}#s'=>"is_object(Mage::getSingleton('giftmessage/message')->load(\$item->getGiftMessageId()))?Mage::getSingleton('giftmessage/message')->load(\$item->getGiftMessageId())->getData(\$matches[1]):\"\"",
			'#{{order_comments_last_data_(.*?)}}#s' => 'is_object(end($item->getVisibleStatusHistory()))?reset($item->getVisibleStatusHistory())->getData($matches[1]):""',
		);
		 
		$newText = Mage::helper('cms')->getBlockTemplateProcessor()->filter($text);
		if (!strcasecmp($text, $newText) == 0) {
			return $newText;
		}
		 
		foreach ($replacements as $key => $value) {
			$obj->callbackParms = array($value, $order);
			$regCount = 0;
			$text = preg_replace_callback($key, array(&$obj, 'checkReplaceResult'), $text, -1, $regCount);
			if($regCount>0){
				return $text;
			}
		}
		 
		if(preg_match ('#{{order_totals_(.*?)}}#s',$text, $match) == 1){
			list($source_field, $totalElement) = explode('_', $match[1], 2);
			$total = Mage::helper('html2pdf')->getTotalForDisplaying($order, $order, $source_field);
			$value = 'is_array($item)?$item[$matches[1]]:""';
			if(isset($total)){
				$obj->callbackParms = array($value, $total);
				$regCount = 0;
				$text = preg_replace_callback('#{{order_totals_'.$source_field.'_(.*?)}}#s', array(&$obj, 'checkReplaceResult'), $text, -1, $regCount);
				if($regCount>0){
					return $text;
				}
			}
		}
		 
		return $text;
	}

	/**
	 * generel function for replacing input text with the correct value, could also be implemented in sub class
	 */
	public function replaceItemText($item, $text, $obj){
		if(method_exists($item, 'getOrderItem')){
			$orderItem = $item->getOrderItem();
		}else{
			$orderItem = $item;
		}

		$newText = Mage::helper('cms')->getBlockTemplateProcessor()->filter($text);
		if (!strcasecmp($text, $newText) == 0) {
			return $newText;
		}

		$replacements = array (
    		'#{{item_data_(.*?)}}#s' => '$item->getData($matches[1])', 
    		'#{{product_data_(.*?)}}#s' => "is_object(Mage::getModel('catalog/product')->load(\$item->getData('product_id')))?Mage::getModel('catalog/product')->load(\$item->getData('product_id'))->getData(\$matches[1]):\"\"",
    		'#{{item_status}}#' => '$item->getStatus()',
			'#{{item_gift_data_(.*?)}}#s'=>"is_object(Mage::getSingleton('giftmessage/message')->load(\$item->getGiftMessageId()))?Mage::getSingleton('giftmessage/message')->load(\$item->getGiftMessageId())->getData(\$matches[1]):\"\"",
			'#{{parent_item_data_(.*?)}}#s' => '$item->getData($matches[1])'
		);
		 
		foreach ($replacements as $key => $value) {
			$obj->callbackParms = array($value, $orderItem);
			$regCount = 0;
			$text = preg_replace_callback($key, array(&$obj, 'checkReplaceResult'), $text, -1, $regCount);
			if($regCount>0){
				return $text;
			}
		}
		
		$optionText = $this->replaceItemOptionValueText($item, $text, $obj);
		if (!strcasecmp($text, $optionText) == 0) {
			return $optionText;
		}
		
		if(is_object(Mage::getModel('catalog/product')->load($orderItem->getData('product_id')))){
			$prodItem = Mage::getModel('catalog/product')->load($orderItem->getData('product_id'));
			$attrText = $this->replaceItemAttrText($prodItem, $text, $obj, $orderItem->getData('product_id'));
			if (!strcasecmp($text, $attrText) == 0) {
				return $attrText;
			}
		}
		
		return $text;
	}

	/**
	 * generel function for replacing input text with the correct value, could also be implemented in sub class
	 */
	public function replaceItemAttrText($item, $text, $obj, $product_id){
		$replacements = array (
			'#{{product_attr_data_(.*?)}}#s' => "isset(\$item[\$matches[1]])?\$item[\$matches[1]]->getFrontend()->getValue(Mage::getModel('catalog/product')->load(".$product_id.")):\"\""
		);
			
		$newText = Mage::helper('cms')->getBlockTemplateProcessor()->filter($text);
		if (!strcasecmp($text, $newText) == 0) {
			return $newText;
		}
	
		$attrs = $item->getAttributes();
	
		foreach ($replacements as $key => $value) {
			$obj->callbackParms = array($value, $attrs);
			$regCount = 0;
			$text = preg_replace_callback($key, array(&$obj, 'checkReplaceResult'), $text, -1, $regCount);
			if($regCount>0){
				return $text;
			}
		}
		return $text;
	}
	
	/**
	 * generel function for replacing input text with the correct value, could also be implemented in sub class
	 */
	public function replaceItemOptionText($option, $text, $obj){
		$replacements = array (
    		'#{{item_option_data_(.*?)}}#s' => '$item[$matches[1]]'
		);
		 
		$newText = Mage::helper('cms')->getBlockTemplateProcessor()->filter($text);
		if (!strcasecmp($text, $newText) == 0) {
			return $newText;
		}

		foreach ($replacements as $key => $value) {
			$obj->callbackParms = array($value, $option);
			$regCount = 0;
			$text = preg_replace_callback($key, array(&$obj, 'checkReplaceResult'), $text, -1, $regCount);
			if($regCount>0){
				return $text;
			}
		}
		return $text;
	}

	
	/**
	 * function for replacing input text with the correct value, this is for option values
	 */
	public function replaceItemOptionValueText($item, $text, $obj){
		$replacements = array (
			'#{{item_option_value_(.*?)}}#s'
		);
		
		$options = $this->getItemOptions($item);

		foreach ($replacements as $key) {
			if(preg_match($key, $text, $matches)==1){
				foreach ($options as $option) {
					$labelKey = str_replace(' ', '_', strtolower(trim($option['label'])));
					if($labelKey==$matches[1]){
						return $option['value'];
					}
				}
				return "";
			}
		}
		return $text;
	}
	
	/**
	 * generel function for replacing input text with the correct value, could also be implemented in sub class
	 */
	public function replaceBundleItemGroupText($optionGroup, $text, $obj){
		$replacements = array (
    		'#{{item_bundle_group_data_(.*?)}}#s' => '$item[$matches[1]]'
		);
		 
		$newText = Mage::helper('cms')->getBlockTemplateProcessor()->filter($text);
		if (!strcasecmp($text, $newText) == 0) {
			return $newText;
		}
		 
		foreach ($replacements as $key => $value) {
			$obj->callbackParms = array($value, $optionGroup);
			$regCount = 0;
			$text = preg_replace_callback($key, array(&$obj, 'checkReplaceResult'), $text, -1, $regCount);
			if($regCount>0){
				return $text;
			}
		}
		return $text;
	}

	/**
	 * generel function for replacing input text with the correct value, could also be implemented in sub class
	 */
	public function replaceBundleItemText($optionItem, $text, $obj){
		$replacements = array (
    		'#{{item_bundle_data_(.*?)}}#s' => '$item->getData($matches[1])',
			'#{{item_bundle_gift_data_(.*?)}}#s'=>"is_object(Mage::getSingleton('giftmessage/message')->load(\$item->getGiftMessageId()))?Mage::getSingleton('giftmessage/message')->load(\$item->getGiftMessageId())->getData(\$matches[1]):\"\"",
		);
		$newText = Mage::helper('cms')->getBlockTemplateProcessor()->filter($text);
		if (!strcasecmp($text, $newText) == 0) {
			return $newText;
		}
		 
		foreach ($replacements as $key => $value) {
			$obj->callbackParms = array($value, $optionItem);
			$regCount = 0;
			$text = preg_replace_callback($key, array(&$obj, 'checkReplaceResult'), $text, -1, $regCount);
			if($regCount>0){
				return $text;
			}
		}
		return $text;
	}

	/**
	 * Do php code from input evaluation - this should be used with great care
	 */
	public function doEval($text){
		$text = str_replace("##", "", $text);
		$text = str_replace("{{", "", $text);
		$text = str_replace("}}", "", $text);
		if($this->_isDevMode()){
			echo "<br />Inline code before transformation:<br><i>".$text."</i>";
			$text = @eval('return ('.$text.');');
			$text = str_replace('$','\$',$text);
			echo "<br />Inline code after transformation:<br><i>".$text."</i><br />";
		}else{
			ob_start();
			$text = @eval('return ('.$text.');');
			$text = str_replace('$','\$',$text);
			ob_end_clean();
		}
		return $text;
			
	}

	/**
	 * Do formating of an input text element using the build in magento functions
	 */
	public function getFormating($text, $prefix = ''){
		if(preg_match("/".$prefix."format_/i",$text)){
			$formatingRegxCodes = array(
						'format_price_txt', 
						'format_date_short',
						'format_date_medium',
						'format_date_long',
						'format_date_full',
						'format_time_short',
						'format_time_medium',
						'format_time_long',
						'format_time_full',
						'format_currency',
						'format_price',
						'format_base_price',
						'format_convert_price',
						'format_integer',
						'format_number_1',
						'format_number_2',			
						'format_number_3',			
						'format_number_4',
						'format_barcode_(.*?)'			
			);
			foreach ($formatingRegxCodes as $formatingRegxCode) {
				if(preg_match("/".$prefix.$formatingRegxCode." /i",$text,$formatingMatch)){
					return array ($formatingMatch[0], preg_replace("/".$formatingRegxCode." /i", '', $text));
				}
			}
		}
		return array(false,$text);
	}

	/**
	 * Formating all text - some char need this to be rendered correctly
	 */
	public function formatText($textFormat, $text, $order){
		$search  = array('$');
		$replace = array('&#36;');
		$textFormat = str_replace("#", "", $textFormat);
		if(strncasecmp(trim($textFormat), 'format_barcode', 14) == 0 || strncasecmp(trim($textFormat), '##format_barcode', 16) == 0){
			$notUse1 = $notUse2 = $type = $barHight = $barWidth = $align = $fgcolor = $showText = $fontsize = $font = null;
			list($notUse1,$notUse2,$type,$barHight,$barWidth,$align,$fgcolor,$showText,$fontsize,$font) = array_pad(explode("_", trim($textFormat)), 10, null);
			return $this->pdf->generateBarcodeHtml($text,$type,$fgcolor,$showText,$fontsize,$font,$barHight,$barWidth,$align);
		}elseif(strcasecmp(trim($textFormat), 'format_price_txt') == 0){
			return str_replace($search, $replace, $order->formatPriceTxt($text));
		}elseif(strcasecmp(trim($textFormat), 'format_date_short') == 0 && strlen($text)>3){
			return str_replace($search, $replace, Mage::helper('core')->formatDate($text, 'short'));
		}elseif(strcasecmp(trim($textFormat), 'format_date_medium') == 0 && strlen($text)>3){
			return str_replace($search, $replace, Mage::helper('core')->formatDate($text, 'medium'));
		}elseif(strcasecmp(trim($textFormat), 'format_date_long') == 0 && strlen($text)>3){
			return str_replace($search, $replace, Mage::helper('core')->formatDate($text, 'long'));
		}elseif(strcasecmp(trim($textFormat), 'format_date_full') == 0 && strlen($text)>3){
			return str_replace($search, $replace, Mage::helper('core')->formatDate($text, 'full'));
		}elseif(strcasecmp(trim($textFormat), 'format_time_short') == 0 && strlen($text)>3){
			return str_replace($search, $replace, Mage::helper('core')->formatTime($text, 'short'));
		}elseif(strcasecmp(trim($textFormat), 'format_time_medium') == 0 && strlen($text)>3){
			return str_replace($search, $replace, Mage::helper('core')->formatTime($text, 'medium'));
		}elseif(strcasecmp(trim($textFormat), 'format_time_long') == 0 && strlen($text)>3){
			return str_replace($search, $replace, Mage::helper('core')->formatTime($text, 'long'));
		}elseif(strcasecmp(trim($textFormat), 'format_time_full') == 0 && strlen($text)>3){
			return str_replace($search, $replace, Mage::helper('core')->formatTime($text, 'full'));
		}elseif(strcasecmp(trim($textFormat), 'format_currency') == 0){
			return str_replace($search, $replace, Mage::helper('core')->formatCurrency($text));
		}elseif(strcasecmp(trim($textFormat), 'format_price') == 0){
			return str_replace($search, $replace, $order->formatPrice($text));
		}elseif(strcasecmp(trim($textFormat), 'format_base_price') == 0){
			return str_replace($search, $replace, $order->formatBasePrice($text));
		}elseif(strcasecmp(trim($textFormat), 'format_convert_price') == 0){
			return str_replace($search, $replace, $order->getStore()->convertPrice($text));
		}elseif(strcasecmp(trim($textFormat), 'format_integer') == 0){
			return str_replace($search, $replace, (int)$text);
		}elseif(strcasecmp(trim($textFormat), 'format_number_1') == 0){
			if($text>0)	return str_replace($search, $replace, number_format($text, 1));
			else return 0;
		}elseif(strcasecmp(trim($textFormat), 'format_number_2') == 0){
			if($text>0)	return str_replace($search, $replace, number_format($text, 2));
			else return 0;
		}elseif(strcasecmp(trim($textFormat), 'format_number_3') == 0){
			if($text>0)	return str_replace($search, $replace, number_format($text, 3));
			else return 0;
		}elseif(strcasecmp(trim($textFormat), 'format_number_4') == 0){
			if($text>0)	return str_replace($search, $replace, number_format($text, 4));
			else return 0;
		}
		return $text;
	}

	/**
	 * Do the replacement of input variable and real value
	 */
	public function checkReplaceResult($matches){
		$item = $this->callbackParms[1];
		$value = $this->callbackParms[0];
		if($this->_isDevMode()){
			echo "<br />Variable in template: <i>".$matches[1]."</i>";
			echo "<br />Evaluated to: <i>".$value."</i>";
			$evalValue = @eval("return ($value);");
			$evalValue = str_replace('$','\$',$evalValue);
			echo "<br />Variable after transformation: <i>".$evalValue."</i><br />";
		}else{
			ob_start();
			$evalValue = @eval("return ({$value});");
			$evalValue = str_replace('$','\$',$evalValue);
			ob_end_clean();
		}
		return $evalValue;
	}

	/**
	 * Used as a step when computing php code in the input
	 */
	public function convertEvalMatches($item, $text = '', $matches, $order, $replaceFunction){
		foreach($matches[0] as $match){
			list ($matchTextFormat, $matchText) = $this->getFormating($match, '##');
			$ptn = "/{{(.*)}}/Uis";
			if(preg_match_all($ptn, $matchText, $singleMatches)>0){
				foreach($singleMatches[0] as $singleMatch){
					$obj = $this;
					list ($singleMatchTextFormat, $singleMatchText) = $this->getFormating($singleMatch);
					if(is_array($replaceFunction)){
						foreach ($replaceFunction as $key => $singleReplaceFuntion){
							$singleMatchText = call_user_func(array($obj,$singleReplaceFuntion), $item[$key], $singleMatchText, $obj);
						}	
					}else{
						$singleMatchText = call_user_func(array($obj,$replaceFunction), $item, $singleMatchText, $obj);
					}
					if(is_string($singleMatchTextFormat))$singleMatchText = $this->formatText($singleMatchTextFormat, $singleMatchText, $order);
					$matchText = str_replace($singleMatch, $singleMatchText, $matchText);
				}
			}
			//			echo $matchText;
			$matchText = $this->doEval($matchText);
			if(is_string($matchTextFormat))$matchText = $this->formatText($matchTextFormat, $matchText, $order);
			$text = str_replace($match, $matchText, $text);
		}
		return $text;
	}

	/**
	 * Find all elements that should be replace in the input text, based on the format that
	 * 		##...## is a peace of php code
	 * 		{{..}} is a value that need translated to real value
	 */
	public function convertText($item, $order, $text = '', $replaceFunction){
		$text = str_replace('<!-- pagebreak -->', '<br pagebreak="true" />', $text);
		$text = str_replace('<!--pagebreak-->', '<br pagebreak="true" />', $text);
		
		$ptn = "/##(.*)##/Uis";
		if(preg_match_all($ptn, $text, $matches)>0){
			$text = $this->convertEvalMatches($item, $text, $matches, $order, $replaceFunction);
		}

		$ptn = "/{{(.*)}}/Uis";
		if(preg_match_all($ptn, $text, $matches)>0){
			foreach($matches[0] as $match){
				$obj = $this;
				list ($matchTextFormat, $matchText) = $this->getFormating($match);
				if(is_array($replaceFunction)){
					foreach ($replaceFunction as $key => $singleReplaceFuntion){
						$matchText = call_user_func(array($obj,$singleReplaceFuntion), $item[$key], $matchText, $obj);
					}	
				}else{
					$matchText = call_user_func(array($obj,$replaceFunction), $item, $matchText, $obj);
				}
				if(is_string($matchTextFormat))$matchText = $this->formatText($matchTextFormat, $matchText, $order);
				$text = str_replace($match, $matchText, $text);
			}
		}
		return $text;
	}

}
