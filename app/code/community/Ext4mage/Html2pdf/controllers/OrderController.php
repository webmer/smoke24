<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Sales orders controller
 *
 * @category   Mage
 * @package    Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
require_once BP.'/app/code/core/Mage/Sales/controllers/OrderController.php';


class Ext4mage_Html2pdf_OrderController extends Mage_Sales_OrderController
{
	const XPATH_CONFIG_SETTINGS_IS_FRONTEND_ACTIVE		= 'html2pdf/settings/is_frontend_active';
	const XPATH_CONFIG_SETTINGS_IS_ACTIVE				= 'html2pdf/settings/is_active';
	
	protected function _isActive() {
		return Mage::getStoreConfig(self::XPATH_CONFIG_SETTINGS_IS_ACTIVE);
	}
	
	protected function _isFrontendActive() {
		return Mage::getStoreConfig(self::XPATH_CONFIG_SETTINGS_IS_FRONTEND_ACTIVE);
	}
	
	/**
	* Print Order Action - ext4mage style
	*/
	public function printAction(){
		if($this->_isActive() && $this->_isFrontendActive()){
			$orderId = (int) $this->getRequest()->getParam('order_id');
			$order = Mage::getModel('sales/order')->load($orderId);
			
			if (!empty($order) && $this->_canViewOrder($order) && $this->_loadValidOrder()) {
				$pdf = Mage::getModel('html2pdf/order_pdf_order')->getPdf(array($order));
				if (method_exists($this, '_prepareDownloadResponse')) {
					return $this->_prepareDownloadResponse(
	   	                    Mage::helper('sales')->__('Order') . "-" . $order->getIncrementId().'.pdf', $pdf->render(),
		   	                'application/pdf'
					);
				} else {
					$content = $pdf->render();
					$this->getResponse()
					->setHttpResponseCode(200)
					->setHeader('Pragma', 'public', true)
		            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
		            ->setHeader('Content-type', 'application/pdf', true)
					->setHeader('Content-Length', strlen($content))
					->setHeader('Content-Disposition', 'attachment; filename="'.Mage::helper('sales')->__('Order') . "-" . $order->getIncrementId().'.pdf'.'"');
					
					 $this->getResponse()->setBody($content);					
		        }
			} else {
				if (Mage::getSingleton('customer/session')->isLoggedIn()) {
					$this->_redirect('*/*/history');
				} else {
					$this->_redirect('sales/guest/form');
				}
			}
		}else{
			parent::printAction();
		}		
	}
  	/**
     * Print Invoice Action - ext4mage style
     */
    public function printInvoiceAction(){
    	
   	  if($this->_isActive() && $this->_isFrontendActive()){
    	$invoiceId = (int) $this->getRequest()->getParam('invoice_id');
    	$orderId = (int) $this->getRequest()->getParam('order_id');
    	$allInvoices = array();
    	if (empty($invoiceId)) {
    		$invoices = Mage::getResourceModel('sales/order_invoice_collection')
    			->setOrderFilter($orderId)
    			->load();
    		foreach ($invoices as $invoice) {
    			$allInvoices[] = $invoice;
    		}
            $order = Mage::getModel('sales/order')->load($orderId);
    	}else{
    		$allInvoices[] = Mage::getModel('sales/order_invoice')->load($invoiceId);
    		$order = $allInvoices[0]->getOrder();
    	}

       	if (!empty($allInvoices) && count($allInvoices) > 0 && $this->_canViewOrder($order)) {
   			$pdf = Mage::getModel('sales/order_pdf_invoice')->getPdf($allInvoices);
   			if (method_exists($this, '_prepareDownloadResponse')) {
   				return $this->_prepareDownloadResponse(
   	                    Mage::helper('sales')->__('Invoice').'-'.$order->getIncrementId().'.pdf', $pdf->render(),
   	                    'application/pdf'
   				);
   			} else {
   				$content = $pdf->render();
   				$this->getResponse()
   				->setHttpResponseCode(200)
   				->setHeader('Pragma', 'public', true)
   				->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
   				->setHeader('Content-type', 'application/pdf', true)
   				->setHeader('Content-Length', strlen($content))
   				->setHeader('Content-Disposition', 'attachment; filename="'.Mage::helper('sales')->__('Invoice') . "-" . $order->getIncrementId().'.pdf'.'"');
   				 
   				$this->getResponse()->setBody($content);
   			}
       	} else {
   		    if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                $this->_redirect('*/*/history');
            } else {
                $this->_redirect('sales/guest/form');
            }
   		}
   	  }else{
		parent::printInvoiceAction(); 		
   	  }
   }
   
   /**
   * Print Shipment Action - ext4mage style
   */
   public function printShipmentAction(){
   	
   	if($this->_isActive() && $this->_isFrontendActive()){
	   	$shipmentId = (int) $this->getRequest()->getParam('shipment_id');
	   	$orderId = (int) $this->getRequest()->getParam('order_id');
	   	$allShipments = array();
	   	if (empty($shipmentId)) {
	   		$shipments = Mage::getResourceModel('sales/order_shipment_collection')
	   		->setOrderFilter($orderId)
	   		->load();
	   		foreach ($shipments as $shipment) {
	   			$allShipments[] = $shipment;
	   		}
	   		$order = Mage::getModel('sales/order')->load($orderId);
	   	}else{
	   		$allShipments[] = Mage::getModel('sales/order_shipment')->load($shipmentId);
	   		$order = $allShipments[0]->getOrder();
	   	}
	   	
	   	if (!empty($allShipments) && count($allShipments) > 0 && $this->_canViewOrder($order)) {
	   		$pdf = Mage::getModel('sales/order_pdf_shipment')->getPdf($allShipments);
	   		if (method_exists($this, '_prepareDownloadResponse')) {
	   			return $this->_prepareDownloadResponse(
   	   	        	Mage::helper('sales')->__('Shipment') . "-".$order->getIncrementId().'.pdf', $pdf->render(),
   	   	            'application/pdf');
	   		} else {
	   			$content = $pdf->render();
	   			$this->getResponse()
	   			->setHttpResponseCode(200)
	   			->setHeader('Pragma', 'public', true)
	   			->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
	   			->setHeader('Content-type', 'application/pdf', true)
	   			->setHeader('Content-Length', strlen($content))
	   			->setHeader('Content-Disposition', 'attachment; filename="'.Mage::helper('sales')->__('Shipment') . "-" . $order->getIncrementId().'.pdf'.'"');
	   				
	   			$this->getResponse()->setBody($content);
	   		}
	   	} else {
	   		if (Mage::getSingleton('customer/session')->isLoggedIn()) {
	   			$this->_redirect('*/*/history');
	   		} else {
	   			$this->_redirect('sales/guest/form');
	   		}
	   	}
   	}else{
   		parent::printShipmentAction();
   	}   	
   }

   /**
   * Print Creditmemo Action - ext4mage style
   */
   public function printCreditmemoAction(){
   	
   	if($this->_isActive() && $this->_isFrontendActive()){
	   	$creditmemoId = (int) $this->getRequest()->getParam('creditmemo_id');
	   	$orderId = (int) $this->getRequest()->getParam('order_id');
	   	$allCreditmemos = array();
	   	if (empty($creditmemoId)) {
	   		$creditmemos = Mage::getResourceModel('sales/order_creditmemo_collection')
	   		->setOrderFilter($orderId)
	   		->load();
	   		foreach ($creditmemos as $creditmemo) {
	   			$allCreditmemos[] = $creditmemo;
	   		}
	   		$order = Mage::getModel('sales/order')->load($orderId);
	   	}else{
	   		$allCreditmemos[] = Mage::getModel('sales/order_creditmemo')->load($creditmemoId);
	   		$order = $allCreditmemos[0]->getOrder();
	   	}
	   	
	   	if (!empty($allCreditmemos) && count($allCreditmemos) > 0 && $this->_canViewOrder($order)) {
	   		$pdf = Mage::getModel('sales/order_pdf_creditmemo')->getPdf($allCreditmemos);
	   		if (method_exists($this, '_prepareDownloadResponse')) {
	   			return $this->_prepareDownloadResponse(
	   	   	    	Mage::helper('sales')->__('Credit Memo') . "-" .$order->getIncrementId().'.pdf', $pdf->render(),
	   	   	        'application/pdf');
	   		} else {
	   			$content = $pdf->render();
	   			$this->getResponse()
	   			->setHttpResponseCode(200)
	   			->setHeader('Pragma', 'public', true)
	   			->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
	   			->setHeader('Content-type', 'application/pdf', true)
	   			->setHeader('Content-Length', strlen($content))
	   			->setHeader('Content-Disposition', 'attachment; filename="'.Mage::helper('sales')->__('Credit Memo') . "-" . $order->getIncrementId().'.pdf'.'"');
	   				
	   			$this->getResponse()->setBody($content);
	   		}
	   	} else {
	   		if (Mage::getSingleton('customer/session')->isLoggedIn()) {
	   			$this->_redirect('*/*/history');
	   		} else {
	   			$this->_redirect('sales/guest/form');
	   		}
	   	}
   	}else{
   		parent::printCreditmemoAction();
   	}   	
   }
}
