<?php

class Ext4mage_Html2pdf_Adminhtml_TemplateController extends Mage_Adminhtml_Controller_Action
{
	const XPATH_CONFIG_SETTINGS_ORDER_ID					= 'html2pdf/settings/order_id';
	const XPATH_CONFIG_SETTINGS_INVOICE_ID				= 'html2pdf/settings/invoice_id';
	const XPATH_CONFIG_SETTINGS_SHIPPING_ID				= 'html2pdf/settings/shipping_id';
	const XPATH_CONFIG_SETTINGS_CREDITMEMO_ID				= 'html2pdf/settings/creditmemo_id';
	const XPATH_CONFIG_SETTINGS_LICENSE					= 'html2pdf/settings/license_code';

	protected function _initAction() {
		$this->loadLayout()
		->_setActiveMenu('html2pdf/template')
		->_addBreadcrumb(Mage::helper('adminhtml')->__('Html2Pdf Template elements'), Mage::helper('adminhtml')->__('Html2Pdf Template elements'));

		return $this;
	}

	public function indexAction() {
		$this->_initAction()
		->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('html2pdf/template')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('html2pdf_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('html2pdf/template');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Template element content'), Mage::helper('adminhtml')->__('Template element content'));
				
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('html2pdf/adminhtml_template_edit'))
			->_addLeft($this->getLayout()->createBlock('html2pdf/adminhtml_template_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('html2pdf')->__('Element does not exist'));
			$this->_redirect('*/*/');
		}
	}

	public function newAction() {
		$this->_forward('edit');
	}

	public function saveAction() {
		Mage::helper('ext4mageshared')->checkLicenseOnline("html2pdf",Mage::getStoreConfig(self::XPATH_CONFIG_SETTINGS_LICENSE));
		if ($data = $this->getRequest()->getPost()) {
				
			$model = Mage::getModel('html2pdf/template');
			$model->setData($data)
			->setId($this->getRequest()->getParam('id'));
				
			try {
				if ($this->getRequest()->getParam('saveas') == 1) {
					$model->setId(null);
				}
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('html2pdf')->__('Element was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back') || $this->getRequest()->getParam('save_as')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('html2pdf')->__('Unable to find element to save'));
		$this->_redirect('*/*/');
	}

	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('html2pdf/template');
					
				$model->setId($this->getRequest()->getParam('id'))
				->delete();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('html2pdf')->__('Template was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

	public function previewAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			$template = Mage::getModel('html2pdf/template')->load($this->getRequest()->getParam('id'));
			try {
				switch ($template->getData("type")) {
					case 1:
						// Order is the template type
						$orderId = Mage::getStoreConfig(self::XPATH_CONFIG_SETTINGS_ORDER_ID);
						$order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
						if (!empty($order)) {
							$pdf = Mage::getModel('html2pdf/order_pdf_order')->getPdf(array($order), $this->getRequest()->getParam('id'));
							$this->_prepareDownloadResponse('order'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(), 'application/pdf');
						}
						break;
					case 2:
						// Invoice is the template type
						$invoiceId = Mage::getStoreConfig(self::XPATH_CONFIG_SETTINGS_INVOICE_ID);
						$invoice = Mage::getModel('sales/order_invoice')->loadByIncrementId($invoiceId);
						if (!empty($invoice)) {
							$pdf = Mage::getModel('sales/order_pdf_invoice')->getPdf(array($invoice), $this->getRequest()->getParam('id'));
							$this->_prepareDownloadResponse('invoice'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(), 'application/pdf');
						}
						break;
					case 3:
						// Shipping is the template type
						$shippingId = Mage::getStoreConfig(self::XPATH_CONFIG_SETTINGS_SHIPPING_ID);
						$shipping = Mage::getModel('sales/order_shipment')->loadByIncrementId($shippingId);
						if (!empty($shipping)) {
							$pdf = Mage::getModel('sales/order_pdf_shipment')->getPdf(array($shipping), $this->getRequest()->getParam('id'));
							$this->_prepareDownloadResponse('shipment'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(), 'application/pdf');
						}
						break;
					case 4:
						// Creditmemo is the template type
						$creditmemoId = Mage::getStoreConfig(self::XPATH_CONFIG_SETTINGS_CREDITMEMO_ID);
						$creditmemo = Mage::getModel('sales/order_creditmemo')->load($creditmemoId);
						if (!empty($creditmemo)) {
							$pdf = Mage::getModel('sales/order_pdf_creditmemo')->getPdf(array($creditmemo), $this->getRequest()->getParam('id'));
							$this->_prepareDownloadResponse('creditmemo'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(), 'application/pdf');
						}
						break;
				}

			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		//$this->_redirect('*/*/');
	}

	public function massDeleteAction() {
		$templateIds = $this->getRequest()->getParam('template');
		if(!is_array($templateIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('html2pdf')->__('Please select template(s)'));
		} else {
			try {
				foreach ($templateIds as $templateId) {
					$html2pdf = Mage::getModel('html2pdf/template')->load($templateId);
					$html2pdf->delete();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(
				Mage::helper('html2pdf')->__('Total of %d template(s) were successfully deleted', count($templateIds))
				);
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}

	public function massStatusAction()
	{
		$templateIds = $this->getRequest()->getParam('template');
		if(!is_array($templateIds)) {
			Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
		} else {
			try {
				foreach ($templateIds as $templateId) {
					$template = Mage::getSingleton('html2pdf/template')
					->load($templateId)
					->setIsActive($this->getRequest()->getParam('status'))
					->setIsMassupdate(true)
					->save();
				}
				$this->_getSession()->addSuccess(
				Mage::helper('html2pdf')->__('Total of %d template(s) were successfully updated', count($templateIds))
				);
			} catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
}