<?php

class Ext4mage_Html2pdf_Adminhtml_TextController extends Mage_Adminhtml_Controller_Action
{

	protected function _initAction() {
		$this->loadLayout()
		->_setActiveMenu('html2pdf/text')
		->_addBreadcrumb(Mage::helper('adminhtml')->__('Html2Pdf Text elements'), Mage::helper('adminhtml')->__('Html2Pdf Text elements'));

		return $this;
	}

	public function indexAction() {
		$this->_initAction()
		->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('html2pdf/text')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('html2pdf_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('html2pdf/text');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Text element content'), Mage::helper('adminhtml')->__('Text element content'));
				
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('html2pdf/adminhtml_text_edit'))
			->_addLeft($this->getLayout()->createBlock('html2pdf/adminhtml_text_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('html2pdf')->__('Element does not exist'));
			$this->_redirect('*/*/');
		}
	}

	public function newAction() {
		$this->_forward('edit');
	}

	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
				
			$model = Mage::getModel('html2pdf/text');
			$model->setData($data)
			->setId($this->getRequest()->getParam('id'));
				
			try {
				if ($this->getRequest()->getParam('saveas') == 1) {
					$model->setId(null);
				}
				 
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('html2pdf')->__('Element was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back') || $this->getRequest()->getParam('save_as')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('html2pdf')->__('Unable to find element to save'));
		$this->_redirect('*/*/');
	}

	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('html2pdf/text');
					
				$model->setId($this->getRequest()->getParam('id'))
				->delete();

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('html2pdf')->__('Element was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

	public function massDeleteAction() {
		$textIds = $this->getRequest()->getParam('text');
		if(!is_array($textIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('html2pdf')->__('Please select element(s)'));
		} else {
			try {
				foreach ($textIds as $textId) {
					$text = Mage::getModel('html2pdf/text')->load($textId);
					$text->delete();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(
				Mage::helper('html2pdf')->__('Total of %d text element(s) were successfully deleted', count($textIds))
				);
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}

	public function massStatusAction()
	{
		$textIds = $this->getRequest()->getParam('text');
		if(!is_array($textIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('html2pdf')->__('Please select element(s)'));
		} else {
			try {
				foreach ($textIds as $textId) {
					$text = Mage::getSingleton('html2pdf/text')
					->load($textId)
					->setIsActive($this->getRequest()->getParam('status'))
					->setIsMassupdate(true)
					->save();
				}
				$this->_getSession()->addSuccess(
				Mage::helper('html2pdf')->__('Total of %d text element(s) were successfully updated', count($textIds))
				);
			} catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
}