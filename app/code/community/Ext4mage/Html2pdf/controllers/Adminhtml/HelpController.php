<?php

class Ext4mage_Html2pdf_Adminhtml_HelpController extends Mage_Adminhtml_Controller_Action
{

	protected function _initAction() {
		$this->loadLayout()
		->_setActiveMenu('html2pdf/help')
		->_addBreadcrumb(Mage::helper('adminhtml')->__('Html2Pdf Help'), Mage::helper('adminhtml')->__('Html2Pdf Help'));
			
		$this->_addLeft($this->getLayout()->createBlock('html2pdf/adminhtml_help_tabs'));

		return $this;
	}

	public function indexAction() {
		$this->_initAction();

		$this->renderLayout();
	}

	public function exportGeneralCsvAction()
	{
		$fileName   = 'html2pdf_general_help.csv';
		$content    = $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_general')->getCsvFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}

	public function exportGeneralXmlAction()
	{
		$fileName   = 'html2pdf_general_help.xml';
		$content    = $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_general')->getExcelFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}

	public function exportOrderCsvAction()
	{
		$fileName   = 'html2pdf_order_help.csv';
		$content    = $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_order')->getCsvFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}

	public function exportOrderXmlAction()
	{
		$fileName   = 'html2pdf_order_help.xml';
		$content    = $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_order')->getExcelFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}

	public function exportInvoiceCsvAction()
	{
		$fileName   = 'html2pdf_invoice_help.csv';
		$content    = $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_invoice')->getCsvFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}

	public function exportInvoiceXmlAction()
	{
		$fileName   = 'html2pdf_invoice_help.xml';
		$content    = $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_invoice')->getExcelFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}

	public function exportShippingCsvAction()
	{
		$fileName   = 'html2pdf_shipping_help.csv';
		$content    = $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_shipping')->getCsvFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}

	public function exportShippingXmlAction()
	{
		$fileName   = 'html2pdf_shipping_help.xml';
		$content    = $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_shipping')->getExcelFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}

	public function exportCreditCsvAction()
	{
		$fileName   = 'html2pdf_credit_help.csv';
		$content    = $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_credit')->getCsvFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}

	public function exportCreditXmlAction()
	{
		$fileName   = 'html2pdf_credit_help.xml';
		$content    = $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_credit')->getExcelFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}

	public function exportProductCsvAction()
	{
		$fileName   = 'html2pdf_product_help.csv';
		$content    = $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_product')->getCsvFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}

	public function exportProductXmlAction()
	{
		$fileName   = 'html2pdf_product_help.xml';
		$content    = $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_product')->getExcelFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}

	public function exportProductbundleCsvAction()
	{
		$fileName   = 'html2pdf_productbundle_help.csv';
		$content    = $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_productbundle')->getCsvFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}

	public function exportProductbundleXmlAction()
	{
		$fileName   = 'html2pdf_productbundle_help.xml';
		$content    = $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_productbundle')->getExcelFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}

	public function exportCustomerCsvAction()
	{
		$fileName   = 'html2pdf_customer_help.csv';
		$content    = $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_customer')->getCsvFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}

	public function exportCustomerXmlAction()
	{
		$fileName   = 'html2pdf_customer_help.xml';
		$content    = $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_customer')->getExcelFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}

	public function exportFormattingCsvAction()
	{
		$fileName   = 'html2pdf_formatting_help.csv';
		$content    = $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_formatting')->getCsvFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}

	public function exportFormattingXmlAction()
	{
		$fileName   = 'html2pdf_formatting_help.xml';
		$content    = $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_formatting')->getExcelFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}
}