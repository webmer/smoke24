<?php

class Ext4mage_Html2pdf_Adminhtml_TableController extends Mage_Adminhtml_Controller_Action
{

	protected function _initAction() {
		$this->loadLayout()
		->_setActiveMenu('html2pdf/table')
		->_addBreadcrumb(Mage::helper('adminhtml')->__('Html2Pdf Table elements'), Mage::helper('adminhtml')->__('Html2Pdf Table elements'));

		return $this;
	}

	public function indexAction() {
		$this->_initAction()
		->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('html2pdf/table')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('html2pdf_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('html2pdf/table');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Table element content'), Mage::helper('adminhtml')->__('Table element content'));
				
			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('html2pdf/adminhtml_table_edit'))
			->_addLeft($this->getLayout()->createBlock('html2pdf/adminhtml_table_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('html2pdf')->__('Element does not exist'));
			$this->_redirect('*/*/');
		}
	}

	public function newAction() {
		$this->_forward('edit');
	}

	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
				
			$model = Mage::getModel('html2pdf/table');
			$columnModel = Mage::getModel('html2pdf/column');
			$model->setData($data)
			->setId($this->getRequest()->getParam('id'));
				
			try {
				if ($this->getRequest()->getParam('saveas') == 1) {
					$model->setId(null);
				}
				 
				$model->save();
				 
				 
				foreach($data['table']['column'] as $column){
					$columnModel->setData(null);
					if($column['column_id']>0){
						$columnModel->setData($column)->setId($column['column_id']);
						if($column['is_delete']==1){
							$columnModel->delete();
							continue;
						}
						if ($this->getRequest()->getParam('saveas') == 1) {
							$columnModel->setData($column)->setId(null);
						}
					}else{
						$columnModel->setData($column)->setId(null);
						if($column['is_delete']==1){
							continue;
						}
					}

					$columnModel->setTableId($model->getId());
					$columnModel->save();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('html2pdf')->__('Element was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back') || $this->getRequest()->getParam('save_as')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('html2pdf')->__('Unable to find element to save'));
		$this->_redirect('*/*/');
	}

	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('html2pdf/table');
					
				$model->setId($this->getRequest()->getParam('id'))
				->delete();

				$columns = Mage::getResourceModel('html2pdf/column');
				$columnsList = $columns->getColumnByTable($this->getRequest()->getParam('id'));

				foreach($columnsList as $column){
					$columnModel = Mage::getModel('html2pdf/column')->load($column['column_id']);
					$columnModel->delete();
				}

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('html2pdf')->__('Table was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

	public function massDeleteAction() {
		$tableIds = $this->getRequest()->getParam('table');
		if(!is_array($tableIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('html2pdf')->__('Please select table element(s)'));
		} else {
			try {
				foreach ($tableIds as $tableId) {
					$table = Mage::getModel('html2pdf/table')->load($tableId);
					$table->delete();
					$columns = Mage::getResourceModel('html2pdf/column');
					$columnsList = $columns->getColumnByTable($tableId);
						
					foreach($columnsList as $column){
						$columnModel = Mage::getModel('html2pdf/column')->load($column['column_id']);
						$columnModel->delete();
					}
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(
				Mage::helper('html2pdf')->__('Total of %d table element(s) were successfully deleted', count($tableIds))
				);
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}

	public function massStatusAction()
	{
		$tableIds = $this->getRequest()->getParam('table');
		if(!is_array($tableIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('html2pdf')->__('Please select table element(s)'));
		} else {
			try {
				foreach ($tableIds as $tableId) {
					$table = Mage::getSingleton('html2pdf/table')
					->load($tableId)
					->setIsActive($this->getRequest()->getParam('status'))
					->setIsMassupdate(true)
					->save();
				}
				$this->_getSession()->addSuccess(
				Mage::helper('html2pdf')->__('Total of %d table element(s) were successfully updated', count($tableIds))
				);
			} catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
}