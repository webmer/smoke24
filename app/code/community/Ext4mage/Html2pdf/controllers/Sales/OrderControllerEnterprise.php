<?php

require_once BP.'/app/code/core/Enterprise/SalesArchive/controllers/Adminhtml/Sales/OrderController.php'; //Mage/Adminhtml/controllers/Sales/OrderController.php';

class Ext4mage_Html2pdf_Sales_OrderController extends Enterprise_SalesArchive_Adminhtml_Sales_OrderController{

	/**
	 * Print invoices for selected orders - ext4mage style
	 */
	public function pdfinvoicesAction(){
		$orderIds = $this->getRequest()->getPost('order_ids');
		$allInvoices = array();
		if (!empty($orderIds)) {
			foreach ($orderIds as $orderId) {
				$invoices = Mage::getResourceModel('sales/order_invoice_collection')
				->setOrderFilter($orderId)
				->load();
				foreach ($invoices as $invoice) {
					$allInvoices[] = $invoice;
				}
			}
			if (!empty($allInvoices) && count($allInvoices) > 0) {
				$pdf = Mage::getModel('sales/order_pdf_invoice')->getPdf($allInvoices);
				return $this->_prepareDownloadResponse(
                    'invoice'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(),
                    'application/pdf'
				);
			} else {
				$this->_getSession()->addError($this->__('There are no printable documents related to selected orders.'));
				$this->_redirect('*/*/');
			}
		}
		$this->_redirect('*/*/');
	}

	/**
	 * Print shipments for selected orders - ext4mage style
	 */
	public function pdfshipmentsAction(){
		$orderIds = $this->getRequest()->getPost('order_ids');
		$allShipments = array();
		if (!empty($orderIds)) {
			foreach ($orderIds as $orderId) {
				$shipments = Mage::getResourceModel('sales/order_shipment_collection')
				->setOrderFilter($orderId)
				->load();
				foreach ($shipments as $shipment) {
					$allShipments[] = $shipment;
				}
			}
			if (!empty($allShipments) && count($allShipments) > 0) {
				$pdf = Mage::getModel('sales/order_pdf_shipment')->getPdf($allShipments);
				return $this->_prepareDownloadResponse(
                    'packingslip'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(),
                    'application/pdf'
				);
			} else {
				$this->_getSession()->addError($this->__('There are no printable documents related to selected orders.'));
				$this->_redirect('*/*/');
			}
		}
		$this->_redirect('*/*/');
	}

	/**
	 * Print creditmemos for selected orders - ext4mage style
	 */
	public function pdfcreditmemosAction(){
		$orderIds = $this->getRequest()->getPost('order_ids');
		$allCreditmemos = array();
		if (!empty($orderIds)) {
			foreach ($orderIds as $orderId) {
				$creditmemos = Mage::getResourceModel('sales/order_creditmemo_collection')
				->setOrderFilter($orderId)
				->load();
				foreach ($creditmemos as $creditmemo) {
					$allCreditmemos[] = $creditmemo;
				}
			}
			if (!empty($allCreditmemos) && count($allCreditmemos) > 0) {
				$pdf = Mage::getModel('sales/order_pdf_creditmemo')->getPdf($allCreditmemos);
				return $this->_prepareDownloadResponse(
                    'creditmemo'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(),
                    'application/pdf'
				);
			} else {
				$this->_getSession()->addError($this->__('There are no printable documents related to selected orders.'));
				$this->_redirect('*/*/');
			}
		}
		$this->_redirect('*/*/');
	}

	/**
	 * Print all documents for selected orders
	 */
	public function pdfdocsAction(){
		$orderIds = $this->getRequest()->getPost('order_ids');
		$flag = false;
		if (!empty($orderIds)) {

			//            $pdf = Mage::getModel('html2pdf/order_pdf_order')->getPdf($orderIds);
			//            $this->_prepareDownloadResponse('order'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(), 'application/pdf');
			//
			//            foreach ($orderIds as $orderId) {
			//                $invoices = Mage::getResourceModel('sales/order_invoice_collection')
			//                    ->setOrderFilter($orderId)
			//                    ->load();
			//            	foreach ($invoices as $invoice) {
			//            		$allInvoices[] = $invoice;
			//            	}
			//            }
			//            if (!empty($allInvoices) && count($allInvoices) > 0) {
			//            	$pdf = Mage::getModel('sales/order_pdf_invoice')->getPdf($allInvoices);
			//                $this->_prepareDownloadResponse(
			//                    'invoice'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(),
			//                    'application/pdf'
			//                );
			//            }

			//
			//            foreach ($orderIds as $orderId) {
			//                $invoices = Mage::getResourceModel('sales/order_invoice_collection')
			//                    ->setOrderFilter($orderId)
			//                    ->load();
			//                if ($invoices->getSize()){
			//                    $flag = true;
			//                    if (!isset($pdf)){
			//                        $pdf = Mage::getModel('sales/order_pdf_invoice')->getPdf($invoices);
			//                    } else {
			//                        $pages = Mage::getModel('sales/order_pdf_invoice')->getPdf($invoices);
			//                        $pdf->pages = array_merge ($pdf->pages, $pages->pages);
			//                    }
			//                }
			//
			//                $shipments = Mage::getResourceModel('sales/order_shipment_collection')
			//                    ->setOrderFilter($orderId)
			//                    ->load();
			//                if ($shipments->getSize()){
			//                    $flag = true;
			//                    if (!isset($pdf)){
			//                        $pdf = Mage::getModel('sales/order_pdf_shipment')->getPdf($shipments);
			//                    } else {
			//                        $pages = Mage::getModel('sales/order_pdf_shipment')->getPdf($shipments);
			//                        $pdf->pages = array_merge ($pdf->pages, $pages->pages);
			//                    }
			//                }
			//
			//                $creditmemos = Mage::getResourceModel('sales/order_creditmemo_collection')
			//                    ->setOrderFilter($orderId)
			//                    ->load();
			//                if ($creditmemos->getSize()) {
			//                    $flag = true;
			//                    if (!isset($pdf)){
			//                        $pdf = Mage::getModel('sales/order_pdf_creditmemo')->getPdf($creditmemos);
			//                    } else {
			//                        $pages = Mage::getModel('sales/order_pdf_creditmemo')->getPdf($creditmemos);
			//                        $pdf->pages = array_merge ($pdf->pages, $pages->pages);
			//                    }
			//                }
			//            }
			//            if ($flag) {
			//                return $this->_prepareDownloadResponse(
			//                    'docs'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf',
			//                    $pdf->render(), 'application/pdf'
			//                );
			//            } else {
			//                $this->_getSession()->addError($this->__('There are no printable documents related to selected orders.'));
			//                $this->_redirect('*/*/');
			//            }
			$this->_getSession()->addError($this->__('Printable documents in order list is not yet implemented in Ext4Mage.'));
			}
			$this->_redirect('*/*/');
}

}