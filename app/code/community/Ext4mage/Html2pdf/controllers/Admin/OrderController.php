<?php

require_once BP.'/app/code/core/Mage/Adminhtml/controllers/Sales/OrderController.php';

class Ext4mage_Html2pdf_Admin_OrderController extends Mage_Adminhtml_Sales_OrderController
{
	public function printAction()
	{
		if ($orderId = $this->getRequest()->getParam('order_id')) {
			if ($order = Mage::getModel('sales/order')->load($orderId)) {
				if ($order->getStoreId()) {
					Mage::app()->setCurrentStore($order->getStoreId());
				}
				$pdf = Mage::getModel('html2pdf/order_pdf_order')->getPdf(array($order));
				return $this->_prepareDownloadResponse('order'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(), 'application/pdf');
			}
		}
		else {
			$this->_getSession()->addError($this->__('There are no printable documents related to selected orders'));
		}
		$this->_redirect('*/*/');
	}

	public function pdfordersAction(){
		$orderIds = $this->getRequest()->getPost('order_ids');
		$flag = true;
		if (!empty($orderIds)) {
			$pdf = Mage::getModel('html2pdf/order_pdf_order')->getPdf($orderIds);
			if ($flag) {
				return $this->_prepareDownloadResponse('order'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(), 'application/pdf');
			} else {
				$this->_getSession()->addError($this->__('There are no printable documents related to selected orders'));
				$this->_redirect('*/*/');
			}
		}
		$this->_redirect('*/*/');
	}
	
	public function printOrgInvoiceAction(){
		$templateId = 2;
		if ($invoiceId = $this->getRequest()->getParam('invoice_id')) {
			if ($invoice = Mage::getModel('sales/order_invoice')->load($invoiceId)) {
            	if ($invoice->getStoreId()) {
					Mage::app()->setCurrentStore($invoice->getStoreId());
				}
				$pdf = Mage::getModel('html2pdf/order_pdf_invoice')->getPdf(array($invoice), $templateId);
				return $this->_prepareDownloadResponse('invoice_org'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(), 'application/pdf');
			}
		}
		else {
			$this->_getSession()->addError($this->__('There are no printable documents related to selected invoice'));
		}
		
		$this->_redirect('*/*/');
	}
	
}