<?php
/**
 * Magento
 *
 * NOTICE
 *
 * This source file a part of Ext4mage_Html2pdf extension
 * all rights to this modul belongs to ext4mage.com
 *
 * @category    Ext4mage
 * @package     Ext4mage_Html2pdf
 * @copyright   Copyright (c) 2011 ext4mage (http://www.ext4mage.com)
 */

class Ext4mage_Html2pdf_Helper_Data extends Mage_Core_Helper_Abstract
{
	private $textVariables = array(array('code'=>'$$customerName'),array('code'=>'$$customerAdress'));
	private $columnVariables = array(array('code'=>'$$productName'),array('code'=>'$$productSku'));
	private $tableAttrVariables = array(array('code'=>'$$productAttrName'),array('code'=>'$$productAttrSku'));
	private $tableTotalVariables = array(array('code'=>'$$orderTotal'),array('code'=>'$$orderTax'));
	protected $_defaultTotalModel = 'sales/order_pdf_total_default';

	/**
	 * Recursively applying str_replace on an array.
	 */
	public function replace_rec($value, $newvalue, $replace_array)
	{
		if (is_array($replace_array)){
			foreach ($replace_array as $key => $val){
				if(is_string($val))
				$replace_array[$key] = str_replace($value, $newvalue, $val);
				elseif(is_array($val))
				$replace_array[$key] = self::replace_rec($value, $newvalue, $val);
			}
		}else{
			return $replace_array;
		}

		return $replace_array;
	}

	function html2rgb($color)
	{
		if (strlen($color) > 2){
			if ($color[0] == '#')
			$color = substr($color, 1);
	
			if (strlen($color) == 6)
			list($r, $g, $b) = array($color[0].$color[1],
			$color[2].$color[3],
			$color[4].$color[5]);
			elseif (strlen($color) == 3)
			list($r, $g, $b) = array($color[0].$color[0], $color[1].$color[1], $color[2].$color[2]);
			else
			return false;
	
			$r = hexdec($r); $g = hexdec($g); $b = hexdec($b);
	
			return array($r, $g, $b);
		}
		return false;
	}

	public function getItemByProductId($itemId,$itemCollection)
	{
		foreach ($itemCollection as $item) {
			if ($item->getProductId()==$itemId) {
				return $item;
			}
		}
		return false;
	}

	public function getTotalsListForDisplaying($source, $order)
	{
		$totals = Mage::getConfig()->getNode('global/pdf/totals')->asArray();
		$totalModels = array();
		foreach ($totals as $index => $totalInfo) {
			if (!empty($totalInfo['model'])) {
				$totalModel = Mage::getModel($totalInfo['model']);
				if ($totalModel instanceof Mage_Sales_Model_Order_Pdf_Total_Default) {
					$totalInfo['model'] = $totalModel;
				} else {
					Mage::throwException(
					Mage::helper('sales')->__('PDF total model should extend Mage_Sales_Model_Order_Pdf_Total_Default')
					);
				}
			} else {
				$totalModel = Mage::getModel($this->_defaultTotalModel);
			}
			$totalModel->setData($totalInfo);

			$totalModel->setOrder($order)
			->setSource($source);
			if ($totalModel->canDisplay()) {
				foreach ($totalModel->getTotalsForDisplay() as $totalData) {
					if(isset($totalData['title']) && strlen($totalData['title'])>0){
						$totalData['source_field'] = $this->removeAllSpecChar($totalData['title']);
					}else{
						$totalData['source_field'] = $this->removeAllSpecChar($totalData['label']);
					}
					$totalModels[] = $totalData;
				}
			}
		}

		return $totalModels;
	}

	public function getTotalForDisplaying($source, $order, $sourceField)
	{
		$totals = Mage::getConfig()->getNode('global/pdf/totals')->asArray();
		$totalModels = null;
		foreach ($totals as $index => $totalInfo) {
			if (!empty($totalInfo['model'])) {
				$totalModel = Mage::getModel($totalInfo['model']);
				if ($totalModel instanceof Mage_Sales_Model_Order_Pdf_Total_Default) {
					$totalInfo['model'] = $totalModel;
				} else {
					Mage::throwException(
					Mage::helper('sales')->__('PDF total model should extend Mage_Sales_Model_Order_Pdf_Total_Default')
					);
				}
			} else {
				$totalModel = Mage::getModel($this->_defaultTotalModel);
			}
			$totalModel->setData($totalInfo);

			$totalModel->setOrder($order)
			->setSource($source);
			if ($totalModel->canDisplay()) {
				foreach ($totalModel->getTotalsForDisplay() as $totalData) {
					if(strlen($totalData['title'])>0){
						$totalData['source_field'] = $this->removeAllSpecChar($totalData['title']);
					}else{
						$totalData['source_field'] = $this->removeAllSpecChar($totalData['label']);
					}
					if(strcasecmp($totalData['source_field'], $sourceField) == 0){
						return $totalData;
					}
				}
			}
		}

		return $totalModels;
	}

	public function removeAllSpecChar($inputString){
		return str_replace (" ", "", preg_replace("/[^A-Za-z0-9]/", "", $inputString));
	}
	
	public function getNewestTemplateUpdate($template = null){
		if ($template == null) return null;
		$dates = array();
		$dates[] = '1970-01-01 00:00:00';
		$dates[] = $template->getUpdateTime();
		$dates[] = $template->getHeaderText()->getUpdateTime();
		$dates[] = $template->getFooterText()->getUpdateTime();
		$dates[] = $template->getMainText()->getUpdateTime();
		$dates[] = $template->getTable()->getUpdateTime();
		if($template->getCrossSellText()) $dates[] = $template->getCrossSellText()->getUpdateTime();
				
		$mostRecent = null;
		foreach($dates as $date){
			$curDate = strtotime($date);
			if ($curDate > $mostRecent) {
				$mostRecent = $curDate;
			}
		}
		$mostRecent = date('Y-m-d H:i:s', $mostRecent); 
		return $mostRecent;
	}

}