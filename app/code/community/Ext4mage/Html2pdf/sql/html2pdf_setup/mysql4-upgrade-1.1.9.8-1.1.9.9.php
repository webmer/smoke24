<?php
/**
 * Magento
 *
 * NOTICE
 *
 * This source file a part of Ext4mage_Html2pdf extension
 * all rights to this modul belongs to ext4mage.com
 *
 * @category    Ext4mage
 * @package     Ext4mage_Html2pdf
 * @copyright   Copyright (c) 2011 ext4mage (http://www.ext4mage.com)
 */

$installer = $this;

$installer->startSetup();

$updateString1 = <<<EOT

ALTER TABLE {$this->getTable('html2pdf/template')} ADD `cross_sell_element_id` INT DEFAULT NULL AFTER `footer_element_id`;
ALTER TABLE {$this->getTable('html2pdf/template')} ADD `cross_sell_num` INT DEFAULT 4 AFTER `cross_sell_element_id`;
ALTER TABLE {$this->getTable('html2pdf/template')} ADD CONSTRAINT `FK_HTML2PDF_TEMPLATE_CROSS` FOREIGN KEY (`cross_sell_element_id`) REFERENCES `{$this->getTable('html2pdf/text')}` (`text_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

EOT;
$installer->run($updateString1);

$installer->endSetup();