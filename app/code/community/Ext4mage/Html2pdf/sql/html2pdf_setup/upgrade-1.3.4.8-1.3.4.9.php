<?php
/**
* Magento
*
* NOTICE
*
* This source file a part of Ext4mage_Html2pdf extension
* all rights to this modul belongs to ext4mage.com
*
* @category    Ext4mage
* @package     Ext4mage_Html2pdf
* @copyright   Copyright (c) 2011 ext4mage (http://www.ext4mage.com)
*/

$installer = $this;

$installer->startSetup();

/**
 * Create table 'html2pdf/file'
 */
$table = $installer->getConnection()
	->newTable($installer->getTable('html2pdf/file'))
	->addColumn('file_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'identity'  => true,
		'nullable'  => false,
		'primary'   => true,
	), 'ID')
	->addColumn('template_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
			'nullable'  => false,
			'primary'   => true,
	), 'template ID')
	->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'default' => null,
	), 'Comment')
	->addColumn('invoice_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'default' => null,
	), 'Comment')
	->addColumn('shipment_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'default' => null,
	), 'Comment')
	->addColumn('creditmemo_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'default' => null,
	), 'Comment')
	->addColumn('filepath', Varien_Db_Ddl_Table::TYPE_TEXT, '255', array('nullable'  => false), 'filename')
	->addColumn('creation_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Creation Time')
	->addForeignKey($installer->getFkName('html2pdf/file', 'template_id', 'html2pdf/template', 'template_id'),
	    'template_id', $installer->getTable('html2pdf/template'), 'template_id',
		Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
	->addForeignKey($installer->getFkName('html2pdf/file', 'order_id', 'sales/order', 'entity_id'),
		'order_id', $installer->getTable('sales/order'), 'entity_id',
		Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
	->addForeignKey($installer->getFkName('html2pdf/file', 'invoice_id', 'sales/invoice', 'entity_id'),
		'invoice_id', $installer->getTable('sales/invoice'), 'entity_id',
		Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
	->addForeignKey($installer->getFkName('html2pdf/file', 'shipment_id', 'sales/shipment', 'entity_id'),
		'shipment_id', $installer->getTable('sales/shipment'), 'entity_id',
		Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
	->addForeignKey($installer->getFkName('html2pdf/file', 'creditmemo_id', 'sales/creditmemo', 'entity_id'),
		'creditmemo_id', $installer->getTable('sales/creditmemo'), 'entity_id',
		Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)		
	->setComment('Generated PDF with references to file');
$installer->getConnection()->createTable($table);
    
$installer->endSetup();
