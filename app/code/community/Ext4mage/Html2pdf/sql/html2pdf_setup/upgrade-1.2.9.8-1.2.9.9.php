<?php
/**
* Magento
*
* NOTICE
*
* This source file a part of Ext4mage_Html2pdf extension
* all rights to this modul belongs to ext4mage.com
*
* @category    Ext4mage
* @package     Ext4mage_Html2pdf
* @copyright   Copyright (c) 2011 ext4mage (http://www.ext4mage.com)
*/

$installer = $this;

$installer->startSetup();

    $installer->getConnection()->changeColumn(
        $installer->getTable('html2pdf/template'),
        'page_size',
        'page_size',
        array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length'    => 255,
            'default'   => 'A4',
			'comment'   => 'Comment'
        )
    );

$installer->endSetup();
