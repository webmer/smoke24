<?php
/**
* Magento
*
* NOTICE
*
* This source file a part of Ext4mage_Html2pdf extension
* all rights to this modul belongs to ext4mage.com
*
* @category    Ext4mage
* @package     Ext4mage_Html2pdf
* @copyright   Copyright (c) 2011 ext4mage (http://www.ext4mage.com)
*/

$installer = $this;

$installer->startSetup();

$installString = <<<EOT
DROP TABLE IF EXISTS {$this->getTable('html2pdf/file')};
CREATE TABLE {$this->getTable('html2pdf/file')} (
`file_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
`template_id` int(11) NOT NULL COMMENT 'template ID',
`order_id` int(10) unsigned DEFAULT NULL COMMENT 'Comment',
`invoice_id` int(10) unsigned DEFAULT NULL COMMENT 'Comment',
`shipment_id` int(10) unsigned DEFAULT NULL COMMENT 'Comment',
`creditmemo_id` int(10) unsigned DEFAULT NULL COMMENT 'Comment',
`filepath` varchar(255) NOT NULL COMMENT 'Content',
`creation_time` timestamp NULL DEFAULT NULL COMMENT 'Creation Time',
PRIMARY KEY (`file_id`,`template_id`),
KEY `FK_HTML2PDF_FILES_TEMPLATE_ID_HTML2PDF_TEMPLATES_TEMPLATE_ID` (`template_id`),
KEY `FK_HTML2PDF_FILES_ORDER_ID_SALES_FLAT_ORDER_ENTITY_ID` (`order_id`),
KEY `FK_HTML2PDF_FILES_INVOICE_ID_SALES_FLAT_INVOICE_ENTITY_ID` (`invoice_id`),
KEY `FK_HTML2PDF_FILES_SHIPMENT_ID_SALES_FLAT_SHIPMENT_ENTITY_ID` (`shipment_id`),
KEY `FK_HTML2PDF_FILES_CREDITMEMO_ID_SALES_FLAT_CREDITMEMO_ENTITY_ID` (`creditmemo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Generated PDF with references to file';
EOT;
$installer->run($installString);

$installer->endSetup();
