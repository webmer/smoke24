<?php
/**
* Magento
*
* NOTICE
*
* This source file a part of Ext4mage_Html2pdf extension
* all rights to this modul belongs to ext4mage.com
*
* @category    Ext4mage
* @package     Ext4mage_Html2pdf
* @copyright   Copyright (c) 2011 ext4mage (http://www.ext4mage.com)
*/

$installer = $this;

$installer->startSetup();

    $installer->getConnection()->addColumn(
        $installer->getTable('html2pdf/column'),'row_span',array(
           	'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
           	'default'   => '0',
			'comment'   => 'First row to soan all rows'
        )
    );
    
$installer->endSetup();
