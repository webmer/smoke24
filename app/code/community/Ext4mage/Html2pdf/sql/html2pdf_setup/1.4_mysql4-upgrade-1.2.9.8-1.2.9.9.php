<?php
/**
* Magento
*
* NOTICE
*
* This source file a part of Ext4mage_Html2pdf extension
* all rights to this modul belongs to ext4mage.com
*
* @category    Ext4mage
* @package     Ext4mage_Html2pdf
* @copyright   Copyright (c) 2011 ext4mage (http://www.ext4mage.com)
*/

$installer = $this;

$installer->startSetup();

$updateString = <<<EOT

ALTER TABLE {$this->getTable('html2pdf/template')} MODIFY COLUMN `page_size` VARCHAR( 255 ) NOT NULL DEFAULT 'A4';

EOT;

$installer->run($updateString);

$installer->endSetup();
