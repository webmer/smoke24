<?php

$installer = $this;

$installer->startSetup();

/**
* Create table 'html2pdf/text'
*/
$table = $installer->getConnection()
	->newTable($installer->getTable('html2pdf/text'))
	->addColumn('text_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
		), 'ID')
	->addColumn('title', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
		), 'Title')
	->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'default'   => '1',
		), 'Is Active')
	->addColumn('type', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'default'   => '1',
		), 'Text type')
	->addColumn('textcontent', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(), 'Content')
	->addColumn('creation_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Creation Time')
	->addColumn('update_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Modification Time')
->setComment('Text element for pdf, can be header, top, bottom or footer');
$installer->getConnection()->createTable($table);

/**
* Create table 'html2pdf/table'
*/
$table = $installer->getConnection()
	->newTable($installer->getTable('html2pdf/table'))
	->addColumn('table_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
		), 'ID')
	->addColumn('title', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
		), 'Title')
	->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'default'   => '1',
		), 'Is Active')
	->addColumn('table_style', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Comment')
	->addColumn('table_width', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(), 'Comment')
	->addColumn('table_cellspacing', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(), 'Comment')
	->addColumn('table_cellpadding', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(), 'Comment')
	->addColumn('header_style', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Comment')
	->addColumn('even_row_style', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Comment')
	->addColumn('odd_row_style', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Comment')
	->addColumn('creation_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Creation Time')
	->addColumn('update_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Modification Time')
	->setComment('Table setup');
$installer->getConnection()->createTable($table);

/**
* Create table 'html2pdf/template'
*/
$table = $installer->getConnection()
	->newTable($installer->getTable('html2pdf/template'))
	->addColumn('template_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
	    'identity'  => true,
	    'nullable'  => false,
	    'primary'   => true,
		), 'ID')
	->addColumn('title', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
	    'nullable'  => false,
		), 'Title')
	->addColumn('type', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'default'   => '1',
		), 'Type')
	->addColumn('active_from', Varien_Db_Ddl_Table::TYPE_DATE, null, array(), 'Comment')
	->addColumn('active_to', Varien_Db_Ddl_Table::TYPE_DATE, null, array(), 'Comment')
	->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
	    'nullable'  => false,
	    'default'   => '1',
		), 'Is Active')
	->addColumn('default_font', Varien_Db_Ddl_Table::TYPE_TEXT, '255', array(
		'nullable'  => false,
	    'default'   => 'arial',
		), 'Comment')
	->addColumn('default_font_size', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
		'nullable'  => false,
	    'default'   => '12',
		), 'Comment')
	->addColumn('default_font_color', Varien_Db_Ddl_Table::TYPE_TEXT, '255', array(
		'nullable'  => false,
	    'default'   => '000000',
		), 'Comment')
	->addColumn('page_margin_top', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
		'nullable'  => false,
	    'default'   => '10',
		), 'Comment')
	->addColumn('page_margin_bottom', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
		'nullable'  => false,
	    'default'   => '10',
		), 'Comment')
	->addColumn('page_margin_left', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
		'nullable'  => false,
	    'default'   => '10',
		), 'Comment')
	->addColumn('page_margin_right', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
		'nullable'  => false,
	    'default'   => '10',
		), 'Comment')
	->addColumn('page_size', Varien_Db_Ddl_Table::TYPE_TEXT, '2', array(
		'nullable'  => false,
	    'default'   => 'A4',
		), 'Comment')
	->addColumn('page_orientation', Varien_Db_Ddl_Table::TYPE_TEXT, '1', array(
		'nullable'  => false,
	    'default'   => 'P',
		), 'Comment')
	->addColumn('header_element_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable'  => false,
	    ), 'Comment')
	->addColumn('text_element_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable'  => false,
	    ), 'Comment')
	->addColumn('table_element_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable'  => false,
	    ), 'Comment')
	->addColumn('footer_element_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable'  => false,
	    ), 'Comment')
	->addColumn('cross_sell_element_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'default'  => NULL,
	    ), 'Comment')
	->addColumn('creation_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Creation Time')
	->addColumn('update_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Modification Time')
    ->addForeignKey($installer->getFkName('html2pdf/template', 'header_element_id', 'html2pdf/text', 'text_id'),
        'header_element_id', $installer->getTable('html2pdf/text'), 'text_id',
        Varien_Db_Ddl_Table::ACTION_NO_ACTION, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('html2pdf/template', 'text_element_id', 'html2pdf/text', 'text_id'),
        'text_element_id', $installer->getTable('html2pdf/text'), 'text_id',
        Varien_Db_Ddl_Table::ACTION_NO_ACTION, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('html2pdf/template', 'table_element_id', 'html2pdf/table', 'table_id'),
        'table_element_id', $installer->getTable('html2pdf/table'), 'table_id',
        Varien_Db_Ddl_Table::ACTION_NO_ACTION, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('html2pdf/template', 'footer_element_id', 'html2pdf/text', 'text_id'),
        'footer_element_id', $installer->getTable('html2pdf/text'), 'text_id',
        Varien_Db_Ddl_Table::ACTION_NO_ACTION, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('html2pdf/template', 'cross_sell_element_id', 'html2pdf/text', 'text_id'),
        'cross_sell_element_id', $installer->getTable('html2pdf/text'), 'text_id',
        Varien_Db_Ddl_Table::ACTION_NO_ACTION, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Template table - used to generete the pdf from');
$installer->getConnection()->createTable($table);

/**
* Create table 'html2pdf/column'
*/
$table = $installer->getConnection()
	->newTable($installer->getTable('html2pdf/column'))
	->addColumn('column_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
		), 'ID')
	->addColumn('table_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
		), 'Table ID')
	->addColumn('title', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
		), 'Title')
	->addColumn('sort_order', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'default'   => '1',
		), 'Comment')
	->addColumn('width', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(), 'Comment')
	->addColumn('custom_style', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Comment')
	->addColumn('value', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
		'nullable'  => false,
 		), 'Comment')
	->addColumn('option_value', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Comment')
	->addColumn('bundle_show', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable'  => false,
	    ), 'Comment')
	->addColumn('bundle_product', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Comment')
	->addColumn('bundle_item_group', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Comment')
	->addColumn('bundle_item', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Comment')
	->addColumn('download_show', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable'  => false,
	    ), 'Comment')
	->addColumn('download_value', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Comment')
	->addColumn('download_option_value', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Comment')
	->addColumn('virtual_show', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable'  => false,
	    ), 'Comment')
	->addColumn('virtual_value', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Comment')
	->addColumn('virtual_option_value', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Comment')
	->addColumn('creation_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Creation Time')
	->addColumn('update_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Modification Time')
	->addIndex($installer->getIdxName('html2pdf/column', array('table_id')),
        array('table_id'))
    ->addForeignKey($installer->getFkName('html2pdf/column', 'table_id', 'html2pdf/table', 'table_id'),
        'table_id', $installer->getTable('html2pdf/table'), 'table_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Table setup');
$installer->getConnection()->createTable($table);


/**
* Create table 'html2pdf/template_store'
*/
$table = $installer->getConnection()
	->newTable($installer->getTable('html2pdf/template_store'))
	->addColumn('template_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
	    'nullable'  => false,
	    'primary'   => true,
		), 'ID')
	->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
	    'unsigned'  => true,
	    'nullable'  => false,
	    'primary'   => true,
		), 'Store ID')
	->addIndex($installer->getIdxName('html2pdf/template_store', array('store_id')),
		array('store_id'))
	->addForeignKey($installer->getFkName('html2pdf/template_store', 'template_id', 'html2pdf/template', 'template_id'),
	    'template_id', $installer->getTable('html2pdf/template'), 'template_id',
		Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
	->addForeignKey($installer->getFkName('html2pdf/template_store', 'store_id', 'core/store', 'store_id'),
	    'store_id', $installer->getTable('core/store'), 'store_id',
		Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
	->setComment('To use different template besed on store');
$installer->getConnection()->createTable($table);

$installer->endSetup();