<?php

$installer = $this;
$installer->startSetup();


/**
 * Drop foreign keys
 */
$installer->getConnection()->dropForeignKey(
$installer->getTable('html2pdf/template'),
    	'FK_HTML2PDF_TEMPLATE_HEADER'
);
$installer->getConnection()->dropForeignKey(
$installer->getTable('html2pdf/template'),
    	'FK_HTML2PDF_TEMPLATE_TEXT'
);
$installer->getConnection()->dropForeignKey(
$installer->getTable('html2pdf/template'),
    	'FK_HTML2PDF_TEMPLATE_TABLE'
);
$installer->getConnection()->dropForeignKey(
$installer->getTable('html2pdf/template'),
    	'FK_HTML2PDF_TEMPLATE_FOOTER'
);
$installer->getConnection()->dropForeignKey(
$installer->getTable('html2pdf/template'),
    	'FK_HTML2PDF_TEMPLATE_CROSS'
);
$installer->getConnection()->dropForeignKey(
$installer->getTable('html2pdf/column'),
    	'FK_HTML2PDF_COLUMN_TABLE'
);
$installer->getConnection()->dropForeignKey(
$installer->getTable('html2pdf/template_store'),
    	'FK_HTML2PDF_TEMPLATE_STORE_TEMPLATE'
);
$installer->getConnection()->dropForeignKey(
$installer->getTable('html2pdf/template_store'),
    	'FK_HTML2PDF_TEMPLATE_STORE_STORE'
);

/**
 * Change columns
 */
$tables = array(
$installer->getTable('html2pdf/text') => array(
		'columns' => array(
			'text_id' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_INTEGER, 
				'identity'  => true,
				'nullable'  => false,
				'primary'   => true,
				'comment' => 'ID'
),
			'title' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'nullable'  => false,
				'lenght' => 255,
				'comment' => 'Title'
),
			'is_active' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT, 
				'nullable'  => false,
				'default'   => '1',
				'comment' => 'Is Active'
),
			'type' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT, 
				'nullable'  => false,
				'default'   => '1',
				'comment' => 'Text type'
),
			'textcontent' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '2M', 
				'comment' => 'Content'
),
			'creation_time' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TIMESTAMP, 
				'comment' => 'Creation Time'
),
			'update_time' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TIMESTAMP, 
				'comment' => 'Modification Time'
				)
				),
		'comment' => 'Text element for pdf, can be header, top, bottom or footer'
		),
		$installer->getTable('html2pdf/table') => array(
		'columns' => array(
			'table_id' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_INTEGER, 
				'identity'  => true,
				'nullable'  => false,
				'primary'   => true,
				'comment' => 'ID'
				),
			'title' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => 255,
				'nullable'  => false,
				'comment' => 'Title'
				),
			'is_active' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT, 
				'nullable'  => false,
				'default'   => '1',
				'comment' => 'Is Active'
				),
			'table_style' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '64k',
				'comment' => 'Comment'
				),
			'table_width' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_INTEGER, 
				'comment' => 'Comment'
				),
			'table_cellspacing' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_INTEGER, 
				'comment' => 'Comment'
				),
			'table_cellpadding' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_INTEGER, 
				'comment' => 'Comment'
				),
			'header_style' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '64k', 
				'comment' => 'Comment'
				),
			'even_row_style' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '64k', 
				'comment' => 'Comment'
				),
			'odd_row_style' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '64k', 
				'comment' => 'Comment'
				),
			'creation_time' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TIMESTAMP, 
				'comment' => 'Creation Time'
				),
			'update_time' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TIMESTAMP, 
				'comment' => 'Modification Time'
				)
				),
		'comment' => 'Table setup'
		),
		$installer->getTable('html2pdf/template') => array(
		'columns' => array(
			'template_id' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_INTEGER, 
				'identity'  => true,
				'nullable'  => false,
				'primary'   => true,
				'comment' => 'ID'
				),
			'title' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => 255, 
				'nullable'  => false,
				'comment' => 'Title'
				),
			'type' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT, 
				'nullable'  => false,
				'default'   => '1',
				'comment' => 'Type'
				),
			'active_from' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_DATE, 
				'comment' => 'Comment'
				),
			'active_to' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_DATE, 
				'comment' => 'Comment'
				),
			'is_active' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT, 
				'nullable'  => false,
				'default'   => '1',
				'comment' => 'Is Active'
				),
			'default_font' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '255', 
				'nullable'  => false,
				'comment' => 'Comment'
				),
			'default_font_size' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT, 
				'nullable'  => false,
				'default'   => '12',
				'comment' => 'Comment'
				),
			'default_font_color' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '255', 
				'nullable'  => false,
				'comment' => 'Comment'
				),
			'page_margin_top' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT, 
				'nullable'  => false,
				'default'   => '10',
				'comment' => 'Comment'
				),
			'page_margin_bottom' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT, 
				'nullable'  => false,
				'default'   => '10',
				'comment' => 'Comment'
				),
			'page_margin_left' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT, 
				'nullable'  => false,
				'default'   => '10',
				'comment' => 'Comment'
				),
			'page_margin_right' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT, 
				'nullable'  => false,
				'default'   => '10',
				'comment' => 'Comment'
				),
			'page_size' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '2', 
				'nullable'  => false,
				'comment' => 'Comment'
				),
			'page_orientation' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '1', 
				'nullable'  => false,
				'comment' => 'Comment'
				),
			'header_element_id' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_INTEGER, 
				'nullable'  => false,
				'comment' => 'Comment'
				),
			'text_element_id' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_INTEGER, 
				'nullable'  => false,
				'comment' => 'Comment'
				),
			'table_element_id' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_INTEGER, 
				'nullable'  => false,
				'comment' => 'Comment'
				),
			'footer_element_id' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_INTEGER, 
				'nullable'  => false,
				'comment' => 'Comment'
				),
			'cross_sell_element_id' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_INTEGER, 
				'default'  => NULL,
				'comment' => 'Comment'
				),
			'creation_time' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TIMESTAMP, 
				'comment' => 'Creation Time'
				),
			'update_time' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TIMESTAMP, 
				'comment' => 'Modification Time'
				)
			),
		'comment' => 'Template table - used to generete the pdf from'
		),
		$installer->getTable('html2pdf/column') => array(
		'columns' => array(
			'column_id' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_INTEGER, 
				'identity'  => true,
				'nullable'  => false,
				'primary'   => true,
				'comment' => 'ID'
				),
			'table_id' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_INTEGER, 
				'nullable'  => false,
				'comment' => 'Table ID'
				),
			'title' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => 255, 
				'nullable'  => false,
				'comment' => 'Title'
				),
			'sort_order' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT, 
				'nullable'  => false,
				'default'   => '1',
				'comment' => 'Comment'
				),
			'width' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_INTEGER, 
				'comment' => 'Comment'
				),
			'custom_style' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '64k',
				'comment' => 'Comment'
				),
			'value' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '64k', 
				'nullable'  => false,
				'comment' => 'Comment'
				),
			'option_value' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '64k', 
				'comment' => 'Comment'
				),
			'bundle_show' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_INTEGER, 
				'nullable'  => false,
				'comment' => 'Comment'
				),
			'bundle_product' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '64k', 
				'comment' => 'Comment'
				),
			'bundle_item_group' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '64k', 
				'comment' => 'Comment'
				),
			'bundle_item' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '64k', 
				'comment' => 'Comment'
				),
			'download_show' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_INTEGER, 
				'nullable'  => false,
				'comment' => 'Comment'
				),
			'download_value' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '64k', 
				'comment' => 'Comment'
				),
			'download_option_value' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '64k', 
				'comment' => 'Comment'
				),
			'virtual_show' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_INTEGER, 
				'nullable'  => false,
				'comment' => 'Comment'
				),
			'virtual_value' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '64k', 
				'comment' => 'Comment'
				),
			'virtual_option_value' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TEXT, 
				'lenght' => '64k', 
				'comment' => 'Comment'
				),
			'creation_time' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TIMESTAMP, 
				'comment' => 'Creation Time'
				),
			'update_time' => array(
				'type' => Varien_Db_Ddl_Table::TYPE_TIMESTAMP, 
				'comment' => 'Modification Time'
				)
				),
		'comment' => 'Table setup'
		),
		$installer->getTable('html2pdf/template_store') => array(
        'columns' => array(
            'template_id' => array(
                'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
                'nullable'  => false,
                'primary'   => true,
                'comment'   => 'ID'
                ),
            'store_id' => array(
                'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
                'unsigned'  => true,
                'nullable'  => false,
                'primary'   => true,
                'comment'   => 'Store ID'
                )
                ),
        'comment' => 'Tempalte To Store Linkage Table'
        )
        );

        $installer->getConnection()->modifyTables($tables);


        /**
         * Add indexes
         */
        $installer->getConnection()->addIndex(
        $installer->getTable('html2pdf/column'),
        $installer->getIdxName('html2pdf/column', array('table_id')),
        array('table_id')
        );
        $installer->getConnection()->addIndex(
        $installer->getTable('html2pdf/template_store'),
        $installer->getIdxName('html2pdf/template_store', array('store_id')),
        array('store_id')
        );

        /**
         * Add foreign keys
         */
        $installer->getConnection()->addForeignKey(
        $installer->getFkName('html2pdf/template', 'header_element_id', 'html2pdf/text', 'text_id'),
        $installer->getTable('html2pdf/template'),
    'header_element_id',
        $installer->getTable('html2pdf/text'),
    'text_id'
        );
        $installer->getConnection()->addForeignKey(
        $installer->getFkName('html2pdf/template', 'text_element_id', 'html2pdf/text', 'text_id'),
        $installer->getTable('html2pdf/template'),
    'text_element_id',
        $installer->getTable('html2pdf/text'),
    'text_id'
        );
        $installer->getConnection()->addForeignKey(
        $installer->getFkName('html2pdf/template', 'footer_element_id', 'html2pdf/text', 'text_id'),
        $installer->getTable('html2pdf/template'),
    'footer_element_id',
        $installer->getTable('html2pdf/text'),
    'text_id'
        );
        $installer->getConnection()->addForeignKey(
        $installer->getFkName('html2pdf/template', 'cross_sell_element_id', 'html2pdf/text', 'text_id'),
        $installer->getTable('html2pdf/template'),
    'cross_sell_element_id',
        $installer->getTable('html2pdf/text'),
    'text_id'
        );
        $installer->getConnection()->addForeignKey(
        $installer->getFkName('html2pdf/template', 'table_element_id', 'html2pdf/table', 'table_id'),
        $installer->getTable('html2pdf/template'),
    'table_element_id',
        $installer->getTable('html2pdf/table'),
    'table_id'
        );
        $installer->getConnection()->addForeignKey(
        $installer->getFkName('html2pdf/column', 'table_id', 'html2pdf/table', 'table_id'),
        $installer->getTable('html2pdf/column'),
    'table_id',
        $installer->getTable('html2pdf/table'),
    'table_id'
        );
        $installer->getConnection()->addForeignKey(
        $installer->getFkName('html2pdf/template_store', 'template_id', 'html2pdf/template', 'template_id'),
        $installer->getTable('html2pdf/template_store'),
    'template_id',
        $installer->getTable('html2pdf/template'),
    'template_id'
        );
        $installer->getConnection()->addForeignKey(
        $installer->getFkName('html2pdf/template_store', 'store_id', 'core/store', 'store_id'),
        $installer->getTable('html2pdf/template_store'),
    'store_id',
        $installer->getTable('core/store'),
    'store_id'
        );

        $installer->endSetup();