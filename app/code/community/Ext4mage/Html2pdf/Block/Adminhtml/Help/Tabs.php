<?php

class Ext4mage_Html2pdf_Block_Adminhtml_Help_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

	public function __construct()
	{
		parent::__construct();
		$this->setId('help_tabs');
		//$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('html2pdf')->__('Html2Pdf Help'));
	}

	protected function _beforeToHtml()
	{
		$this->addTab('generel_section', array(
          'label'     => Mage::helper('html2pdf')->__('General'),
          'title'     => Mage::helper('html2pdf')->__('All pdf - general variabels'),
          'content'   => $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_general')->toHtml(),
		));
		$this->addTab('invoice_section', array(
          'label'     => Mage::helper('html2pdf')->__('Invoice pdf'),
          'title'     => Mage::helper('html2pdf')->__('Invoice pdf - variabels'),
          'content'   => $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_invoice')->toHtml(),
		));
		$this->addTab('shipping_section', array(
          'label'     => Mage::helper('html2pdf')->__('Shipping pdf'),
          'title'     => Mage::helper('html2pdf')->__('Shipping pdf - variabels'),
          'content'   => $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_shipping')->toHtml(),
		));
		$this->addTab('credit_section', array(
          'label'     => Mage::helper('html2pdf')->__('Credit memo pdf'),
          'title'     => Mage::helper('html2pdf')->__('Credit memo pdf - variabels'),
          'content'   => $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_credit')->toHtml(),
		));
		$this->addTab('product_section', array(
          'label'     => Mage::helper('html2pdf')->__('Product'),
          'title'     => Mage::helper('html2pdf')->__('Product variables'),
          'content'   => $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_product')->toHtml(),
		));
		$this->addTab('productbundle_section', array(
          'label'     => Mage::helper('html2pdf')->__('Product Bundle'),
          'title'     => Mage::helper('html2pdf')->__('Product Bundle variables'),
          'content'   => $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_productbundle')->toHtml(),
		));
		$this->addTab('cross_section', array(
          'label'     => Mage::helper('html2pdf')->__('Cross Sell'),
          'title'     => Mage::helper('html2pdf')->__('Cross Sell variables'),
          'content'   => $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_crosssell')->toHtml(),
		));
		$this->addTab('customer_section', array(
          'label'     => Mage::helper('html2pdf')->__('Customer'),
          'title'     => Mage::helper('html2pdf')->__('Order Customer variables'),
          'content'   => $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_customer')->toHtml(),
		));
		$this->addTab('formatting_section', array(
          'label'     => Mage::helper('html2pdf')->__('Formatting types'),
          'title'     => Mage::helper('html2pdf')->__('Example of formatting variables'),
          'content'   => $this->getLayout()->createBlock('html2pdf/adminhtml_help_tab_formatting')->toHtml(),
		));

		return parent::_beforeToHtml();
	}
}