<?php

class Ext4mage_Html2pdf_Block_Adminhtml_Help_Tab_Crosssell extends Mage_Adminhtml_Block_Widget_Grid
{
	const XPATH_CONFIG_SETTINGS_ORDER_ID				= 'html2pdf/settings/order_id';
	const XPATH_CONFIG_SETTINGS_INVOICE_ID    			= 'html2pdf/settings/invoice_id';

	public function __construct()
	{
		parent::__construct();
		$this->setId('helpProductGrid');
		$this->setFilterVisibility(false);
		$this->setPagerVisibility(false);
	}

	protected function _prepareCollection()
	{
		$collection = new Varien_Data_Collection();
		$orderId = Mage::getStoreConfig(self::XPATH_CONFIG_SETTINGS_ORDER_ID);
		$order = Mage::getModel('sales/order')->loadByIncrementId($orderId);

		if(!$order->getData() && count($order->getData())==0){
			$invoiceId = Mage::getStoreConfig(self::XPATH_CONFIG_SETTINGS_INVOICE_ID);
			$invoice = Mage::getModel('sales/order_invoice')->loadByIncrementId($invoiceId);
			$order = $invoice->getOrder();
			if(!$order->getData() && count($order->getData())==0){
				$row = new Varien_Object(array('key'=>"order_data_?", 'value'=>'Selected order not present',
	      						'object'=>'Order data'));
				$collection->addItem($row);
				$this->setCollection($collection);
				return parent::_prepareCollection();
			}
		}

		$crossItems = Mage::helper('html2pdf/crosssell')->getItems($order->getAllItems());
		
		$crossNum = 0;
		if(!empty($crossItems)){
		  foreach ($crossItems as $crossItem){
  		  	  $row = new Varien_Object(array('key'=>"cross_".$crossNum."_start", 'value'=>"Start off cross sell product $crossNum",
		  		      						'object'=>'Cross Item'));
		  	  $collection->addItem($row);
			  foreach ($crossItem->getData() as $key => $value) {
				if(!is_string($value)) $value = print_r($value, 1);
				$row = new Varien_Object(array('key'=>"cross_data_$key", 'value'=>htmlentities($value),
	      						'object'=>'Cross Item data'));
				$collection->addItem($row);
			  }
  		  	  $row = new Varien_Object(array('key'=>"cross_".$crossNum."_end", 'value'=>"End off cross sell product $crossNum",
		  		      						'object'=>'Cross Item'));
		  	  $collection->addItem($row);
			  $crossNum++;
		  }
		}else{
			$row = new Varien_Object(array('key'=>"cross_{$crossNum}_data_", 'value'=>'Selected order items do not have any cross sell items',
			      						'object'=>'Cross Item data'));
			$collection->addItem($row);
		}

		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('object', array(
	      'header'    => Mage::helper('html2pdf')->__('Object'),
	      'align'     =>'left',
	      'index'     => 'object',
	      'sortable'  => false,
    	  'type'      => 'text',
          'width'     => '20%'
		));
		$this->addColumn('variable', array(
          'header'    => Mage::helper('html2pdf')->__('Key'),
          'align'     =>'left',
          'index'     => 'key',
	      'sortable'  => false,
    	  'type'      => 'text',
          'width'     => '40%'
		));
		$this->addColumn('value', array(
          'header'    => Mage::helper('html2pdf')->__('Exampel value'),
          'align'     =>'left',
          'index'     => 'value',
	      'sortable'  => false,
    	  'type'      => 'text',
          'width'     => '40%'
		));

		$this->addExportType('*/*/exportProductCsv', Mage::helper('html2pdf')->__('CSV'));
		$this->addExportType('*/*/exportProductXml', Mage::helper('html2pdf')->__('XML'));

		return parent::_prepareColumns();
	}
	
	/**
	* Get row edit url
	*
	* @return string
	*/
	public function getRowUrl($row)
	{
		return false;
		//return $this->getUrl('*/*/edit', array('type'=>$row->getId()));
	}
}