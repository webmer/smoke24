<?php

class Ext4mage_Html2pdf_Block_Adminhtml_Table_Edit_Tab_Columns_Column extends Mage_Adminhtml_Block_Widget
{
	protected $_itemCount = 1;

	public function __construct()
	{
		parent::__construct();
		$this->setTemplate('html2pdf/columns/column.phtml');
	}

	public function getItemCount()
	{
		return $this->_itemCount;
	}

	public function setItemCount($itemCount)
	{
		$this->_itemCount = max($this->_itemCount, $itemCount);
		return $this;
	}

	protected function _prepareLayout()
	{
		$this->setChild('delete_button',
		$this->getLayout()->createBlock('adminhtml/widget_button')
		->setData(array(
                    'label' => Mage::helper('html2pdf')->__('Delete Column'),
                    'class' => 'delete delete-table-column '
		))
		);

		return parent::_prepareLayout();
	}

	public function getAddButtonId()
	{
		$buttonId = $this->getLayout()
		->getBlock('html2pdf.admin.table.columns')
		->getChild('add_button')->getId();
		return $buttonId;
	}

	public function getDeleteButtonHtml()
	{
		return $this->getChildHtml('delete_button');
	}

	public function getColumnValues()
	{
		$columns = Mage::getResourceModel('html2pdf/column');
		$columnsList = $columns->getColumnByTable($this->getRequest()->getParam('id'));

		$values = array();
		foreach ($columnsList as $column) {

			$this->setItemCount($column['column_id']);

			$value = array();

			$value['column_id'] = $column['column_id'];
			$value['title'] = $column['title'];
			$value['sort_order'] = $column['sort_order'];
			$value['width'] = $column['width'];
			$value['row_span'] = $column['row_span'];
			$value['custom_style'] = $column['custom_style'];
			$value['value'] = $column['value'];
			$value['option_value'] = $column['option_value'];
			$value['bundle_show'] = $column['bundle_show'];
			$value['bundle_product'] = $column['bundle_product'];
			$value['bundle_item_group'] = $column['bundle_item_group'];
			$value['bundle_item'] = $column['bundle_item'];
			$value['download_show'] = $column['download_show'];
			$value['download_value'] = $column['download_value'];
			$value['download_option_value'] = $column['download_option_value'];
			$value['virtual_show'] = $column['virtual_show'];
			$value['virtual_value'] = $column['virtual_value'];
			$value['virtual_option_value'] = $column['virtual_option_value'];
			$value['id'] = $column['column_id'];
			$value['item_count'] = $this->getItemCount();
			$values[] = new Varien_Object($value);
		}

		return $values;
	}

}
