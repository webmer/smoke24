<?php

class Ext4mage_Html2pdf_Block_Adminhtml_Table_Edit_Tab_Generel extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('generel_fieldset', array('legend'=>Mage::helper('html2pdf')->__('Element information')));
		 
		$fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('html2pdf')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
		));

		$fieldset->addField('is_active', 'select', array(
          'label'     => Mage::helper('html2pdf')->__('Status'),
          'name'      => 'is_active',
          'required'  => true,
          'values'    => array(
		array(
                  'value'     => 1,
                  'label'     => Mage::helper('html2pdf')->__('Enabled'),
		),

		array(
                  'value'     => 2,
                  'label'     => Mage::helper('html2pdf')->__('Disabled'),
		),
		),
		));

		$fieldset->addField('table_style', 'text', array(
          'label'     => Mage::helper('html2pdf')->__('Table style'),
          'required'  => false,
          'name'      => 'table_style',
		));

		$fieldset->addField('table_width', 'text', array(
          'label'     => Mage::helper('html2pdf')->__('Table width in %'),
          'required'  => false,
          'name'      => 'table_width',
		));

		$fieldset->addField('table_cellspacing', 'text', array(
          'label'     => Mage::helper('html2pdf')->__('Table cellspacing in px'),
          'required'  => false,
          'name'      => 'table_cellspacing',
		));

		$fieldset->addField('table_cellpadding', 'text', array(
          'label'     => Mage::helper('html2pdf')->__('Table cellpadding in px'),
          'required'  => false,
          'name'      => 'table_cellpadding',
		));

		$fieldset->addField('header_style', 'text', array(
          'label'     => Mage::helper('html2pdf')->__('Header style'),
          'required'  => false,
          'name'      => 'header_style',
		));

		$fieldset->addField('even_row_style', 'text', array(
          'label'     => Mage::helper('html2pdf')->__('Even product row style'),
          'required'  => false,
          'name'      => 'even_row_style',
		));

		$fieldset->addField('odd_row_style', 'text', array(
          'label'     => Mage::helper('html2pdf')->__('Odd product row style'),
          'required'  => false,
          'name'      => 'odd_row_style',
		));

		$fieldset->addField('saveas', 'hidden', array(
	        'name'      => 'saveas',
	        'value'     => '0',
		));

		if ( Mage::getSingleton('adminhtml/session')->getHtml2pdfData() )
		{
			$form->setValues(Mage::getSingleton('adminhtml/session')->getHtml2pdfData());
			Mage::getSingleton('adminhtml/session')->setTableData(null);
		} elseif ( Mage::registry('html2pdf_data') ) {
			$form->setValues(Mage::registry('html2pdf_data')->getData());
		}
		return parent::_prepareForm();
	}
}