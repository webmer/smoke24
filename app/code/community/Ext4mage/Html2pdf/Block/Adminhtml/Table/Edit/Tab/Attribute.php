<?php

class Ext4mage_Html2pdf_Block_Adminhtml_Table_Edit_Tab_Attribute extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('attribute_fieldset', array('legend'=>Mage::helper('html2pdf')->__('Element information')));
		 
		$fieldset->addField('attr_mulit_action', 'select', array(
          'label'     => Mage::helper('html2pdf')->__('Action when multi attributes'),
          'name'      => 'attr_mulit_action',
          'required'  => true,
          'values'    => array(
		array(
                  'value'     => 1,
                  'label'     => Mage::helper('html2pdf')->__('New line'),
		),

		array(
                  'value'     => 2,
                  'label'     => Mage::helper('html2pdf')->__('None - handled in text below'),
		),
		),
		));

		$fieldset->addField('attr_multi_values_action', 'select', array(
          'label'     => Mage::helper('html2pdf')->__('Action between multi values'),
          'name'      => 'attr_multi_values_action',
          'required'  => true,
          'values'    => array(
		array(
                  'value'     => 1,
                  'label'     => Mage::helper('html2pdf')->__('Repeat name and value on new line'),
		),

		array(
                  'value'     => 2,
                  'label'     => Mage::helper('html2pdf')->__('Repeat value on new line'),
		),
		array(
                  'value'     => 3,
                  'label'     => Mage::helper('html2pdf')->__('Use comma between values'),
		),
		array(
                  'value'     => 4,
                  'label'     => Mage::helper('html2pdf')->__('Use dash between values'),
		),
		),
		));
		 
		$wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig(
		array('tab_id' => $this->getTabId())
		);

		try{
			$wysiwygConfig2 = Mage::getSingleton('cms/wysiwyg_config')->getConfig(
			array('add_widgets' => false, 'add_variables' => true,'add_directives' => true )
			);
			 
			$wysiwygConfig2->setData(Mage::helper('html2pdf')->replace_rec(
                        '/html2pdf/',
                        '/'.(string)Mage::app()->getConfig()->getNode('admin/routers/adminhtml/args/frontName').'/',
			$wysiwygConfig2->getData()
			)
			);
		}catch (Exception $ex){
			$wysiwygConfig2 = null;
		}

		 
		$fieldset->addField('attr_text', 'editor', array(
          'name'      => 'attr_text',
          'label'     => Mage::helper('html2pdf')->__('Attribute text'),
          'title'     => Mage::helper('html2pdf')->__('Attribute text'),
          'style'     => 'width:700px; height:500px;',
          'required'  => true,
          'config'    => $wysiwygConfig2
		));

		if ( Mage::getSingleton('adminhtml/session')->getHtml2pdfData() )
		{
			$form->setValues(Mage::getSingleton('adminhtml/session')->getHtml2pdfData());
			Mage::getSingleton('adminhtml/session')->setTableData(null);
		} elseif ( Mage::registry('html2pdf_data') ) {
			$form->setValues(Mage::registry('html2pdf_data')->getData());
		}
		return parent::_prepareForm();
	}
}