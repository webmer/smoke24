<?php

class Ext4mage_Html2pdf_Block_Adminhtml_Table_Edit_Tab_Columns extends Mage_Adminhtml_Block_Widget
{
	public function __construct()
	{
		parent::__construct();
		$this->setTemplate('html2pdf/columns.phtml');
	}


	protected function _prepareLayout()
	{
		$this->setChild('add_button',
		$this->getLayout()->createBlock('adminhtml/widget_button')
		->setData(array(
                    'label' => Mage::helper('html2pdf')->__('Add new column'),
                    'class' => 'add',
                    'id'    => 'add_new_column'
		))
		);

		$this->setChild('column_box',
		$this->getLayout()->createBlock('html2pdf/adminhtml_table_edit_tab_columns_column')
		);

		return parent::_prepareLayout();
	}

	public function getAddButtonHtml()
	{
		return $this->getChildHtml('add_button');
	}

	public function getColumnBoxHtml()
	{
		return $this->getChildHtml('column_box');
	}

}
