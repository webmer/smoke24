<?php

class Ext4mage_Html2pdf_Block_Adminhtml_Table_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

	public function __construct()
	{
		parent::__construct();
		$this->setId('table_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('html2pdf')->__('HTML2PDF Table'));
	}

	protected function _beforeToHtml()
	{
		$this->addTab('general_section', array(
          'label'     => Mage::helper('html2pdf')->__('General'),
          'title'     => Mage::helper('html2pdf')->__('General table information'),
          'content'   => $this->getLayout()->createBlock('html2pdf/adminhtml_table_edit_tab_generel')->toHtml(),
		));

		$this->addTab('column_section', array(
          'label'     => Mage::helper('html2pdf')->__('Table columns'),
          'title'     => Mage::helper('html2pdf')->__('Table columns content'),
          'content'   => $this->getLayout()->createBlock('html2pdf/adminhtml_table_edit_tab_columns', 'html2pdf.admin.table.columns')->toHtml(),
		));

		return parent::_beforeToHtml();
	}
}