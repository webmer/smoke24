<?php

class Ext4mage_Html2pdf_Block_Adminhtml_Table_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('tableGrid');
		$this->setDefaultSort('table_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('html2pdf/table')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('table_id', array(
          'header'    => Mage::helper('html2pdf')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'table_id',
		));

		$this->addColumn('title', array(
          'header'    => Mage::helper('html2pdf')->__('Title'),
          'align'     =>'left',
          'index'     => 'title',
		));

		$this->addColumn('creation_time', array(
			'header'    => Mage::helper('html2pdf')->__('Created at'),
			'align'     => 'left',
			'width'     => '120px',
			'type'      => 'date',
			'default'   => '--',
			'index'     => 'creation_time',
		));

		$this->addColumn('update_time', array(
			'header'    => Mage::helper('html2pdf')->__('Updated at'),
			'align'     => 'left',
			'width'     => '120px',
			'type'      => 'date',
			'default'   => '--',
			'index'     => 'update_time',
		));

		$this->addColumn('is_active', array(
          'header'    => Mage::helper('html2pdf')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'is_active',
          'type'      => 'options',
          'options'   => array(
		1 => 'Enabled',
		2 => 'Disabled',
		),
		));
		 
		$this->addColumn('action',
		array(
                'header'    =>  Mage::helper('html2pdf')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
		array(
                        'caption'   => Mage::helper('html2pdf')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
		)
		),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
		));

		return parent::_prepareColumns();
	}

	protected function _prepareMassaction()
	{
		$this->setMassactionIdField('table_id');
		$this->getMassactionBlock()->setFormFieldName('table');

		$this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('html2pdf')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('html2pdf')->__('Are you sure?')
		));

		$statuses = Mage::getSingleton('html2pdf/status')->getOptionArray();

		array_unshift($statuses, array('label'=>'', 'value'=>''));
		$this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('html2pdf')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('html2pdf')->__('Status'),
                         'values' => $statuses
		)
		)
		));
		return $this;
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}

}