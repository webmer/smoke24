<?php
class Ext4mage_Html2pdf_Block_Adminhtml_Template extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_controller = 'adminhtml_template';
		$this->_blockGroup = 'html2pdf';
		$this->_headerText = Mage::helper('html2pdf')->__('HTML2PDF Templates');
		$this->_addButtonLabel = Mage::helper('html2pdf')->__('Create new Template');
		parent::__construct();
	}
}