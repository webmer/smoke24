<?php
class Ext4mage_Html2pdf_Block_Adminhtml_Table extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_controller = 'adminhtml_table';
		$this->_blockGroup = 'html2pdf';
		$this->_headerText = Mage::helper('html2pdf')->__('HTML2PDF Table');
		$this->_addButtonLabel = Mage::helper('html2pdf')->__('Create new Table');
		parent::__construct();
	}
}