<?php

class Ext4mage_Html2pdf_Block_Adminhtml_Text_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		parent::__construct();

		$helpLinkUrl = "http://ext4mage.com/gethelp/html2pdf/text/edit.html";
		 
		$this->_objectId = 'id';
		$this->_blockGroup = 'html2pdf';
		$this->_controller = 'adminhtml_text';

		$this->_addButton('save_as_button', array(
            'label'     => Mage::helper('html2pdf')->__('Save As'),
            'onclick'   => 'saveAs();',
            'class'     => 'save'
		));

		$this->_updateButton('save', 'label', Mage::helper('html2pdf')->__('Save Element'));
		$this->_updateButton('delete', 'label', Mage::helper('html2pdf')->__('Delete Element'));

		$this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('html2pdf')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
		), -100);

		$this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('html2pdf_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'html2pdf_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'html2pdf_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }

            function saveAs(){
				$('saveas').value = '1';
                editForm.submit($('edit_form').action+'back/edit/');
            }

			$('page-help-link').href = '".$helpLinkUrl."';
        ";
	}

	public function getHeaderText()
	{
		if( Mage::registry('html2pdf_data') && Mage::registry('html2pdf_data')->getId() ) {
			return Mage::helper('html2pdf')->__("Edit '%s'", $this->htmlEscape(Mage::registry('html2pdf_data')->getTitle()));
		} else {
			return Mage::helper('html2pdf')->__('Add Text');
		}
	}

	/**
	 * Load Wysiwyg on demand and Prepare layout
	 */
	protected function _prepareLayout(){
		if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
			$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
		}
		parent::_prepareLayout();
	}

}