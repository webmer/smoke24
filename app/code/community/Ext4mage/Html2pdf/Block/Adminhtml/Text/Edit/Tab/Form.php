<?php

class Ext4mage_Html2pdf_Block_Adminhtml_Text_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('text_edit_form', array('legend'=>Mage::helper('html2pdf')->__('Element information')));
		 
		$fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('html2pdf')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
		));

		$fieldset->addField('is_active', 'select', array(
          'label'     => Mage::helper('html2pdf')->__('Status'),
          'name'      => 'is_active',
          'required'  => true,
          'values'    => array(
		array(
                  'value'     => 1,
                  'label'     => Mage::helper('html2pdf')->__('Enabled'),
		),

		array(
                  'value'     => 2,
                  'label'     => Mage::helper('html2pdf')->__('Disabled'),
		),
		),
		));

		$fieldset->addField('type', 'select', array(
          'label'     => Mage::helper('html2pdf')->__('Type'),
          'name'      => 'type',
          'required'  => true,
          'values'    => array(
		array(
                  'value'     => 1,
                  'label'     => Mage::helper('html2pdf')->__('Header'),
		),

		array(
                  'value'     => 2,
                  'label'     => Mage::helper('html2pdf')->__('Main text'),
		),
		array(
                  'value'     => 3,
                  'label'     => Mage::helper('html2pdf')->__('Footer'),
		),
		array(
                  'value'     => 4,
                  'label'     => Mage::helper('html2pdf')->__('Cross sell'),
		),
		),
		));
		 
		$wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig(
		array('tab_id' => $this->getTabId())
		);
		
		try{
			$config = Mage::getSingleton('cms/wysiwyg_config')->getConfig(
			array(
                        'add_widgets' => false,
                        'add_variables' => true,
                		'add_directives' => true,
			)
			);
			$config->setData(Mage::helper('html2pdf')->replace_rec(
                        '/html2pdf/',
                        '/'.(string)Mage::app()->getConfig()->getNode('admin/routers/adminhtml/args/frontName').'/',
			$config->getData()
			)
			);
		}
		catch (Exception $ex){
			$config = null;
		}
		 
		$fieldset->addField('textcontent', 'editor', array(
          'name'      => 'textcontent',
          'label'     => Mage::helper('html2pdf')->__('Text Content'),
          'title'     => Mage::helper('html2pdf')->__('Text Content'),
          'style'     => 'width:700px; height:500px;',
          'required'  => true,
		  'wysiwyg'   => true,
          'config'    => $config
		));

		$fieldset->addField('saveas', 'hidden', array(
	        'name'      => 'saveas',
	        'value'     => '0',
		));
		 
		if ( Mage::getSingleton('adminhtml/session')->getHtml2pdfData() )
		{
			$form->setValues(Mage::getSingleton('adminhtml/session')->getHtml2pdfData());
			Mage::getSingleton('adminhtml/session')->setTextData(null);
		} elseif ( Mage::registry('html2pdf_data') ) {
			$form->setValues(Mage::registry('html2pdf_data')->getData());
		}
		return parent::_prepareForm();
	}
}