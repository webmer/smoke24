<?php

class Ext4mage_Html2pdf_Block_Adminhtml_Text_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

	public function __construct()
	{
		parent::__construct();
		$this->setId('text_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('html2pdf')->__('HTML2PDF Text'));
	}

	protected function _beforeToHtml()
	{
		$this->addTab('form_section', array(
          'label'     => Mage::helper('html2pdf')->__('Text content'),
          'title'     => Mage::helper('html2pdf')->__('Text content'),
          'content'   => $this->getLayout()->createBlock('html2pdf/adminhtml_text_edit_tab_form')->toHtml(),
		));
		 
		return parent::_beforeToHtml();
	}
}