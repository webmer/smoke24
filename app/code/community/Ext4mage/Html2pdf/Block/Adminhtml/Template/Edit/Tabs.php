<?php

class Ext4mage_Html2pdf_Block_Adminhtml_Template_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

	public function __construct()
	{
		parent::__construct();
		$this->setId('template_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('html2pdf')->__('HTML2PDF Template setup'));
	}

	protected function _beforeToHtml()
	{
		$this->addTab('general_section', array(
          'label'     => Mage::helper('html2pdf')->__('General'),
          'title'     => Mage::helper('html2pdf')->__('General template information'),
          'content'   => $this->getLayout()->createBlock('html2pdf/adminhtml_template_edit_tab_generel')->toHtml(),
		));

		$this->addTab('element_section', array(
          'label'     => Mage::helper('html2pdf')->__('Elements'),
          'title'     => Mage::helper('html2pdf')->__('Template elements'),
          'content'   => $this->getLayout()->createBlock('html2pdf/adminhtml_template_edit_tab_element')->toHtml(),
		));

		$this->addTab('style_section', array(
          'label'     => Mage::helper('html2pdf')->__('Style'),
          'title'     => Mage::helper('html2pdf')->__('Template styles'),
          'content'   => $this->getLayout()->createBlock('html2pdf/adminhtml_template_edit_tab_style')->toHtml(),
		));

		return parent::_beforeToHtml();
	}
}