<?php

class Ext4mage_Html2pdf_Block_Adminhtml_Template_Edit_Tab_Element extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('element_fieldset', array('legend'=>Mage::helper('html2pdf')->__('Template elements')));
		 
		$fieldset->addField('header_element_id', 'select', array(
          'label'     => Mage::helper('html2pdf')->__('Header text'),
          'name'      => 'header_element_id',
          'required'  => true,
          'values'    => Mage::getSingleton('html2pdf/text')->getTextElementsForForm(1)
		));
		 
		$fieldset->addField('text_element_id', 'select', array(
          'label'     => Mage::helper('html2pdf')->__('Main text'),
          'name'      => 'text_element_id',
          'required'  => true,
          'values'    => Mage::getSingleton('html2pdf/text')->getTextElementsForForm(2)
		));
		 
		$fieldset->addField('footer_element_id', 'select', array(
          'label'     => Mage::helper('html2pdf')->__('Footer text'),
          'name'      => 'footer_element_id',
          'required'  => true,
          'values'    => Mage::getSingleton('html2pdf/text')->getTextElementsForForm(3)
		));
		 
		$fieldset->addField('table_element_id', 'select', array(
          'label'     => Mage::helper('html2pdf')->__('Product Table'),
          'name'      => 'table_element_id',
          'required'  => true,
          'values'    => Mage::getSingleton('html2pdf/table')->getTablesForForm()
		));

		$fieldset->addField('cross_sell_element_id', 'select', array(
          'label'     => Mage::helper('html2pdf')->__('Cross sell text'),
          'name'      => 'cross_sell_element_id',
          'required'  => false,
          'values'    => Mage::getSingleton('html2pdf/text')->getTextElementsForForm(4)
		));
		
		if ( Mage::getSingleton('adminhtml/session')->getHtml2pdfData() )
		{
			$form->setValues(Mage::getSingleton('adminhtml/session')->getHtml2pdfData());
			Mage::getSingleton('adminhtml/session')->setTableData(null);
		} elseif ( Mage::registry('html2pdf_data') ) {
			$form->setValues(Mage::registry('html2pdf_data')->getData());
		}
		return parent::_prepareForm();
	}
}