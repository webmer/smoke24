<?php

class Ext4mage_Html2pdf_Block_Adminhtml_Template_Edit_Tab_Generel extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('generel_fieldset', array('legend'=>Mage::helper('html2pdf')->__('Template information')));
		 
		$fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('html2pdf')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
		));

		$fieldset->addField('type', 'select', array(
          'label'     => Mage::helper('html2pdf')->__('Type'),
          'name'      => 'type',
          'required'  => true,
          'values'    => array(
		array(
                  'value'     => 1,
                  'label'     => Mage::helper('html2pdf')->__('Order'),
		),
		array(
                  'value'     => 2,
                  'label'     => Mage::helper('html2pdf')->__('Invoice'),
		),
		array(
                  'value'     => 3,
                  'label'     => Mage::helper('html2pdf')->__('Shipment'),
		),
		array(
                  'value'     => 4,
                  'label'     => Mage::helper('html2pdf')->__('Creditmemo'),
		),
		),
		));

		/**
		 * Check is single store mode
		 */
		if (!Mage::app()->isSingleStoreMode()) {
			$fieldset->addField('store_id', 'multiselect', array(
                'name'      => 'stores[]',
                'label'     => Mage::helper('html2pdf')->__('Store'),
                'title'     => Mage::helper('html2pdf')->__('Store'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true)
			));
		}
		else {
			$fieldset->addField('store_id', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
			));
		}

		$dateFormatIso = Mage::app()->getLocale()->getDateFormat(
		Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
		);
		
		$fieldset->addField('cross_sell_num', 'text', array(
		          'label'     => Mage::helper('html2pdf')->__('Numnber of cross-sell items'),
		          'required'  => false,
		          'name'      => 'cross_sell_num'
		));

		$fieldset->addField('active_from', 'date', array(
            'name'      => 'active_from',
            'label'     => Mage::helper('html2pdf')->__('Active from'),
            'image'     => $this->getSkinUrl('images/grid-cal.gif'),
            'format'    => $dateFormatIso
		));

		$fieldset->addField('active_to', 'date', array(
            'name'      => 'active_to',
            'label'     => Mage::helper('html2pdf')->__('Active to'),
            'image'     => $this->getSkinUrl('images/grid-cal.gif'),
            'format'    => $dateFormatIso
		));

		$fieldset->addField('is_active', 'select', array(
          'label'     => Mage::helper('html2pdf')->__('Status'),
          'name'      => 'is_active',
          'required'  => true,
          'values'    => array(
		array(
                  'value'     => 1,
                  'label'     => Mage::helper('html2pdf')->__('Enabled'),
		),

		array(
                  'value'     => 2,
                  'label'     => Mage::helper('html2pdf')->__('Disabled'),
		),
		),
		));

		$fieldset->addField('saveas', 'hidden', array(
	        'name'      => 'saveas',
	        'value'     => '0',
		));

		if ( Mage::getSingleton('adminhtml/session')->getHtml2pdfData() )
		{
			$form->setValues(Mage::getSingleton('adminhtml/session')->getHtml2pdfData());
			Mage::getSingleton('adminhtml/session')->setTableData(null);
		} elseif ( Mage::registry('html2pdf_data') ) {
			$form->setValues(Mage::registry('html2pdf_data')->getData());
		}
		return parent::_prepareForm();
	}
}