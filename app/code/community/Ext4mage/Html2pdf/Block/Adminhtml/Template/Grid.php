<?php

class Ext4mage_Html2pdf_Block_Adminhtml_Template_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('templateGrid');
		$this->setDefaultSort('template_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('html2pdf/template')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('template_id', array(
          'header'    => Mage::helper('html2pdf')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'template_id',
		));

		$this->addColumn('title', array(
          'header'    => Mage::helper('html2pdf')->__('Title'),
          'align'     =>'left',
          'index'     => 'title',
		));

		$this->addColumn('type', array(
          'header'    => Mage::helper('html2pdf')->__('Type'),
          'align'     => 'left',
          'width'     => '120px',
          'index'     => 'type',
          'type'      => 'options',
          'options'   => array(
		1 => 'Order',
		2 => 'Invoice',
		3 => 'Shipment',
		4 => 'Creditmemo',
		),
		));

		/**
		 * Check is single store mode
		 */
		if (!Mage::app()->isSingleStoreMode()) {
			$this->addColumn('store_id', array(
                'header'        => Mage::helper('html2pdf')->__('Store'),
                'index'         => 'store_id',
                'type'          => 'store',
                'store_all'     => true,
                'store_view'    => true,
                'sortable'      => false,
                'filter_condition_callback'
			=> array($this, '_filterStoreCondition'),
			));
		}


		$this->addColumn('active_from', array(
			'header'    => Mage::helper('html2pdf')->__('Active from'),
			'align'     => 'left',
			'width'     => '120px',
			'type'      => 'date',
			'default'   => '--',
			'index'     => 'active_from',
		));

		$this->addColumn('active_to', array(
			'header'    => Mage::helper('html2pdf')->__('Active to'),
			'align'     => 'left',
			'width'     => '120px',
			'type'      => 'date',
			'default'   => '--',
			'index'     => 'active_to',
		));

		$this->addColumn('is_active', array(
          'header'    => Mage::helper('html2pdf')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'is_active',
          'type'      => 'options',
          'options'   => array(
		1 => 'Enabled',
		2 => 'Disabled',
		),
		));
		 
		$this->addColumn('action',
		array(
                'header'    =>  Mage::helper('html2pdf')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
		array(
                        'caption'   => Mage::helper('html2pdf')->__('Preview'),
                        'url'       => array('base'=> '*/*/preview'),
                        'field'     => 'id'
		)
		),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
		));

		return parent::_prepareColumns();
	}

	protected function _prepareMassaction()
	{
		$this->setMassactionIdField('template_id');
		$this->getMassactionBlock()->setFormFieldName('template');

		$this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('html2pdf')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('html2pdf')->__('Are you sure?')
		));

		$statuses = Mage::getSingleton('html2pdf/status')->getOptionArray();

		array_unshift($statuses, array('label'=>'', 'value'=>''));
		$this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('html2pdf')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('html2pdf')->__('Status'),
                         'values' => $statuses
		)
		)
		));
		return $this;
	}

	protected function _afterLoadCollection()
	{
		$this->getCollection()->walk('afterLoad');
		parent::_afterLoadCollection();
	}

	protected function _filterStoreCondition($collection, $column)
	{
		if (!$value = $column->getFilter()->getValue()) {
			return;
		}

		$this->getCollection()->addStoreFilter($value);
	}


	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}

}