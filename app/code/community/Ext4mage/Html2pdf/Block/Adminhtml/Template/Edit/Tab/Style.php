<?php

class Ext4mage_Html2pdf_Block_Adminhtml_Template_Edit_Tab_Style extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('style_fieldset', array('legend'=>Mage::helper('html2pdf')->__('Style information')));

		$fieldset->addField('default_font', 'select', array(
          'label'     => Mage::helper('html2pdf')->__('Default font'),
          'name'      => 'default_font',
          'required'  => false,
          'values'    => Mage::getSingleton('html2pdf/template')->pageFont,
		));

		$fieldset->addField('default_font_size', 'text', array(
          'label'     => Mage::helper('html2pdf')->__('Default font size in pt'),
          'required'  => false,
          'name'      => 'default_font_size',
		));

		$fieldset->addField('default_font_color', 'text', array(
          'label'     => Mage::helper('html2pdf')->__('Default font color (hex value)'),
          'required'  => false,
          'name'      => 'default_font_color',
		));

		$fieldset->addField('page_margin_top', 'text', array(
          'label'     => Mage::helper('html2pdf')->__('Page top margin in mm'),
          'required'  => false,
          'name'      => 'page_margin_top',
		));

		$fieldset->addField('page_margin_bottom', 'text', array(
          'label'     => Mage::helper('html2pdf')->__('Page bottom margin in mm'),
          'required'  => false,
          'name'      => 'page_margin_bottom',
		));

		$fieldset->addField('page_margin_left', 'text', array(
          'label'     => Mage::helper('html2pdf')->__('Page left margin in mm'),
          'required'  => false,
          'name'      => 'page_margin_left',
		));

		$fieldset->addField('page_margin_right', 'text', array(
          'label'     => Mage::helper('html2pdf')->__('Page right margin in mm'),
          'required'  => false,
          'name'      => 'page_margin_right',
		));

		$fieldset->addField('page_size', 'select', array(
          'label'     => Mage::helper('html2pdf')->__('Default page size'),
          'name'      => 'page_size',
          'required'  => false,
    	  'values'    => Mage::getSingleton('html2pdf/template')->pageFormat,
		));

		$fieldset->addField('page_orientation', 'select', array(
          'label'     => Mage::helper('html2pdf')->__('Page orientation'),
          'name'      => 'page_orientation',
          'required'  => false,
          'values'    => array(
		array(
                  'value'     => 'P',
                  'label'     => Mage::helper('html2pdf')->__('Portrait'),
		),
		array(
                  'value'     => 'L',
                  'label'     => Mage::helper('html2pdf')->__('Landscape'),
		),
		),
		));

		$fieldset->addField('image_optimize', 'select', array(
				'label'     => Mage::helper('html2pdf')->__('Image optimization'),
				'name'      => 'image_optimize',
				'required'  => false,
				'values'    => array(
						array(
								'value'     => '0',
								'label'     => Mage::helper('html2pdf')->__('None'),
						),
						array(
								'value'     => '1',
								'label'     => Mage::helper('html2pdf')->__('Low quality - High compression'),
						),
						array(
								'value'     => '2',
								'label'     => Mage::helper('html2pdf')->__('Medium quality - Medium compression'),
						),
						array(
								'value'     => '3',
								'label'     => Mage::helper('html2pdf')->__('High quality - Low compression'),
						),
				),
		));
		
		if ( Mage::getSingleton('adminhtml/session')->getHtml2pdfData() )
		{
			$form->setValues(Mage::getSingleton('adminhtml/session')->getHtml2pdfData());
			Mage::getSingleton('adminhtml/session')->setTableData(null);
		} elseif ( Mage::registry('html2pdf_data') ) {
			$form->setValues(Mage::registry('html2pdf_data')->getData());
		}
		return parent::_prepareForm();
	}
}