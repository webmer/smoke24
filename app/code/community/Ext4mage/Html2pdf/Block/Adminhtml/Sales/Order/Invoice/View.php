<?php
class Ext4mage_Html2pdf_Block_Adminhtml_Sales_Order_Invoice_View extends Mage_Adminhtml_Block_Sales_Order_Invoice_View {

	const XPATH_CONFIG_SETTINGS_IS_ACTIVE	= 'html2pdf/settings/is_active';
	
	protected function _isActive() {
		return Mage::getStoreConfig(self::XPATH_CONFIG_SETTINGS_IS_ACTIVE);
	}
	
	public function __construct() {
        parent::__construct();
        
	    if($this->_isActive())
	        $this->_addButton('printorg', array(
	            'label'     => Mage::helper('sales')->__('Print Original'),
	            'class'     => 'save',
	            'onclick'   => 'setLocation(\''.$this->getOrgPrintUrl().'\')'
	            )
	        );
    }

    public function getOrgPrintUrl() {
        return $this->getUrl('html2pdf/admin_order/printOrgInvoice', array(
        	'invoice_id' => $this->getInvoice()->getId()
        ));
    }
}