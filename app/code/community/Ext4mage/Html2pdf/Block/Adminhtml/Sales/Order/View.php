<?php
class Ext4mage_Html2pdf_Block_Adminhtml_Sales_Order_View extends Mage_Adminhtml_Block_Sales_Order_View {

	const XPATH_CONFIG_SETTINGS_IS_ACTIVE		= 'html2pdf/settings/is_active';
	
	protected function _isActive() {
		return Mage::getStoreConfig(self::XPATH_CONFIG_SETTINGS_IS_ACTIVE);
	}
	
	public function __construct() {
        parent::__construct();
        
	    if($this->_isActive())
	        $this->_addButton('print', array(
	            'label'     => Mage::helper('sales')->__('Print'),
	            'class'     => 'save',
	            'onclick'   => 'setLocation(\''.$this->getPrintUrl().'\')'
	            )
	        );
    }

    public function getPrintUrl() {
        return $this->getUrl('html2pdf/admin_order/print', array(
        'order_id' => $this->getOrder()->getId()
        ));
    }
}