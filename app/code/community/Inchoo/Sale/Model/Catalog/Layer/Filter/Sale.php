<?php

/**
* Inchoo
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magentocommerce.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Please do not edit or add to this file if you wish to upgrade
* Magento or this extension to newer versions in the future.
** Inchoo *give their best to conform to
* "non-obtrusive, best Magento practices" style of coding.
* However,* Inchoo *guarantee functional accuracy of
* specific extension behavior. Additionally we take no responsibility
* for any possible issue(s) resulting from extension usage.
* We reserve the full right not to provide any kind of support for our free extensions.
* Thank you for your understanding.
*
* @category Inchoo
* @package Sale
* @author Marko Martinović <marko.martinovic@inchoo.net>
* @copyright Copyright (c) Inchoo (http://inchoo.net/)
* @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
*/

class Inchoo_Sale_Model_Catalog_Layer_Filter_Sale extends Mage_Catalog_Model_Layer_Filter_Abstract
{

    const FILTER_ON_SALE = 1;
    const FILTER_NOT_ON_SALE = 2;

    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->_requestVar = 'category';
    }

    /**
     * Apply sale filter to layer
     *
     * @param   Zend_Controller_Request_Abstract $request
     * @param   Mage_Core_Block_Abstract $filterBlock
     * @return  Mage_Catalog_Model_Layer_Filter_Sale
     */
    public function apply(Zend_Controller_Request_Abstract $request, $filterBlock)
    {
        $_current_category = Mage::registry('current_category');
        $filter = $request->getParam($this->getRequestVar());
        if (!$filter || Mage::registry('inchoo_sale_filter')) {
            return $this;
        }

        $category = Mage::getModel('catalog/category')
            ->load($filter);
        try{
            /**
             * @var Mage_Catalog_Model_Resource_Product_Collection $pCollection
             */
            $pCollection = $this->getLayer()->getProductCollection();
//            $select = $pCollection->getSelect();
//            $select->where('category_id in ('.$filter.')');
            $pCollection->joinField('category_id', 'catalog/category_product', 'category_id',
                'product_id = entity_id', null, 'left')
                ->addAttributeToFilter('category_id', array('in' => array($_current_category->getId(), $category->getId())));
            $pIds = [];
            $productCollection = Mage::getModel('catalog/product')->getCollection()->addFieldToFilter('entity_id', array('in'=> $pCollection->getAllIds()));

            foreach ($productCollection as $item) {
                if(in_array($_current_category->getId(),$item->getCategoryIds()) && in_array($category->getId(),$item->getCategoryIds())) {
                    $pIds[] = $item->getId();
                }
            }

            $pCollection->addFieldToFilter('entity_id', array('in'=> $pIds));
            $pCollection->getSelect()
                ->group('e.entity_id');
        }catch(Exception $e){
            var_dump($e->getMessage(), $e->getTraceAsString());
        }



        /* @var $select Zend_Db_Select */

//            $select->where('price_index.final_price < price_index.price');
        $stateLabel = $category->getName();

        $state = $this->_createItem(
            $stateLabel, $filter
        )->setVar($this->_requestVar);
        /* @var $state Mage_Catalog_Model_Layer_Filter_Item */

        $this->getLayer()->getState()->addFilter($state);

        Mage::register('inchoo_sale_filter', true);

        return $this;
    }

    /**
     * Get filter name
     *
     * @return string
     */
    public function getName()
    {
        return Mage::helper('inchoo_sale')->__('Neuigkeiten');
    }

    /**
     * Get data array for building sale filter items
     *
     * @return array
     */
    protected function _getItemsData()
    {
        $data = array();
        $_current_category = Mage::registry('current_category');

        $productCollection =  Mage::getBlockSingleton('catalog/product_list')->getLoadedProductCollection();

        $productCollection = Mage::getModel('catalog/product')->getCollection()->addFieldToFilter('entity_id', array('in'=> $productCollection->getAllIds()));
        $categoryIds = [];
        $i = 0;
        foreach ($productCollection->getItems() as $product) {
            $categoryIds = array_unique(array_merge($product->getCategoryIds(),$categoryIds));
            $i++;
        }

        /**
         * @var $categories Mage_Catalog_Model_Category[]
         */
        $categories = $this->_getLeftCategoryCollection()
            ->addIdFilter($categoryIds)->addFieldToFilter('level', 3)->load();

        foreach ($categories as $category) {
            $pIds = [];
            $productCollection = Mage::getModel('catalog/product')->getCollection()
                ->joinField('category_id', 'catalog/category_product', 'category_id',
                'product_id = entity_id', null, 'left')
                ->addAttributeToFilter('category_id', array('in' => array($_current_category->getId(), $category->getId())));
            $productCollection->getSelect()->group('e.entity_id');

            foreach ($productCollection as $item) {
                if(in_array($_current_category->getId(),$item->getCategoryIds()) && in_array($category->getId(),$item->getCategoryIds())) {
                    $pIds[] = $item->getId();
                }
            }

            if(count($pIds)-1) {

                $data[] = array(
                    'label' => $category->getName(),
                    'value' => $category->getId(),
                    'count' => count($pIds)-1, //
                );
            }

        }

        return $data;
    }

    /**
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Collection
     */
    protected function _getLeftCategoryCollection()
    {
        $collection = Mage::getResourceModel('catalog/category_collection');
        $collection->addAttributeToSelect('url_key')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('all_children')
            ->addAttributeToFilter('is_active', 1)
            ->addAttributeToFilter('include_in_menu', 1)
            ->setOrder('position', 'ASC')
            ->joinUrlRewrite();
        return $collection;
    }

    protected function _getCount()
    {
        // Clone the select
    	$select = clone $this->getLayer()->getProductCollection()->getSelect();
        /* @var $select Zend_Db_Select */

	$select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);
        $select->reset(Zend_Db_Select::WHERE);

        // Count the on sale and not on sale
        $sql = 'SELECT IF(final_price >= price, "no", "yes") as on_sale, COUNT(*) as count from ('
                .$select->__toString().') AS q GROUP BY on_sale';

        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        /* @var $connection Zend_Db_Adapter_Abstract */

        return $connection->fetchPairs($sql);
    }

}
