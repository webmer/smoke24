<?php 

require_once("app/Mage.php");
Mage::app();


function products($category_id){
    $json = array('success' => true, 'products' => array());
    $category = Mage::getModel ('catalog/category')->load($category_id);
    $products = Mage::getResourceModel('catalog/product_collection')
                  // ->addAttributeToSelect('*')
                  ->AddAttributeToSelect('name')
                  ->addAttributeToSelect('price')
                  ->addFinalPrice()
                  ->addAttributeToSelect('small_image')
                  ->addAttributeToSelect('image')
                  ->addAttributeToSelect('thumbnail')
                  ->addAttributeToSelect('short_description')
                  ->addUrlRewrite()
                  ->AddCategoryFilter($category);
    Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
    $currencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
    foreach($products as $product){ 
        $json['products'][] = array(
                'id'                    => $product->getId(),
                'name'                  => $product->getName(),
                'description'           => $product->getShortDescription(),
                'pirce'                 => Mage::helper('core')->currency($product->getPrice(), true, false), //." ".$currencyCode,
                'href'                  => $product->getProductUrl(),
                'thumb'                 => (string)Mage::helper('catalog/image')->init($product, 'thumbnail')
            );
    }
    return $json;
}

function productByName($productName) {
	$json = array('success' => true, 'products' => array());
	echo "going to search for ".$productName;
	$products = Mage::getResourceModel('catalog/product_collection')
					->addAttributeToFilter('name', ['like' . $productName . '%'])
					->AddAttributeToSelect('name')
                	->addAttributeToSelect('price')
                	->addFinalPrice()
                	->addAttributeToSelect('small_image')
                	->addAttributeToSelect('image')
                 	->addAttributeToSelect('thumbnail')
                 	->addAttributeToSelect('short_description')
                 	->addUrlRewrite();
					
	echo "<br>We got those products: ";
	print_r($products);
	
	Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
    $currencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
    foreach($products as $product){ 
        $json['products'][] = array(
                'id'                    => $product->getId(),
                'name'                  => $product->getName(),
                'description'           => $product->getShortDescription(),
                'pirce'                 => Mage::helper('core')->currency($product->getPrice(), true, false), //." ".$currencyCode,
                'href'                  => $product->getProductUrl(),
                'thumb'                 => (string)Mage::helper('catalog/image')->init($product, 'thumbnail')
            );
    }
    return $json;
}


function test2($pname) {
//	$_product = Mage::registry('current_product');
//	$_collection = $_product->getCollection()->addAttributeToFilter('name', ['like' => $_product->getName() . '%']);
	
	$json = array();
	$products = Mage::getModel('catalog/product')
                         ->getCollection()
						 ->AddAttributeToSelect('name')
						 ->AddAttributeToSelect('productUrl')
						 ->AddAttributeToSelect('ean_stk')
						 ->AddAttributeToSelect('ean_ve')
						 ->addAttributeToFilter('name', ['like' => $pname . '%']);
						 
	//print_r($products);
	
	Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
    $currencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
    foreach($products as $product){ 
        $json[] = array(
                'name'                  => $product->getName(),
                'href'                  => $product->getProductUrl(),
                'ean_stk'               => $product->getean_stk(),
                'ean_ve'                => $product->getean_ve()
            );
    }
    return $json;
}

function test3($pname) {
//	$_product = Mage::registry('current_product');
//	$_collection = $_product->getCollection()->addAttributeToFilter('name', ['like' => $_product->getName() . '%']);
	
	$json = array();
	$products = Mage::getModel('catalog/product')
                         ->getCollection()
						 ->AddAttributeToSelect('name')
						 ->AddAttributeToSelect('productUrl')
						 ->AddAttributeToSelect('ean_stk')
						 ->AddAttributeToSelect('ean_ve')
						 ->addAttributeToFilter('ean_stk', ['like' => $pname . '%']);
						 
	//print_r($products);
	
	Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
    $currencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
    foreach($products as $product){ 
        $json[] = array(
                'name'                  => $product->getName(),
                'href'                  => $product->getProductUrl(),
                'ean_stk'               => $product->getean_stk(),
                'ean_ve'                => $product->getean_ve()
            );
    }
    return $json;
}

function test4($pname) {
//	$_product = Mage::registry('current_product');
//	$_collection = $_product->getCollection()->addAttributeToFilter('name', ['like' => $_product->getName() . '%']);
	
	$json = array();
	$products = Mage::getModel('catalog/product')
                         ->getCollection()
						 ->AddAttributeToSelect('name')
						 ->AddAttributeToSelect('productUrl')
						 ->AddAttributeToSelect('ean_stk')
						 ->AddAttributeToSelect('ean_ve')
						 ->addAttributeToFilter('ean_ve', ['like' => $pname . '%']);
						 
	//print_r($products);
	
	Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
    $currencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
    foreach($products as $product){ 
        $json[] = array(
                'name'                  => $product->getName(),
                'href'                  => $product->getProductUrl(),
                'ean_stk'               => $product->getean_stk(),
                'ean_ve'                => $product->getean_ve()
            );
    }
    return $json;
}

/*if(Mage::app()->getRequest()->getParam('searchname')){
	print_r( productByName(Mage::app()->getRequest()->getParam('searchname')));
}*/

//print_r(products("Zigaretten"));
$json = array('success' => true);

$data = test2(Mage::app()->getRequest()->getParam('searchname'));
$data2 = test3(Mage::app()->getRequest()->getParam('searchname'));
$data3 = test4(Mage::app()->getRequest()->getParam('searchname'));

$jsonInfo = array_merge($data, $data2, $data3);


$json['products'] = $jsonInfo;

header('Content-Type: application/json');
echo json_encode($json);

?>