<?php

ini_set('register_argc_argv', 0);

if (!isset($argc) || is_null($argc))
{
    echo 'Not CLI mode';
}

if ($argc < 2) {
    echo "Usage:\nphp inithost.php http://magento.dev/\n";
    die();
}

$value = $argv[1];

define('MAGENTO_ROOT', dirname(__FILE__));
$mageFilename = MAGENTO_ROOT . '/app/Mage.php';
require_once $mageFilename;

Mage::app('');
Mage::getConfig()->saveConfig('web/unsecure/base_url', $value, 'default', 0);
Mage::getConfig()->saveConfig('web/secure/base_url', $value, 'default', 0);
echo "Done\n";