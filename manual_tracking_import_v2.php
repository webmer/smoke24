<?php
require_once 'app/Mage.php';
umask(0);
/* not Mage::run(); */
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

if (isset($_GET['id'])) {
  $profileId = intval($_GET['id']);
} else if (isset($_GET['profile_id'])) {
  $profileId = intval($_GET['profile_id']);
} else if ($argv[1]) { // First command line parameter
  $profileId = intval($argv[1]);
} else {
  echo "No profile ID specified.";
  die();
}

$profile = Mage::getModel('xtento_trackingimport/profile')->load($profileId);
$importModel = Mage::getModel('xtento_trackingimport/import', array('profile' => $profile));
$importModel->cronImport();

?>