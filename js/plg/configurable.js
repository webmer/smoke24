/**
 * Created by avolodin on 30.08.17.
 */

(function($){
    console.log($.fn.jquery);

    window.plgExportOrder = (function(){

        var defaultSelector ='.massaction-checkbox';

        function getParams(selector) {
            var params = {};
            $(selector).each(function() {
                if(!$(this).is(':checked')) {
                    return;
                }

                var value = $(this).val();
                var name = $(this).attr('name');

                if(typeof params[name] == 'undefined' ) {
                    params[name] = value;
                } else {
                    if(!$.isArray(params[name])) {
                        var oldValue = params[name];
                        params[name] = [];
                        params[name].push(oldValue);
                    }
                    params[name].push(value);
                }
            });
            return params;
        }

        var redirect = function(url, method) {

            console.log('tt',this);
            var data = [];

            $.each(getParams($(defaultSelector)), function(name,item){
                if($.isArray(item)) {
                    $.each(item, function(i,val){
                        var input = $('<input>', {
                            name: name+'[]',
                            value: val
                        });

                        data.push(input);
                    })
                } else {
                    var input = $('<input>', {
                        name: name,
                        value: item
                    });
                    data.push(input);
                }
                // var t = $()
                // data.push();
            });

            console.log(data);
            data.push(($('<button>',{type:'submit'})));

            var form = $('<form>', {
                method: method,
                action: url,
                style: 'display:none;'
            })
                .append(data);

            $(this).before(form);

            form.submit();
        };

        return function(self, url){
            $.proxy(redirect, self, url, 'POST')();
        }

    })();

})(jQuery);
