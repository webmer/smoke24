/**
 * Created by u.titov on 16.02.2017.
 */


jQuery( document ).ready(function() {

var timer;

jQuery(".col-left.sidebar").attrchange({
    trackValues: true,
    callback: function (event) {
        clearTimeout(timer);
        timer = setTimeout(function () {
            jQuery(".pin-wrapper").height(jQuery(".col-left.sidebar").outerHeight() + jQuery(".col-left.sidebar").position().top);
        }, 50);
    }
});
});

