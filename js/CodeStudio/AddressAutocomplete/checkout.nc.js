function CodeStudio_AddressAutocomplete_process_address(a, c) {
    var tmp = a.address_components.reverse();
    tmp.forEach(function (d) {
        switch (d.types[0]) {
            case"establishment":
            case"administrative_area_level_2":
                var e = c + "\\:street2";
                $CodeStudio_AddressAutocomplete_jQuery("#" + e).length > 0 && $CodeStudio_AddressAutocomplete_jQuery("#" + e).val(d.long_name);
                break;
            case"locality":
                var e = c + "\\:city";
                $CodeStudio_AddressAutocomplete_jQuery("#" + e).val(d.long_name);
                break;
            case"administrative_area_level_1":
                var e = c + "\\:region";
                var selected = false;
                var select = $CodeStudio_AddressAutocomplete_jQuery("#" + e + "_id");
                var input = $CodeStudio_AddressAutocomplete_jQuery("#" + e );
                input.hide();select.show();
                select.find("option").each(function () {
                    var self = $CodeStudio_AddressAutocomplete_jQuery(this);
                    var text = self.text();
                    var id = self.attr('value') || null;
                    console.log(self,id);
                    var regions = billingRegionUpdater.regions.CH;
                    if(text == d.long_name || a.formatted_address.search(text)!= -1 || id && regions[id]['code'] == d.short_name ) {
                        $CodeStudio_AddressAutocomplete_jQuery(this).prop("selected", !0);
                        selected = true;
                    }
                });
                if(!selected){
                    if(typeof select.data().chosen != 'undefined' ){
                        select.chosen("destroy");
                    }
                    select.hide();
                    input.val(d.long_name).show();
                } else {
                    if(typeof select.data().chosen == 'undefined'){
                        select.chosen().hide();
                    } else {
                        select.trigger("chosen:updated").hide();
                    }
                }
                break;
            case"postal_code":
                var e = c + "\\:postcode";
                $CodeStudio_AddressAutocomplete_jQuery("#" + e).val(d.long_name);
                break;
            case"country":
                var e = c + "\\:country_id";
                var select = $CodeStudio_AddressAutocomplete_jQuery("#" + e);
                var box = select.parents('.input-box').first();
                var chosen = box.find('.chosen-single');
                select.val(d.short_name);
                $CodeStudio_AddressAutocomplete_jQuery('[id*=":telephone"').intlTelInput("setCountry", d.short_name);
                billingRegionUpdater.update();
                var t = select.find(":selected").text();
                chosen.find('span').text(t);

                break;
        }
    });
    var b = c + "\\:street1";
    $CodeStudio_AddressAutocomplete_jQuery("#" + b).val(a.name)
}
var $CodeStudio_AddressAutocomplete_jQuery = jQuery.noConflict();
$CodeStudio_AddressAutocomplete_jQuery(function () {
    var b = new google.maps.places.Autocomplete(document.getElementById("billing:street1"), {types: ["address"], componentRestrictions: {country:['ch','li']}});
    google.maps.event.addListener(b, "place_changed", function () {
        var d = b.getPlace();
        if (d.types) {
            var c = !1, e = !1;
            $CodeStudio_AddressAutocomplete_jQuery("#billing\\:street1").parents("form").find("input, select, textarea, button").each(function () {
                c || e ? c && !e && (e = $CodeStudio_AddressAutocomplete_jQuery(this)) : "billing:street1" == $CodeStudio_AddressAutocomplete_jQuery(this).attr("id") && (c = !0)
            }), $CodeStudio_AddressAutocomplete_jQuery(e).focus(), CodeStudio_AddressAutocomplete_process_address(d, "billing")
        }
    });
    var a = new google.maps.places.Autocomplete(document.getElementById("shipping:street1"), {types: ["address"]});
    google.maps.event.addListener(a, "place_changed", function () {
        var d = a.getPlace();
        if (d.types) {
            var c = !1, f = !1;
            $CodeStudio_AddressAutocomplete_jQuery("#shipping\\:street1").parents("form").find("input, select, textarea, button").each(function () {
                c || f ? c && !f && (f = $CodeStudio_AddressAutocomplete_jQuery(this)) : "shipping:street1" == $CodeStudio_AddressAutocomplete_jQuery(this).attr("id") && (c = !0)
            }), $CodeStudio_AddressAutocomplete_jQuery(f).focus(), CodeStudio_AddressAutocomplete_process_address(d, "shipping")
        }
    })
});