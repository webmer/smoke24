<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Datatrans/ExternalCheckout/MasterPass/AbstractRequestParameterBuilder.php';
//require_once 'Customweb/Datatrans/Util.php';
//require_once 'Customweb/Payment/ExternalCheckout/IProviderService.php';

class Customweb_Datatrans_ExternalCheckout_MasterPass_RedirectionRequestParameterBuilder extends Customweb_Datatrans_ExternalCheckout_MasterPass_AbstractRequestParameterBuilder {
	
	private $token;

	public function __construct(Customweb_Datatrans_Container $container, Customweb_Payment_ExternalCheckout_IContext $context, $token){
		parent::__construct($container, $context);
		$this->token = $token;
	}

	public function build(){
		$parameters = array_merge($this->getMerchantIdParameters(), $this->getShopParameters(), $this->getOperationModeParameters(), 
				$this->getTransactionAmountParameters(), $this->getMobileParameters(), $this->getReferenceNumber());
		
		$paymentMethod = $this->getContainer()->getPaymentMethod($this->getExternalCheckoutContext()->getPaymentMethod(), Customweb_Payment_ExternalCheckout_IProviderService::AUTHORIZATION_METHOD_NAME);
				
		$parameters['paymentmethod'] = $paymentMethod->getPaymentMethodType();
		$parameters['uppDisplayShippingDetails'] = 'yes';
		$parameters['uppReturnMaskedCC'] = 'yes';
		$parameters['confirmationUrl']  = $this->getContainer()->getEndpointAdapter()->getUrl("masterpass", "update-context", 
						array(
							'context-id' => $this->getExternalCheckoutContext()->getContextId(),
							'token' => $this->token 
						));
		
		
		
		// Add sign depending on the settings
		
		if ($this->getConfiguration()->getSecurityLevel() != 'level0') {
			$parameters['sign'] = Customweb_Datatrans_Util::getRequestSign($this->getConfiguration(), $parameters);
		}
		return $parameters;
	}
}
	