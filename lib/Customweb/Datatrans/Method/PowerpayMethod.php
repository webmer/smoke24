<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2013 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.customweb.ch/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.customweb.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Datatrans/Method/DefaultMethod.php';
//require_once 'Customweb/Payment/Util.php';

class Customweb_Datatrans_Method_PowerpayMethod extends Customweb_Datatrans_Method_DefaultMethod {

	public function getFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $authorizationMethod, $isMoto, $customerPaymentContext) {
		$elements = array();
		
		$birthday = $orderContext->getBillingDateOfBirth();
		if ($birthday === null || empty($birthday)) {
			/* @var $customerPaymentContext Customweb_Payment_Authorization_IPaymentCustomerContext */
			$defaultYear = null;
			$defaultMonth = null;
			$defaultDay = null;
			if ($customerPaymentContext !== null) {
				$map = $customerPaymentContext->getMap();
				if (isset($map['date_of_birth']['year'])) {
					$defaultYear = $map['date_of_birth']['year'];
				}
				if (isset($map['date_of_birth']['month'])) {
					$defaultMonth = $map['date_of_birth']['month'];
				}
				if (isset($map['date_of_birth']['day'])) {
					$defaultDay = $map['date_of_birth']['day'];
				}
			}
			
			$elements[] = Customweb_Form_ElementFactory::getDateOfBirthElement('year', 'month', 'day', $defaultYear, $defaultMonth, $defaultDay);
		}
		
		$gender = $orderContext->getBillingGender();
		if ($gender === null || empty($gender)) {
			$genders = array(
				'none' => '',
				'female' => Customweb_I18n_Translation::__('Female'),
				'male' => Customweb_I18n_Translation::__('Male'),
			);
			$control = new Customweb_Form_Control_Select('gender', $genders, null);
			$control->addValidator(new Customweb_Form_Validator_NotEmpty($control, Customweb_I18n_Translation::__("Please select your gender.")));
			
			$element = new Customweb_Form_Element(
				Customweb_I18n_Translation::__('Gender'),
				$control,
				Customweb_I18n_Translation::__('Please select your gender.')
			);
			$elements[] = $element;
		}
	
		return $elements;
	}
	
	protected function getCustomerParameters(Customweb_Datatrans_Authorization_Transaction $transaction) {
		$parameters = array();
		$parameters = $this->getBillingAddressParameters($transaction);
		
		return $parameters;
	}
	

	public function getRecurringAuthorizationParameters(Customweb_Datatrans_Authorization_Transaction $transaction) {
		$parameters = $this->getAuthorizationParameters($transaction, array());
		if ($transaction->getInitialTransaction() !== null) {
			if (!isset($parameters['uppCustomerGender'])) {
				$parameters['uppCustomerGender'] = $transaction->getInitialTransaction()->getCustomerGender();
			}
			if (!isset($parameters['uppCustomerBirthDate'])) {
				$parameters['uppCustomerBirthDate'] = $transaction->getInitialTransaction()->getCustomerBirthDate();
			}
		}
		return $parameters;
	}
	
	public function getAuthorizationParameters(Customweb_Datatrans_Authorization_Transaction $transaction, array $formData) {
		$parameters = parent::getAuthorizationParameters($transaction, $formData);
		
		if (isset($formData['year']) && isset($formData['month']) && isset($formData['day'])) {
			$parameters['uppCustomerBirthDate'] = $formData['year'] . '-' . $formData['month'] . '-' . $formData['day'];
			$customerContext = $transaction->getPaymentCustomerContext();
			if ($customerContext !== null) {
				$map = array();
				$map['date_of_birth']['year'] = intval($formData['year']);
				$map['date_of_birth']['month'] = intval($formData['month']);
				$map['date_of_birth']['day'] = intval($formData['day']);
				$customerContext->updateMap($map);
			}
		}
		
		if (isset($formData['gender'])) {
			if ($formData['gender'] == 'female') {
				$parameters['uppCustomerGender'] = 'female';
			}
			else {
				$parameters['uppCustomerGender'] = 'male';
			}
		}
		
		if (isset($parameters['uppCustomerGender'])) {
			$transaction->setCustomerGender($parameters['uppCustomerGender']);
		}
		
		if (isset($parameters['uppCustomerBirthDate'])) {
			$transaction->setCustomerBirthDate($parameters['uppCustomerBirthDate']);
		}
		
		return $parameters;
	}
	
	
}