<?php

//require_once 'Customweb/Payment/Endpoint/Controller/Abstract.php';
//require_once 'Customweb/Twint/Update/StatusHandler.php';
//require_once 'Customweb/Core/Http/Response.php';
//require_once 'Customweb/Twint/Util.php';
//require_once 'Customweb/Twint/Container.php';
//require_once 'Customweb/Payment/Authorization/ErrorMessage.php';
//require_once 'Customweb/I18n/Translation.php';



/**
 *
 * @author Sebastian Bossert
 * @Controller("process")
 *
 */
class Customweb_Twint_Endpoint_Process extends Customweb_Payment_Endpoint_Controller_Abstract {

	/**
	 * @Action("poll")
	 *
	 * @param Customweb_Core_Http_IRequest $request
	 * @param Customweb_Twint_Authorization_Transaction
	 * @return Customweb_Core_Http_Response
	 */
	public function poll(Customweb_Core_Http_IRequest $request, Customweb_Twint_Authorization_Transaction $transaction){
		$url = $this->process($request, $transaction);
		switch ($url) {
			case $transaction->getSuccessUrl():
			case $transaction->getFailedUrl():
				$state = 'COMPLETE';
				break;
			default:
				$state = 'UNKNOWN';
				break;
		}
		
		return $this->getJsonResponse($state, $url);
	}

	/**
	 * @Action("continue")
	 *
	 * @param Customweb_Core_Http_IRequest $request
	 * @param Customweb_Twint_Authorization_Transaction
	 * @return Customweb_Core_Http_Response
	 */
	public function processContinue(Customweb_Core_Http_IRequest $request, Customweb_Twint_Authorization_Transaction $transaction){
		$parameters = $request->getParameters();
		if (!$this->isHashCorrect($transaction, $parameters)) {
			$transaction->addErrorMessage(
					new Customweb_Payment_Authorization_ErrorMessage(
							Customweb_I18n_Translation::__('An attempt to process the transaction was made with an incorrect hash.')));
			return Customweb_Core_Http_Response::redirect($transaction->getFailedUrl());
		}
		
		$url = $this->process($request, $transaction);
		switch ($url) {
			case $transaction->getSuccessUrl():
			case $transaction->getFailedUrl():
				break;
			default:
				Customweb_Twint_Util::cancelOrder(new Customweb_Twint_Container($this->getContainer()), 
						$transaction->getPaymentId());
				$url = $transaction->getFailedUrl();
				break;
		}
		
		return Customweb_Core_Http_Response::redirect($url);
	}

	/**
	 * @Action("cancel")
	 *
	 * @param Customweb_Core_Http_IRequest $request
	 * @param Customweb_Twint_Authorization_Transaction
	 * @return Customweb_Core_Http_Response
	 */
	public function cancel(Customweb_Core_Http_IRequest $request, Customweb_Twint_Authorization_Transaction $transaction){
		$parameters = $request->getParameters();
		if (!$this->isHashCorrect($transaction, $parameters)) {
			$transaction->addErrorMessage(
					new Customweb_Payment_Authorization_ErrorMessage(
							Customweb_I18n_Translation::__('An attempt to cancel the transaction was made with an incorrect hash.')));
			return Customweb_Core_Http_Response::redirect($transaction->getFailedUrl());
		}
		
		$this->executeUpdate($transaction);
		
		if ($transaction->isAuthorized()) {
			return Customweb_Core_Http_Response::redirect($transaction->getSuccessUrl());
		}
		else if (!$transaction->isAuthorizationFailed()) {
			try {
				Customweb_Twint_Util::cancelOrder(new Customweb_Twint_Container($this->getContainer()), 
						$transaction->getPaymentId());
				$transaction->setAuthorizationFailed(Customweb_I18n_Translation::__('The order has been cancelled.'));
			}
			catch (Exception $exc) {
				$transaction->setAuthorizationFailed($exc->getMessage());
			}
		}
		
		return Customweb_Core_Http_Response::redirect($transaction->getFailedUrl());
	}

	/**
	 * Verifies if the hash is present in the parameters, and if it is correct.
	 *
	 * @param Customweb_Twint_Authorization_Transaction $transaction
	 * @param array $parameters
	 * @return boolean
	 */
	private function isHashCorrect(Customweb_Twint_Authorization_Transaction $transaction, array $parameters){
		if (isset($parameters['formHash'])) {
			if (hash_hmac('sha1', $transaction->getExternalTransactionId(), $transaction->getFormKey()) == $parameters['formHash']) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Updates the given transaction, and returns the appropriate url to redirect to.
	 *
	 * @param Customweb_Core_Http_IRequest $request
	 * @param Customweb_Twint_Authorization_Transaction $transaction
	 * @return string
	 */
	private function process(Customweb_Core_Http_IRequest $request, Customweb_Twint_Authorization_Transaction $transaction){
		$this->executeUpdate($transaction);
		if ($transaction->isAuthorized()) {
			$url = $transaction->getSuccessUrl();
		}
		else if ($transaction->isAuthorizationFailed()) {
			$url = $transaction->getFailedUrl();
		}
		else {
			$url = "";
		}
		
		return $url;
	}

	/**
	 * Executes an update on the transaction, and catches any occurring exceptions.
	 *
	 * @param Customweb_Twint_Authorization_Transaction $transaction
	 */
	private function executeUpdate(Customweb_Twint_Authorization_Transaction $transaction){
		try {
			$statusHandler = new Customweb_Twint_Update_StatusHandler($this->getContainer());
			$statusHandler->update($transaction);
		}
		catch (Exception $exc) {
		}
	}

	/**
	 * Creates a json object as response:
	 * {"status":"$status", "url":"$url", "timestamp": "time()"}
	 *
	 * @param string $status COMPLETE / UNKNOWN
	 * @param string $url
	 * @return Customweb_Core_Http_Response
	 */
	private function getJsonResponse($status, $url){
		$json = '{"status":"' . $status . '", "redirect":"' . $url . '", "timestamp": "' . time() . '"}';
		return Customweb_Core_Http_Response::_($json)->setContentType("application/json");
	}
}
