<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
*/

//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/Token50Type.php';
/**
 * @XmlType(name="CouponType", namespace="http://service.twint.ch/merchant/types/v1")
 */ 
class Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CouponType {
	/**
	 * @XmlElement(name="CouponId", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type
	 */
	private $couponId;
	
	public function __construct() {
	}
	
	/**
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CouponType
	 */
	public static function _() {
		$i = new Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CouponType();
		return $i;
	}
	/**
	 * Returns the value for the property couponId.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type
	 */
	public function getCouponId(){
		return $this->couponId;
	}
	
	/**
	 * Sets the value for the property couponId.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type $couponId
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CouponType
	 */
	public function setCouponId($couponId){
		if ($couponId instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type) {
			$this->couponId = $couponId;
		}
		else {
			throw new BadMethodCallException("Type of argument couponId must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type.");
		}
		return $this;
	}
	
	
	
}