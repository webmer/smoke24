<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
*/

//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/MerchantInformationType.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/UuidType.php';
//require_once 'Customweb/Twint/Stubs/Org/W3/XMLSchema/Boolean.php';
/**
 * @XmlType(name="MonitorCheckInRequestType", namespace="http://service.twint.ch/merchant/types/v1")
 */ 
class Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MonitorCheckInRequestType {
	/**
	 * @XmlElement(name="MerchantInformation", type="Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType
	 */
	private $merchantInformation;
	
	/**
	 * @XmlElement(name="CustomerRelationUUID", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType
	 */
	private $customerRelationUUID;
	
	/**
	 * @XmlValue(name="WaitForResponse", simpleType=@XmlSimpleTypeDefinition(typeName='boolean', typeNamespace='http://www.w3.org/2001/XMLSchema', type='Customweb_Twint_Stubs_Org_W3_XMLSchema_Boolean'), namespace="http://service.twint.ch/merchant/types/v1")
	 * @var boolean
	 */
	private $waitForResponse;
	
	public function __construct() {
	}
	
	/**
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MonitorCheckInRequestType
	 */
	public static function _() {
		$i = new Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MonitorCheckInRequestType();
		return $i;
	}
	/**
	 * Returns the value for the property merchantInformation.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType
	 */
	public function getMerchantInformation(){
		return $this->merchantInformation;
	}
	
	/**
	 * Sets the value for the property merchantInformation.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType $merchantInformation
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MonitorCheckInRequestType
	 */
	public function setMerchantInformation($merchantInformation){
		if ($merchantInformation instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType) {
			$this->merchantInformation = $merchantInformation;
		}
		else {
			throw new BadMethodCallException("Type of argument merchantInformation must be Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property customerRelationUUID.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType
	 */
	public function getCustomerRelationUUID(){
		return $this->customerRelationUUID;
	}
	
	/**
	 * Sets the value for the property customerRelationUUID.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType $customerRelationUUID
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MonitorCheckInRequestType
	 */
	public function setCustomerRelationUUID($customerRelationUUID){
		if ($customerRelationUUID instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType) {
			$this->customerRelationUUID = $customerRelationUUID;
		}
		else {
			throw new BadMethodCallException("Type of argument customerRelationUUID must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property waitForResponse.
	 * 
	 * @return Customweb_Twint_Stubs_Org_W3_XMLSchema_Boolean
	 */
	public function getWaitForResponse(){
		return $this->waitForResponse;
	}
	
	/**
	 * Sets the value for the property waitForResponse.
	 * 
	 * @param boolean $waitForResponse
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MonitorCheckInRequestType
	 */
	public function setWaitForResponse($waitForResponse){
		if ($waitForResponse instanceof Customweb_Twint_Stubs_Org_W3_XMLSchema_Boolean) {
			$this->waitForResponse = $waitForResponse;
		}
		else {
			$this->waitForResponse = Customweb_Twint_Stubs_Org_W3_XMLSchema_Boolean::_()->set($waitForResponse);
		}
		return $this;
	}
	
	
	
}