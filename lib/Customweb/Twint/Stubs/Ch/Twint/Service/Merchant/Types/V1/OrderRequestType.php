<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
*/

//require_once 'Customweb/Twint/Stubs/Org/W3/XMLSchema/Float.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/Token3Type.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/PostingType.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/Token50Type.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/Token250Type.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/OrderLinkType.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/OrderKindType.php';
/**
 * @XmlType(name="OrderRequestType", namespace="http://service.twint.ch/merchant/types/v1")
 */ 
class Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderRequestType {
	/**
	 * @XmlValue(name="RequestedAmount", simpleType=@XmlSimpleTypeDefinition(typeName='decimal', typeNamespace='http://www.w3.org/2001/XMLSchema', type='Customweb_Twint_Stubs_Org_W3_XMLSchema_Float'), namespace="http://service.twint.ch/merchant/types/v1")
	 * @var float
	 */
	private $requestedAmount;
	
	/**
	 * @XmlElement(name="Currency", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token3Type", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token3Type
	 */
	private $currency;
	
	/**
	 * @XmlElement(name="PostingType", type="Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_PostingType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_PostingType
	 */
	private $postingType;
	
	/**
	 * @XmlElement(name="MerchantTransactionReference", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type
	 */
	private $merchantTransactionReference;
	
	/**
	 * @XmlElement(name="EReceiptURL", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token250Type", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token250Type
	 */
	private $eReceiptURL;
	
	/**
	 * @XmlElement(name="Link", type="Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderLinkType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderLinkType
	 */
	private $link;
	
	/**
	 * @XmlAttribute(name="type", simpleType=@XmlSimpleTypeDefinition(typeName='orderKindType', typeNamespace='http://service.twint.ch/merchant/types/v1', type='Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderKindType')) 
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderKindType
	 */
	private $type;
	
	public function __construct() {
	}
	
	/**
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderRequestType
	 */
	public static function _() {
		$i = new Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderRequestType();
		return $i;
	}
	/**
	 * Returns the value for the property requestedAmount.
	 * 
	 * @return Customweb_Twint_Stubs_Org_W3_XMLSchema_Float
	 */
	public function getRequestedAmount(){
		return $this->requestedAmount;
	}
	
	/**
	 * Sets the value for the property requestedAmount.
	 * 
	 * @param float $requestedAmount
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderRequestType
	 */
	public function setRequestedAmount($requestedAmount){
		if ($requestedAmount instanceof Customweb_Twint_Stubs_Org_W3_XMLSchema_Float) {
			$this->requestedAmount = $requestedAmount;
		}
		else {
			$this->requestedAmount = Customweb_Twint_Stubs_Org_W3_XMLSchema_Float::_()->set($requestedAmount);
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property currency.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token3Type
	 */
	public function getCurrency(){
		return $this->currency;
	}
	
	/**
	 * Sets the value for the property currency.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token3Type $currency
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderRequestType
	 */
	public function setCurrency($currency){
		if ($currency instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token3Type) {
			$this->currency = $currency;
		}
		else {
			throw new BadMethodCallException("Type of argument currency must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token3Type.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property postingType.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_PostingType
	 */
	public function getPostingType(){
		return $this->postingType;
	}
	
	/**
	 * Sets the value for the property postingType.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_PostingType $postingType
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderRequestType
	 */
	public function setPostingType($postingType){
		if ($postingType instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_PostingType) {
			$this->postingType = $postingType;
		}
		else {
			throw new BadMethodCallException("Type of argument postingType must be Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_PostingType.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property merchantTransactionReference.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type
	 */
	public function getMerchantTransactionReference(){
		return $this->merchantTransactionReference;
	}
	
	/**
	 * Sets the value for the property merchantTransactionReference.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type $merchantTransactionReference
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderRequestType
	 */
	public function setMerchantTransactionReference($merchantTransactionReference){
		if ($merchantTransactionReference instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type) {
			$this->merchantTransactionReference = $merchantTransactionReference;
		}
		else {
			throw new BadMethodCallException("Type of argument merchantTransactionReference must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property eReceiptURL.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token250Type
	 */
	public function getEReceiptURL(){
		return $this->eReceiptURL;
	}
	
	/**
	 * Sets the value for the property eReceiptURL.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token250Type $eReceiptURL
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderRequestType
	 */
	public function setEReceiptURL($eReceiptURL){
		if ($eReceiptURL instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token250Type) {
			$this->eReceiptURL = $eReceiptURL;
		}
		else {
			throw new BadMethodCallException("Type of argument eReceiptURL must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token250Type.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property link.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderLinkType
	 */
	public function getLink(){
		return $this->link;
	}
	
	/**
	 * Sets the value for the property link.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderLinkType $link
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderRequestType
	 */
	public function setLink($link){
		if ($link instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderLinkType) {
			$this->link = $link;
		}
		else {
			throw new BadMethodCallException("Type of argument link must be Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderLinkType.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property type.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderKindType
	 */
	public function getType(){
		return $this->type;
	}
	
	/**
	 * Sets the value for the property type.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderKindType $type
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderRequestType
	 */
	public function setType($type){
		if ($type instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderKindType) {
			$this->type = $type;
		}
		else {
			throw new BadMethodCallException("Type of argument type must be Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderKindType.");
		}
		return $this;
	}
	
	
	
}