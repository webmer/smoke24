<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
*/

//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/Token50Type.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/UuidType.php';
/**
 * @XmlType(name="OrderLinkType", namespace="http://service.twint.ch/merchant/types/v1")
 */ 
class Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderLinkType {
	/**
	 * @XmlElement(name="MerchantTransactionReference", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type
	 */
	private $merchantTransactionReference;
	
	/**
	 * @XmlElement(name="OrderUUID", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType
	 */
	private $orderUUID;
	
	public function __construct() {
	}
	
	/**
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderLinkType
	 */
	public static function _() {
		$i = new Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderLinkType();
		return $i;
	}
	/**
	 * Returns the value for the property merchantTransactionReference.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type
	 */
	public function getMerchantTransactionReference(){
		return $this->merchantTransactionReference;
	}
	
	/**
	 * Sets the value for the property merchantTransactionReference.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type $merchantTransactionReference
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderLinkType
	 */
	public function setMerchantTransactionReference($merchantTransactionReference){
		if ($merchantTransactionReference instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type) {
			$this->merchantTransactionReference = $merchantTransactionReference;
		}
		else {
			throw new BadMethodCallException("Type of argument merchantTransactionReference must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property orderUUID.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType
	 */
	public function getOrderUUID(){
		return $this->orderUUID;
	}
	
	/**
	 * Sets the value for the property orderUUID.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType $orderUUID
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderLinkType
	 */
	public function setOrderUUID($orderUUID){
		if ($orderUUID instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType) {
			$this->orderUUID = $orderUUID;
		}
		else {
			throw new BadMethodCallException("Type of argument orderUUID must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType.");
		}
		return $this;
	}
	
	
	
}