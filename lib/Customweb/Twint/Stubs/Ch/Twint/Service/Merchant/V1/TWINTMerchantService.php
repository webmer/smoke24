<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
*/


//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/NumericTokenType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/Token100Type.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/Token250Type.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/Token3Type.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/Token50Type.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/UuidType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Fault/Types/V1/BaseFault.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Fault/Types/V1/BaseFaultElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Fault/Types/V1/ErrorCode.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Header/Types/V1/RequestHeaderElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Header/Types/V1/RequestHeaderType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Header/Types/V1/ResponseHeaderElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Header/Types/V1/ResponseHeaderType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/BeaconSecurityType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/CancelCheckInRequestElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/CancelCheckInRequestType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/CancelOrderRequestElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/CancelOrderRequestType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/CancelOrderResponseElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/CancelOrderResponseType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/CheckInNotificationType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/CheckSystemStatusRequestElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/CheckSystemStatusRequestElementElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/CodeValueType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/ConfirmOrderRequestElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/ConfirmOrderRequestType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/ConfirmOrderResponseElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/ConfirmOrderResponseType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/CouponType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/CustomerInformationType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/DataURIScheme.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/EnrollCashRegisterRequestElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/EnrollCashRegisterRequestType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/EnrollCashRegisterResponseElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/EnrollCashRegisterResponseType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/KeyValueType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/LoyaltyType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/MerchantInformationType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/MonitorCheckInRequestElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/MonitorCheckInRequestType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/MonitorCheckInResponseElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/MonitorCheckInResponseType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/MonitorOrderRequestElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/MonitorOrderRequestType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/MonitorOrderResponseElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/MonitorOrderResponseType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/OrderKindType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/OrderLinkType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/OrderRequestType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/OrderStatusType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/OrderType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/PostingType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/RequestCheckInRequestElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/RequestCheckInRequestType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/RequestCheckInResponseElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/RequestCheckInResponseType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/StartOrderRequestElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/StartOrderRequestType.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/StartOrderResponseElement.php');
//require_once('Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/StartOrderResponseType.php');
//require_once('Customweb/Twint/Stubs/Org/W3/XMLSchema/AnyType.php');
//require_once('Customweb/Twint/Stubs/Org/W3/XMLSchema/Boolean.php');
//require_once('Customweb/Twint/Stubs/Org/W3/XMLSchema/Date.php');
//require_once('Customweb/Twint/Stubs/Org/W3/XMLSchema/DateTime.php');
//require_once('Customweb/Twint/Stubs/Org/W3/XMLSchema/Float.php');
//require_once('Customweb/Twint/Stubs/Org/W3/XMLSchema/Integer.php');
//require_once('Customweb/Twint/Stubs/Org/W3/XMLSchema/String.php');
//require_once 'Customweb/Soap/AbstractService.php';
/**
 */

class Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_V1_TWINTMerchantService extends Customweb_Soap_AbstractService {

	/**
	 * @var Customweb_Soap_IClient
	 */
	private $soapClient;
		
	/**
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_RequestCheckInRequestElement $requestCheckInRequestElement
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_RequestCheckInResponseElement
	 */ 
	public function requestCheckIn(Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_RequestCheckInRequestElement $requestCheckInRequestElement){
		$data = func_get_args();
		if (count($data) > 0) {;
			$data = current($data);
		} else {;
			 throw new InvalidArgumentException();
		};
		$call = $this->createSoapCall("RequestCheckIn", $data, "Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_RequestCheckInResponseElement", "RequestCheckIn");
		$call->setStyle(Customweb_Soap_ICall::STYLE_DOCUMENT);
		$call->setInputEncoding(Customweb_Soap_ICall::ENCODING_LITERAL);
		$call->setOutputEncoding(Customweb_Soap_ICall::ENCODING_LITERAL);
		$call->setSoapVersion(Customweb_Soap_ICall::SOAP_VERSION_11);
		$call->setLocationUrl($this->resolveLocation("http://service.twint.ch/merchant/v1"));
		$result = $this->getClient()->invokeOperation($call);
		return $result;
		
	}
		
	/**
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MonitorCheckInRequestElement $monitorCheckInRequestElement
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MonitorCheckInResponseElement
	 */ 
	public function monitorCheckIn(Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MonitorCheckInRequestElement $monitorCheckInRequestElement){
		$data = func_get_args();
		if (count($data) > 0) {;
			$data = current($data);
		} else {;
			 throw new InvalidArgumentException();
		};
		$call = $this->createSoapCall("MonitorCheckIn", $data, "Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MonitorCheckInResponseElement", "MonitorCheckIn");
		$call->setStyle(Customweb_Soap_ICall::STYLE_DOCUMENT);
		$call->setInputEncoding(Customweb_Soap_ICall::ENCODING_LITERAL);
		$call->setOutputEncoding(Customweb_Soap_ICall::ENCODING_LITERAL);
		$call->setSoapVersion(Customweb_Soap_ICall::SOAP_VERSION_11);
		$call->setLocationUrl($this->resolveLocation("http://service.twint.ch/merchant/v1"));
		$result = $this->getClient()->invokeOperation($call);
		return $result;
		
	}
		
	/**
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CancelCheckInRequestElement $cancelCheckInRequestElement
	 * @return void
	 */ 
	public function cancelCheckIn(Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CancelCheckInRequestElement $cancelCheckInRequestElement){
		$data = func_get_args();
		if (count($data) > 0) {;
			$data = current($data);
		} else {;
			 throw new InvalidArgumentException();
		};
		$call = $this->createSoapCall("CancelCheckIn", $data, "Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_V1_CancelCheckIn_Response", "CancelCheckIn");
		$call->setStyle(Customweb_Soap_ICall::STYLE_DOCUMENT);
		$call->setInputEncoding(Customweb_Soap_ICall::ENCODING_LITERAL);
		$call->setOutputEncoding(Customweb_Soap_ICall::ENCODING_LITERAL);
		$call->setSoapVersion(Customweb_Soap_ICall::SOAP_VERSION_11);
		$call->setLocationUrl($this->resolveLocation("http://service.twint.ch/merchant/v1"));
		$result = $this->getClient()->invokeOperation($call);
		
	}
		
	/**
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderRequestElement $startOrderRequestElement
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderResponseElement
	 */ 
	public function startOrder(Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderRequestElement $startOrderRequestElement){
		$data = func_get_args();
		if (count($data) > 0) {;
			$data = current($data);
		} else {;
			 throw new InvalidArgumentException();
		};
		$call = $this->createSoapCall("StartOrder", $data, "Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderResponseElement", "StartOrder");
		$call->setStyle(Customweb_Soap_ICall::STYLE_DOCUMENT);
		$call->setInputEncoding(Customweb_Soap_ICall::ENCODING_LITERAL);
		$call->setOutputEncoding(Customweb_Soap_ICall::ENCODING_LITERAL);
		$call->setSoapVersion(Customweb_Soap_ICall::SOAP_VERSION_11);
		$call->setLocationUrl($this->resolveLocation("http://service.twint.ch/merchant/v1"));
		$result = $this->getClient()->invokeOperation($call);
		return $result;
		
	}
		
	/**
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MonitorOrderRequestElement $monitorOrderRequestElement
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MonitorOrderResponseElement
	 */ 
	public function monitorOrder(Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MonitorOrderRequestElement $monitorOrderRequestElement){
		$data = func_get_args();
		if (count($data) > 0) {;
			$data = current($data);
		} else {;
			 throw new InvalidArgumentException();
		};
		$call = $this->createSoapCall("MonitorOrder", $data, "Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MonitorOrderResponseElement", "MonitorOrder");
		$call->setStyle(Customweb_Soap_ICall::STYLE_DOCUMENT);
		$call->setInputEncoding(Customweb_Soap_ICall::ENCODING_LITERAL);
		$call->setOutputEncoding(Customweb_Soap_ICall::ENCODING_LITERAL);
		$call->setSoapVersion(Customweb_Soap_ICall::SOAP_VERSION_11);
		$call->setLocationUrl($this->resolveLocation("http://service.twint.ch/merchant/v1"));
		$result = $this->getClient()->invokeOperation($call);
		return $result;
		
	}
		
	/**
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_ConfirmOrderRequestElement $confirmOrderRequestElement
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_ConfirmOrderResponseElement
	 */ 
	public function confirmOrder(Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_ConfirmOrderRequestElement $confirmOrderRequestElement){
		$data = func_get_args();
		if (count($data) > 0) {;
			$data = current($data);
		} else {;
			 throw new InvalidArgumentException();
		};
		$call = $this->createSoapCall("ConfirmOrder", $data, "Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_ConfirmOrderResponseElement", "ConfirmOrder");
		$call->setStyle(Customweb_Soap_ICall::STYLE_DOCUMENT);
		$call->setInputEncoding(Customweb_Soap_ICall::ENCODING_LITERAL);
		$call->setOutputEncoding(Customweb_Soap_ICall::ENCODING_LITERAL);
		$call->setSoapVersion(Customweb_Soap_ICall::SOAP_VERSION_11);
		$call->setLocationUrl($this->resolveLocation("http://service.twint.ch/merchant/v1"));
		$result = $this->getClient()->invokeOperation($call);
		return $result;
		
	}
		
	/**
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CancelOrderRequestElement $cancelOrderRequestElement
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CancelOrderResponseElement
	 */ 
	public function cancelOrder(Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CancelOrderRequestElement $cancelOrderRequestElement){
		$data = func_get_args();
		if (count($data) > 0) {;
			$data = current($data);
		} else {;
			 throw new InvalidArgumentException();
		};
		$call = $this->createSoapCall("CancelOrder", $data, "Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CancelOrderResponseElement", "CancelOrder");
		$call->setStyle(Customweb_Soap_ICall::STYLE_DOCUMENT);
		$call->setInputEncoding(Customweb_Soap_ICall::ENCODING_LITERAL);
		$call->setOutputEncoding(Customweb_Soap_ICall::ENCODING_LITERAL);
		$call->setSoapVersion(Customweb_Soap_ICall::SOAP_VERSION_11);
		$call->setLocationUrl($this->resolveLocation("http://service.twint.ch/merchant/v1"));
		$result = $this->getClient()->invokeOperation($call);
		return $result;
		
	}
		
	/**
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_EnrollCashRegisterRequestElement $enrollCashRegisterRequestElement
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_EnrollCashRegisterResponseElement
	 */ 
	public function enrollCashRegister(Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_EnrollCashRegisterRequestElement $enrollCashRegisterRequestElement){
		$data = func_get_args();
		if (count($data) > 0) {;
			$data = current($data);
		} else {;
			 throw new InvalidArgumentException();
		};
		$call = $this->createSoapCall("EnrollCashRegister", $data, "Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_EnrollCashRegisterResponseElement", "EnrollCashRegister");
		$call->setStyle(Customweb_Soap_ICall::STYLE_DOCUMENT);
		$call->setInputEncoding(Customweb_Soap_ICall::ENCODING_LITERAL);
		$call->setOutputEncoding(Customweb_Soap_ICall::ENCODING_LITERAL);
		$call->setSoapVersion(Customweb_Soap_ICall::SOAP_VERSION_11);
		$call->setLocationUrl($this->resolveLocation("http://service.twint.ch/merchant/v1"));
		$result = $this->getClient()->invokeOperation($call);
		return $result;
		
	}
		
	/**
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CheckSystemStatusRequestElement $checkSystemStatusRequestElement
	 * @return void
	 */ 
	public function checkSystemStatus(Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CheckSystemStatusRequestElement $checkSystemStatusRequestElement){
		$data = func_get_args();
		if (count($data) > 0) {;
			$data = current($data);
		} else {;
			 throw new InvalidArgumentException();
		};
		$call = $this->createSoapCall("CheckSystemStatus", $data, "Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_V1_CheckSystemStatus_Response", "CheckSystemStatus");
		$call->setStyle(Customweb_Soap_ICall::STYLE_DOCUMENT);
		$call->setInputEncoding(Customweb_Soap_ICall::ENCODING_LITERAL);
		$call->setOutputEncoding(Customweb_Soap_ICall::ENCODING_LITERAL);
		$call->setSoapVersion(Customweb_Soap_ICall::SOAP_VERSION_11);
		$call->setLocationUrl($this->resolveLocation("http://service.twint.ch/merchant/v1"));
		$result = $this->getClient()->invokeOperation($call);
		
	}
	
}