<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
*/

//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/MerchantInformationType.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/UuidType.php';
/**
 * @XmlType(name="CancelOrderRequestType", namespace="http://service.twint.ch/merchant/types/v1")
 */ 
class Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CancelOrderRequestType {
	/**
	 * @XmlElement(name="MerchantInformation", type="Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType
	 */
	private $merchantInformation;
	
	/**
	 * @XmlElement(name="OrderUUID", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType
	 */
	private $orderUUID;
	
	public function __construct() {
	}
	
	/**
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CancelOrderRequestType
	 */
	public static function _() {
		$i = new Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CancelOrderRequestType();
		return $i;
	}
	/**
	 * Returns the value for the property merchantInformation.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType
	 */
	public function getMerchantInformation(){
		return $this->merchantInformation;
	}
	
	/**
	 * Sets the value for the property merchantInformation.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType $merchantInformation
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CancelOrderRequestType
	 */
	public function setMerchantInformation($merchantInformation){
		if ($merchantInformation instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType) {
			$this->merchantInformation = $merchantInformation;
		}
		else {
			throw new BadMethodCallException("Type of argument merchantInformation must be Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property orderUUID.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType
	 */
	public function getOrderUUID(){
		return $this->orderUUID;
	}
	
	/**
	 * Sets the value for the property orderUUID.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType $orderUUID
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CancelOrderRequestType
	 */
	public function setOrderUUID($orderUUID){
		if ($orderUUID instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType) {
			$this->orderUUID = $orderUUID;
		}
		else {
			throw new BadMethodCallException("Type of argument orderUUID must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType.");
		}
		return $this;
	}
	
	
	
}