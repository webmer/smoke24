<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
*/

//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/UuidType.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/OrderStatusType.php';
//require_once 'Customweb/Twint/Stubs/Org/W3/XMLSchema/DateTime.php';
//require_once 'Customweb/Twint/Stubs/Org/W3/XMLSchema/Float.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/OrderRequestType.php';
/**
 * @XmlType(name="OrderType", namespace="http://service.twint.ch/merchant/types/v1")
 */ 
class Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderType extends Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderRequestType {
	/**
	 * @XmlElement(name="UUID", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType
	 */
	private $uUID;
	
	/**
	 * @XmlElement(name="Status", type="Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderStatusType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderStatusType
	 */
	private $status;
	
	/**
	 * @XmlValue(name="CreationTimestamp", simpleType=@XmlSimpleTypeDefinition(typeName='dateTime', typeNamespace='http://www.w3.org/2001/XMLSchema', type='Customweb_Twint_Stubs_Org_W3_XMLSchema_DateTime'), namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Xml_Binding_DateHandler_DateTime
	 */
	private $creationTimestamp;
	
	/**
	 * @XmlValue(name="AuthorizedAmount", simpleType=@XmlSimpleTypeDefinition(typeName='decimal', typeNamespace='http://www.w3.org/2001/XMLSchema', type='Customweb_Twint_Stubs_Org_W3_XMLSchema_Float'), namespace="http://service.twint.ch/merchant/types/v1")
	 * @var float
	 */
	private $authorizedAmount;
	
	/**
	 * @XmlValue(name="Fee", simpleType=@XmlSimpleTypeDefinition(typeName='decimal', typeNamespace='http://www.w3.org/2001/XMLSchema', type='Customweb_Twint_Stubs_Org_W3_XMLSchema_Float'), namespace="http://service.twint.ch/merchant/types/v1")
	 * @var float
	 */
	private $fee;
	
	/**
	 * @XmlValue(name="AuthorizationTimestamp", simpleType=@XmlSimpleTypeDefinition(typeName='dateTime', typeNamespace='http://www.w3.org/2001/XMLSchema', type='Customweb_Twint_Stubs_Org_W3_XMLSchema_DateTime'), namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Xml_Binding_DateHandler_DateTime
	 */
	private $authorizationTimestamp;
	
	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderType
	 */
	public static function _() {
		$i = new Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderType();
		return $i;
	}
	/**
	 * Returns the value for the property uUID.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType
	 */
	public function getUUID(){
		return $this->uUID;
	}
	
	/**
	 * Sets the value for the property uUID.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType $uUID
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderType
	 */
	public function setUUID($uUID){
		if ($uUID instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType) {
			$this->uUID = $uUID;
		}
		else {
			throw new BadMethodCallException("Type of argument uUID must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property status.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderStatusType
	 */
	public function getStatus(){
		return $this->status;
	}
	
	/**
	 * Sets the value for the property status.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderStatusType $status
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderType
	 */
	public function setStatus($status){
		if ($status instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderStatusType) {
			$this->status = $status;
		}
		else {
			throw new BadMethodCallException("Type of argument status must be Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderStatusType.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property creationTimestamp.
	 * 
	 * @return Customweb_Twint_Stubs_Org_W3_XMLSchema_DateTime
	 */
	public function getCreationTimestamp(){
		return $this->creationTimestamp;
	}
	
	/**
	 * Sets the value for the property creationTimestamp.
	 * 
	 * @param Customweb_Xml_Binding_DateHandler_DateTime $creationTimestamp
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderType
	 */
	public function setCreationTimestamp($creationTimestamp){
		if ($creationTimestamp instanceof Customweb_Twint_Stubs_Org_W3_XMLSchema_DateTime) {
			$this->creationTimestamp = $creationTimestamp;
		}
		else {
			$this->creationTimestamp = Customweb_Twint_Stubs_Org_W3_XMLSchema_DateTime::_()->set($creationTimestamp);
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property authorizedAmount.
	 * 
	 * @return Customweb_Twint_Stubs_Org_W3_XMLSchema_Float
	 */
	public function getAuthorizedAmount(){
		return $this->authorizedAmount;
	}
	
	/**
	 * Sets the value for the property authorizedAmount.
	 * 
	 * @param float $authorizedAmount
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderType
	 */
	public function setAuthorizedAmount($authorizedAmount){
		if ($authorizedAmount instanceof Customweb_Twint_Stubs_Org_W3_XMLSchema_Float) {
			$this->authorizedAmount = $authorizedAmount;
		}
		else {
			$this->authorizedAmount = Customweb_Twint_Stubs_Org_W3_XMLSchema_Float::_()->set($authorizedAmount);
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property fee.
	 * 
	 * @return Customweb_Twint_Stubs_Org_W3_XMLSchema_Float
	 */
	public function getFee(){
		return $this->fee;
	}
	
	/**
	 * Sets the value for the property fee.
	 * 
	 * @param float $fee
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderType
	 */
	public function setFee($fee){
		if ($fee instanceof Customweb_Twint_Stubs_Org_W3_XMLSchema_Float) {
			$this->fee = $fee;
		}
		else {
			$this->fee = Customweb_Twint_Stubs_Org_W3_XMLSchema_Float::_()->set($fee);
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property authorizationTimestamp.
	 * 
	 * @return Customweb_Twint_Stubs_Org_W3_XMLSchema_DateTime
	 */
	public function getAuthorizationTimestamp(){
		return $this->authorizationTimestamp;
	}
	
	/**
	 * Sets the value for the property authorizationTimestamp.
	 * 
	 * @param Customweb_Xml_Binding_DateHandler_DateTime $authorizationTimestamp
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderType
	 */
	public function setAuthorizationTimestamp($authorizationTimestamp){
		if ($authorizationTimestamp instanceof Customweb_Twint_Stubs_Org_W3_XMLSchema_DateTime) {
			$this->authorizationTimestamp = $authorizationTimestamp;
		}
		else {
			$this->authorizationTimestamp = Customweb_Twint_Stubs_Org_W3_XMLSchema_DateTime::_()->set($authorizationTimestamp);
		}
		return $this;
	}
	
	
	
}