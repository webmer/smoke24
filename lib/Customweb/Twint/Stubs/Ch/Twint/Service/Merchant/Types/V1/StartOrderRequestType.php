<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
*/

//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/MerchantInformationType.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/OrderRequestType.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/CouponType.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/Token100Type.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/UuidType.php';
//require_once 'Customweb/Twint/Stubs/Org/W3/XMLSchema/Boolean.php';
/**
 * @XmlType(name="StartOrderRequestType", namespace="http://service.twint.ch/merchant/types/v1")
 */ 
class Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderRequestType {
	/**
	 * @XmlElement(name="MerchantInformation", type="Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType
	 */
	private $merchantInformation;
	
	/**
	 * @XmlElement(name="Order", type="Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderRequestType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderRequestType
	 */
	private $order;
	
	/**
	 * @XmlList(name="ProcessedCoupon", type='Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CouponType', namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CouponType[]
	 */
	private $processedCoupon;
	
	/**
	 * @XmlElement(name="OfflineAuthorization", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token100Type", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token100Type
	 */
	private $offlineAuthorization;
	
	/**
	 * @XmlElement(name="CustomerRelationUUID", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType
	 */
	private $customerRelationUUID;
	
	/**
	 * @XmlValue(name="QRCodeRendering", simpleType=@XmlSimpleTypeDefinition(typeName='boolean', typeNamespace='http://www.w3.org/2001/XMLSchema', type='Customweb_Twint_Stubs_Org_W3_XMLSchema_Boolean'), namespace="http://service.twint.ch/merchant/types/v1")
	 * @var boolean
	 */
	private $qRCodeRendering;
	
	public function __construct() {
		$this->processedCoupon = new ArrayObject();
	}
	
	/**
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderRequestType
	 */
	public static function _() {
		$i = new Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderRequestType();
		return $i;
	}
	/**
	 * Returns the value for the property merchantInformation.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType
	 */
	public function getMerchantInformation(){
		return $this->merchantInformation;
	}
	
	/**
	 * Sets the value for the property merchantInformation.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType $merchantInformation
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderRequestType
	 */
	public function setMerchantInformation($merchantInformation){
		if ($merchantInformation instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType) {
			$this->merchantInformation = $merchantInformation;
		}
		else {
			throw new BadMethodCallException("Type of argument merchantInformation must be Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property order.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderRequestType
	 */
	public function getOrder(){
		return $this->order;
	}
	
	/**
	 * Sets the value for the property order.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderRequestType $order
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderRequestType
	 */
	public function setOrder($order){
		if ($order instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderRequestType) {
			$this->order = $order;
		}
		else {
			throw new BadMethodCallException("Type of argument order must be Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderRequestType.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property processedCoupon.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CouponType[]
	 */
	public function getProcessedCoupon(){
		return $this->processedCoupon;
	}
	
	/**
	 * Sets the value for the property processedCoupon.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CouponType $processedCoupon
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderRequestType
	 */
	public function setProcessedCoupon($processedCoupon){
		if (is_array($processedCoupon)) {
			$processedCoupon = new ArrayObject($processedCoupon);
		}
		if ($processedCoupon instanceof ArrayObject) {
			$this->processedCoupon = $processedCoupon;
		}
		else {
			throw new BadMethodCallException("Type of argument processedCoupon must be ArrayObject.");
		}
		return $this;
	}
	
	/**
	 * Adds the given $item to the list of items of processedCoupon.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CouponType $item
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderRequestType
	 */
	public function addProcessedCoupon(Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CouponType $item) {
		if (!($this->processedCoupon instanceof ArrayObject)) {
			$this->processedCoupon = new ArrayObject();
		}
		$this->processedCoupon[] = $item;
		return $this;
	}
	
	/**
	 * Returns the value for the property offlineAuthorization.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token100Type
	 */
	public function getOfflineAuthorization(){
		return $this->offlineAuthorization;
	}
	
	/**
	 * Sets the value for the property offlineAuthorization.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token100Type $offlineAuthorization
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderRequestType
	 */
	public function setOfflineAuthorization($offlineAuthorization){
		if ($offlineAuthorization instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token100Type) {
			$this->offlineAuthorization = $offlineAuthorization;
		}
		else {
			throw new BadMethodCallException("Type of argument offlineAuthorization must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token100Type.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property customerRelationUUID.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType
	 */
	public function getCustomerRelationUUID(){
		return $this->customerRelationUUID;
	}
	
	/**
	 * Sets the value for the property customerRelationUUID.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType $customerRelationUUID
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderRequestType
	 */
	public function setCustomerRelationUUID($customerRelationUUID){
		if ($customerRelationUUID instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType) {
			$this->customerRelationUUID = $customerRelationUUID;
		}
		else {
			throw new BadMethodCallException("Type of argument customerRelationUUID must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property qRCodeRendering.
	 * 
	 * @return Customweb_Twint_Stubs_Org_W3_XMLSchema_Boolean
	 */
	public function getQRCodeRendering(){
		return $this->qRCodeRendering;
	}
	
	/**
	 * Sets the value for the property qRCodeRendering.
	 * 
	 * @param boolean $qRCodeRendering
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderRequestType
	 */
	public function setQRCodeRendering($qRCodeRendering){
		if ($qRCodeRendering instanceof Customweb_Twint_Stubs_Org_W3_XMLSchema_Boolean) {
			$this->qRCodeRendering = $qRCodeRendering;
		}
		else {
			$this->qRCodeRendering = Customweb_Twint_Stubs_Org_W3_XMLSchema_Boolean::_()->set($qRCodeRendering);
		}
		return $this;
	}
	
	
	
}