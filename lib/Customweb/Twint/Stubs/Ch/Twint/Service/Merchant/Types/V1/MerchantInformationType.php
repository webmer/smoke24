<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
*/

//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/UuidType.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/Token50Type.php';
/**
 * @XmlType(name="MerchantInformationType", namespace="http://service.twint.ch/merchant/types/v1")
 */ 
class Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType {
	/**
	 * @XmlElement(name="MerchantUUID", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType
	 */
	private $merchantUUID;
	
	/**
	 * @XmlElement(name="MerchantAliasId", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type
	 */
	private $merchantAliasId;
	
	/**
	 * @XmlElement(name="CashRegisterId", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type
	 */
	private $cashRegisterId;
	
	/**
	 * @XmlElement(name="ServiceAgentUUID", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType
	 */
	private $serviceAgentUUID;
	
	public function __construct() {
	}
	
	/**
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType
	 */
	public static function _() {
		$i = new Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType();
		return $i;
	}
	/**
	 * Returns the value for the property merchantUUID.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType
	 */
	public function getMerchantUUID(){
		return $this->merchantUUID;
	}
	
	/**
	 * Sets the value for the property merchantUUID.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType $merchantUUID
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType
	 */
	public function setMerchantUUID($merchantUUID){
		if ($merchantUUID instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType) {
			$this->merchantUUID = $merchantUUID;
		}
		else {
			throw new BadMethodCallException("Type of argument merchantUUID must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property merchantAliasId.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type
	 */
	public function getMerchantAliasId(){
		return $this->merchantAliasId;
	}
	
	/**
	 * Sets the value for the property merchantAliasId.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type $merchantAliasId
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType
	 */
	public function setMerchantAliasId($merchantAliasId){
		if ($merchantAliasId instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type) {
			$this->merchantAliasId = $merchantAliasId;
		}
		else {
			throw new BadMethodCallException("Type of argument merchantAliasId must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property cashRegisterId.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type
	 */
	public function getCashRegisterId(){
		return $this->cashRegisterId;
	}
	
	/**
	 * Sets the value for the property cashRegisterId.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type $cashRegisterId
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType
	 */
	public function setCashRegisterId($cashRegisterId){
		if ($cashRegisterId instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type) {
			$this->cashRegisterId = $cashRegisterId;
		}
		else {
			throw new BadMethodCallException("Type of argument cashRegisterId must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token50Type.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property serviceAgentUUID.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType
	 */
	public function getServiceAgentUUID(){
		return $this->serviceAgentUUID;
	}
	
	/**
	 * Sets the value for the property serviceAgentUUID.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType $serviceAgentUUID
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType
	 */
	public function setServiceAgentUUID($serviceAgentUUID){
		if ($serviceAgentUUID instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType) {
			$this->serviceAgentUUID = $serviceAgentUUID;
		}
		else {
			throw new BadMethodCallException("Type of argument serviceAgentUUID must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType.");
		}
		return $this;
	}
	
	
	
}