<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
*/

//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/UuidType.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/OrderStatusType.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/NumericTokenType.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/DataURIScheme.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/CustomerInformationType.php';
/**
 * @XmlType(name="StartOrderResponseType", namespace="http://service.twint.ch/merchant/types/v1")
 */ 
class Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderResponseType {
	/**
	 * @XmlElement(name="OrderUUID", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType
	 */
	private $orderUUID;
	
	/**
	 * @XmlElement(name="OrderStatus", type="Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderStatusType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderStatusType
	 */
	private $orderStatus;
	
	/**
	 * @XmlElement(name="Token", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_NumericTokenType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_NumericTokenType
	 */
	private $token;
	
	/**
	 * @XmlElement(name="QRCode", type="Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_DataURIScheme", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_DataURIScheme
	 */
	private $qRCode;
	
	/**
	 * @XmlElement(name="CustomerInformation", type="Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CustomerInformationType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CustomerInformationType
	 */
	private $customerInformation;
	
	public function __construct() {
	}
	
	/**
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderResponseType
	 */
	public static function _() {
		$i = new Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderResponseType();
		return $i;
	}
	/**
	 * Returns the value for the property orderUUID.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType
	 */
	public function getOrderUUID(){
		return $this->orderUUID;
	}
	
	/**
	 * Sets the value for the property orderUUID.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType $orderUUID
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderResponseType
	 */
	public function setOrderUUID($orderUUID){
		if ($orderUUID instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType) {
			$this->orderUUID = $orderUUID;
		}
		else {
			throw new BadMethodCallException("Type of argument orderUUID must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property orderStatus.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderStatusType
	 */
	public function getOrderStatus(){
		return $this->orderStatus;
	}
	
	/**
	 * Sets the value for the property orderStatus.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderStatusType $orderStatus
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderResponseType
	 */
	public function setOrderStatus($orderStatus){
		if ($orderStatus instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderStatusType) {
			$this->orderStatus = $orderStatus;
		}
		else {
			throw new BadMethodCallException("Type of argument orderStatus must be Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderStatusType.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property token.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_NumericTokenType
	 */
	public function getToken(){
		return $this->token;
	}
	
	/**
	 * Sets the value for the property token.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_NumericTokenType $token
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderResponseType
	 */
	public function setToken($token){
		if ($token instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_NumericTokenType) {
			$this->token = $token;
		}
		else {
			throw new BadMethodCallException("Type of argument token must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_NumericTokenType.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property qRCode.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_DataURIScheme
	 */
	public function getQRCode(){
		return $this->qRCode;
	}
	
	/**
	 * Sets the value for the property qRCode.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_DataURIScheme $qRCode
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderResponseType
	 */
	public function setQRCode($qRCode){
		if ($qRCode instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_DataURIScheme) {
			$this->qRCode = $qRCode;
		}
		else {
			throw new BadMethodCallException("Type of argument qRCode must be Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_DataURIScheme.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property customerInformation.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CustomerInformationType
	 */
	public function getCustomerInformation(){
		return $this->customerInformation;
	}
	
	/**
	 * Sets the value for the property customerInformation.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CustomerInformationType $customerInformation
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderResponseType
	 */
	public function setCustomerInformation($customerInformation){
		if ($customerInformation instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CustomerInformationType) {
			$this->customerInformation = $customerInformation;
		}
		else {
			throw new BadMethodCallException("Type of argument customerInformation must be Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_CustomerInformationType.");
		}
		return $this;
	}
	
	
	
}