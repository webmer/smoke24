<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
*/

//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/MerchantInformationType.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/UuidType.php';
//require_once 'Customweb/Twint/Stubs/Org/W3/XMLSchema/Float.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/Token3Type.php';
//require_once 'Customweb/Twint/Stubs/Org/W3/XMLSchema/Boolean.php';
/**
 * @XmlType(name="ConfirmOrderRequestType", namespace="http://service.twint.ch/merchant/types/v1")
 */ 
class Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_ConfirmOrderRequestType {
	/**
	 * @XmlElement(name="MerchantInformation", type="Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType
	 */
	private $merchantInformation;
	
	/**
	 * @XmlElement(name="OrderUUID", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType
	 */
	private $orderUUID;
	
	/**
	 * @XmlValue(name="RequestedAmount", simpleType=@XmlSimpleTypeDefinition(typeName='decimal', typeNamespace='http://www.w3.org/2001/XMLSchema', type='Customweb_Twint_Stubs_Org_W3_XMLSchema_Float'), namespace="http://service.twint.ch/merchant/types/v1")
	 * @var float
	 */
	private $requestedAmount;
	
	/**
	 * @XmlElement(name="Currency", type="Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token3Type", namespace="http://service.twint.ch/merchant/types/v1")
	 * @var Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token3Type
	 */
	private $currency;
	
	/**
	 * @XmlValue(name="PartialConfirmation", simpleType=@XmlSimpleTypeDefinition(typeName='boolean', typeNamespace='http://www.w3.org/2001/XMLSchema', type='Customweb_Twint_Stubs_Org_W3_XMLSchema_Boolean'), namespace="http://service.twint.ch/merchant/types/v1")
	 * @var boolean
	 */
	private $partialConfirmation;
	
	public function __construct() {
	}
	
	/**
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_ConfirmOrderRequestType
	 */
	public static function _() {
		$i = new Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_ConfirmOrderRequestType();
		return $i;
	}
	/**
	 * Returns the value for the property merchantInformation.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType
	 */
	public function getMerchantInformation(){
		return $this->merchantInformation;
	}
	
	/**
	 * Sets the value for the property merchantInformation.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType $merchantInformation
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_ConfirmOrderRequestType
	 */
	public function setMerchantInformation($merchantInformation){
		if ($merchantInformation instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType) {
			$this->merchantInformation = $merchantInformation;
		}
		else {
			throw new BadMethodCallException("Type of argument merchantInformation must be Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_MerchantInformationType.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property orderUUID.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType
	 */
	public function getOrderUUID(){
		return $this->orderUUID;
	}
	
	/**
	 * Sets the value for the property orderUUID.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType $orderUUID
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_ConfirmOrderRequestType
	 */
	public function setOrderUUID($orderUUID){
		if ($orderUUID instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType) {
			$this->orderUUID = $orderUUID;
		}
		else {
			throw new BadMethodCallException("Type of argument orderUUID must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property requestedAmount.
	 * 
	 * @return Customweb_Twint_Stubs_Org_W3_XMLSchema_Float
	 */
	public function getRequestedAmount(){
		return $this->requestedAmount;
	}
	
	/**
	 * Sets the value for the property requestedAmount.
	 * 
	 * @param float $requestedAmount
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_ConfirmOrderRequestType
	 */
	public function setRequestedAmount($requestedAmount){
		if ($requestedAmount instanceof Customweb_Twint_Stubs_Org_W3_XMLSchema_Float) {
			$this->requestedAmount = $requestedAmount;
		}
		else {
			$this->requestedAmount = Customweb_Twint_Stubs_Org_W3_XMLSchema_Float::_()->set($requestedAmount);
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property currency.
	 * 
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token3Type
	 */
	public function getCurrency(){
		return $this->currency;
	}
	
	/**
	 * Sets the value for the property currency.
	 * 
	 * @param Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token3Type $currency
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_ConfirmOrderRequestType
	 */
	public function setCurrency($currency){
		if ($currency instanceof Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token3Type) {
			$this->currency = $currency;
		}
		else {
			throw new BadMethodCallException("Type of argument currency must be Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_Token3Type.");
		}
		return $this;
	}
	
	
	/**
	 * Returns the value for the property partialConfirmation.
	 * 
	 * @return Customweb_Twint_Stubs_Org_W3_XMLSchema_Boolean
	 */
	public function getPartialConfirmation(){
		return $this->partialConfirmation;
	}
	
	/**
	 * Sets the value for the property partialConfirmation.
	 * 
	 * @param boolean $partialConfirmation
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_ConfirmOrderRequestType
	 */
	public function setPartialConfirmation($partialConfirmation){
		if ($partialConfirmation instanceof Customweb_Twint_Stubs_Org_W3_XMLSchema_Boolean) {
			$this->partialConfirmation = $partialConfirmation;
		}
		else {
			$this->partialConfirmation = Customweb_Twint_Stubs_Org_W3_XMLSchema_Boolean::_()->set($partialConfirmation);
		}
		return $this;
	}
	
	
	
}