<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/UuidType.php';
//require_once 'Customweb/Twint/StubBuilder/Create.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/StartOrderResponseElement.php';
//require_once 'Customweb/Twint/Method/Template/Alias.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/DataURIScheme.php';
//require_once 'Customweb/Twint/StubBuilder/ConfirmOrder.php';
//require_once 'Customweb/Twint/Authorization/Transaction.php';
//require_once 'Customweb/Twint/Container.php';
//require_once 'Customweb/Form/Element.php';
//require_once 'Customweb/Twint/Method/IMethod.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Payment/Authorization/AbstractPaymentMethodWrapper.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Merchant/Types/V1/OrderKindType.php';
//require_once 'Customweb/Twint/Update/StatusHandler.php';
//require_once 'Customweb/Twint/Util.php';
//require_once 'Customweb/Util/Currency.php';
//require_once 'Customweb/Util/Invoice.php';
//require_once 'Customweb/Twint/Stubs/Ch/Twint/Service/Base/Types/V1/NumericTokenType.php';
//require_once 'Customweb/Twint/StubBuilder/Refund.php';
//require_once 'Customweb/Form/Control/TextInput.php';
//require_once 'Customweb/Twint/Method/Template/Token.php';



/**
 * @Method()
 *
 * @author Sebastian Bossert
 */
class Customweb_Twint_Method_Twint extends Customweb_Payment_Authorization_AbstractPaymentMethodWrapper implements 
		Customweb_Twint_Method_IMethod {
	private $container;
	private static $paymentInformationMap = array(
		'twint' => array(
			'machine_name' => 'Twint',
 			'method_name' => 'Twint',
 			'parameters' => array(
			),
 			'not_supported_features' => array(
			),
 			'supported_currencies' => array(
				0 => 'CHF',
 			),
 			'image_color' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAAyCAYAAACXpx/YAAAF20lEQVR42u1cLYzcRhQ2CCgICCgICAgIiKpILSgICIjUKAEBBwIKCrZSVFW6xPZFAQEBG+lAwIGCgFQqCAgIaKWCAwUFAQEBAQeuu97TgQULAhYEHDiwYDtvZvc8783z2l577fHpPWmUaG/W65nP773v/YyDoE4ZbF8MhvFukESJGh+DJH4QiJwTGYU/KVAnaszJ+DcYRjdkg7oqw/Cm0VYHWHvMFMivg4OdS7JhXZHB9mVlgt/kAIvHKPqigH4UvH9xQTbQVxn//JXS2mcKsJNS4OKRBIPoB9lM3wRIUxIeVwCWjv3gvyfXZGNb97OKJAFZqg9Y7J+T6KX45zYENh3IkQFhvuHxWYVYD2XTmxAgQUCGgBRtHlgy4k/BUXxLQNgYO1bkxyQq5i2Pd8FxeEUAqUuA7ADpaR9Ye5yq0dfMXaSCOQaSYzZz7umYSNpzbYasiI2/wBK2vXNbACsd2yr22g2A55rRi5QGOG9jT4IkfKXJF/hpiIlB612GfaoBGIb3g6PH13WOGpg4sOL6GPYbAaxWgBU4HJMd7HxHcswHynxezXEDMwHYL4ATXd9lvxO+t+aNM+c5aU4B2B+AwcQ6mrt9WWnjj3guQ3xA67mKURL92SjA2tKo+ysyluvVVTLymZPdy/h8+T07UTMIv1EKcVePo/Db7EhG5/lfLSp0e3qfj55+XX1NfFnvwAFWm2FGy/G8i6nPVf/SjRg9/r5RgLGlyRtjK++eMndqnUbRc/053fxRFKffU7+bWq7frev95dwj8JXsevrUCQ9Lr4nV3niXFBteZ1zgJdHQPWcOLABvxBevATbrsDpT4ntkjYeLPXpIPv/Hsn7PCgGsFYLtgsHDLq3WAjA8jRjgDxkX6JNF7jPX2iJzxs0BHPX0PZ4Nwh3sv9lrHkV/sA8xIpeWlppk0YkVzt0oBLBJMGGLCA/HKP4Nk9Lw+Mzl0TXZltX8n6ypmGb2C83TYREJnWgZsFLcXZFkoXVYAK0khMrVpNHALrqfZYRh2pbSqhi+VjbAyO2RtTkJqIwkD+qq4faH3Uj1xBQzJWMm7WkVKuKnK8MrXwE26001aPmQ0maHpdabDhf+Hldr8GmmKzD3YKePezUCzPwgJDCKzbt5Zi4oky7bw9UWwNQtgZsBZoyLIKl226Yf2G9xgFdrKHZndQOsNJFWcfgwx42XwYeAtqLvAnVnuznGZJx4AbBhywuA1XqMX1zcM4Qzlr9NNW3muiR/ATYLQ5RehQasD1ULpqbaiRFZctVn5k38ABiFdIfWfe3rrJ3dV5aC/YEBwGOAuR8Gk1xkXp5pXsuENwgwJoS2r3yQGa6A1ncPYCYVmZqreW7Kkvfdp64JL5LKbBpg54GbnrktE64QgJXWdxBg1wTDIrnMFiUYNAGQxa6N6Z96B7CTlrX2wWW5nzMA6ALAzA1gcmF8EbtJmlXblaSPrmkumqNuGGDDCWaZGjqM3ubeW3cAViSDMsQ0sTHV+ersjd1La8ukrFiqytQwwDhcStx4WTcoZluvjgHMXwA0l6YjKdhg0nUaTj0Qa5nmFgFehkt2fhlfc4KSId0GWC/0vgMen+TvOVWTyuXDqgDrWHyZq+0V+o7pNu1nWijYD/rgonVDgiT+RY9heId54BaDaZgwFSrzd0pK0zlbVu55qzrAnKnmcqhQNVrV12zOE0vBv8WOjlXNb2/ZawGguBy4X3CeAOwVwHbAn1cu5Ezh2g32AvA6AK9bvpuirgYu8OdMdbU+7L4AVlZQqWuNg2KgyXknECEpAj4XF9LLnz6UI6brgqwzNuPA36Z3eZFLZTHpx+d+nVEKj52wTKSigL9EKbgWhn5Zi3Idcqpwg2IC9E/NA6yY8qq0p0jtLLvXyAE1yPly5TaRBsRUVTZ1hniiGbaIBwJ52VH4t5zeP+9iymSHld6/seo0oogHonugw1/L5ZTlDTod9c+6jXSW+w4seUdll7Nh7NvwZrqbQ1KM5wlo3UX5Tmu1vIeydfkfg3D90CyRmHsAAAAASUVORK5CYII=',
 			'image_grey' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAAyEAAAAADiXJxHAAAFSUlEQVR42t2azWsiSRTAPegptxxz8A8IeDM5LOzRPSS5+CcIXmagAxLIcTYHDyPBrBAJHYIMYpBBFneHIYR1mpBEpJEQyYRgvnETQkjWFRFDMGJwefvmUdXdfrQdBdt+IG3ZVvWv+9X7qrI0dR11IX0QETeLF2NNkx8WPRfl/eLE8gxK4l1xb6SB76c3iwSLErSnAjXvSAI/XW7PKmFJVidz8uvLSAE35rPOkNwaFyUi3thGBvhibCPRCZYkOV52mx64uJd4pweWZvT+lJlmtAq45k0Fgnb9uChrruMlEwK/vuTk1cleYUlijrtTUwHf2CKiUViSr6GqYArgsjs5/lZYlJVqptCYH2rg15f9qZVqf3BRxIlhDkAtx0v9hCXLfZseWuA1V/+Bl2dSgaEFVt9qSJbiN7ayu7h3vEQ2e6WaClwvlvL30zk55tADvD1rEuCYg9nZx3Nsi0oVD/+X46XuntokwBGxzrmVzx+gbb1cF7SB54gA30+zPOnMim3M/FQFlh99+WYM+PH8Nq0VGPfpks4o5uPP4TcMbP79rVAqlP5J8gHTxZgU357d9ZxZn+vdRuKAoxLBRiX2zqniAXM35qCbeHAZA0atUct6GSJ4tO+kT3IjaKfbP1yA3z5/gPPUr3D+5RfqsZTn8/Wwj1xiu5E44PQBXpoKsLb9KWzb9VBLKY8tnYPQ3oGbTayqFD7ilZ+E5RmK0H//GX7JOrXAdYHVYkgwadUBfLiAncTnWFumgG0sErv6kQOvl40An/gzBRCyD/gNR/7rPXvEaDDxnb6+YF6OpSUl8P4UaWLWuVNBY7qRgKlHI6G2RiUayaJ9m5mCti0nk4OiVLCz/+5mtHAMBFKawpgDztMH2A/4jPtpzMh+6J8CGHFoNAqi+LAHazbsfjjgjYRWTVDZ4CljanHwXumy+glcF/ANwSOl8gO8+6yT71EJjEExTYO6gN9P/LqA2R+vF7Vt8JyjEtnpdrWutwDTZLqy3Z2iNuH7RvU/s7YCVr9RnGi6gSMiZTrM7TDfvFN5PMez2zSLmtfLKMr6lzFguQGtO5WdCvQsxXHmAnjQTlOpr8AwGDY/19kcleKk1uQVmcEio1bzKq2lMWB0dp/+n1LJ8YoHq2bwGZ+ja/oMzP5c+KhtU6tzexU3BoymEOch+FPmWuTGwIBZKAmKpW7j5/dKlVRcG2oaBaYHF/bB5DrxU38ProEBMxVuzLOIi0wGBQG8xX6uh339AqaAFu+BrO6ai10xAGDWARoMmEf80gs6j80iqXOruNoocM2LvdMb3bKqexsIsDhBNhECjrDv6ZK/KQgzQzKljK0zJ6PA6JoogofSolK/BgTMX5Acp2CSsBvzETEnt1fntwGDa8KoGQ+w2Hyhf0DAyzPXi4THAn/qiBKI9mliN+DbNES2/I3RUXZnCrxGXS/Sw8Xj7vT70fejv/+gBwfCChSHC/CdjCkcVzZoubJ1BWZqzeLU1Ull3TnvN32Jh5ctbuZUBUwHefNFbSMDjM5fnR4yNexUvB9i4E5pXtiHNYcTP78UjmrduZ5NIecQAmPy1X6J7GJMvZ4YlfJ+TNbbryYO7wKqBaKbztWLXmW4t71Y0PXIjf6sL20kyJ0N/XJpVdiyvg12dTLrHO6VQ9WCODh1fQspre2yMgA1BTBa5N6X1+JzD66mSY4Wm1pq3l5WjMWJvL9poqPNtqWy+8+fRmG9XzcwpmZQ/e+0o0O5qmh6YCjbHeVax8tm2bPTIzDOaCmujLRgV5Y591nqAsZSD+3NC9p3PebdSasbGBPxryEpbradldrjP5ZQfxcoLQqBAAAAAElFTkSuQmCC',
 		),
 	);
	/**
	 * The name to use for the alias field.
	 *
	 * @var string
	 */
	const ALIAS_FIELD = "twint-alias-label";
	/**
	 * How long the widget will wait between sending requests.
	 * 2 seconds.
	 *
	 * @var integer
	 */
	const UPDATE_INTERVAL = 2000;
	/**
	 * How long the widget will wait before timing a request out.
	 * 30 seconds.
	 *
	 * @var integer
	 */
	const UPDATE_TIMEOUT = 30000;

	public function __construct(Customweb_Payment_Authorization_IPaymentMethod $paymentMethod, Customweb_DependencyInjection_IContainer $container){
		parent::__construct($paymentMethod);
		if (!$container instanceof Customweb_Twint_Container) {
			$container = new Customweb_Twint_Container($container);
		}
		$this->container = $container;
	}

	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext){
		$formFields = array();
		if ($aliasTransaction == 'new') {
			$formFields[] = $this->createAliasLabelField();
		}
		return $formFields;
	}

	public function getWidgetHTML(Customweb_Payment_Authorization_ITransaction $transaction, array $formData){
		if (!$transaction instanceof Customweb_Twint_Authorization_Transaction) {
			throw new Exception('Transaction must be of type Customweb_Twint_Authorization_Transaction.');
		}
		
		$this->cancelActiveAliasRequest($transaction, $formData);
		
		$response = $this->sendStartOrderRequest($transaction, $formData);
		
		$orderId = $response->getOrderUUID();
		if (empty($orderId)) {
			throw new Exception(Customweb_I18n_Translation::__("The order could not be started: The order id is missing."));
		}
		
		$id = $orderId->get();
		$transaction->setPaymentId($id);
		
		$aliasTransaction = $transaction->getTransactionContext()->getAlias();
		$isAlias = $aliasTransaction instanceof Customweb_Twint_Authorization_Transaction;
		if ($isAlias) {
			$this->getContainer()->getStorageAdapter()->write($id, $aliasTransaction->getAlias());
		}
		
		if (isset($formData[self::ALIAS_FIELD])) {
			$transaction->setTemporaryAliasForDisplay(strip_tags($formData[self::ALIAS_FIELD]));
		}
		
		try {
			if ($isAlias) {
				$templateRenderer = new Customweb_Twint_Method_Template_Alias($this->getContainer(), $response, $transaction);
			}
			else {
				$templateRenderer = new Customweb_Twint_Method_Template_Token($this->getContainer(), $response, $transaction);
			}
			$template = $templateRenderer->renderTemplate();
			
			return $template;
		}
		catch (Exception $exc) {
			Customweb_Twint_Util::cancelOrder($this->getContainer(), $id);
			throw $exc;
		}
	}

	private function createAliasLabelField(){
		$control = new Customweb_Form_Control_TextInput(self::ALIAS_FIELD);
		$control->setRequired(false);
		$field = new Customweb_Form_Element(Customweb_I18n_Translation::__("Twint Account Name"), $control);
		$field->setRequired(false);
		$field->setDescription(
				Customweb_I18n_Translation::__(
						'This name is used to associate your twint account with your shop account. If this field is not filled your twint account is not stored in your shop account.'));
		return $field;
	}

	private function cancelActiveAliasRequest(Customweb_Twint_Authorization_Transaction $transaction, array $formData){
		$aliasTransaction = $transaction->getTransactionContext()->getAlias();
		if ($aliasTransaction == 'new' || empty($aliasTransaction)) {
			return;
		}
		try {
			$previousId = $this->getContainer()->getStorageAdapter()->read($aliasTransaction->getAlias());
			$statusHandler = new Customweb_Twint_Update_StatusHandler($this->getContainer());
			$response = $statusHandler->monitor($previousId);
			$status = $response->getOrder()->getStatus()->getStatus()->getCode()->get();
			$reason = $response->getOrder()->getStatus()->getReason()->getCode()->get();
			if ($status == 1 && $reason != Customweb_Twint_Update_StatusHandler::PROCESSING_MERCHANT) {
				Customweb_Twint_Util::cancelOrder(new Customweb_Twint_Container($this->getContainer()), $previousId);
			}
		}
		catch (Exception $exc) {
		}
	}

	public function cancel(Customweb_Twint_Authorization_Transaction $transaction){
		$transaction->cancelDry();
		
		Customweb_Twint_Util::cancelOrder($this->getContainer(), $transaction->getPaymentId());
		
		$transaction->cancel();
	}

	public function partialRefund(Customweb_Twint_Authorization_Transaction $transaction, $items, $close){
		$transaction->refundByLineItemsDry($items, $close);
		
		$amount = Customweb_Util_Invoice::getTotalAmountIncludingTax($items);
		$refundBuilder = new Customweb_Twint_StubBuilder_Refund($transaction, $this->getContainer(), $amount);
		$refundStub = $refundBuilder->build();
		$this->getContainer()->getSoapService()->startOrder($refundStub);
		
		$transaction->refundByLineItems($items, $close);
	}

	public function partialCapture(Customweb_Twint_Authorization_Transaction $transaction, $items, $close){
		$transaction->partialCaptureByLineItemsDry($items, $close);
		
		$amount = Customweb_Util_Invoice::getTotalAmountIncludingTax($items);
		if (Customweb_Util_Currency::compareAmount(Customweb_Util_Invoice::getTotalAmountIncludingTax($transaction->getUncapturedLineItems()), 
				$amount, $transaction->getCurrencyCode()) === 0) {
			$close = true;
		}
		
		$isPartial = !$close;
		
		$confirmBuilder = new Customweb_Twint_StubBuilder_ConfirmOrder($this->getContainer(), $transaction, $amount, $isPartial);
		$confirmStub = $confirmBuilder->build();
		$this->getContainer()->getSoapService()->confirmOrder($confirmStub);
		
		$transaction->partialCaptureByLineItems($items, $close);
	}

	public function validate(Customweb_Payment_Authorization_IOrderContext $orderContext, Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext, array $formData){
		return;
	}

	public function isDeferredCapturingActive(){
		return $this->getPaymentMethodConfigurationValue('capturing') == 'deferred';
	}

	/**
	 *
	 * @param Customweb_Twint_Authorization_Transaction $transaction
	 * @param array $formData
	 * @return Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderResponseElement
	 */
	protected function sendStartOrderRequest(Customweb_Twint_Authorization_Transaction $transaction, array $formData){
		// prevent start order from being started twice
		if ($transaction->getPaymentId() != null) {
			return $this->createStartOrderResponseFromTransaction($transaction);
		}
		
		if ($this->getContainer()->getPaymentMethodByTransaction($transaction)->isDeferredCapturingActive()) {
			$orderType = Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderKindType::PAYMENT_DEFERRED();
		}
		else {
			$orderType = Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_OrderKindType::PAYMENT_CONFIRM();
		}
		
		$requestBuilder = new Customweb_Twint_StubBuilder_Create($transaction, $this->getContainer(), $orderType);
		$startOrderRequest = $requestBuilder->build();
		$response = $this->getContainer()->getSoapService()->startOrder($startOrderRequest);
		
		$this->saveStartOrderResponse($transaction, $response);
		
		return $response;
	}

	private function createStartOrderResponseFromTransaction(Customweb_Twint_Authorization_Transaction $transaction){
		$response = new Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderResponseElement();
		$response->setOrderUUID(Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_UuidType::_()->set($transaction->getPaymentId()));
		$response->setQRCode(
				Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_DataURIScheme::_()->set($transaction->getQRCode()));
		$response->setToken(Customweb_Twint_Stubs_Ch_Twint_Service_Base_Types_V1_NumericTokenType::_()->set($transaction->getToken()));
		return $response;
	}

	private function saveStartOrderResponse(Customweb_Twint_Authorization_Transaction $transaction, Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderResponseElement $response){
		$temp = $response->getToken();
		if (!empty($temp)) {
			$transaction->setToken($temp->get());
		}
		$temp = $response->getOrderUUID();
		if (!empty($temp)) {
			$transaction->setPaymentId($temp->get());
		}
		$temp = $response->getQRCode();
		if (!empty($temp)) {
			$transaction->setQRCode($temp->get());
		}
	}

	protected function getPaymentInformationMap(){
		return self::$paymentInformationMap;
	}

	/**
	 *
	 * @return Customweb_Twint_Container
	 *
	 */
	protected function getContainer(){
		return $this->container;
	}
}
