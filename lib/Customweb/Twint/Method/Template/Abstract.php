<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Mvc/Template/RenderContext.php';
//require_once 'Customweb/Util/Currency.php';
//require_once 'Customweb/Mvc/Template/SecurityPolicy.php';
//require_once 'Customweb/Util/JavaScript.php';
//require_once 'Customweb/Twint/AbstractAdapter.php';
//require_once 'Customweb/Twint/Method/Twint.php';
//require_once 'Customweb/I18n/Translation.php';



/**
 *
 * @author Sebastian Bossert
 */
abstract class Customweb_Twint_Method_Template_Abstract extends Customweb_Twint_AbstractAdapter {
	private $transaction;
	private $response;

	public function __construct(Customweb_DependencyInjection_IContainer $container, Customweb_Twint_Stubs_Ch_Twint_Service_Merchant_Types_V1_StartOrderResponseElement $response, Customweb_Twint_Authorization_Transaction $transaction){
		parent::__construct($container);
		$this->transaction = $transaction;
		$this->response = $response;
	}

	public abstract function renderTemplate();

	/**
	 * Initialises a template and renders it.
	 *
	 * @param String $templateName
	 * @param String $css
	 * @param array $variables
	 *
	 * @return String
	 */
	protected function createTemplate($templateName, $css, array $variables){
		$templateRenderer = $this->getContainer()->getBean('Customweb_Mvc_Template_IRenderer');
		/* @var $templateRenderer Customweb_Mvc_Template_IRenderer */
		
		$assetResolver = $this->getContainer()->getBean('Customweb_Asset_IResolver');
		/* @var $assetResolver Customweb_Asset_IResolver */
		
		$templateContext = new Customweb_Mvc_Template_RenderContext();
		$templateContext->setSecurityPolicy(new Customweb_Mvc_Template_SecurityPolicy());
		$templateContext->setTemplate($templateName);
		
		$loadingImage = (string) $assetResolver->resolveAssetUrl('img/loading.gif');
		$amount = Customweb_Util_Currency::formatAmount($this->getAmount(), $this->getCurrency());
		
		$twintLogo = (string) $assetResolver->resolveAssetUrl('img/twint_logo.png');
		$pfLogo = (string) $assetResolver->resolveAssetUrl('img/pf_logo.png');
		$templateContext->addVariable('twintLogo', $twintLogo);
		$templateContext->addVariable('pfLogo', $pfLogo);
		
		$appImage = (string) $assetResolver->resolveAssetUrl('img/icon_app.png');
		$camImage = (string) $assetResolver->resolveAssetUrl('img/icon_cam.png');
		$payImage = (string) $assetResolver->resolveAssetUrl('img/icon_pay.png');
		$templateContext->addVariable('payImage', $payImage);
		$templateContext->addVariable('appImage', $appImage);
		$templateContext->addVariable('camImage', $camImage);
		
		$loadingImage = (string) $assetResolver->resolveAssetUrl('img/loading.gif');
		$templateContext->addVariable('loadingImage', $loadingImage);
		
		$templateContext->addVariable('price', $amount);
		$templateContext->addVariable('script', $this->getPollingScript());
		$templateContext->addVariable('styleUrl', $assetResolver->resolveAssetUrl($css));
		
		$templateContext->addVariable('cancelText', Customweb_I18n_Translation::__('Cancel'));
		$templateContext->addVariable('cancelUrl', $this->getCancelUrl());
		$templateContext->addVariable('confirmText', Customweb_I18n_Translation::__('Confirm TWINT payment'));
		
		$templateContext->addVariable('continueText', Customweb_I18n_Translation::__('Continue'));
		$templateContext->addVariable('continueUrl', $this->getContinueUrl());
		
		$templateContext->addVariable('formHash', 
				hash_hmac('sha1', $this->getTransaction()->getExternalTransactionId(), $this->getTransaction()->getFormKey()));
		
		foreach ($variables as $key => $value) {
			$templateContext->addVariable($key, $value);
		}
		
		return $templateRenderer->render($templateContext);
	}

	protected function getPollingScript(){
		$pollingFunction = $this->getPollingFunction();
		$jQuerySnippet = Customweb_Util_JavaScript::getLoadJQueryCode('1.11.2', 'cwJQuery', $pollingFunction);
		//@formatter:off
		return 
'<script type="text/javascript">
var twintCanRun = true;
' . $jQuerySnippet . '
function twintCancel() {
	twintCanRun = false;
	document.getElementById("twint-image-loading").style.visibility = "visible";
	return false;
}
</script>';
		//@formatter:on
	}

	protected function getPollingFunction(){
		//@formatter:off
		$pollingFunction =
'
(function twintPoll() {
	if(typeof window.jQuery == "undefined") {
		window.jQuery = cwJQuery;
	}
	if(twintCanRun === false) {
		document.getElementById("twint-cancel-form").submit();
		return;
	}
    setTimeout(function() {
        window.jQuery.ajax({
            url: "' . $this->getPollingUrl() . '",
            type: "POST",
            success: function(data) {
                if(data.status == "COMPLETE") {
            		window.location.replace(data.redirect);
            		return;
            	}
            	twintPoll();
            },
            dataType: "json",
            error: twintPoll,
           	cache: false,
            timeout: ' . Customweb_Twint_Method_Twint::UPDATE_TIMEOUT . '
        })
    },  ' . Customweb_Twint_Method_Twint::UPDATE_INTERVAL . ');
})';
		//@formatter:on
		return $pollingFunction;
	}

	protected function getPollingUrl(){
		return $this->getContainer()->getEndpointAdapter()->getUrl('process', 'poll', array(
			"cw_transaction_id" => $this->getId() 
		));
	}

	protected function getCancelUrl(){
		return $this->getContainer()->getEndpointAdapter()->getUrl('process', 'cancel', array(
			"cw_transaction_id" => $this->getId() 
		));
	}

	protected function getContinueUrl(){
		return $this->getContainer()->getEndpointAdapter()->getUrl('process', 'continue', array(
			"cw_transaction_id" => $this->getId() 
		));
	}

	protected function getTransaction(){
		return $this->transaction;
	}

	protected function getResponse(){
		return $this->response;
	}

	protected function getId(){
		return $this->getTransaction()->getExternalTransactionId();
	}

	protected function getAmount(){
		return $this->getTransaction()->getAuthorizationAmount();
	}

	protected function getCurrency(){
		return $this->transaction->getCurrencyCode();
	}
}