<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Twint/Method/Template/Abstract.php';
//require_once 'Customweb/I18n/Translation.php';



/**
 *
 * @author Sebastian Bossert
 */
class Customweb_Twint_Method_Template_Token extends Customweb_Twint_Method_Template_Abstract {

	public function renderTemplate(){
		$responseImage = $this->getResponse()->getQRCode();
		if (!empty($responseImage)) {
			$image = $responseImage->get();
		}
		$responseToken = $this->getResponse()->getToken();
		if (!empty($responseToken)) {
			$token = $responseToken->get();
		}
		if (empty($token) && empty($image)) {
			throw new Exception(Customweb_I18n_Translation::__("The order was cancelled: QR-Code and Numeric Code were not returned."));
		}
		
		$tokens = array_map('intval', str_split($token));
		
		$variables = array(
			'token1' => $tokens[0],
			'token2' => $tokens[1],
			'token3' => $tokens[2],
			'token4' => $tokens[3],
			'token5' => $tokens[4],
			'qrCode' => $image,
			'confirmText' => Customweb_I18n_Translation::__('Confirm TWINT payment'),
			'qrCodeText' => Customweb_I18n_Translation::__('QR Code'),
			'step1' => Customweb_I18n_Translation::__('1. Open the TWINT App on your smartphone.'),
			'step2' => Customweb_I18n_Translation::__("2. Select the 'Pay with code' menu."),
			'step3' => Customweb_I18n_Translation::__("3. Select the 'Code' tab and enter the code. Or select the 'QR scanner' tab and point your smartphone camera at the QR code."),
			'tokenText' => Customweb_I18n_Translation::__('Code'),
			'orText' => Customweb_I18n_Translation::__("or") 
		);
		
		return $this->createTemplate('token', 'token.css', $variables);
	}
}