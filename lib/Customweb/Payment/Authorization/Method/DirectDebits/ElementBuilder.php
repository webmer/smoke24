<?php 

/**
 * This class provides a simple interface to create the form elements for 
 * direct debits.
 * 
 * SEPA:
 * The IBAN identifies the bank and the account of the bank. However until 2016 
 * the regulation requires to send also the BIC along to identify the bank. In theory
 * they are redundant information. However a direct checks between the two numbers
 * are not possible, because the structure of them are different (e.g. the country code
 * can be different.)
 * 
 * @author Thomas Hunziker
 *
 */
class Customweb_Payment_Authorization_Method_DirectDebits_ElementBuilder {
	
	private $ibanFieldName;
	
	private $bicFieldName;
	
	
}