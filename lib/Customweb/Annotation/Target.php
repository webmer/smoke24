<?php 

//require_once 'Customweb/IAnnotation.php';


/**
 *
 */
class Customweb_Annotation_Target implements Customweb_IAnnotation {
	
	private $targets = array();
	
	public function getTargets() {
		return $this->targets;
	}
	
	public function setValue($targets) {
		if (is_string($targets)) {
			$this->targets = array(strtolower($targets));
		}
		else if (is_array($targets)) {
			foreach ($targets as $target) {
				$this->targets[] = strtolower($target);
			}
		}
		else {
			throw new Exception("The value of the 'Target' annotation must be either a string or an array.");
		}
	}
	
}