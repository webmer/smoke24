<?php
/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Core/Http/Request.php';
//require_once 'Customweb/Core/Http/Client/Factory.php';
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/Payment/Authorization/IInvoiceItem.php';
//require_once 'Customweb/Util/Currency.php';
//require_once 'Customweb/Payment/BackendOperation/Adapter/Service/ICapture.php';
//require_once 'Customweb/Util/Invoice.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/PayPal/BackendOperation/Adapter/AbstractAdapter.php';

//require_once 'Customweb/PayPal/Authorization/Transaction.php';

/**
 *
 * @author Thomas Brenner
 * @Bean
 *
 */
class Customweb_PayPal_BackendOperation_Adapter_CaptureAdapter extends Customweb_PayPal_BackendOperation_Adapter_AbstractAdapter implements 
		Customweb_Payment_BackendOperation_Adapter_Service_ICapture {

	public function capture(Customweb_Payment_Authorization_ITransaction $transaction){
		/* @var $transaction Customweb_PayPal_Authorization_Transaction */
		$items = $transaction->getUncapturedLineItems();
		$this->partialCapture($transaction, $items, true);
	}

	public function partialCapture(Customweb_Payment_Authorization_ITransaction $transaction, $items, $close){
		$transaction->partialCaptureByLineItemsDry($items, $close);
		/* @var $transaction Customweb_PayPal_Authorization_Transaction */
		$amount = Customweb_Util_Invoice::getTotalAmountIncludingTax($items);
		
		$allItems = $transaction->getUncapturedLineItems();
		$totalAmount = Customweb_Util_Invoice::getTotalAmountIncludingTax($allItems);
		
		if ($totalAmount <= $amount) {
			$close = true;
		}
		$authorizationId = $transaction->getPaymentId();
		//Order Authorization needs to be authorized, before capture
		if ($transaction->getAuthorizationType() == 'order') {
			$parameters = $this->doAuthorization($transaction, $items);
			if (isset($parameters['TRANSACTIONID'])) {
				$authorizationId = $parameters['TRANSACTIONID'];
			}
			$transaction->setSellerProtection($parameters['PROTECTIONELIGIBILITY']);
			if ($this->getConfiguration()->isMarkTransactionAsUncertain()) {
				if (!strcmp($transaction->getSellerProtection(), 'Ineligible')) {
					$transaction->setAuthorizationUncertain();
				}
			}
		}
		$completion = 'Complete';
		if ($close == false) {
			$completion = 'NotComplete';
		}
		
		$body = array_merge($this->getSecurityParameters(), 
				array(
					'METHOD' => "DoCapture",
					'AUTHORIZATIONID' => $authorizationId,
					'AMT' => Customweb_Util_Currency::formatAmount($amount, $transaction->getCurrencyCode(), '.', ''),
					'COMPLETETYPE' => $completion,
					'CURRENCYCODE' => $transaction->getCurrencyCode() 
				));
		
		$client = Customweb_Core_Http_Client_Factory::createClient();
		$request = new Customweb_Core_Http_Request($this->getConfiguration()->getApiUrlPaypal());
		$request->setMethod("POST");
		$request->setBody($body);
		$response = $client->send($request);
		$parameters = $response->getParsedBody();
		
		if (!isset($parameters['ACK']) || !($parameters['ACK'] == 'Success')) {
			$error = 'No error provided by PayPal.';
			if (isset($referencePaymentResponse['L_LONGMESSAGE0'])) {
				$error = $referencePaymentResponse['L_LONGMESSAGE0'];
			}
			throw new Exception(
					Customweb_I18n_Translation::__("The transaction could not be captured. Error: @error", array(
						'@error' => $error 
					)));
		}
		else {
			$captureId = $parameters['TRANSACTIONID'];
			$item = $transaction->partialCaptureByLineItems($items, $close);
			$item->setCaptureId($captureId);
		}
	}

	protected function doAuthorization(Customweb_Payment_Authorization_ITransaction $transaction, $items){
		$amount = Customweb_Util_Invoice::getTotalAmountIncludingTax($items);
		$body = array_merge($this->getSecurityParameters(), $this->getLineItemsParameters($transaction, $items), 
				$this->getReferenceShippingAddressParameters($transaction), $this->getAmountParameters($transaction, $items), 
				array(
					'METHOD' => "DoAuthorization",
					'TRANSACTIONID' => $transaction->getPaymentId(),
					'CURRENCYCODE' => $transaction->getCurrencyCode() 
				));
		$client = Customweb_Core_Http_Client_Factory::createClient();
		$request = new Customweb_Core_Http_Request($this->getConfiguration()->getApiUrlPaypal());
		$request->setMethod("POST");
		$request->setBody($body);
		$response = $client->send($request);
		$parameters = $response->getParsedBody();
		
		if (!isset($parameters['ACK']) || !($parameters['ACK'] == 'Success')) {
			$error = 'No error provided by PayPal.';
			if (isset($parameters['L_LONGMESSAGE0'])) {
				$error = $parameters['L_LONGMESSAGE0'];
			}
			elseif(isset($parameters['L_SHORTMESSAGE0'])) {
				$error = $parameters['L_SHORTMESSAGE0'];
			}
			throw new Exception(
					Customweb_I18n_Translation::__("The transaction could not be captured. Error: @error", array(
						'@error' => $error 
					)));
		}
		
		if (isset($parameters['PAYMENTSTATUS']) && $parameters['PAYMENTSTATUS'] == 'Pending' && isset($referencePaymentResponse['PAYMENTSTATUS']) &&
				 $referencePaymentResponse['PAYMENTSTATUS'] == 'Pending') {
			$transaction->setAuthorizationUncertain();
		}
		return $parameters;
	}

	protected function getLineItemsParameters(Customweb_Payment_Authorization_ITransaction $transaction, $items){
		$loopcounter = 0;
		$parameters = array();
		
		$totalAmount = 0;
		foreach ($items as $item) {
			if ($item->getQuantity() == 0) {
				continue;
			}
			if (!$this->getConfiguration()->isTaxSendActive()) {
				$amount = $item->getAmountIncludingTax() / $item->getQuantity();
			}
			else {
				$amount = $item->getAmountExcludingTax() / $item->getQuantity();
			}
			if ($item->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT) {
				$amount = $amount * -1;
			}
			$parameters['L_PAYMENTREQUEST_0_NAME' . $loopcounter] = Customweb_Util_String::substrUtf8($item->getName(), 0, 127);
			$parameters['L_PAYMENTREQUEST_0_QTY' . $loopcounter] = number_format($item->getQuantity(), 0, "", "");
			$parameters['L_PAYMENTREQUEST_0_AMT' . $loopcounter] = Customweb_Util_Currency::formatAmount($amount, $transaction->getCurrencyCode(), 
					'.', '');
			$totalAmount = $totalAmount + $parameters['L_PAYMENTREQUEST_0_AMT' . $loopcounter] * $parameters['L_PAYMENTREQUEST_0_QTY' . $loopcounter];
			$loopcounter++;
		}
		if (!$this->getConfiguration()->isTaxSendActive()) {
			$actualTotal = Customweb_Util_Invoice::getTotalAmountIncludingTax($items);
		}
		else {
			$actualTotal = Customweb_Util_Invoice::getTotalAmountExcludingTax($items);
		}
		if (Customweb_Util_Currency::compareAmount($actualTotal, $totalAmount, $transaction->getCurrencyCode()) !== 0) {
			$amount = $actualTotal - $totalAmount;
			$parameters['L_PAYMENTREQUEST_0_NAME' . (string) $loopcounter] = Customweb_I18n_Translation::__("Rounding Adjustments");
			$parameters['L_PAYMENTREQUEST_0_QTY' . $loopcounter] = 1;
			$parameters['L_PAYMENTREQUEST_0_AMT' . $loopcounter] = Customweb_Util_Currency::formatAmount($amount, $transaction->getCurrencyCode(), 
					'.', '');
			$loopcounter++;
		}
		return $parameters;
	}

	protected function getAmountParameters($transaction, $items){
		$itemAmount = Customweb_Util_Currency::formatAmount(Customweb_Util_Invoice::getTotalAmountExcludingTax($items), 
				$transaction->getCurrencyCode(), '.', '');
		$totalAmount = Customweb_Util_Currency::formatAmount(Customweb_Util_Invoice::getTotalAmountIncludingTax($items), 
				$transaction->getCurrencyCode(), '.', '');
		$taxAmount = Customweb_Util_Currency::formatAmount($totalAmount - $itemAmount, $transaction->getCurrencyCode(), '.', '');
		
		if ($this->getConfiguration()->isTaxSendActive()) {
			return array(
				'ITEMAMT' => $itemAmount,
				'AMT' => $totalAmount,
				'TAXAMT' => $taxAmount 
			);
		}
		else {
			return array(
				'ITEMAMT' => $totalAmount,
				'AMT' => $totalAmount 
			);
		}
	}

	protected function getReferenceShippingAddressParameters($transaction){
		$shipping = array();
		$orderContext = $transaction->getTransactionContext()->getOrderContext();
		$shipping['SHIPTONAME'] = Customweb_Util_String::substrUtf8(
				$orderContext->getShippingFirstName() . ' ' . $orderContext->getShippingLastName(), 0, 128);
		$shipping['SHIPTOSTREET'] = Customweb_Util_String::substrUtf8($orderContext->getShippingStreet(), 0, 100);
		$shipping['SHIPTOCITY'] = Customweb_Util_String::substrUtf8($orderContext->getShippingCity(), 0, 40);
		$shipping['SHIPTOZIP'] = Customweb_Util_String::substrUtf8($orderContext->getShippingPostCode(), 0, 20);
		$shipping['SHIPTOCOUNTRYCODE'] = $orderContext->getShippingCountryIsoCode();
		$state = $orderContext->getShippingState();
		if (!empty($state)) {
			$shipping['SHIPTOSTATE'] = $state;
		}
		return $shipping;
	}
}