<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Core/Http/Request.php';
//require_once 'Customweb/Core/Http/Client/Factory.php';
//require_once 'Customweb/Payment/BackendOperation/Adapter/Service/IRefund.php';
//require_once 'Customweb/Util/Currency.php';
//require_once 'Customweb/Util/Invoice.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/PayPal/BackendOperation/Adapter/AbstractAdapter.php';



/**
 *
 * @author Thomas Brenner
 * @Bean
 *
 */
class Customweb_PayPal_BackendOperation_Adapter_RefundAdapter extends Customweb_PayPal_BackendOperation_Adapter_AbstractAdapter implements 
		Customweb_Payment_BackendOperation_Adapter_Service_IRefund {

	public function refund(Customweb_Payment_Authorization_ITransaction $transaction){
		$items = $transaction->getTransactionContext()->getOrderContext()->getInvoiceItems();
		return $this->partialRefund($transaction, $items, true);
	}

	public function partialRefund(Customweb_Payment_Authorization_ITransaction $transaction, $items, $close){
		
		/* @var $transaction Customweb_PayPal_Authorization_Transaction */
		/* @var $capture Customweb_PayPal_Authorization_TransactionCapture */
		
		$transaction->refundByLineItemsDry($items, $close);
		
		$amount = Customweb_Util_Invoice::getTotalAmountIncludingTax($items);
		$totalNotRefundedAmount = $transaction->getRefundableAmount();
		
		$residualAmount = $amount;
		
		if ($amount >= $totalNotRefundedAmount) {
				$close = true;
		}
		//The logic to refund from different captures
		foreach ($transaction->getCaptures() as $capture) {
			// Check if we are finished with refunding
			if ($residualAmount <= 0) {
				break;
			}
			$refundtype = 'Partial';
			
			$amountToRefund = $residualAmount;
			$refundableAmount = $capture->getAmount() - $capture->getRefundedAmount();
			if ($refundableAmount < $residualAmount) {
				$amountToRefund = $refundableAmount;
				$refundtype == 'Full';
			}
			
			if ($amountToRefund <= 0) {
				continue;
			}
			
			$residualAmount = $residualAmount - $amountToRefund;
			
			$closeRefund = false;
			if ($close) {
				if ($residualAmount <= 0) {
					$closeRefund = true;
				}
			}
			
			$additionalInformation = array(
				'TRANSACTIONID' => $capture->getCaptureId(),
				'REFUNDTYPE' => $refundtype,
				'CURRENCYCODE' => $transaction->getCurrencyCode(),
			);
			
			if ($refundtype == 'Partial') {
				$additionalInformation['AMT'] = Customweb_Util_Currency::formatAmount($amountToRefund, $transaction->getCurrencyCode(), '.', '');
			}
			
			$transaction->refundDry($amountToRefund, $closeRefund);
			$this->doRefund($additionalInformation, $closeRefund, $transaction, $capture, $amountToRefund);
		}
	}
	
	//Processing the HTTP-Request to refund
	public function doRefund($additionalInformation, $close, Customweb_PayPal_Authorization_Transaction $transaction, Customweb_PayPal_Authorization_TransactionCapture $capture, $amount){
	
		$body = array_merge($additionalInformation, $this->getSecurityParameters(),
			array( 'METHOD' => "RefundTransaction", )
		);
		
		$client = Customweb_Core_Http_Client_Factory::createClient();
		$request = new Customweb_Core_Http_Request($this->getConfiguration()->getApiUrlPaypal());
		$request->setMethod("POST");
		$request->setBody($body);
		$response = $client->send($request);
		
		$parameters = $response->getParsedBody();
		
		if (!isset($parameters['ACK']) || strtolower($parameters['ACK']) != 'success') {
			$error = 'No error provided by PayPal.';
			if (isset($parameters['L_LONGMESSAGE0'])) {
				$error = $parameters['L_LONGMESSAGE0'];
			}
			elseif(isset($parameters['L_SHORTMESSAGE0'])) {
				$error = $parameters['L_SHORTMESSAGE0'];
			}			
			throw new Exception(
					Customweb_I18n_Translation::__('Refund failed. Error message is: !message.', 
							array(
								'!message' => $error 
							)));
		}
		
		$message = Customweb_I18n_Translation::__("PayPal Refund.");
		$refund = $transaction->refund($amount, $close, $message);
		$refund->setRefundId($parameters['REFUNDTRANSACTIONID']);
		$capture->setRefundedAmount(($capture->getRefundedAmount() + $amount));
	}
}