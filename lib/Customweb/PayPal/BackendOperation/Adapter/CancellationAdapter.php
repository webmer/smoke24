<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Core/Http/Request.php';
//require_once 'Customweb/Core/Http/Client/Factory.php';
//require_once 'Customweb/Payment/BackendOperation/Adapter/Service/ICancel.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/PayPal/BackendOperation/Adapter/AbstractAdapter.php';



/**
 *
 * @author Thomas Brenner
 * @Bean
 *
 */
class Customweb_PayPal_BackendOperation_Adapter_CancellationAdapter extends Customweb_PayPal_BackendOperation_Adapter_AbstractAdapter implements 
		Customweb_Payment_BackendOperation_Adapter_Service_ICancel {

	public function cancel(Customweb_Payment_Authorization_ITransaction $transaction){
		$transaction->cancelDry();
		
		$body = array_merge($this->getSecurityParameters(),
				array(
					'METHOD' => "DoVoid",
					'AUTHORIZATIONID' => $transaction->getPaymentId()
				));
		
		$client = Customweb_Core_Http_Client_Factory::createClient();
		$request = new Customweb_Core_Http_Request($this->getConfiguration()->getApiUrlPaypal());
		$request->setMethod("POST");
		$request->setBody($body);
		$response = $client->send($request);
		
		$parameters = $response->getParsedBody();
		if (!isset($parameters['ACK']) || $parameters['ACK'] != 'Success') {
			$error = 'No error provided by PayPal.';
			if (isset($parameters['L_LONGMESSAGE0'])) {
				$error = $parameters['L_LONGMESSAGE0'];
			}
			elseif(isset($parameters['L_SHORTMESSAGE0'])) {
				$error = $parameters['L_SHORTMESSAGE0'];
			}
			throw new Exception(Customweb_I18n_Translation::__("The transaction could not be cancelled. Error: @error", array('@error' => $error)));
		}
		$transaction->cancel();
		
	}
}