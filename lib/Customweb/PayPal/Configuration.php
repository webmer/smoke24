<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */


//require_once 'Customweb/I18n/LocalizableString.php';
//require_once 'Customweb/Core/Charset.php';


/**
 *
 * @author Thomas Brenner
 * @Bean       
 *        
 */
class Customweb_PayPal_Configuration {
	
	/**
	 *         	  	  	    	  
	 * 
	 * @var Customweb_Payment_IConfigurationAdapter
	 */
	private $configurationAdapter = null;

	public function __construct(Customweb_Payment_IConfigurationAdapter $configurationAdapter) {
		$this->configurationAdapter = $configurationAdapter;
	}

	/**
	 * Returns whether the gateway is in test mode or in live mode.
	 *         	  	  	    	  
	 * 
	 * @return boolean True if the system is in test mode. Else return false.
	 */
	public function isTestMode() {
		return $this->getConfigurationAdapter ()
			->getConfigurationValue ( 'operation_mode' ) != 'live';
	}

	public function getEmailAddress() {
		$email = null;
		if ($this->isTestMode ()) {
			$email =  trim ( $this->getConfigurationAdapter ()
				->getConfigurationValue ( 'paypal_email_testing' ) );
		} else {
			$email =  trim ( $this->getConfigurationAdapter ()
				->getConfigurationValue ( 'paypal_email_live' ) );
		}
		if(empty($email)) {
			throw new Exception(new Customweb_I18n_LocalizableString("Email address not configured"));
		}
		return $email;
	}

	public function getUrlPaypal() {
		if ($this->isTestMode ()) {
			return "https://www.sandbox.paypal.com/";
		} else {
			return "https://www.paypal.com/";
		}
	}

	public function getApiUrlPaypal() {
		if ($this->isTestMode ()) {
			return "https://api-3t.sandbox.paypal.com/nvp";
		} else {
			return "https://api-3t.paypal.com/nvp";
		}
	}

	public function getClassicUsername() {
		$username = null;
		if ($this->isTestMode ()) {
			$username = trim ( $this->getConfigurationAdapter ()
				->getConfigurationValue ( 'paypal_classic_username_test' ) );
		} else {
			$username = trim ( $this->getConfigurationAdapter ()
				->getConfigurationValue ( 'paypal_classic_username_live' ) );
		}
		if(empty($username)) {
			throw new Exception(new Customweb_I18n_LocalizableString("Classic username not configured"));
		}
		return $username;
	}

	public function getClassicPassword() {
		$password = null;
		if ($this->isTestMode ()) {
			$password = trim ( $this->getConfigurationAdapter ()
				->getConfigurationValue ( 'paypal_classic_password_test' ) );
		} else {
			$password =  trim ( $this->getConfigurationAdapter ()
				->getConfigurationValue ( 'paypal_classic_password_live' ) );
		}
		if(empty($password)) {
			throw new Exception(new Customweb_I18n_LocalizableString("Classic password not configured"));
		}
		return $password;
		
	}

	public function getClassicSignature() {
		$signature = null;
		if ($this->isTestMode ()) {
			$signature = trim ( $this->getConfigurationAdapter ()
				->getConfigurationValue ( 'paypal_classic_signature_test' ) );
		} else {
			$signature = trim ( $this->getConfigurationAdapter ()
				->getConfigurationValue ( 'paypal_classic_signature_live' ) );
		}
		if(empty($signature)) {
			throw new Exception(new Customweb_I18n_LocalizableString("Classic signature not configured"));
		}
		return $signature;
	}

	/**
	 * If a transaction has no seller protection, this transaction may be marked as uncertain.
	 *
	 *
	 * @return boolean
	 */
	public function isMarkTransactionAsUncertain() {
		if (strtolower ( $this->getConfigurationAdapter ()
			->getConfigurationValue ( 'mark_uncertain_transactions' ) ) == "on") {
			return true;
		} else {
			return false;
		}
	}

	public function getOrderIdSchema(){
		return $this->getConfigurationAdapter()->getConfigurationValue('order_id_schema');
	}
	
	/**
	 *
	 * @return Customweb_Payment_IConfigurationAdapter
	 */
	public function getConfigurationAdapter() {
		return $this->configurationAdapter;
	}
	
	/**
	 * @return string
	 */
	public function getEncodingCharsetName() {
		if ($this->getConfigurationAdapter()->existsConfiguration('encoding')) {
			return $this->getConfigurationAdapter()->getConfigurationValue('encoding');
		}
		else {
			return 'windows-1252';
		}
	}
	
	/**
	 * @return Customweb_Core_Charset
	 */
	public function getEncodingCharset() {
		return Customweb_Core_Charset::forName($this->getEncodingCharsetName());
	}
	
	public function getCheckoutType(){
		return $this->getConfigurationAdapter()->getConfigurationValue('checkout_type');
	}
	
	public function isTaxSendActive(){
		return $this->getConfigurationAdapter()->getConfigurationValue('send_taxes') == 'yes';
	}
}
