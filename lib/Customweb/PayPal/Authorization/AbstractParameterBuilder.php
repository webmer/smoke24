<?php

//require_once 'Customweb/PayPal/Util.php';
//require_once 'Customweb/Payment/Util.php';
//require_once 'Customweb/PayPal/Container.php';
//require_once 'Customweb/Payment/Authorization/ITransactionContext.php';



/**
 *
 * @author Thomas Brenner
 *
 */
class Customweb_PayPal_Authorization_AbstractParameterBuilder {
	private $container;
	private $transaction;

	public function __construct(Customweb_DependencyInjection_IContainer $container, Customweb_PayPal_Authorization_Transaction $transaction){
		$this->container = new Customweb_PayPal_Container($container);
		$this->transaction = $transaction;
	}

	/**
	 *
	 * @return Customweb_PayPal_Authorization_Transaction
	 */
	public function getTransaction(){
		return $this->transaction;
	}

	/**
	 *
	 * @return Customweb_Payment_Authorization_IOrderContext
	 */
	public function getOrderContext(){
		return $this->getTransactionContext()->getOrderContext();
	}

	/**
	 *
	 * @return Customweb_Payment_Authorization_ITransactionContext
	 */
	public function getTransactionContext(){
		return $this->transaction->getTransactionContext();
	}

	/**
	 *
	 * @return Customweb_PayPal_Configuration
	 */
	protected function getConfiguration(){
		return $this->getContainer()->getConfiguration();
	}

	protected function getContainer(){
		return $this->container;
	}

	protected function getPaymentAction(){
		if (Customweb_PayPal_Util::isZeroCheckout($this->getTransactionContext())) {
			$paymentAction = "authorization";
		}
		else {
			if ($this->getTransactionContext()->getCapturingMode() === null) {
				$capturingMode = $this->getOrderContext()->getPaymentMethod()->getPaymentMethodConfigurationValue('capturing');
				if ($capturingMode == 'order') {
					$paymentAction = "order";
				}
				else if ($capturingMode == 'authorization') {
					$paymentAction = "authorization";
				}
				else {
					$paymentAction = "sale";
				}
			}
			else {
				if ($this->getTransactionContext()->getCapturingMode() == Customweb_Payment_Authorization_ITransactionContext::CAPTURING_MODE_DEFERRED) {
					$paymentAction = "authorization";
				}
				else {
					$paymentAction = "sale";
				}
			}
		}
		$this->getTransaction()->setAuthorizationType($paymentAction);
		return $paymentAction;
	}

	protected function getNotificationUrl(){
		$notificationUrl = $this->container->getBean('Customweb_Payment_Endpoint_IAdapter')->getUrl('endpoint', 'process',
				array(
					'cw_transaction_id' => $this->getTransaction()->getExternalTransactionId(),
				));
		return $notificationUrl;
	}

	protected function getCancelUrl(){
		$securityHash = $this->getTransaction()->getSecuritySignature('endpoint/cancel');
		$cancelUrl = $this->container->getBean('Customweb_Payment_Endpoint_IAdapter')->getUrl('endpoint', 'cancel', 
				array(
					'cw_transaction_id' => $this->getTransaction()->getExternalTransactionId(),
					'cw_hash' => $securityHash 
				));
		return $cancelUrl;
	}

	protected function getBuildNotationCode(){
		return 'Customweb_EC_MAG';
	}

	protected function getInvoiceNumber(){
		return Customweb_Payment_Util::applyOrderSchema($this->getConfiguration()->getOrderIdSchema(), 
				$this->getTransaction()->getExternalTransactionId(), 127);
	}
}