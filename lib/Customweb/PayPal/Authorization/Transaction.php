<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Payment/Authorization/DefaultTransaction.php';
//require_once 'Customweb/PayPal/Util.php';
//require_once 'Customweb/Util/Url.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/PayPal/Authorization/TransactionCapture.php';



/**
 *
 * @author Thomas Brenner
 *
 *
 */
class Customweb_PayPal_Authorization_Transaction extends Customweb_Payment_Authorization_DefaultTransaction {
	private $payerId;
	private $transaction_id;
	private $authorizationType;
	private $sellerprotection;
	private $token = null;
	private $billingAgreementId;
	private $notificationUrl;
	private $statusECheck;

	public function setAuthorizationType($authorizationType){
		$this->authorizationType = $authorizationType;
	}

	
	public function addAuthorizationParameters(array $param) {
		$authParams = $this->getAuthorizationParameters();
		foreach($params as $key => $value) {
			$authParams[$key] = $value;
		}
		$this->setAuthorizationParameters($authParams);
	}
	
	public function getAuthorizationType(){
		return $this->authorizationType;
	}

	public function getNotificationUrl(){
		return Customweb_Util_Url::appendParameters($this->notificationUrl, array(
			'cw_transaction_id' => $this->getExternalTransactionId() 
		));
	}

	public function setSellerProtection($sellerprotection){
		$this->sellerprotection = $sellerprotection;
	}

	public function getSellerProtection(){
		return $this->sellerprotection;
	}

	public function getPayerId(){
		return $this->payerId;
	}

	public function setPayerId($payer_id){
		$this->payerId = $payer_id;
	}

	protected function buildNewCaptureObject($captureId, $amount, $status = NULL){
		return new Customweb_PayPal_Authorization_TransactionCapture($captureId, $amount, $status);
	}

	protected function getTransactionSpecificLabels(){
		return array(
			'seller_protection' => array(
				'label' => Customweb_I18n_Translation::__('Seller Protection'),
				'value' => $this->getSellerProtection() 
			) 
		);
	}

	public function getToken(){
		return $this->token;
	}

	public function setToken($token){
		$this->token = $token;
	}

	public function setBillingAgreementId($id){
		$this->billingAgreementId = $id;
	}

	public function getBillingAgreementId(){
		return $this->billingAgreementId;
	}

	protected function getCustomOrderStatusSettingKey($statusKey){
		$method = $this->getPaymentMethod();
		if ($this->getStatusECheck() == 'Completed') {
			if ($method->existsPaymentMethodConfigurationValue('status_success_after_uncertain')) {
				$updateSuccess = $method->getPaymentMethodConfigurationValue('status_success_after_uncertain');
				if ($updateSuccess != 'no_status_change' && $updateSuccess != 'none') {
					$statusKey = 'status_success_after_uncertain';
				}
			}
		}
		else if ($this->getStatusECheck() == 'Refused') {
			if ($method->existsPaymentMethodConfigurationValue('status_refused_after_uncertain')) {
				$updateRefused = $method->getPaymentMethodConfigurationValue('status_refused_after_uncertain');
				if ($updateRefused != 'no_status_change' && $updateRefused != 'none') {
					$statusKey = 'status_refused_after_uncertain';
				}
			}
		}
		
		return $statusKey;
	}

	public function getStatusECheck(){
		return $this->statusECheck;
	}

	public function setStatusECheck($statusECheck){
		$this->statusECheck = $statusECheck;
		return $this;
	}
	
	public function isPartialRefundPossible() {
		if(Customweb_PayPal_Util::isZeroCheckout($this->getTransactionContext())) {
			return false;
		}
		return parent::isPartialRefundPossible();
	}
}