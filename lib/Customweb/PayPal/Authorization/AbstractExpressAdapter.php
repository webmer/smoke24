<?php

//require_once 'Customweb/Core/Http/Request.php';
//require_once 'Customweb/Core/Http/Client/Factory.php';
//require_once 'Customweb/PayPal/Authorization/AbstractAdapter.php';
//require_once 'Customweb/PayPal/Util.php';
//require_once 'Customweb/Util/Currency.php';



/**
 *
 * @author
 *
 */
abstract class Customweb_PayPal_Authorization_AbstractExpressAdapter extends Customweb_PayPal_Authorization_AbstractAdapter {

	abstract protected function getParameterBuilder(Customweb_Payment_Authorization_ITransaction $transaction);

	protected function doReferencePaymentRequest(Customweb_PayPal_Authorization_Transaction $transaction){
		$builder = $this->getParameterBuilder($transaction);
		
		$request = new Customweb_Core_Http_Request($this->getConfiguration()->getApiUrlPaypal());
		$request->setMethod("POST");
		$request->setBody($builder->buildPerformReferencePaymentRequest());
		
		$client = Customweb_Core_Http_Client_Factory::createClient();
		$response = $client->send($request);
		
		return $response->getParsedBody();
	}

	protected function validateInformation(Customweb_Payment_Authorization_ITransaction $transaction, array $parameters){
		if (!($parameters['ACK'] == "Success" || $parameters['ACK'] == 'SuccessWithWarning')) {
			return 0;
		}
		if (Customweb_Util_Currency::compareAmount($parameters['AMT'], 
				Customweb_PayPal_Util::getCheckoutAmount($transaction->getTransactionContext()), 
				$transaction->getTransactionContext()->getOrderContext()->getCurrencyCode()) != 0) {
			return 0;
		}
		if ($parameters['CURRENCYCODE'] != $transaction->getTransactionContext()->getOrderContext()->getCurrencyCode()) {
			return 0;
		}
		if ($parameters['BILLINGAGREEMENTID'] != $transaction->getBillingAgreementId()) {
			return 0;
		}
		return 1;
	}
}