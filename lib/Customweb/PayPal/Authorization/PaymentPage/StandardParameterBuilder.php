<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */
//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/PayPal/Util.php';
//require_once 'Customweb/Payment/Authorization/IInvoiceItem.php';
//require_once 'Customweb/Util/Currency.php';
//require_once 'Customweb/Util/Invoice.php';
//require_once 'Customweb/PayPal/Authorization/AbstractParameterBuilder.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Core/Charset.php';



/**
 *
 * @author Nico Eigenmann
 *
 */
class Customweb_PayPal_Authorization_PaymentPage_StandardParameterBuilder extends Customweb_PayPal_Authorization_AbstractParameterBuilder {

	public function buildParameters(){
		$parameters = array_merge($this->getBaseParameters(), $this->getAddressParameters(), $this->getLineItems(), $this->getAmountParameters(), $this->getAuthorizationType());
		
		$targetCharset = $this->getConfiguration()->getEncodingCharset();
		$utf8 = Customweb_Core_Charset::forName("UTF-8");
		foreach ($parameters as $key => $value) {
			$parameters[$key] = Customweb_Core_Charset::convert($value, $utf8, $targetCharset);
		}
		
		return $parameters;
	}

	protected function getBaseParameters(){
		return array(
			'business' => $this->getConfiguration()->getEmailAddress(),
			'cmd' => "_cart",
			'upload' => "1",
			'no_note' => "1",
			'lc' => Customweb_PayPal_Util::getCleanLanguageCode($this->getOrderContext()->getLanguage()),
			'currency_code' => $this->getOrderContext()->getCurrencyCode(),
			'email' => Customweb_Util_String::substrUtf8($this->getOrderContext()->getShippingEMailAddress(), 0, 127),
			'return' => Customweb_Util_String::substrUtf8($this->getTransaction()->getSuccessUrl(), 0, 1024),
			'cancel_return' => Customweb_Util_String::substrUtf8($this->getCancelUrl(), 0, 1024),
			'notify_url' => Customweb_Util_String::substrUtf8($this->getNotificationUrl(), 0, 255),
			'bn' => $this->getBuildNotationCode(),
			'invoice' => $this->getInvoiceNumber(),
		);
	}

	protected function getAuthorizationType(){
		$parameters = array();
		$paymentAction = $this->getPaymentAction();
		$parameters['paymentaction'] = $paymentAction;
		
		return $parameters;
	}

	protected function getAddressParameters(){
		$parameters = array(
			'first_name' => Customweb_Util_String::substrUtf8($this->getOrderContext()->getShippingFirstName(), 0, 32),
			'last_name' => Customweb_Util_String::substrUtf8($this->getOrderContext()->getShippingLastName(), 0, 32),
			'address1' => Customweb_Util_String::substrUtf8($this->getOrderContext()->getShippingStreet(), 0, 100),
			'city' => Customweb_Util_String::substrUtf8($this->getOrderContext()->getShippingCity(), 0, 40),
			'zip' => Customweb_Util_String::substrUtf8($this->getOrderContext()->getShippingPostCode(), 0, 32),
			'country' => Customweb_Util_String::substrUtf8($this->getOrderContext()->getShippingCountryIsoCode(), 0, 2) 
		);
		
		$state = $this->getOrderContext()->getShippingState();
		if (!empty($state)) {
			$parameters['state'] = $this->getOrderContext()->getShippingState();
		}
		
		return $parameters;
	}

	protected function getLineItems(){
		$loopcounter = 1;
		$parameters = array();
		
		$totalAmount = 0;
		if(Customweb_PayPal_Util::isZeroCheckout($this->getTransactionContext())){
			$parameters['item_name_1'] = Customweb_I18n_Translation::__('Validation Item');
			$parameters['quantity_1'] = 1;
			$parameters['amount_1'] = Customweb_Util_Currency::formatAmount(Customweb_PayPal_Util::getCheckoutAmount($this->getTransactionContext()), $this->getTransaction()->getCurrencyCode(), '.', '');
		}
		else {
			foreach ($this->getOrderContext()->getInvoiceItems() as $item) {
				if ($item->getQuantity() == 0) {
					continue;
				}
				if(!$this->getConfiguration()->isTaxSendActive()) {
					$amount = $item->getAmountIncludingTax() / $item->getQuantity();
				}
				else {
					$amount = $item->getAmountExcludingTax() / $item->getQuantity();
				}
				if ($item->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT) {
					$amount = $amount * -1;
				}
				
				// We ignore discounts. We calculate later the difference.
				if ($amount >= 0) {
					$parameters['item_name_' . $loopcounter] = Customweb_Util_String::substrUtf8($item->getName(), 0, 127);
					$parameters['quantity_' . $loopcounter] = number_format($item->getQuantity(), 0, "", "");
					$parameters['amount_' . $loopcounter] = Customweb_Util_Currency::formatAmount($amount, $this->getOrderContext()->getCurrencyCode(), 
							'.', '');
					$totalAmount = $totalAmount + $parameters['amount_' . $loopcounter] * $parameters['quantity_' . $loopcounter];
					$loopcounter++;
				}
			}
			
			// Add fix for rounding errors.
			if(!$this->getConfiguration()->isTaxSendActive()) {
				$actualTotal = Customweb_Util_Invoice::getTotalAmountIncludingTax($this->getOrderContext()->getInvoiceItems());
			}
			else {
				$actualTotal = Customweb_Util_Invoice::getTotalAmountExcludingTax($this->getOrderContext()->getInvoiceItems());
			}
			$discount = 0;
			$amountComparison = Customweb_Util_Currency::compareAmount($totalAmount, $actualTotal, 
					$this->getOrderContext()->getCurrencyCode());
			if ($amountComparison > 0) {
				$discount += Customweb_Util_Currency::formatAmount($totalAmount - $actualTotal, 
						$this->getOrderContext()->getCurrencyCode(), '.', '');
			}
			else if ($amountComparison < 0) {
				$parameters['item_name_' . $loopcounter] = Customweb_I18n_Translation::__("Rounding Adjustments");
				$parameters['quantity_' . $loopcounter] = 1;
				$parameters['amount_' . $loopcounter] = Customweb_Util_Currency::formatAmount($actualTotal - $totalAmount, $this->getOrderContext()->getCurrencyCode(), '.', '');
				$loopcounter++;
			}
			if ($discount > 0) {
				$parameters['discount_amount_cart'] = $discount;
			}
		}
		
		return $parameters;
	}
	
	protected function getAmountParameters(){
	
		if (Customweb_PayPal_Util::isZeroCheckout($this->getTransactionContext())) {
			$itemAmount = Customweb_Util_Currency::formatAmount(Customweb_PayPal_Util::getCheckoutAmount($this->getTransactionContext()),
					$this->getTransaction()->getCurrencyCode(), '.', '');
			$totalAmount = Customweb_Util_Currency::formatAmount(
					Customweb_PayPal_Util::getCheckoutAmount($this->getTransactionContext()), $this->getTransaction()->getCurrencyCode(),
					'.', '');
			$taxAmount = Customweb_Util_Currency::formatAmount(0, $this->getTransaction()->getCurrencyCode(), '.', '');
		}
		else {
			$itemAmount = Customweb_Util_Currency::formatAmount(Customweb_Util_Invoice::getTotalAmountExcludingTax($this->getOrderContext()->getInvoiceItems()),
					$this->getTransaction()->getCurrencyCode(), '.', '');
			$totalAmount = Customweb_Util_Currency::formatAmount($this->getTransaction()->getAuthorizationAmount(), $this->getTransaction()->getCurrencyCode(),
					'.', '');
			$taxAmount = Customweb_Util_Currency::formatAmount($totalAmount- $itemAmount, $this->getTransaction()->getCurrencyCode(),
					'.', '');
		}
		if($this->getConfiguration()->isTaxSendActive()) {
	
			return array(
				'tax_cart' => $taxAmount
			);
		}
		else {
			return array(
			);
		}
	
	}
}