<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */
//require_once 'Customweb/Core/Http/Request.php';
//require_once 'Customweb/Core/Http/Client/Factory.php';
//require_once 'Customweb/Payment/Exception/PaymentErrorException.php';
//require_once 'Customweb/Core/Http/Response.php';
//require_once 'Customweb/PayPal/Util.php';
//require_once 'Customweb/PayPal/Authorization/PaymentPage/ExpressParameterBuilder.php';
//require_once 'Customweb/Payment/Authorization/ErrorMessage.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/PayPal/Authorization/AbstractExpressAdapter.php';



/**
 */
class Customweb_PayPal_Authorization_PaymentPage_ExpressAdapter extends Customweb_PayPal_Authorization_AbstractExpressAdapter {

	protected function getParameterBuilder(Customweb_Payment_Authorization_ITransaction $transaction){
		return new Customweb_PayPal_Authorization_PaymentPage_ExpressParameterBuilder($this->getContainer(), $transaction);
	}

	private function buildUrl(Customweb_Payment_Authorization_ITransaction $transaction){
		if ($transaction->getToken() != null) {
			return $this->getConfiguration()->getUrlPaypal() . 'cgi-bin/webscr?cmd=_express-checkout&token=' . $transaction->getToken() .
					 '&useraction=commit';
		}
		$parameters = $this->doExpressCheckoutRequest($transaction);
		if ($parameters['ACK'] != "Success") {
			$error = 'No error provided by PayPal.';
			if (isset($parameters['L_LONGMESSAGE0'])) {
				$error = $parameters['L_LONGMESSAGE0'];
			}
			elseif (isset($parameters['L_SHORTMESSAGE0'])) {
				$error = $parameters['L_SHORTMESSAGE0'];
			}
			throw new Customweb_Payment_Exception_PaymentErrorException(
					new Customweb_Payment_Authorization_ErrorMessage(
							Customweb_I18n_Translation::__("Your payment was not successful, please contact the merchant."), 
							Customweb_I18n_Translation::__("The recurring payment could not be initialized. Error: @error", 
									array(
										'@error' => $error 
									))));
		}
		$transaction->setToken($parameters['TOKEN']);
		$url = $this->getConfiguration()->getUrlPaypal() . 'cgi-bin/webscr?cmd=_express-checkout&token=' . $transaction->getToken() .
				 '&useraction=commit';
		return $url;
	}

	public function getFormActionUrl(Customweb_Payment_Authorization_ITransaction $transaction, array $formData){
		return $this->buildUrl($transaction);
	}

	public function getParameters(Customweb_Payment_Authorization_ITransaction $transaction, array $formData){
		return array();
	}

	public function getRedirectionUrl(Customweb_Payment_Authorization_ITransaction $transaction, array $formData){
		return $this->buildUrl($transaction);
	}

	public function processAuthorization(Customweb_Payment_Authorization_ITransaction $transaction, array $parameters){
		$referencePaymentResponse = array();
		
		if ($transaction->isAuthorizationFailed() || ($transaction->isAuthorized() && $transaction->getStatusECheck() != 'pending')) {
			return new Customweb_Core_Http_Response();
		}
		
		if (isset($parameters['payment_type']) && $parameters['payment_type'] == "echeck" && isset($referencePaymentResponse['PAYMENTSTATUS']) &&
				 $referencePaymentResponse['PAYMENTSTATUS'] != 'Pending') {
			return $this->handleEcheck($transaction, $parameters);
		}
		
		try {
			$checkoutDetailsAnswer = $this->doCheckoutDetailsRequest($transaction);
			if (($checkoutDetailsAnswer['ACK'] != "Success")) {
				throw new Customweb_Payment_Exception_PaymentErrorException(
						new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__('Payment was not successful.'), 
								Customweb_I18n_Translation::__('Error retrieving the checkout details.')));
			}
			
			$transaction->setPayerId($checkoutDetailsAnswer['PAYERID']);
			$paymentAnswer = $this->doCheckoutPaymentRequest($transaction);
			if ($paymentAnswer['ACK'] == 'Failure' || $paymentAnswer['ACK'] == 'FailureWithWarning') {
				$error = 'No error provided by PayPal.';
				if (isset($paymentAnswer['PAYMENTINFO_0_LONGMESSAGE'])) {
					$error = $paymentAnswer['PAYMENTINFO_0_LONGMESSAGE'];
				}
				elseif (isset($paymentAnswer['L_LONGMESSAGE0'])) {
					$error = $paymentAnswer['L_LONGMESSAGE0'];
				}
				elseif (isset($paymentAnswer['L_SHORTMESSAGE0'])) {
					$error = $paymentAnswer['L_SHORTMESSAGE0'];
				}
				throw new Customweb_Payment_Exception_PaymentErrorException(
						new Customweb_Payment_Authorization_ErrorMessage(
								Customweb_I18n_Translation::__("Your payment was not successful, please contact the merchant."), 
								Customweb_I18n_Translation::__("The payment could not be initialized. Error: @error", 
										array(
											'@error' => $error 
										))));
			}
			if (!($paymentAnswer['ACK'] == "Success" || $paymentAnswer['ACK'] == 'SuccessWithWarning')) {
				throw new Customweb_Payment_Exception_PaymentErrorException(
						new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__('Payment was not successful.'), 
								Customweb_I18n_Translation::__('The payment request has state: @answer', array('@answer' => $paymentAnswer['ACK']))));
			}
			if (isset($paymentAnswer['BILLINGAGREEMENTID'])) {
				$transaction->setBillingAgreementId($paymentAnswer['BILLINGAGREEMENTID']);
			}
			$transaction->setPaymentId($paymentAnswer['PAYMENTINFO_0_TRANSACTIONID']);
			$transaction->authorize(Customweb_I18n_Translation::__('Customer sucessfully returned from the PayPal payment page.'));
			if (isset($paymentAnswer['PAYMENTINFO_0_PAYMENTTYPE']) && $paymentAnswer['PAYMENTINFO_0_PAYMENTTYPE'] == 'echeck' &&
					 isset($paymentAnswer['PAYMENTINFO_0_PAYMENTSTATUS']) && $paymentAnswer['PAYMENTINFO_0_PAYMENTSTATUS'] == 'Pending') {
				$transaction->setAuthorizationUncertain();
				$transaction->setStatusECheck('pending');
			}
			if ($transaction->getAuthorizationType() != 'order') {
				$transaction->setSellerProtection($paymentAnswer['PAYMENTINFO_0_PROTECTIONELIGIBILITY']);
				
				if ($this->getConfiguration()->isMarkTransactionAsUncertain()) {
					if (!strcmp($transaction->getSellerProtection(), 'Ineligible')) {
						$transaction->setAuthorizationUncertain();
					}
				}
			}
			if (Customweb_PayPal_Util::isZeroCheckout($transaction->getTransactionContext())) {
				//Mark all "zero" Checkouts as captured, so they can not be captured later
				$transaction->partialCaptureByLineItems($transaction->getTransactionContext()->getOrderContext()->getInvoiceItems());
			}
			//direct capture of sale
			else if ($transaction->getAuthorizationType() == 'sale') {
				$item = $transaction->capture();
				$item->setCaptureId($transaction->getPaymentId());
			}
			if (isset($paymentAnswer['REDIRECTREQUIRED']) && $paymentAnswer['REDIRECTREQUIRED'] == 'true') {
				$url = $this->getConfiguration()->getUrlPaypal() . 'cgi-bin/webscr?cmd=_express-checkout&token=' . $transaction->getToken();
				return ("redirect: " . $url);
			}
			//}
		}
		catch (Exception $e) {
			$transaction->setAuthorizationFailed($e->getMessage());
			return ("redirect: " . $transaction->getFailedUrl());
		}
		return ("redirect: " . $transaction->getSuccessUrl());
	}

	protected function doExpressCheckoutRequest(Customweb_PayPal_Authorization_Transaction $transaction){
		$builder = $this->getParameterBuilder($transaction);
		
		$request = new Customweb_Core_Http_Request($this->getConfiguration()->getApiUrlPaypal());
		$request->setMethod("POST");
		$request->setBody($builder->buildExpressCheckoutRequest());
		
		$client = Customweb_Core_Http_Client_Factory::createClient();
		$response = $client->send($request);
		
		return $response->getParsedBody();
	}

	protected function doCheckoutDetailsRequest(Customweb_PayPal_Authorization_Transaction $transaction){
		$builder = $this->getParameterBuilder($transaction);
		
		$request = new Customweb_Core_Http_Request($this->getConfiguration()->getApiUrlPaypal());
		$request->setMethod("POST");
		$request->setBody($builder->buildCheckoutDetailsRequest());
		
		$client = Customweb_Core_Http_Client_Factory::createClient();
		$response = $client->send($request);
		
		return $response->getParsedBody();
	}

	protected function doCheckoutPaymentRequest(Customweb_PayPal_Authorization_Transaction $transaction){
		$builder = $this->getParameterBuilder($transaction);
		
		$request = new Customweb_Core_Http_Request($this->getConfiguration()->getApiUrlPaypal());
		$request->setMethod("POST");
		$request->setBody($builder->buildCheckoutPaymentRequest());
		
		$client = Customweb_Core_Http_Client_Factory::createClient();
		$response = $client->send($request);
		
		return $response->getParsedBody();
	}

	private function handleEcheck(Customweb_PayPal_Authorization_Transaction $transaction, array $parameters){
		if (isset($parameters['payment_status'])) {
			switch ($parameters['payment_status']) {
				case 'Completed':
					$transaction->setAuthorizationUncertain(false);
					$transaction->setStatusECheck('Completed');
					return new Customweb_Core_Http_Response();
					break;
				case 'Denied':
					$transaction->setUncertainTransactionFinallyDeclined();
					$transaction->setStatusECheck('Refused');
					return new Customweb_Core_Http_Response();
					break;
			}
		}
	}

}