<?php

//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/PayPal/Authorization/AbstractExpressParameterBuilder.php';
//require_once 'Customweb/Core/Charset.php';

class Customweb_PayPal_Authorization_PaymentPage_ExpressParameterBuilder extends Customweb_PayPal_Authorization_AbstractExpressParameterBuilder {

	public function buildCheckoutDetailsRequest(){
		$parameters = array_merge($this->getSecurityParameters(), $this->getShippingAddressParameters());
		$parameters['METHOD'] = 'GetExpressCheckoutDetails';
		$parameters['TOKEN'] = $this->getTransaction()->getToken();
		
		$targetCharset = $this->getConfiguration()->getEncodingCharset();
		$utf8 = Customweb_Core_Charset::forName("UTF-8");
		foreach ($parameters as $key => $value) {
			$parameters[$key] = Customweb_Core_Charset::convert($value, $utf8, $targetCharset);
		}
		
		return $parameters;
	}

	public function buildExpressCheckoutRequest(){
		$recurring = array();
		if ($this->getTransactionContext()->createRecurringAlias()) {
			
			$recurring = $this->getBillingAgreementDescription();
			$recurring['L_BILLINGTYPE0'] = 'MerchantInitiatedBilling';
		}
		
		$parameters = array_merge($this->getSecurityParameters(), $this->getShippingAddressParameters(), $this->getLineItems('L_PAYMENTREQUEST_0_'), 
				$this->getAmountParameters('PAYMENTREQUEST_0_'), $this->getResponseUrls(), $recurring, 
				array(
					'METHOD' => 'SetExpressCheckout',
					'PAYMENTREQUEST_0_PAYMENTACTION' => $this->getPaymentAction(),
					'PAYMENTREQUEST_0_CURRENCYCODE' => $this->getTransaction()->getCurrencyCode(),
					'PAYMENTREQUEST_0_NOTIFYURL' => $this->getNotificationUrl() 
				));
		
		$targetCharset = $this->getConfiguration()->getEncodingCharset();
		$utf8 = Customweb_Core_Charset::forName("UTF-8");
		foreach ($parameters as $key => $value) {
			$parameters[$key] = Customweb_Core_Charset::convert($value, $utf8, $targetCharset);
		}
		return $parameters;
	}

	public function buildCheckoutPaymentRequest(){
		$parameters = array_merge($this->getSecurityParameters(), $this->getShippingAddressParameters(), $this->getLineItems('L_PAYMENTREQUEST_0_'), 
				$this->getAmountParameters('PAYMENTREQUEST_0_'), 
				array(
					'METHOD' => 'DoExpressCheckoutPayment',
					'TOKEN' => $this->getTransaction()->getToken(),
					'PAYERID' => $this->getTransaction()->getPayerId(),
					'PAYMENTREQUEST_0_CURRENCYCODE' => $this->getTransaction()->getCurrencyCode(),
					'PAYMENTREQUEST_0_NOTIFYURL' => $this->getNotificationUrl(),
					'PAYMENTREQUEST_0_PAYMENTACTION' => $this->getTransaction()->getAuthorizationType(),
					'PAYMENTREQUEST_0_INVNUM' => $this->getInvoiceNumber(),
					'BUTTONSOURCE' => $this->getBuildNotationCode() 
				));
		
		$targetCharset = $this->getConfiguration()->getEncodingCharset();
		$utf8 = Customweb_Core_Charset::forName("UTF-8");
		foreach ($parameters as $key => $value) {
			$parameters[$key] = Customweb_Core_Charset::convert($value, $utf8, $targetCharset);
		}
		return $parameters;
	}

	protected function getShippingAddressParameters(){
		$shipping = array();
		$shipping['PAYMENTREQUEST_0_SHIPTONAME'] = Customweb_Util_String::substrUtf8(
				$this->getOrderContext()->getShippingFirstName() . ' ' . $this->getOrderContext()->getShippingLastName(), 0, 128);
		$shipping['PAYMENTREQUEST_0_SHIPTOSTREET'] = Customweb_Util_String::substrUtf8($this->getOrderContext()->getShippingStreet(), 0, 100);
		$shipping['PAYMENTREQUEST_0_SHIPTOCITY'] = Customweb_Util_String::substrUtf8($this->getOrderContext()->getShippingCity(), 0, 40);
		$shipping['PAYMENTREQUEST_0_SHIPTOZIP'] = Customweb_Util_String::substrUtf8($this->getOrderContext()->getShippingPostCode(), 0, 20);
		$shipping['PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE'] = $this->getOrderContext()->getShippingCountryIsoCode();
		$state = $this->getOrderContext()->getShippingState();
		if (!empty($state)) {
			$shipping['PAYMENTREQUEST_0_SHIPTOSTATE'] = $state;
		}
		return $shipping;
	}

	protected function getBillingAgreementDescription(){
		$orderId = $this->getTransactionContext()->getOrderId();
		if (empty($orderId)) {
			$orderId = $this->getTransaction()->getExternalTransactionId();
		}
		$description = Customweb_Util_String::substrUtf8($orderId, 0, 64);
		return array(
			'L_BILLINGAGREEMENTDESCRIPTION0' => $description 
		);
	}
}
