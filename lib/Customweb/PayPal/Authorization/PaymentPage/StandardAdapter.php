<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */
//require_once 'Customweb/Core/Http/Request.php';
//require_once 'Customweb/Payment/Exception/PaymentErrorException.php';
//require_once 'Customweb/Core/Http/IRequest.php';
//require_once 'Customweb/Core/Http/Response.php';
//require_once 'Customweb/PayPal/Authorization/PaymentPage/StandardParameterBuilder.php';
//require_once 'Customweb/PayPal/AbstractAdapter.php';
//require_once 'Customweb/Core/Charset.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Core/Http/Client/Factory.php';
//require_once 'Customweb/Util/Url.php';
//require_once 'Customweb/PayPal/Util.php';
//require_once 'Customweb/Util/Currency.php';
//require_once 'Customweb/Payment/Authorization/ErrorMessage.php';



/**
 *
 * @author Nico Eigenmann
 *
 */
class Customweb_PayPal_Authorization_PaymentPage_StandardAdapter extends Customweb_PayPal_AbstractAdapter {

	public function getFormActionUrl(Customweb_Payment_Authorization_ITransaction $transaction, array $formData){
		return $this->getConfiguration()->getUrlPaypal() . 'cgi-bin/webscr';
	}

	public function getParameters(Customweb_Payment_Authorization_ITransaction $transaction, array $formData){
		$builder = new Customweb_PayPal_Authorization_PaymentPage_StandardParameterBuilder($this->getContainer(), $transaction);
		return $builder->buildParameters();
	}

	public function getRedirectionUrl(Customweb_Payment_Authorization_ITransaction $transaction, array $formData){
		$builder = new Customweb_PayPal_Authorization_PaymentPage_StandardParameterBuilder($this->getContainer(), $transaction);
		return Customweb_Util_Url::appendParameters($this->getFormActionUrl($transaction, $formData), $this->getParameters($transaction, $formData));
	}

	public function processAuthorization(Customweb_PayPal_Authorization_Transaction $transaction, array $parameters){
		
		// Verification of Paypal link:
		$confirmNotificationParameters = array(
			'cmd' => '_notify-validate' 
		);
		$confirmNotificationParameters = array_merge($confirmNotificationParameters, $this->cleanUpNotificationParameters($transaction, $parameters));
		
		$verificationUrl = $this->getConfiguration()->getUrlPaypal() . 'cgi-bin/webscr';
		
		$request = new Customweb_Core_Http_Request($verificationUrl);
		$request->setMethod(Customweb_Core_Http_IRequest::METHOD_POST);
		$request->setBody($confirmNotificationParameters);
		
		$client = Customweb_Core_Http_Client_Factory::createClient();
		
		//If transaction failed or already authorized, we acknowledge the IPN and stop
		if ($transaction->isAuthorizationFailed() || ($transaction->isAuthorized() && $transaction->getStatusECheck() != 'pending')) {
			try {
				$response = $client->send($request);
			}
			catch (Exception $e) {
			}
			return new Customweb_Core_Http_Response();
		}
		
		try {
			$response = $client->send($request);
		}
		catch (Exception $e) {
			$transaction->setAuthorizationFailed($e->getMessage());
			return new Customweb_Core_Http_Response();
		}
		
		$answer = $response->getBody();
		try {
			
			if (stristr($answer, "VERIFIED") !== false) {
				
				// Convert to UTF-8
				$sourceCharset = $this->getConfiguration()->getEncodingCharset();
				$utf8 = Customweb_Core_Charset::forName("UTF-8");
				foreach ($parameters as $key => $value) {
					$parameters[$key] = Customweb_Core_Charset::convert($value, $sourceCharset, $utf8);
				}
				
				if (isset($parameters['payment_type']) && $parameters['payment_type'] == 'echeck') {
					switch ($parameters['payment_status']) {
						case 'Pending':
							$transaction->setStatusECheck("pending");
							$transaction->setAuthorizationUncertain();
						case 'Completed':
							$transaction->setAuthorizationUncertain(false);
							$transaction->setStatusECheck('Completed');
							return new Customweb_Core_Http_Response();
							break;
						case 'Denied':
							$transaction->setStatusECheck('Refused');
							$transaction->setUncertainTransactionFinallyDeclined();
							return new Customweb_Core_Http_Response();
							break;
					}
				}
				
				$transaction->setAuthorizationParameters($parameters);
				
				// Check the most significant values:
				// Check Seller id
				if (!$parameters['receiver_id'] == $this->getConfiguration()->getEmailAddress()) {
					throw new Customweb_Payment_Exception_PaymentErrorException(
							new Customweb_Payment_Authorization_ErrorMessage(
									Customweb_I18n_Translation::__("Your payment was not successful, please contact the merchant."), 
									Customweb_I18n_Translation::__('Possible fraud detected. Receiver id was invalid.')));
				}
				// Check amount:
				$parameters['mc_gross'] = str_replace(',', '', $parameters['mc_gross']);
				if (Customweb_Util_Currency::compareAmount($parameters['mc_gross'], 
						Customweb_PayPal_Util::getCheckoutAmount($transaction->getTransactionContext()), $transaction->getCurrencyCode()) !=
						 0) {
					throw new Customweb_Payment_Exception_PaymentErrorException(
							new Customweb_Payment_Authorization_ErrorMessage(
									Customweb_I18n_Translation::__("Your payment was not successful, please contact the merchant."), 
									Customweb_I18n_Translation::__('Possible fraud detected. Total amount was invalid.')));
				}
				//Check currency
				if (!$parameters['mc_currency'] == $transaction->getTransactionContext()->getOrderContext()->getCurrencyCode()) {
					throw new Customweb_Payment_Exception_PaymentErrorException(
							new Customweb_Payment_Authorization_ErrorMessage(
									Customweb_I18n_Translation::__("Your payment was not successful, please contact the merchant."), 
									Customweb_I18n_Translation::__('Possible fraud detected. Currency was invalid.')));
				}
				
				//No Errors in Check authorize
				$transaction->setPaymentId($parameters['txn_id']);
				$transaction->authorize(Customweb_I18n_Translation::__('Customer sucessfully returned from the PayPal payment page.'));
				
				if ($transaction->getAuthorizationType() != 'order') {
					$transaction->setSellerProtection($parameters['protection_eligibility']);
					if ($this->getConfiguration()->isMarkTransactionAsUncertain()) {
						if (!strcmp($transaction->getSellerProtection(), 'Ineligible')) {
							$transaction->setAuthorizationUncertain();
						}
					}
				}
				if(Customweb_PayPal_Util::isZeroCheckout($transaction->getTransactionContext())) {
					//Mark all "zero" Checkouts as captured, so they can not be captured later  
					$transaction->partialCaptureByLineItems($transaction->getTransactionContext()->getOrderContext()->getInvoiceItems());
				}
				else if ($transaction->getAuthorizationType() == 'sale') {
					$item = $transaction->capture();
					$item->setCaptureId($parameters['txn_id']);
				}
			}
			else {
				throw new Customweb_Payment_Exception_PaymentErrorException(
						new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__("Payment was not successful."), 
								Customweb_I18n_Translation::__("The notification request could not be verified by PayPal. (Anwser was '!answer').", 
										array(
											'!answer' => $answer 
										))));
			}
		}
		catch (Exception $e) {
			$transaction->setAuthorizationFailed($e->getMessage());
		}
		return new Customweb_Core_Http_Response();
	}

	/**
	 * This function cleans up the array of parameters with exclusively PayPal-parameters
	 *
	 * @param Customweb_PayPal_Authorization_Transaction $transaction
	 * @param List of parameters to cleanup
	 * @return cleaned Array of Parameters
	 */
	protected function cleanUpNotificationParameters(Customweb_PayPal_Authorization_Transaction $transaction, $parameters){
		$cleaned = array();
		$customParameters = $transaction->getTransactionContext()->getCustomParameters();
		foreach ($parameters as $key => $value) {
			if (!isset($customParameters[$key])) {
				$cleaned[$key] = $value;
			}
		}
		return $cleaned;
	}
}