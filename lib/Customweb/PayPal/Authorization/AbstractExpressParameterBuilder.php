<?php

//require_once 'Customweb/Util/String.php';
//require_once 'Customweb/PayPal/Util.php';
//require_once 'Customweb/Payment/Authorization/IInvoiceItem.php';
//require_once 'Customweb/Util/Currency.php';
//require_once 'Customweb/Util/Invoice.php';
//require_once 'Customweb/PayPal/Authorization/AbstractParameterBuilder.php';
//require_once 'Customweb/Core/Charset.php';
//require_once 'Customweb/I18n/Translation.php';



/**
 *
 * @author Thomas Hunziker
 *
 * Parameter description: https://developer.paypal.com/webapps/developer/docs/classic/api/merchant/SetExpressCheckout_API_Operation_NVP/
 *
 *
 */
class Customweb_PayPal_Authorization_AbstractExpressParameterBuilder extends Customweb_PayPal_Authorization_AbstractParameterBuilder {

	protected function getLineItems($keyPrefix){
		$loopcounter = 0;
		$parameters = array();
		
		$totalAmount = 0;
		if(Customweb_PayPal_Util::isZeroCheckout($this->getTransactionContext())) {
			$parameters[$keyPrefix.'NAME0'] = Customweb_I18n_Translation::__('Validation Item');
			$parameters[$keyPrefix.'QTY0'] = 1;
			$parameters[$keyPrefix.'AMT0'] = Customweb_Util_Currency::formatAmount(Customweb_PayPal_Util::getCheckoutAmount($this->getTransactionContext()), $this->getTransaction()->getCurrencyCode(), '.', '');
			
		}
		else {
			foreach ($this->getOrderContext()->getInvoiceItems() as $item) {
				if ($item->getQuantity() == 0) {
					continue;
				}
				if(!$this->getConfiguration()->isTaxSendActive()) {
					$amount = $item->getAmountIncludingTax() / $item->getQuantity();
				}
				else {
					$amount = $item->getAmountExcludingTax() / $item->getQuantity();
				}
				if ($item->getType() == Customweb_Payment_Authorization_IInvoiceItem::TYPE_DISCOUNT) {
					$amount = $amount * -1;
				}
				$parameters[$keyPrefix.'NAME' . $loopcounter] = Customweb_Util_String::substrUtf8($item->getName(), 0, 127);
				$parameters[$keyPrefix.'QTY' . $loopcounter] = number_format($item->getQuantity(), 0, "", "");
				$parameters[$keyPrefix.'AMT' . $loopcounter] = Customweb_Util_Currency::formatAmount($amount, 
						$this->getTransaction()->getCurrencyCode(), '.', '');
				$totalAmount = $totalAmount + $parameters[$keyPrefix.'AMT' . $loopcounter] * $parameters[$keyPrefix.'QTY' . $loopcounter];
				$loopcounter++;
			}
			
			if(!$this->getConfiguration()->isTaxSendActive()) {
				$actualTotal = Customweb_Util_Invoice::getTotalAmountIncludingTax($this->getOrderContext()->getInvoiceItems());
			}
			else {
				$actualTotal = Customweb_Util_Invoice::getTotalAmountExcludingTax($this->getOrderContext()->getInvoiceItems());
			}
			if (Customweb_Util_Currency::compareAmount($actualTotal, $totalAmount, 
					$this->getTransaction()->getCurrencyCode()) !== 0) {
				$amount = $actualTotal - $totalAmount;
				$parameters[$keyPrefix.'NAME' . (string) $loopcounter] = Customweb_I18n_Translation::__("Rounding Adjustments");
				$parameters[$keyPrefix.'QTY' . $loopcounter] = 1;
				$parameters[$keyPrefix.'AMT' . $loopcounter] = Customweb_Util_Currency::formatAmount($amount, 
						$this->getTransaction()->getCurrencyCode(), '.', '');
				$loopcounter++;
			}
		}
		return $parameters;
	}
	
	protected function getAmountParameters($keyPrefix){
	
		if (Customweb_PayPal_Util::isZeroCheckout($this->getTransactionContext())) {
			$itemAmount = Customweb_Util_Currency::formatAmount(Customweb_PayPal_Util::getCheckoutAmount($this->getTransactionContext()),
					$this->getTransaction()->getCurrencyCode(), '.', '');
			$totalAmount = Customweb_Util_Currency::formatAmount(
					Customweb_PayPal_Util::getCheckoutAmount($this->getTransactionContext()), $this->getTransaction()->getCurrencyCode(),
					'.', '');
			$taxAmount = Customweb_Util_Currency::formatAmount(0, $this->getTransaction()->getCurrencyCode(), '.', '');
		}
		else {
			$itemAmount = Customweb_Util_Currency::formatAmount(Customweb_Util_Invoice::getTotalAmountExcludingTax($this->getOrderContext()->getInvoiceItems()),
					$this->getTransaction()->getCurrencyCode(), '.', '');
			$totalAmount = Customweb_Util_Currency::formatAmount($this->getTransaction()->getAuthorizationAmount(), $this->getTransaction()->getCurrencyCode(),
					'.', '');
			$taxAmount = Customweb_Util_Currency::formatAmount($totalAmount- $itemAmount, $this->getTransaction()->getCurrencyCode(),
					'.', '');
		}
		if($this->getConfiguration()->isTaxSendActive()) {
				
			return array(
				$keyPrefix.'ITEMAMT' => $itemAmount,
				$keyPrefix.'AMT' => $totalAmount,
				$keyPrefix.'TAXAMT' => $taxAmount
			);
		}
		else {
			return array(
				$keyPrefix.'ITEMAMT' => $totalAmount,
				$keyPrefix.'AMT' => $totalAmount,
			);
		}
	
	}

	public function buildPerformReferencePaymentRequest(){
		$parameters = array_merge($this->getSecurityParameters(), $this->getReferenceShippingAddressParameters(), $this->getLineItems('L_'), $this->getAmountParameters(''),
				array(
					'METHOD' => "DoReferenceTransaction",
					'REFERENCEID' => $this->getTransaction()->getBillingAgreementId(),
					'PAYMENTACTION' => $this->getPaymentAction(),
					'REQCONFIRMSHIPPING' => '1',
					'CURRENCYCODE' => $this->getTransaction()->getCurrencyCode(),
					'INVNUM' => $this->getInvoiceNumber(),
					'RECURRING' => 'Y' 
				));
		
		$targetCharset = $this->getConfiguration()->getEncodingCharset();
		$utf8 = Customweb_Core_Charset::forName("UTF-8");
		foreach ($parameters as $key => $value) {
			$parameters[$key] = Customweb_Core_Charset::convert($value, $utf8, $targetCharset);
		}
		
		return $parameters;
	}

	protected function getReferenceShippingAddressParameters(){
		$shipping = array();
		$shipping['SHIPTONAME'] = Customweb_Util_String::substrUtf8(
				$this->getOrderContext()->getShippingFirstName() . ' ' . $this->getOrderContext()->getShippingLastName(), 0, 128);
		$shipping['SHIPTOSTREET'] = Customweb_Util_String::substrUtf8($this->getOrderContext()->getShippingStreet(), 0, 100);
		$shipping['SHIPTOCITY'] = Customweb_Util_String::substrUtf8($this->getOrderContext()->getShippingCity(), 0, 40);
		$shipping['SHIPTOZIP'] = Customweb_Util_String::substrUtf8($this->getOrderContext()->getShippingPostCode(), 0, 20);
		$shipping['SHIPTOCOUNTRYCODE'] = $this->getOrderContext()->getShippingCountryIsoCode();
		$state = $this->getOrderContext()->getShippingState();
		if (!empty($state)) {
			$shipping['SHIPTOSTATE'] = $state;
		}
		return $shipping;
	}

	protected function getSecurityParameters(){
		return array(
			'PWD' => $this->getConfiguration()->getClassicPassword(),
			'USER' => $this->getConfiguration()->getClassicUsername(),
			'SIGNATURE' => $this->getConfiguration()->getClassicSignature(),
			'VERSION' => '119.0' 
		);
	}

	protected function getResponseUrls(){
		return array(
			'RETURNURL' => $this->getNotificationUrl(),
			'CANCELURL' => $this->getCancelUrl() 
		);
	}
}