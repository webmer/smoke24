<?php

//require_once 'Customweb/Payment/Authorization/Recurring/IAdapter.php';
//require_once 'Customweb/PayPal/Authorization/Transaction.php';
//require_once 'Customweb/Payment/Authorization/ErrorMessage.php';
//require_once 'Customweb/I18n/Translation.php';
//require_once 'Customweb/Payment/Exception/RecurringPaymentErrorException.php';
//require_once 'Customweb/PayPal/Authorization/Recurring/ParameterBuilder.php';
//require_once 'Customweb/PayPal/Authorization/AbstractExpressAdapter.php';



/**
 * @Bean
 */
class Customweb_PayPal_Authorization_Recurring_Adapter extends Customweb_PayPal_Authorization_AbstractExpressAdapter implements 
		Customweb_Payment_Authorization_Recurring_IAdapter {

			
	public function getAdapterPriority(){
		return 500;
	}
	
	public function getAuthorizationMethodName(){
		return self::AUTHORIZATION_METHOD_NAME;
	}

	
	/**
	 * This method creates a new recurring transaction.
	 *
	 * @param Customweb_Payment_Recurring_ITransactionContext $transactionContext
	 */
	public function createTransaction(Customweb_Payment_Authorization_Recurring_ITransactionContext $transactionContext){
		$transaction = new Customweb_PayPal_Authorization_Transaction($transactionContext);
		$transaction->setAuthorizationMethod(self::AUTHORIZATION_METHOD_NAME);
		$transaction->setLiveTransaction(!$this->getConfiguration()->isTestMode());
		return $transaction;
	}
	
	public function isAuthorizationMethodSupported(Customweb_Payment_Authorization_IOrderContext $orderContext){
		return true;
	}
	
	protected function getParameterBuilder(Customweb_Payment_Authorization_ITransaction $transaction){
		return new Customweb_PayPal_Authorization_Recurring_ParameterBuilder($this->getContainer(), $transaction);
	}

	/**
	 * This method returns true, when the given payment method supports recurring payments.
	 *
	 * @param Customweb_Payment_Authorization_IPaymentMethod $paymentMethod
	 * @return boolean
	 */
	public function isPaymentMethodSupportingRecurring(Customweb_Payment_Authorization_IPaymentMethod $paymentMethod){
		if ($this->getConfiguration()->getCheckoutType() == 'express') {
			return true;
		}
		return false;
	}
	
	
	public function process(Customweb_Payment_Authorization_ITransaction $transaction){
		$initialTransaction = $transaction->getTransactionContext()->getInitialTransaction();
		$transaction->setBillingAgreementId($initialTransaction->getBillingAgreementId());
		
		$referencePaymentResponse = $this->doReferencePaymentRequest($transaction);
		
		if($referencePaymentResponse['ACK'] == 'Failure' || $referencePaymentResponse['ACK'] == 'FailureWithWarning') {
			$error = 'No error provided by PayPal.';
			if (isset($referencePaymentResponse['L_LONGMESSAGE0'])) {
				$error = $referencePaymentResponse['L_LONGMESSAGE0'];
			}
			elseif(isset($referencePaymentResponse['L_SHORTMESSAGE0'])) {
				$error = $referencePaymentResponse['L_SHORTMESSAGE0'];
			}
			$message = new Customweb_Payment_Authorization_ErrorMessage(
							Customweb_I18n_Translation::__("Your payment was not successful."),
							Customweb_I18n_Translation::__("The recurring payment could not be conducted. Error: @error", array('@error' => $error)));
			
			$transaction->setAuthorizationFailed($message);
			throw new Customweb_Payment_Exception_RecurringPaymentErrorException($message->getBackendMessage());
			
		}
		
		if (!$this->validateInformation($transaction, $referencePaymentResponse)) {
			$message = new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__('Payment was not successful.'),
							Customweb_I18n_Translation::__('The recurring transaction could not be valdidated.'));
			$transaction->setAuthorizationFailed($message);
			throw new Customweb_Payment_Exception_RecurringPaymentErrorException($message->getBackendMessage());
		}
		else {
			$transaction->setPaymentId($referencePaymentResponse['TRANSACTIONID']);
			$transaction->authorize(Customweb_I18n_Translation::__('Recurring payment'));
			//direct capture of sale
			if ($transaction->getAuthorizationType() == 'sale') {
				$item = $transaction->capture();
				$item->setCaptureId($referencePaymentResponse['TRANSACTIONID']);
			}
			if ($transaction->getAuthorizationType() != 'order') {
				$transaction->setSellerProtection($referencePaymentResponse['PROTECTIONELIGIBILITY']);
				if ($this->getConfiguration()->isMarkTransactionAsUncertain()) {
					if (!strcmp($transaction->getSellerProtection(), 'Ineligible')) {
						$transaction->setAuthorizationUncertain();
					}
				}
			}
		}
	}
}