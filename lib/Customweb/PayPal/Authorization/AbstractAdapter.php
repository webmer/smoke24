<?php

//require_once 'Customweb/PayPal/AbstractAdapter.php';



/**
 *
 * @author Thomas Brenner
 *        
 */
class Customweb_PayPal_Authorization_AbstractAdapter extends Customweb_PayPal_AbstractAdapter {

	public function validate(Customweb_Payment_Authorization_IOrderContext $orderContext, Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext, array $formData) {
		return true;
	}

	public function preValidate(Customweb_Payment_Authorization_IOrderContext $orderContext, Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext) {
		return true;
	}

	public function isDeferredCapturingSupported(Customweb_Payment_Authorization_IOrderContext $orderContext, Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext) {
		return $orderContext->getPaymentMethod ()
			->existsPaymentMethodConfigurationValue ( 'capturing' );
	}

}