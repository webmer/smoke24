<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/PayPal/Container.php';



/**
 *
 * @author Thomas Brenner
 * @Bean
 *
 */
class Customweb_PayPal_AbstractAdapter
{
	/**
	 * Configuration object.
	 *
	 * @var Customweb_PayPal_Container
	 */
	private $container;
	
	public function __construct(Customweb_DependencyInjection_IContainer $container) {
		$this->container = new Customweb_PayPal_Container($container);
	}
	
	public function getContainer() {
		return $this->container;
	}
	
	/**
	 * @return Customweb_PayPal_Configuration
	 */
	public function getConfiguration() {
		return $this->getContainer()->getConfiguration();
	}
	

	

	
}