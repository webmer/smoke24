<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Core/Http/Response.php';
//require_once 'Customweb/Payment/Endpoint/Controller/Process.php';
//require_once 'Customweb/Payment/Authorization/ErrorMessage.php';
//require_once 'Customweb/I18n/Translation.php';



/**
 * @Controller("endpoint")
 */
class Customweb_PayPal_Endpoint_Process extends Customweb_Payment_Endpoint_Controller_Process {

	/**
	 * @Action("process")
	 */
	public function process(Customweb_PayPal_Authorization_Transaction $transaction, Customweb_Core_Http_IRequest $request){
		$parameters = $request->getParameters();
		
		$adapter = $this->getAdapterFactory()->getAuthorizationAdapterByName($transaction->getAuthorizationMethod());
		$response = $adapter->processAuthorization($transaction, $parameters);
		return $response;
	}

	/**
	 * @Action("cancel")
	 */
	public function cancel(Customweb_PayPal_Authorization_Transaction $transaction, Customweb_Core_Http_IRequest $request){
		$parameters = $request->getParameters();
		if (!isset($parameters['cw_hash'])) {
			throw new Exception("Request could not be verified.");
		}
		$transaction->checkSecuritySignature('endpoint/cancel', $parameters['cw_hash']);
		$transaction->setAuthorizationFailed(
				new Customweb_Payment_Authorization_ErrorMessage(Customweb_I18n_Translation::__('Transaction cancelled'), 
						Customweb_I18n_Translation::__('Transaction cancelled by the customer.')));
		return Customweb_Core_Http_Response::redirect($transaction->getFailedUrl());
	}
}