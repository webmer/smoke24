<?php
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2016 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

//require_once 'Customweb/Core/Util/Language.php';
//require_once 'Customweb/Util/Currency.php';

/**
 *
 * @author Thomas Brenner
 * 
 *
 */
final class Customweb_PayPal_Util {

	private function __construct() {
		// prevent any instantiation of this class
	}

	public static function getCleanLanguageCode($lang) {
		$supportedLanguages = array('de_DE','en_US','fr_FR','da_DK',
				'cs_CZ','es_ES','hr_HR','it_IT','hu_HU','nl_NL',
				'no_NO','pl_PL','pt_PT','ru_RU','ro_RO','sk_SK',
				'sl_SI','fi_FI','sv_SE','tr_TR','el_GR','ja_JP'
		);
		return Customweb_Core_Util_Language::getCleanLanguageCode($lang,$supportedLanguages);
	}

	public static function isZeroCheckout(Customweb_Payment_Authorization_ITransactionContext $transactionContext) {
		$orderContext = $transactionContext->getOrderContext();
		if((Customweb_Util_Currency::compareAmount($orderContext->getOrderAmountInDecimals(), 0, $orderContext->getCurrencyCode()) == 0) && $transactionContext->createRecurringAlias()) {
			return true;
		}
		return false;
	}
	
	public static function getCheckoutAmount(Customweb_Payment_Authorization_ITransactionContext $transactionContext) {
		if(self::isZeroCheckout($transactionContext)) {
			return 1;
		}
		return $transactionContext->getOrderContext()->getOrderAmountInDecimals();
	}
	
}