<?php
// Extend the TCPDF class to create custom Header and Footer
require_once(dirname(__FILE__).'/tcpdf.php');

class Ext4mageHtml2PDF extends tcpdf {
	/**
	 * HTML String to print in header.
	 * @protected
	 */
	protected $header_html = '';
	
	/**
	 * HTML String to print in footer.
	 * @protected
	 */
	protected $footer_html = '';

	/**
	 * Height of footer in mm
	 * @protected
	 */
	protected $footer_height = 15;
	
	protected $defaultFont = 'times';
	protected $defaultFontSize = 12;
	protected $defaultFontColor = array();
	protected $defaultImage = null;
	
	//Used to if image should be optimized for size or not
	protected $optimizeImageSize = 2;
	
	//Use for special language char problems
	public function __construct($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding='UTF-8', $diskcache=false, $pdfa=false) {
		//Normal setup
		parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
		//Problem setup
		//parent::__construct($orientation, $unit, $format, false, 'ISO-8859-1', $diskcache, $pdfa);
	}
	
	//Page header
    public function Header() {
        // Write header html
        $this->SetFont($this->defaultFont, '', $this->defaultFontSize);
        $this->SetTextColor($this->defaultFontColor[0],$this->defaultFontColor[1],$this->defaultFontColor[2]);
		//$this->writeHTML($this->header_html, true, false, true, false, '');
		$this->writeHTMLCell(0, '', '', '', $this->header_html, 0, 1);
        $this->tMargin = $this->GetY();
    }

    // Page footer
    public function Footer() {
        // Position from bottom
    	$this->SetY(-1*$this->footer_height);
    	// Write footer html
    	$this->SetFont($this->defaultFont, '', $this->defaultFontSize);
        $this->SetTextColor($this->defaultFontColor[0],$this->defaultFontColor[1],$this->defaultFontColor[2]);
		$this->writeHTML($this->footer_html, true, false, true, false, '');
		$this->bMargin = $this->footer_height;
    }
    
    //Header html setter
    public function setHeaderHtml($header_html, $defaultFont = 'times', $defaultFontSize = 12, $defaultFontColor = array()){
    	$this->defaultFont = $defaultFont;
    	$this->defaultFontSize = $defaultFontSize;
    	$this->defaultFontColor = $defaultFontColor;
    	$this->header_html = $header_html;
    }

    //Footer html setter
    public function setFooterHtml($footer_html, $defaultFont = 'times', $defaultFontSize = 12, $defaultFontColor = array()){
    	// Find the height of the footer html to set bottom margin
    	$this->defaultFont = $defaultFont;
    	$this->defaultFontSize = $defaultFontSize;
    	$this->defaultFontColor = $defaultFontColor;
    	$this->AddPage();
    	$startY = $this->GetY();
    	$this->SetFont($this->defaultFont, '', $this->defaultFontSize);
        $this->SetTextColor($this->defaultFontColor[0],$this->defaultFontColor[1],$this->defaultFontColor[2]);
		$this->writeHTML($footer_html, true, false, true, false, '');
    	$this->footer_height = $this->getFooterMargin() + ($this->GetY()-$startY);
    	$this->deletePage($this->PageNo());
    	$this->SetAutoPageBreak(TRUE, $this->footer_height);
    	$this->bMargin = $this->footer_height;
    	
    	$this->footer_html = $footer_html;    	
    }
    
    public function getHeaderHtml(){
    	return $this->header_html;
    }

    public function getFooterHtml(){
    	return $this->footer_html;
    }
    
    public function setDefaultImage(string $defaultImage){
    	$this->defaultImage = $defaultImage;
    }

    public function setOptimizeImageSize($optimizeImageSize){
    	$this->optimizeImageSize = $optimizeImageSize;
    }
    
    public function Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array()) {
    	if ($file[0] != '@') {	
    		if ($file{0} === '*') {
    			// image as external stream
    			$file = substr($file, 1);
    			$exurl = $file;
    		}
    		// check if is local file
    		if (!@file_exists($file)) {
    			// encode spaces on filename (file is probably an URL)
    			$file = str_replace(' ', '%20', $file);
    		}
    		if (@file_exists($file)) {
    			// get image dimensions
    			$imsize = @getimagesize($file);
    		} else {
    			$imsize = false;
    		}
    		if ($imsize === FALSE) {
    			$retcode = 400;
    			if (function_exists('curl_init')) {
    				// try to get remote file data using cURL
    				$cs = curl_init($file); // curl session
    				curl_setopt($cs, CURLOPT_NOBODY, true);
    				curl_setopt($cs, CURLOPT_FAILONERROR, true);
    				curl_setopt($cs, CURLOPT_RETURNTRANSFER, true);
    				if ((ini_get('open_basedir') == '') AND (!ini_get('safe_mode'))) {
    					curl_setopt($cs, CURLOPT_FOLLOWLOCATION, true);
    				}
    				curl_setopt($cs, CURLOPT_CONNECTTIMEOUT, 5);
    				curl_setopt($cs, CURLOPT_TIMEOUT, 30);
    				curl_setopt($cs, CURLOPT_SSL_VERIFYPEER, false);
    				curl_setopt($cs, CURLOPT_SSL_VERIFYHOST, false);
    				curl_setopt($cs, CURLOPT_USERAGENT, 'TCPDF');
    				curl_exec($cs);
    				$retcode = curl_getinfo($cs, CURLINFO_HTTP_CODE);
    				curl_close($cs);
    			} elseif (($w > 0) AND ($h > 0)) {
    				// get measures from specified data
    				$pw = $this->getHTMLUnitToUnits($w, 0, $this->pdfunit, true) * $this->imgscale * $this->k;
    				$ph = $this->getHTMLUnitToUnits($h, 0, $this->pdfunit, true) * $this->imgscale * $this->k;
    				$imsize = array($pw, $ph);
    			}
    		}
    		if ($imsize === FALSE && $retcode >= 400) {
    			if (substr($file, 0, -34) == K_PATH_CACHE.'msk') { // mask file
    				// get measures from specified data
    				$pw = $this->getHTMLUnitToUnits($w, 0, $this->pdfunit, true) * $this->imgscale * $this->k;
    				$ph = $this->getHTMLUnitToUnits($h, 0, $this->pdfunit, true) * $this->imgscale * $this->k;
    				$imsize = array($pw, $ph);
    			} else {
    				if($this->defaultImage === null){
    					return "";	
    				}
    				return parent::Image(resizeImage($this->defaultImage, $h, $w), $x, $y, $w, $h, $type, $link, $align, $resize, $dpi, $palign, $ismask, $imgmask, $border, $fitbox, $hidden, $fitonpage, $alt, $altimgs);
    			}
    		}    		
    	}
//        Mage::getSingleton('devel/devel');
//        krumo($file);
        
        if(strpos($file,'/var/html2pdf/')!=true){
             $file = $this->resizeImage($file, $h, $w);
        }
       

//        try {
//            $file = $this->resizeImage($file, $h, $w);
//        } catch (Exception $exc) {
//           krumo::backtrace();
//        }


    	return parent::Image($file, $x, $y, $w, $h, $type, $link, $align, $resize, $dpi, $palign, $ismask, $imgmask, $border, $fitbox, $hidden, $fitonpage, $alt, $altimgs);
    }
    
    function resizeImage($imageUrl, $imageHeight = 0, $imageWidth= 0){
    	$resizedFolder = 'html2pdf'.DS.'resized';
    	
    	if($this->optimizeImageSize==0){
    		return $imageUrl;
    	}
    	
    	if($imageHeight == 0)
    		$imageHeight = null;
    	else
    		$imageHeight = $imageHeight * ($this->imgscale * $this->k) * $this->optimizeImageSize;
    	
    	if($imageWidth == 0)
    		$imageWidth = null;
    	else
    		$imageWidth = $imageWidth * ($this->imgscale * $this->k) * $this->optimizeImageSize;
    	
    	if(empty($imageHeight) && empty($imageWidth))
    		return $imageUrl;
    	
    	// create folder
    	if(!file_exists("./media/".$resizedFolder))
    		mkdir("./media/".$resizedFolder,0777);
    
    	// get image name
    	$info = pathinfo($imageUrl);
    	$file_name =  basename($imageUrl,'.'.$info['extension']);
    	
    	// resized image path (media/catalog/category/resized/IMAGE_NAME)
    	$imageResized = Mage::getBaseDir('media').DS.$resizedFolder.DS.$file_name.'_'.$imageHeight.'_'.$imageWidth.'.'.$info['extension'];
    
    	$dirImg = Mage::getBaseDir().strstr($imageUrl, '/media/');
    	$dirImg = str_replace("/",DS,$dirImg);
    	// if resized image doesn't exist, save the resized image to the resized directory
    	if (!file_exists($imageResized) && file_exists($dirImg) && !empty($imageUrl)) :
	    	$imageObj = new Varien_Image($dirImg);
	    	$imageObj->constrainOnly(TRUE);
	    	$imageObj->keepAspectRatio(TRUE);
	    	$imageObj->keepTransparency(TRUE);
	    	$imageObj->keepFrame(FALSE);
	    	
	    	$imageObj->resize($imageWidth, $imageHeight);
	    	$imageObj->save($imageResized);
    	endif;
    	 
    	return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).$resizedFolder.'/'.$file_name.'_'.$imageHeight.'_'.$imageWidth.'.'.$info['extension'];
    }
	
    /*
     * Funtion to print out barcodes, function can print both 1D and 2D (QR)
     */
    function generateBarcodeHtml($text=null,$type=null,$fgcolor=null,$showText=null,$fontsize=null,$font=null,$barHight=null,$barWidth=null,$align=null){
    	
    	if($text == null || strlen($text)==0) return '';
    	if($type == null || strlen($type)==0) $type = 'UPCA';
    	if($fgcolor == null || strlen($fgcolor)==0) $fgcolor = '#000000';
    	if(strncasecmp($showText, "y", 1)==0) $showText = true; else $showText = false;
    	if($fontsize == null || !is_int($fontsize)) $fontsize = 12;
    	if($font == null || strlen($font)==0) $font = 'helvetica';
    	if($barHight == null || strlen($barHight)==0) $barHight = '';
    	if($barWidth == null || strlen($barWidth)==0) $barWidth = '';
    	if($align == null || strlen($align)==0) $align = 'N';
    	
    	$text = str_replace('{{','', $text);
    	$text = str_replace('}}','', $text);
    	$fgcolor = TCPDF_COLORS::convertHTMLColorToDec($fgcolor, $this->spot_colors);
    	$style = array('fgcolor'=>$fgcolor, 'text'=>$showText, 'fontsize'=>$fontsize, 'font'=>$font, 'stretchtext'=>0, 'border'=>false, 'bgcolor'=>false);
    	if(strncasecmp($type, 'QR', 2)==0){
    		$params2d = TCPDF_STATIC::serializeTCPDFtagParameters(array($text, 'QRCODE,M', '', '', $barWidth, $barHight, $style, $align));
    		return '<tcpdf method="write2DBarcode" params="'.$params2d.'" />';
    	}else{
    		$barTypes = array("C39","C39+","C39E","C39E+","C93","S25","S25+","I25","I25+","C128","C128A","C128B","C128C","EAN2","EAN5","EAN8","EAN13","UPCA","UPCE","MSI","MSI+","POSTNET","PLANET","RMS4CC","KIX","IMB","CODABAR","CODE11","PHARMA","PHARMA2T");
    		if (in_array(strtolower($type), array_map('strtolower', $barTypes))) {
    			$params = TCPDF_STATIC::serializeTCPDFtagParameters(array($text, $type, '', '', $barWidth, $barHight, 0.4, $style, $align));
	    		return '<tcpdf method="write1DBarcode" params="'.$params.'" />';
    		}
    		return "";    		
    	}
    	
    	
    	 
    }
}
?>