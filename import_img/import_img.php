<?php
/* @var $installer Mage_Core_Model_Resource_Setup */

ini_set('memory_limit','-1');
set_time_limit ("30000");

require '../app/Mage.php';
Mage::app();


class MagentoSoapClient extends \SoapClient
{
    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        $curl = curl_init($location);

        $options = [
            CURLOPT_VERBOSE => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $request,
            CURLOPT_HEADER => false,
            CURLOPT_NOSIGNAL => true,
            CURLOPT_HTTPHEADER => [
                sprintf('Content-Type: %s', $version == 2 ? 'application/soap+xml' : 'text/xml'),
                sprintf('SOAPAction: %s', $action)
            ],
            CURLOPT_TIMEOUT => 180,
            CURLOPT_CONNECTTIMEOUT => 15,
        ];

        curl_setopt_array($curl, $options);
        $response = curl_exec($curl);
        $curlErrorMessage = curl_error($curl);
        $curlErrorNumber = curl_errno($curl);
        curl_close($curl);

        if ($curlErrorNumber && $curlErrorNumber !== 18) { // 18: Ignore incorrect length sent by faulty configured webservers
            throw new Exception($curlErrorMessage, $curlErrorNumber);
        }

        if (! $one_way) {
            return ($response);
        }
    }
}



//$wsdlUrl = 'http://olymp.dev/index.php/api/soap/?wsdl=1';
$baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
//$host = parse_url($baseUrl, PHP_URL_HOST);
$wsdlUrl = $baseUrl . 'index.php/api/soap/?wsdl=1';
// Create the custom SOAPClient
$client = new MagentoSoapClient($wsdlUrl);
$session = $client->login('webmer', '123456');



$importDir = Mage::getBaseDir('media') . DS . 'catalog/product/update_img/';

//$product = mage::getModel('catalog/product')->load(1782);
//var_dump($product);
//die;

//$file = basename($importDir);         // $file is set to "index.php"
//echo $file;
//$file = basename($importDir, ".jpg"); // $file is set to "index"
//echo $file;
$types = array();




if ($handle = opendir($importDir)) {
    while (false !== ($file = readdir($handle))) {

        $types = array('image', 'small_image', 'thumbnail');
        if( $file == '.' || $file == '..')
            continue;
            //echo $file;


            $image_path = $importDir . $file;
            $sku = basename($file, ".jpg");

        
        
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku',$sku);

        if(!method_exists($product, 'getId') ) {
            echo ('Fail sku='.$sku.'<br>');
            continue;
        }
            if (file_exists($image_path)) {
                $image = getimagesize($image_path);
                $file = array(
                    'content' => base64_encode(file_get_contents($image_path)),
                    'mime' => $image['mime']
                );
                if (!is_null($client)) {

//                    echo $sku . '<br>';
//                    print_r($file);

                    $client->call($session, 'catalog_product_attribute_media.create',
                        array($sku, array(
                            'file' => $file,
                            'label' => '',
                            'position' => 10,
                            'exclude' => 0,
                            'types' => $types
                        ), null, 'sku'));
                    echo "Add new images " .$sku.' file '.'<br>';

                } else {
                    echo 'Error' . $sku.'<br>';
                }
            }



    }
    closedir($handle);
}



die;

$files111 = scandir($importDir);
$files222 = scandir($importDir, 1);

print_r($files111);
print_r($files222);
die;
$mediaGallery = $product->getMediaGallery();
//if there are images
if (isset($mediaGallery['images'])){
    //loop through the images
    echo "dsd";
    foreach ($mediaGallery['images'] as $image){
        //print_r($image);
        //set the first image as the base image
        Mage::getSingleton('catalog/product_action')->updateAttributes(array($product->getId()), array('thumbnail'=> 'update_img/test.jpg','small_image' => 'update_img/test.jpg','image'=> 'update_img/test.jpg',), 0);
        //stop
        break;
    }
}







?>